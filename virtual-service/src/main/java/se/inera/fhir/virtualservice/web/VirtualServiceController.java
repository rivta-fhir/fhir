package se.inera.fhir.virtualservice.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import se.inera.fhir.virtualservice.service.ProxyService;

import java.util.Map;

@RestController
@RequestMapping("/fhir")
public class VirtualServiceController {

    private Logger log = LoggerFactory.getLogger(getClass());

    private ProxyService proxyService;

    public VirtualServiceController(ProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @GetMapping("/{fhirVersion}/{producerId}/{resourceType}")
    public Mono<Object> proxyQuery(
            @PathVariable final String fhirVersion,
            @PathVariable final String producerId,
            @PathVariable final String resourceType,
            @RequestParam final Map<String,String> allRequestParams
    ) {

        log.info("Call to virtual-service with fhirVersion: {}, producerId: {}, resourceType: {}, parameters: {}",
                fhirVersion, producerId, resourceType, allRequestParams.toString());

        return this.proxyService.handleQuery(
                fhirVersion,
                producerId,
                resourceType,
                allRequestParams
        ).log();
    }

    @GetMapping("/{fhirVersion}/{producerId}/{resourceType}/{resourceId}")
    public Mono<Object> proxyFetch(
            @PathVariable final String fhirVersion,
            @PathVariable final String producerId,
            @PathVariable final String resourceType,
            @PathVariable final String resourceId
    ) {

        log.info("Call to virtual-service with fhirVersion: {}, producerId: {}, resourceType: {}, resourceId: {}",
                fhirVersion, producerId, resourceType, resourceId);

        return this.proxyService.handleFetch(
                fhirVersion,
                producerId,
                resourceType,
                resourceId
        ).log();
    }

}
