package se.inera.fhir.virtualservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtualServiceApplication.class, args);
	}

}
