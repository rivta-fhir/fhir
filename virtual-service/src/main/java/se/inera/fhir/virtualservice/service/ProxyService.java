package se.inera.fhir.virtualservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.common.dto.SourceSystemDto;
import se.inera.fhir.common.util.LinkRewriter;

import java.util.Map;

@Service
public class ProxyService {

    @Value("${serviceDiscoveryUrl}")
    private String serviceDiscoveryUrl;

    @Value("${virtualServiceExternalUrl}")
    private String virtualServiceExternalUrl;


    public Mono<Object> handleQuery(
            final String fhirVersion,
            final String producerId,
            final String resourceType,
            final Map<String,String> parameters
    ) {
        return getSourceSystemDto(producerId)
                .flatMap(sourceSystemDto -> {
                    if (!sourceSystemDto.getFhirVersion().equals(fhirVersion)) {
                        return Mono.empty();
                    }
                    return queryProducer(sourceSystemDto.getUrl(), resourceType, parameters)
                            .flatMap(this::replaceUrls);
                });
    }

    public Mono<Object> handleFetch(
            final String fhirVersion,
            final String producerId,
            final String resourceType,
            final String resourceId
    ) {
        return getSourceSystemDto(producerId)
                .flatMap(sourceSystemDto -> {
                    if (!sourceSystemDto.getFhirVersion().equals(fhirVersion)) {
                        return Mono.empty();
                    }
                    return fetchResource(sourceSystemDto.getUrl(), resourceType, resourceId);
                })
                .flatMap(this::replaceUrls);
    }

    private Mono<Object> replaceUrls(Object bundle) {
        return getSourceSystemDtos()
                .collectList().map(sourceSystemDtos -> {
                    sourceSystemDtos.forEach(sourceSystemDto -> LinkRewriter.replaceUrls(
                            sourceSystemDto.getUrl(),
                            virtualServiceExternalUrl + "/" + sourceSystemDto.getFhirVersion() + "/" + sourceSystemDto.getId(),
                            bundle));
                    return bundle;
                });
    }


    private Flux<SourceSystemDto> getSourceSystemDtos() {
        return WebClient
                .builder()
                .baseUrl(serviceDiscoveryUrl)
                .build().get()
                .exchange()
                .flatMapMany(c -> c.bodyToFlux(SourceSystemDto.class));
    }

    private Mono<SourceSystemDto> getSourceSystemDto(
            final String id) {
        return WebClient
                .builder()
                .baseUrl(serviceDiscoveryUrl + "/" + id)
                .build().get()
                .exchange()
                .flatMap(c -> c.bodyToMono(SourceSystemDto.class));
    }

    private Mono<Object> fetchResource(
            final String url,
            final String resourceType,
            final String resourceId) {

        return WebClient
                .builder()
                .baseUrl(url + "/" + resourceType + "/" + resourceId)
                .defaultHeader("Content-type", "application/fhir+json")
                .defaultHeader("Accept", "application/fhir+json")
                .build().get()
                .exchange()
                .flatMap(response -> response.bodyToMono(Object.class));
    }

    private Mono<Object> queryProducer(
            final String url,
            final String resourceType,
            final Map<String,String> parameters) {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.setAll(parameters);

        return WebClient
                .builder()
                .baseUrl(url + "/" + resourceType)
                .defaultHeader("Content-type", "application/fhir+json")
                .defaultHeader("Accept", "application/fhir+json")
                .build().get()
                .uri(builder -> builder.queryParams(queryParams).build())
                .exchange()
                .flatMap(response -> response.bodyToMono(Object.class));
    }
}
