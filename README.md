# Quick start

## Running in OpenShift

Before beginning, install openshift cli, kubectl and helm v3. Login to your OpenShift instance.

### Create secrets
Start out by creating a secret with the database configuration:

    oc create secret generic mongodb \
       --from-literal=username=inera \
       --from-literal=password=<a password of your choice> \
       --from-literal=host=mongodb \
       --from-literal=port=27017 \n
       --from-literal=authenticationDatabase=admin \n
       --from-literal=database=fhir_labs

Next, create an docker registry secret. Open kubernetes/fhir-labs/values.yaml and replace imagePullSecret with the name of your registry secret, and update the docker registry to your docker registry. Next, open kubernetes/Makefile DOCKER-REGISTRY with the URL to your docker registry. Last, open deploy-openshift and replace BASE_URL with the base url to your openshift install and update CLUSTERINFO with the output of `oc project | shasum`.

After the above has been performed, run `make openshift` and wait for the build and deploy to complete.

As a last step, configure an OpenShift-route and expose the openshift-proxy pod on the path `/`.

## Running in IDE
First start mongodb on a local port.

```
docker run -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=inera -e MONGO_INITDB_ROOT_PASSWORD=qev6gHxX4DkDdbsCy0Ch mongo
```

Mongodb will now be accessible on port 27017 on localhost. 

Now you are ready to start the java applications. Start via you IDE or run `mvn spring-boot:run`.