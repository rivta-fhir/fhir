package se.sll.ahrr.medicationhistory.v2_0;

/*-
 * #%L
 * rivta-fhir-medicationhistory
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.model.dstu2.valueset.MedicationStatementStatusEnum;
import org.apache.commons.lang3.RandomStringUtils;
import org.hl7.fhir.dstu3.model.*;
import org.junit.Before;
import org.junit.Test;
import riv.clinicalprocess.activityprescription.actoutcome._2.*;
import riv.clinicalprocess.activityprescription.actoutcome.enums._2.NonReplaceableEnum;
import riv.clinicalprocess.activityprescription.actoutcome.enums._2.TypeOfPrescriptionEnum;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class MedicationHistoryDataTransformerTest
{

    private MedicationHistoryDataTransformer medicationHistoryDataTransformer;

    @Before
    public void setUp() throws Exception
    {
        medicationHistoryDataTransformer = new MedicationHistoryDataTransformer();
    }

    @Test
    public void medicationRequestStatusActive() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        // Insättning
        medicationPrescriptionType.setTypeOfPrescription(TypeOfPrescriptionEnum.I);
        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                encounter,
                patient
        );
        assert MedicationRequest.MedicationRequestStatus.ACTIVE == medicationRequest.getStatus();
    }

    @Test
    public void medicationRequestStatusStopped() throws Exception
    {
//        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
//        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
//        Encounter encounter = new Encounter();
//        Patient patient = new Patient();
//
//        // Utsättning
//        medicationPrescriptionType.setTypeOfPrescription(TypeOfPrescriptionEnum.U);
//        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
//                medicationPrescriptionType,
//                patientSummaryHeaderType,
//                encounter,
//                patient
//        );
//        assert MedicationRequest.MedicationRequestStatus.STOPPED == medicationRequest.getStatus();

        assert true;
    }

    @Test
    public void medicationRequestIntentOrderByDispensation() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        medicationPrescriptionType.setDispensationAuthorization(new DispensationAuthorizationType());

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                encounter,
                patient
        );
        assert MedicationRequest.MedicationRequestIntent.ORDER == medicationRequest.getIntent();
    }

    @Test
    public void medicationRequestIntentOrderByAdministration() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        medicationPrescriptionType.getAdministration().add(new AdministrationType());

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                encounter,
                patient
        );
        assert MedicationRequest.MedicationRequestIntent.ORDER == medicationRequest.getIntent();
    }

    @Test
    public void medicationRequestIntentPlanned() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                encounter,
                patient
        );
        assert MedicationRequest.MedicationRequestIntent.PLAN == medicationRequest.getIntent();
    }

    @Test
    public void medicationRequestContext() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                encounter,
                patient
        );

        assert encounter == ((Encounter) medicationRequest.getContext().getResource());
    }

    @Test
    public void medicationRequestSubject() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        Encounter encounter = new Encounter();
        Patient patient = new Patient();

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
            medicationPrescriptionType,
            patientSummaryHeaderType,
            encounter,
            patient
        );

        assert patient == ((Patient) medicationRequest.getSubject().getResource());
    }

    @Test
    public void medicationRequestAuthoredOn() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        patientSummaryHeaderType.setDocumentTime("20110101010101");

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert 1293840061 == medicationRequest.getAuthoredOn().toInstant().getEpochSecond();
    }

    @Test
    public void medicationRequestRequester() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        HealthcareProfessionalType healthcareProfessionalType = new HealthcareProfessionalType();

        healthcareProfessionalType.setHealthcareProfessionalName("PROFESSIONAL NAME");
        healthcareProfessionalType.setHealthcareProfessionalHSAId("HSA-ID");

        medicationPrescriptionType.setPrescriber(healthcareProfessionalType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert "urn:oid:1.2.752.129.2.1.4.1".equals(medicationRequest.getRecorderTarget().getIdentifier().get(0).getSystem());
        assert "HSA-ID".equals(medicationRequest.getRecorderTarget().getIdentifier().get(0).getValue());
        assert "PROFESSIONAL NAME".equals(medicationRequest.getRecorderTarget().getName().get(0).getText());
    }

    @Test
    public void medicationRequestPrescriptionPrincipalReasonCode() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();


        PrescriptionReasonType prescriptionReasonType = new PrescriptionReasonType();

        CVType reason = new CVType();
        reason.setCode("REASON CODE");
        reason.setCodeSystem("REASON CODE SYSTEM");

        prescriptionReasonType.setReason(reason);
        medicationPrescriptionType.getPrincipalPrescriptionReason().add(prescriptionReasonType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert 1 == medicationRequest.getReasonCode().size();
        assert "urn:oid:REASON CODE SYSTEM".equals(medicationRequest.getReasonCode().get(0).getCoding().get(0).getSystem());
        assert "REASON CODE".equals(medicationRequest.getReasonCode().get(0).getCoding().get(0).getCode());
    }

    @Test
    public void medicationRequestPrescriptionAdditionalReasonCode() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        PrescriptionReasonType prescriptionReasonType = new PrescriptionReasonType();

        CVType reason = new CVType();
        reason.setCode("REASON CODE");
        reason.setCodeSystem("REASON CODE SYSTEM");

        prescriptionReasonType.setReason(reason);
        medicationPrescriptionType.getAdditionalPrescriptionReason().add(prescriptionReasonType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert 1 == medicationRequest.getReasonCode().size();
        assert "urn:oid:REASON CODE SYSTEM".equals(medicationRequest.getReasonCode().get(0).getCoding().get(0).getSystem());
        assert "REASON CODE".equals(medicationRequest.getReasonCode().get(0).getCoding().get(0).getCode());
    }

    @Test
    public void medicationRequestPrescriptionPrincipalReasonCodeOtherReason() throws Exception
    {

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();


        PrescriptionReasonType prescriptionReasonType = new PrescriptionReasonType();
        prescriptionReasonType.setOtherReason("OTHER REASON");
        medicationPrescriptionType.getPrincipalPrescriptionReason().add(prescriptionReasonType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
            medicationPrescriptionType,
            patientSummaryHeaderType,
            null,
            null
        );

        assert 1 == medicationRequest.getReasonCode().size();
        assert "OTHER REASON".equals(medicationRequest.getReasonCode().get(0).getText());
    }

    @Test
    public void medicationRequestPrescriptionAdditionalReasonCodeOtherReason() throws Exception
    {

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        PrescriptionReasonType prescriptionReasonType = new PrescriptionReasonType();
        prescriptionReasonType.setOtherReason("OTHER REASON");
        medicationPrescriptionType.getAdditionalPrescriptionReason().add(prescriptionReasonType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert 1 == medicationRequest.getReasonCode().size();
        assert "OTHER REASON".equals(medicationRequest.getReasonCode().get(0).getText());
    }

    @Test
    public void medicationRequestNote() throws Exception
    {

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        medicationPrescriptionType.setPrescriptionNote("Prescription note");

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert "Prescription note".equals(medicationRequest.getNote().get(0).getText());
    }

    @Test
    public void medicationRequestSubstitutionPrescriber() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();
        dispensationAuthorizationType.setNonReplaceable(NonReplaceableEnum.PRESCRIBER);
        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert false == medicationRequest.getSubstitution().getAllowed();
        assert "Prescriber".equals(medicationRequest.getSubstitution().getReason().getText());
    }

    @Test
    public void medicationRequestSubstitutionPatient() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();
        dispensationAuthorizationType.setNonReplaceable(NonReplaceableEnum.PATIENT);
        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        assert false == medicationRequest.getSubstitution().getAllowed();
        assert "Patient".equals(medicationRequest.getSubstitution().getReason().getText());
    }

    @Test
    public void medicationRequestExtensionPrecedingPrescriptionId() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        IIType iiType = new IIType();

        iiType.setRoot("system");
        iiType.setExtension("document");

        medicationPrescriptionType.setPrecedingPrescriptionId(iiType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-PrecedingPrescriptionId").get(0);

        assert "document".equals(extension.getValue().castToIdentifier(extension.getValue()).getValue());
        assert "system".equals(extension.getValue().castToIdentifier(extension.getValue()).getSystem());
    }

    @Test
    public void medicationRequestExtensionSucceedingPrescriptionId() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        IIType iiType = new IIType();

        iiType.setRoot("system");
        iiType.setExtension("document");

        medicationPrescriptionType.setSucceedingPrescriptionId(iiType);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-SucceedingPrescriptionId").get(0);

        assert "document".equals(extension.getValue().castToIdentifier(extension.getValue()).getValue());
        assert "system".equals(extension.getValue().castToIdentifier(extension.getValue()).getSystem());
    }

    @Test
    public void medicationRequestExtensionStartOfFirstTreatment() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        medicationPrescriptionType.setStartOfFirstTreatment("20120202020202");

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-StartOfFirstTreatment").get(0);

        assert "2012-02-02T02:02:02+01:00".equals(extension.castToDateTime(extension.getValue()).getValueAsString());
    }

    @Test
    public void medicationRequestExtensionEndOfTreatmentReason() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        CVType endOfTreatmentReason = new CVType();
        endOfTreatmentReason.setCode("CODE");
        endOfTreatmentReason.setCodeSystem("SYSTEM");

        medicationPrescriptionType.setEndOfTreatmentReason(endOfTreatmentReason);


        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-EndOfTreatmentReason").get(0);
        CodeableConcept codeableConcept = extension.castToCodeableConcept(extension.getValue());

        assert "urn:oid:SYSTEM".equals(codeableConcept.getCoding().get(0).getSystem());
        assert "CODE".equals(codeableConcept.getCoding().get(0).getCode());
    }

    @Test
    public void medicationRequestExtensionTreatmentPurpose() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        medicationPrescriptionType.setTreatmentPurpose("Some purpose");

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-TreatmentPurpose").get(0);

        assert "Some purpose".equals(extension.getValue().castToString(extension.getValue()).getValue());
    }

    @Test
    public void medicationRequestExtensionSelfMedicationTrue() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        medicationPrescriptionType.setSelfMedication(true);

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-SelfMedication").get(0);

        assert true == extension.getValue().castToBoolean(extension.getValue()).booleanValue();
    }

    @Test
    public void medicationRequestExtensionSelfMedicationFalse() throws Exception
    {

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();

        MedicationRequest medicationRequest = medicationHistoryDataTransformer.medicationRequest(
                medicationPrescriptionType,
                patientSummaryHeaderType,
                null,
                null
        );

        Extension extension = medicationRequest.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-SelfMedication").get(0);

        assert false == extension.getValue().castToBoolean(extension.getValue()).booleanValue();
    }

    @Test
    public void dosageBase() throws Exception
    {
        DosageType dosageType = new DosageType();
        dosageType.setSetDosage(new SetDosageType());

        dosageType.setDosageInstruction("patient instruction");
        dosageType.setShortNotation("additional instruction");

        Dosage dosage = medicationHistoryDataTransformer.dosage(new DrugChoiceType(), dosageType).get(0);

        assert "patient instruction".equals(dosage.getPatientInstruction());
        assert "additional instruction".equals(dosage.getText());
    }

    @Test
    public void dosageRouteOfAdministration() throws Exception
    {
        // Route of administration
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        DrugType drugType = new DrugType();
        CVType routeOfAdministration = new CVType();
        routeOfAdministration.setCode("left arm");
        routeOfAdministration.setCodeSystem("1.1.1.1");
        drugType.setRouteOfAdministration(routeOfAdministration);
        drugChoiceType.setDrug(drugType);

        DosageType dosageType = new DosageType();
        dosageType.setSetDosage(new SetDosageType());

        Dosage dosage = medicationHistoryDataTransformer.dosage(drugChoiceType, dosageType).get(0);

        assert "urn:oid:1.1.1.1".equals(dosage.getRoute().getCoding().get(0).getSystem());
        assert "left arm".equals(dosage.getRoute().getCoding().get(0).getCode());
    }

    @Test
    public void dosageSetDosage() throws Exception
    {
        DosageType dosageType = new DosageType();
        dosageType.setSetDosage(new SetDosageType());

        Dosage dosage = medicationHistoryDataTransformer.dosage(new DrugChoiceType(), dosageType).get(0);

        Extension dosageTypeExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType").get(0);
        assertNotNull(dosageTypeExtension);
        assert "SetDosage".equals(dosageTypeExtension.getValue().castToString(dosageTypeExtension.getValue()).getValueAsString());
    }

    @Test
    public void dosageMaximumDosage() throws Exception
    {
        DosageType dosageType = new DosageType();
        dosageType.setMaximumDosage(new MaximumDosageType());

        Dosage dosage = medicationHistoryDataTransformer.dosage(new DrugChoiceType(), dosageType).get(0);

        Extension dosageTypeExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType").get(0);
        assertNotNull(dosageTypeExtension);
        assert "MaximumDosage".equals(dosageTypeExtension.getValue().castToString(dosageTypeExtension.getValue()).getValueAsString());
    }

    @Test
    public void dosageConditionalDosage() throws Exception
    {
        DosageType dosageType = new DosageType();
        dosageType.setConditionalDosage(new ConditionalDosageType());

        Dosage dosage = medicationHistoryDataTransformer.dosage(new DrugChoiceType(), dosageType).get(0);

        Extension dosageTypeExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType").get(0);
        assertNotNull(dosageTypeExtension);
        assert "ConditionalDosage".equals(dosageTypeExtension.getValue().castToString(dosageTypeExtension.getValue()).getValueAsString());
    }

    @Test
    public void dosageMultiple() throws Exception
    {
        DosageType dosageType = new DosageType();
        dosageType.setSetDosage(new SetDosageType());
        dosageType.setMaximumDosage(new MaximumDosageType());
        dosageType.setConditionalDosage(new ConditionalDosageType());

        assert 3 == medicationHistoryDataTransformer.dosage(new DrugChoiceType(), dosageType).size();
    }

    @Test
    public void setDosageTimingAndDoseFrequencyDosage() throws Exception
    {
        Dosage dosage = new Dosage();

        FrequencyDosageType frequencyDosageType = new FrequencyDosageType();

        PQIntervalType dose = new PQIntervalType();
        dose.setHigh(10000.0);
        dose.setLow(5000.0);
        dose.setUnit("kg");
        frequencyDosageType.setDose(dose);

        PQType frequency = new PQType();
        frequency.setValue(6);
        frequency.setUnit("h");
        frequencyDosageType.setFrequency(frequency);

        medicationHistoryDataTransformer.setDosageTimingAndDose(
                dosage,
                frequencyDosageType,
                null,
                null,
                null,
                null,
                null
        );

        assert 10000.0 == dosage.getDose().castToRange(dosage.getDose()).getHigh().getValue().doubleValue();
        assert 5000.0 == dosage.getDose().castToRange(dosage.getDose()).getLow().getValue().doubleValue();


        Extension doseUnitExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit").get(0);
        assertNotNull(doseUnitExtension);
        assert "kg".equals(doseUnitExtension.getValue().castToString(doseUnitExtension.getValue()).getValueAsString());

        assert 6 == dosage.getTiming().getRepeat().getFrequency();
        Extension unitExtension = dosage.getTiming().getRepeat().getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-Frequency-Unit").get(0);
        assertNotNull(unitExtension);
        assert "h".equals(unitExtension.getValue().castToString(unitExtension.getValue()).getValueAsString());
    }

    @Test
    public void setDosageTimingAndDosePeriodDosage() throws Exception
    {
        Dosage dosage = new Dosage();

        PeriodDosageType periodDosageType = new PeriodDosageType();

        PQIntervalType dose = new PQIntervalType();
        dose.setHigh(11000.0);
        dose.setLow(6000.0);
        dose.setUnit("ml");
        periodDosageType.setDose(dose);

        PQType period = new PQType();
        period.setValue(6.6);
        period.setUnit("h");
        periodDosageType.setPeriod(period);

        medicationHistoryDataTransformer.setDosageTimingAndDose(
                dosage,
                null,
                null,
                periodDosageType,
                null,
                null,
                null
        );

        assert 11000.0 == dosage.getDose().castToRange(dosage.getDose()).getHigh().getValue().doubleValue();
        assert 6000.0 == dosage.getDose().castToRange(dosage.getDose()).getLow().getValue().doubleValue();

        Extension doseUnitExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit").get(0);
        assertNotNull(doseUnitExtension);
        assert "ml".equals(doseUnitExtension.getValue().castToString(doseUnitExtension.getValue()).getValueAsString());

        assert 6.6 == dosage.getTiming().getRepeat().getPeriod().doubleValue();
        assert Timing.UnitsOfTime.H == dosage.getTiming().getRepeat().getPeriodUnit();
    }

    @Test
    public void setDosageTimingAndDoseSingleDose() throws Exception
    {
        Dosage dosage = new Dosage();

        SingleDoseType singleDoseType = new SingleDoseType();

        PQIntervalType dose = new PQIntervalType();
        dose.setHigh(11500.0);
        dose.setLow(7500.0);
        dose.setUnit("mg");
        singleDoseType.setDose(dose);

        singleDoseType.setTime("20110101010101");

        medicationHistoryDataTransformer.setDosageTimingAndDose(
                dosage,
                null,
                null,
                null,
                null,
                singleDoseType,
                null
        );

        assert 11500.0 == dosage.getDose().castToRange(dosage.getDose()).getHigh().getValue().doubleValue();
        assert 7500.0 == dosage.getDose().castToRange(dosage.getDose()).getLow().getValue().doubleValue();

        Extension doseUnitExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit").get(0);
        assertNotNull(doseUnitExtension);
        assert "mg".equals(doseUnitExtension.getValue().castToString(doseUnitExtension.getValue()).getValueAsString());

        assert "2011-01-01T01:01:01+01:00".equals(dosage.getTiming().getEvent().get(0).asStringValue());
    }

    @Test
    public void setDosageTimingAndDoseUnstructuredDosageInformation() throws Exception
    {
        Dosage dosage = new Dosage();

        UnstructuredDosageInformationType unstructuredDosageInformationType = new UnstructuredDosageInformationType();
        unstructuredDosageInformationType.setText("Unstructured dosage information");

        medicationHistoryDataTransformer.setDosageTimingAndDose(
                dosage,
                null,
                null,
                null,
                null,
                null,
                unstructuredDosageInformationType
        );

        Extension unstructuredDosageInformationExtension = dosage.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-UnstructuredDosageInformation").get(0);
        assertNotNull(unstructuredDosageInformationExtension);
        assert "Unstructured dosage information".equals(unstructuredDosageInformationExtension.getValue().castToString(unstructuredDosageInformationExtension.getValue()).getValueAsString());
    }

    @Test
    public void medicationUnstructuredInformationType()
    {
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        UnstructuredDrugInformationType unstructuredDrugInformationType = new UnstructuredDrugInformationType();
        drugChoiceType.setUnstructuredDrugInformation(unstructuredDrugInformationType);

        unstructuredDrugInformationType.setUnstructuredInformation("unstructured information");

        Medication medication = medicationHistoryDataTransformer.medication(drugChoiceType);

        assert medication.getCode().getText().equals("unstructured information");
    }

    @Test
    public void medicationMerchandiseType()
    {
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        MerchandiseType merchandiseType = new MerchandiseType();
        drugChoiceType.setMerchandise(merchandiseType);

        String articleCode = RandomStringUtils.random(10, true, true);

        CVType articleNumber = new CVType();
        articleNumber.setCode(articleCode);

        merchandiseType.setArticleNumber(articleNumber);

        Medication medication = medicationHistoryDataTransformer.medication(drugChoiceType);

        assert medication.getCode().getCoding().get(0).getCode().equals(articleCode);
        assert medication.getCode().getCoding().get(0).getSystem().equals("urn:oid: 1.2.752.129.2.2.3.1.1");
    }

    @Test
    public void medicationDrugArticleType()
    {
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        DrugArticleType drugArticleType = new DrugArticleType();
        drugChoiceType.setDrugArticle(drugArticleType);

        String nplPackIdCode = RandomStringUtils.random(10, true, true);

        CVType nplPackId = new CVType();
        nplPackId.setCode(nplPackIdCode);
        nplPackId.setCodeSystem("npl-pack-id-system");

        drugArticleType.setNplPackId(nplPackId);

        Medication medication = medicationHistoryDataTransformer.medication(drugChoiceType);

        assert medication.getPackage().getContainer().getCoding().get(0).getCode().equals(nplPackIdCode);
        assert medication.getPackage().getContainer().getCoding().get(0).getSystem().equals("urn:oid:npl-pack-id-system");
    }

    @Test
    public void medicationDrugType() throws Exception
    {
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        DrugType drugType = new DrugType();
        drugChoiceType.setDrug(drugType);

        String nplCode = RandomStringUtils.random(10, true, true);

        CVType nplId = new CVType();
        nplId.setCode(nplCode);

        drugType.setNplId(nplId);
        drugType.setPharmaceuticalForm("pharmaceutical form");

        Medication medication = medicationHistoryDataTransformer.medication(drugChoiceType);

        assert medication.getCode().getCoding().get(0).getCode().equals(nplCode);
        assert medication.getCode().getCoding().get(0).getSystem().equals("urn:oid:1.2.752.129.2.1.5.1");
        assert medication.getForm().getText().equals("pharmaceutical form");
    }

    @Test
    public void medicationGenericsType() throws Exception
    {
        DrugChoiceType drugChoiceType = new DrugChoiceType();
        GenericsType genericsType = new GenericsType();
        drugChoiceType.setGenerics(genericsType);

        genericsType.setForm("generics pharmaceutical form");
        genericsType.setSubstance("generics substance");

        PQType strength = new PQType();
        strength.setValue(10000.1);
        strength.setUnit("ml");
        genericsType.setStrength(strength);


        Medication medication = medicationHistoryDataTransformer.medication(drugChoiceType);

        assert medication.getForm().getText().equals("generics pharmaceutical form");
        assert ((Substance) medication.getIngredient().get(0).getItemReference().getResource()).getCode().getText().equals("generics substance");

        assert 2 == medication.getExtension().size();

        Extension valueExtension = medication.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Value").get(0);
        assertNotNull(valueExtension);
        DecimalType value = valueExtension.getValue().castToDecimal(valueExtension.getValue());
        assert value.getValueAsString().equals("10000.1");

        Extension unitExtension = medication.getExtensionsByUrl( "http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Unit").get(0);
        assertNotNull(unitExtension);
        StringType unit = unitExtension.getValue().castToString(unitExtension.getValue());
        assert unit.getValue().equals("ml");
    }

    @Test
    public void medicationAdministrationsSubject() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();
        Patient patient = new Patient();

        MedicationAdministration medicationAdministration = medicationHistoryDataTransformer.medicationAdministrations(administrationType, null, patient).get(0);

        assert patient == medicationAdministration.getSubject().getResource();
    }

    @Test
    public void medicationAdministrationContext() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();
        Encounter encounter = new Encounter();

        MedicationAdministration medicationAdministration = medicationHistoryDataTransformer.medicationAdministrations(administrationType, encounter, null).get(0);

        assert encounter == medicationAdministration.getContext().getResource();
    }

    @Test
    public void medicationAdministrationPerformer() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();

        HealthcareProfessionalType healthcareProfessionalType = new HealthcareProfessionalType();
        healthcareProfessionalType.setHealthcareProfessionalName("Professional name");
        healthcareProfessionalType.setHealthcareProfessionalHSAId("HSA-ID");

        administrationType.setAdministeringHealthcareProfessional(healthcareProfessionalType);

        MedicationAdministration medicationAdministration = medicationHistoryDataTransformer.medicationAdministrations(administrationType, null, null).get(0);

        assert 1 == medicationAdministration.getPerformer().size();

        Practitioner practitioner = (Practitioner) medicationAdministration.getPerformer().get(0).getActor().getResource();

        assert "urn:oid:1.2.752.129.2.1.4.1".equals(practitioner.getIdentifier().get(0).getSystem());
        assert "HSA-ID".equals(practitioner.getIdentifier().get(0).getValue());
        assert "Professional name".equals(practitioner.getName().get(0).getText());
    }

    @Test
    public void medicationAdministrationNote() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();
        administrationType.setAdministrationComment("Administration comment");

        MedicationAdministration medicationAdministration = medicationHistoryDataTransformer.medicationAdministrations(administrationType, null, null).get(0);

        assert "Administration comment".equals(medicationAdministration.getNote().get(0).getText());
    }

    @Test
    public void medicationAdministrationRoute() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();

        CVType routeOfAdministration = new CVType();
        routeOfAdministration.setCodeSystem("CODE SYSTEM");
        routeOfAdministration.setCode("CODE");

        administrationType.setRouteOfAdministration(routeOfAdministration);

        MedicationAdministration medicationAdministration = medicationHistoryDataTransformer.medicationAdministrations(administrationType, null, null).get(0);

        assert "urn:oid:CODE SYSTEM".equals(medicationAdministration.getDosage().getRoute().getCoding().get(0).getSystem());
        assert "CODE".equals(medicationAdministration.getDosage().getRoute().getCoding().get(0).getCode());
    }

    @Test
    public void medicationAdministrationDosages() throws Exception
    {
        AdministrationType administrationType = new AdministrationType();

        DrugChoiceType drugChoiceType = new DrugChoiceType();

        DosageType dosageType = new DosageType();
        dosageType.setSetDosage(new SetDosageType());

        drugChoiceType.getDosage().add(dosageType);

        administrationType.setDrug(drugChoiceType);

        Patient patient = new Patient();
        Encounter encounter = new Encounter();

        List<MedicationAdministration> medicationAdministrationList = medicationHistoryDataTransformer.medicationAdministrations(administrationType, encounter, patient);

        assert 2 == medicationAdministrationList.size();

        MedicationAdministration medicationAdministrationMain = medicationAdministrationList.get(0);
        MedicationAdministration medicationAdministrationDosage = medicationAdministrationList.get(1);

        // Assert that we reuse references
        assert medicationAdministrationMain.getContext().getResource() == medicationAdministrationDosage.getContext().getResource();
        assert medicationAdministrationMain.getSubject().getResource() == medicationAdministrationDosage.getSubject().getResource();
        assert medicationAdministrationMain.getMedicationReference().getResource() == medicationAdministrationDosage.getMedicationReference().getResource();

        assert 1 == medicationAdministrationDosage.getExtensionsByUrl("http://fhir.sll.se/Extension/DosageInstruction").size();

        assert medicationAdministrationDosage.getPartOf().get(0).getResource() == medicationAdministrationMain;
    }

    @Test
    public void medicationDispenseIdentifier() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        IIType identifier = new IIType();

        identifier.setRoot("source system");
        identifier.setExtension("document id");

        dispensationAuthorizationType.setDispensationAuthorizationId(identifier);
        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert "source system".equals(medicationDispense.getIdentifier().get(0).getSystem());
        assert "document id".equals(medicationDispense.getIdentifier().get(0).getValue());
    }

    @Test
    public void medicationDispenseDistributionMethod() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setDistributionMethod("The distribution method");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);


        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert "The distribution method".equals(medicationDispense.getCategory().getText());
    }

    @Test
    public void medicationDispenseSubject() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        medicationPrescriptionType.setDispensationAuthorization(new DispensationAuthorizationType());

        Patient patient = new Patient();

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                patient
        );

        assert patient == (Patient) medicationDispense.getSubject().getResource();
    }

    @Test
    public void medicationDispenseContext() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        medicationPrescriptionType.setDispensationAuthorization(new DispensationAuthorizationType());

        Encounter encounter = new Encounter();

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                encounter,
                null
        );

        assert encounter == (Encounter) medicationDispense.getContext().getResource();
    }

    @Test
    public void medicationDispensePerformer() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        HealthcareProfessionalType healthcareProfessionalType = new HealthcareProfessionalType();

        healthcareProfessionalType.setHealthcareProfessionalHSAId("HSA-ID");
        healthcareProfessionalType.setHealthcareProfessionalName("Professional name");

        dispensationAuthorizationType.setDispensationAuthorizer(healthcareProfessionalType);
        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert "urn:oid:1.2.752.129.2.1.4.1".equals(((Practitioner) medicationDispense.getPerformer().get(0).getActor().getResource()).getIdentifier().get(0).getSystem());
        assert "HSA-ID".equals(((Practitioner) medicationDispense.getPerformer().get(0).getActor().getResource()).getIdentifier().get(0).getValue());

        assert "Professional name".equals(((Practitioner) medicationDispense.getPerformer().get(0).getActor().getResource()).getName().get(0).getText());
    }

    @Test
    public void medicationDispenseAuthorizingPrescription() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        medicationPrescriptionType.setDispensationAuthorization(new DispensationAuthorizationType());

        MedicationRequest medicationRequest = new MedicationRequest();

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                medicationRequest,
                null,
                null
        );

        assert medicationRequest == medicationDispense.getAuthorizingPrescription().get(0).getResource();
    }

    @Test
    public void medicationDispenseQuantity() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setTotalAmount(10000.0);
        dispensationAuthorizationType.setPackageUnit("unit");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert 10000.0 == medicationDispense.getQuantity().getValue().doubleValue();
        assert "unit".equals(medicationDispense.getQuantity().getUnit());
    }

    @Test
    public void medicationDispenseNote() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setDispensationAuthorizerComment("A comment");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert "A comment".equals(medicationDispense.getNote().get(0).getText());
    }

    @Test
    public void medicationDispenseValidUntil() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setValidUntil("20110101");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        assert "2011-01-01".equals(medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-ValidUntil").get(0).getValue().castToDate(medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-ValidUntil").get(0).getValue()).getValueAsString());
    }

    @Test
    public void medicationDispenseReceivingPharmacy() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        IIType receivingPharmacy = new IIType();
        receivingPharmacy.setRoot("System");
        receivingPharmacy.setExtension("Id");
        dispensationAuthorizationType.setReceivingPharmacy(receivingPharmacy);

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        Extension extension = medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-ReceivingPharmacy").get(0);
        assert "System".equals(extension.getValue().castToIdentifier(extension.getValue()).getSystem());
        assert "Id".equals(extension.getValue().castToIdentifier(extension.getValue()).getValue());
    }

    @Test
    public void medicationDispenseMinimumDispensationInterval() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        PQType pqType = new PQType();
        pqType.setUnit("d");
        pqType.setValue(10);

        dispensationAuthorizationType.setMinimumDispensationInterval(pqType);

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        Extension extension = medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-MinimumDispensationInterval").get(0);

        assert 10 == extension.getValue().castToSimpleQuantity(extension.getValue()).getValue().doubleValue();
        assert "d".equals(extension.getValue().castToSimpleQuantity(extension.getValue()).getUnit());
    }

    @Test
    public void medicationDispenseFirstDispensationBefore() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setFirstDispensationBefore("20110101");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        Extension extension = medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-FirstDispensationBefore").get(0);

        assert "2011-01-01".equals(extension.getValue().castToDate(extension.getValue()).getValueAsString());
    }

    @Test
    public void medicationDispensePrescriptionSignatura() throws Exception
    {
        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
        DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();

        dispensationAuthorizationType.setPrescriptionSignatura("Signatura");

        medicationPrescriptionType.setDispensationAuthorization(dispensationAuthorizationType);

        MedicationDispense medicationDispense = medicationHistoryDataTransformer.medicationDispense(
                medicationPrescriptionType,
                null,
                null,
                null
        );

        Extension extension = medicationDispense.getExtensionsByUrl("http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-PrescriptionSignatura").get(0);

        assert "Signatura".equals(extension.getValue().castToString(extension.getValue()).getValue());
    }

    @Test
    public void medicationStatementPatient() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        PersonIdType patientId = new PersonIdType();
        patientId.setId("191212121212");
        patientId.setType("1.2.752.129.2.1.3.1");
        patientSummaryHeaderType.setPatientId(patientId);

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(patientSummaryHeaderType);

        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert "urn:oid:1.2.752.129.2.1.3.1".equals(((Patient) medicationStatement.getSubject().getResource()).getIdentifier().get(0).getSystem());
        assert "191212121212".equals(((Patient) medicationStatement.getSubject().getResource()).getIdentifier().get(0).getValue());
    }

    @Test
    public void medicationStatementEncounter() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        patientSummaryHeaderType.setCareContactId("CareContactId");

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(patientSummaryHeaderType);

        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert "CareContactId".equals(((Encounter) medicationStatement.getContext().getResource()).getIdentifier().get(0).getValue());
    }

    @Test
    public void medicationStatementIdAndIdentifier() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        PatientSummaryHeaderType patientSummaryHeaderType = new PatientSummaryHeaderType();
        patientSummaryHeaderType.setSourceSystemHSAId("source system hsaid");
        patientSummaryHeaderType.setDocumentId("document id");

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(patientSummaryHeaderType);

        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert Riv2FhirTransformationHelper.getId("source system hsaid", "document id").equals(
                medicationStatement.getId()
        );

        assert "source system hsaid".equals(medicationStatement.getIdentifier().get(0).getSystem());
        assert "document id".equals(medicationStatement.getIdentifier().get(0).getValue());
    }

    @Test
    public void medicationStatementStatusActive() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(new PatientSummaryHeaderType());
        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();

        medicationMedicalRecordType.getMedicationMedicalRecordBody().setMedicationPrescription(medicationPrescriptionType);

        medicationPrescriptionType.setTypeOfPrescription(TypeOfPrescriptionEnum.I);

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert MedicationStatement.MedicationStatementStatus.ACTIVE == medicationStatement.getStatus();
    }

    @Test
    public void medicationStatementStatusStopped() throws Exception
    {
//        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();
//
//        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
//        medicationMedicalRecordType.setMedicationMedicalRecordHeader(new PatientSummaryHeaderType());
//        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());
//
//        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
//
//        medicationMedicalRecordType.getMedicationMedicalRecordBody().setMedicationPrescription(medicationPrescriptionType);
//
//        medicationPrescriptionType.setTypeOfPrescription(TypeOfPrescriptionEnum.U);
//
//        medicationMedicalRecordTypes.add(medicationMedicalRecordType);
//
//        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);
//
//        assert MedicationStatement.MedicationStatementStatus.STOPPED == medicationStatement.getStatus();

        assert true;
    }

    @Test
    public void medicationStatementEffective() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(new PatientSummaryHeaderType());
        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();

        medicationMedicalRecordType.getMedicationMedicalRecordBody().setMedicationPrescription(medicationPrescriptionType);

        medicationPrescriptionType.setStartOfTreatment("20110101010101");
        medicationPrescriptionType.setEndOfTreatment("20120202020202");

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        Period period = medicationStatement.getEffective().castToPeriod(medicationStatement.getEffective());

        assert "2011-01-01T00:01:01Z".equals(period.getStart().toInstant().toString());
        assert "2012-02-02T01:02:02Z".equals(period.getEnd().toInstant().toString());
    }

    @Test
    public void medicationStatementTakenY() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(new PatientSummaryHeaderType());
        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();

        medicationMedicalRecordType.getMedicationMedicalRecordBody().setMedicationPrescription(medicationPrescriptionType);

        medicationPrescriptionType.getAdministration().add(new AdministrationType());

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert MedicationStatement.MedicationStatementTaken.Y == medicationStatement.getTaken();
    }


    @Test
    public void medicationStatementTakenUNK() throws Exception
    {
        List<MedicationMedicalRecordType> medicationMedicalRecordTypes = new LinkedList<>();

        MedicationMedicalRecordType medicationMedicalRecordType = new MedicationMedicalRecordType();
        medicationMedicalRecordType.setMedicationMedicalRecordHeader(new PatientSummaryHeaderType());
        medicationMedicalRecordType.setMedicationMedicalRecordBody(new MedicationMedicalRecordBodyType());

        MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();

        medicationMedicalRecordType.getMedicationMedicalRecordBody().setMedicationPrescription(medicationPrescriptionType);

        medicationMedicalRecordTypes.add(medicationMedicalRecordType);

        MedicationStatement medicationStatement = medicationHistoryDataTransformer.transformToFhir(medicationMedicalRecordTypes).get(0);

        assert MedicationStatement.MedicationStatementTaken.UNK == medicationStatement.getTaken();
    }
}
