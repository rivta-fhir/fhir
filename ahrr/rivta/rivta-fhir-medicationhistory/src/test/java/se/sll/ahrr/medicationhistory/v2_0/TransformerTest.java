package se.sll.ahrr.medicationhistory.v2_0;

/*-
 * #%L
 * rivta-fhir-medicationhistory
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import riv.clinicalprocess.activityprescription.actoutcome._2.*;
import riv.clinicalprocess.activityprescription.actoutcome.enums._2.NonReplaceableEnum;
import riv.clinicalprocess.activityprescription.actoutcome.enums._2.TypeOfPrescriptionEnum;
import se.sll.ahrr.MockData.Helpers;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;

public class TransformerTest {
//    private final MockFactory mockDataFactory = new MockFactory();
//
//    private enum MedicationType {
//        DRUG_ARTICLE,
//        UNSTRUCTURED_DRUG_INFORMATION,
//        MERCHANDISE,
//        DRUG,
//        GENERICS
//    }
//
//    @Test
//    public void verifyRiv2Fhir() throws Exception {
//    }
//
//    //@Test
//    public void verifySelfMedicating() {
//        try {
//            final PatientSummaryHeaderType header = createHeader();
//            final MedicationMedicalRecordBodyType body = createBody(MedicationType.DRUG, true);
//            final MedicationStatement medicationStatement = new MedicationHistoryDataTransformer()
//                    .transformToFhir(Collections.singletonList(getMedicationMedicalRecordType(header, body))).get(0);
//            assertSelfMedication(medicationStatement, body);
//        } catch (Exception e) {
//            fail("Error while verifying SelfMedicating");
//        }
//    }
//
//    @Test
//    public void verifyPrincipalPrescriptionOtherReason() {
//        try {
//            final PatientSummaryHeaderType header = createHeader();
//            final MedicationMedicalRecordBodyType body = createBody(MedicationType.DRUG_ARTICLE, false);
//            body.getMedicationPrescription().getPrincipalPrescriptionReason().get(0).getReason().setCode("46021000052104");
//            body.getMedicationPrescription().getPrincipalPrescriptionReason().get(0).setOtherReason("OtherReason");
//            final MedicationStatement medicationStatement = new MedicationHistoryDataTransformer().transformToFhir(Collections.singletonList(getMedicationMedicalRecordType(header, body))).get(0);
//            assertEquals(medicationStatement.getReasonCode().get(0).getCodingFirstRep().getCode(), "46021000052104");
//            assertEquals(medicationStatement.getReasonCode().get(0).getCoding().get(1).getDisplay(), "OtherReason");
//        } catch (Exception e) {
//            fail("Error while verifying PrincipalPrescriptionOtherReason");
//        }
//    }
//
//    @Test
//    public void verifyMedicationTypes() {
//        try {
//            for (MedicationType medicationType : MedicationType.values()) {
//                final PatientSummaryHeaderType header = createHeader();
//                final MedicationMedicalRecordBodyType body = createBody(medicationType, false);
//                final MedicationMedicalRecordType medicalRecordType = getMedicationMedicalRecordType(header, body);
//                final MedicationStatement medicationStatement = new MedicationHistoryDataTransformer().transformToFhir(Collections.singletonList(medicalRecordType)).get(0);
//
//                switch (medicationType) {
//                    case DRUG_ARTICLE:
//                        assertMedicationDrugArticleType(body, medicationStatement);
//                        break;
//                    case UNSTRUCTURED_DRUG_INFORMATION:
//                        assertMedicationUnstructuredDrugInformationType(body, medicationStatement);
//                        break;
//                    case MERCHANDISE:
//                        assertMedicationMerchandiseType(body, medicationStatement);
//                        break;
//                    case DRUG:
//                        assertMedicationDrugType(body, medicationStatement);
//                        break;
//                    case GENERICS:
//                        assertMedicationGenericsType(body, medicationStatement);
//                        break;
//                }
//
//                assertTrue(findMedicationRequestPrescription(medicationStatement).getNote().stream().anyMatch(r -> r.hasText() && r.getText().equals(body.getMedicationPrescription().getDrug().getComment())));
//                for (DosageType dosageType : body.getMedicationPrescription().getDrug().getDosage()) {
//                    assertDosageShortNotation(dosageType.getShortNotation(), medicationStatement.getDosage());
//                    assertDosageUnitDose(dosageType.getUnitDose(), medicationStatement.getDosage());
//                    assertTrue(medicationStatement.getDosage().stream().anyMatch(r -> r.getMethod() != null && r.getMethod().getText() != null && r.getMethod().getText().equals(dosageType.getDosageInstruction())));
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            fail("Error while verifying medication types");
//        }
//    }
//
//    @Test
//    public void verifyCommons() throws Exception {
//        final PatientSummaryHeaderType header = createHeader();
//        final MedicationMedicalRecordBodyType body = createBody(MedicationType.DRUG, false);
//        final MedicationMedicalRecordType medicalRecord = getMedicationMedicalRecordType(header, body);
//        final MedicationStatement medicationStatement = new MedicationHistoryDataTransformer().transformToFhir(Collections.singletonList(medicalRecord)).get(0);
//
//        //Verify header
//        assertEquals(Riv2FhirTransformationHelper.getId(
//                header.getSourceSystemHSAId(),
//                header.getDocumentId()
//        ), medicationStatement.getId());
//        assertEquals(header.getSourceSystemHSAId(), medicationStatement.getIdentifier().get(0).getSystem());
//        assertEquals(header.getDocumentId(), medicationStatement.getIdentifier().get(0).getValue());
//        assertEquals(header.getCareContactId(), ((Encounter) medicationStatement.getContext().getResource()).getIdentifier().get(0).getValue());
//        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), ((Practitioner) medicationStatement.getInformationSource().getResource()));
//        assertLongDateString(header.getAccountableHealthcareProfessional().getAuthorTime(), medicationStatement.getDateAsserted());
//        Assertions.assertLegalAuthenticator(header.getLegalAuthenticator(), ((Encounter) medicationStatement.getContext().getResource()).getExtension().get(0));
//        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) ((Encounter) medicationStatement.getContext().getResource()).getServiceProvider().getResource());
//        final MedicationRequest medRequestPrescription = findMedicationRequestPrescription(medicationStatement);
//        assertTrue(medRequestPrescription.getNote().stream().anyMatch(r -> r.getText().equals(body.getMedicationPrescription().getEndOfTreatmentReason().getOriginalText())));
//        final MedicationRequest medRequestDispensation = (MedicationRequest) medicationStatement.getBasedOn().get(0).getResource();
//        assertTrue(isDispensation(medRequestDispensation));
//        assertDispensationAuthorization(body.getMedicationPrescription().getDispensationAuthorization(), medRequestDispensation);
//    }
//
//    private MedicationRequest findMedicationRequestPrescription(final MedicationStatement medicationStatement) {
//        if (medicationStatement.getBasedOn().isEmpty()) {
//            fail("MedicationRequestPrescription not found");
//            return null;
//        }
//
//        MedicationRequest medicationRequest = (MedicationRequest) medicationStatement.getBasedOn().get(0).getResource();
//        while (!medicationRequest.getBasedOn().isEmpty()) {
//            medicationRequest = (MedicationRequest) medicationRequest.getBasedOn().get(0).getResource();
//        }
//        return medicationRequest;
//    }
//
//    private boolean isDispensation(final MedicationRequest medicationRequest) {
//        return !medicationRequest.getBasedOn().isEmpty();
//    }
//
//    private void assertMedicationUnstructuredDrugInformationType(final MedicationMedicalRecordBodyType body, final MedicationStatement medicationStatement) {
//        try {
//            assertNotNull(body.getMedicationPrescription().getDrug().getUnstructuredDrugInformation());
//            final Medication medication = (Medication) medicationStatement.getMedicationReference().getResource();
//            final MedicationRequest medRequestPrescription = findMedicationRequestPrescription(medicationStatement);
//            assertNotNull(medication);
//            assertNotNull(medRequestPrescription);
//            assertNotNull(medRequestPrescription.getMedicationReference().getResource());
//            assertTrue(medication.equalsDeep((Medication) medRequestPrescription.getMedicationReference().getResource()));
//            assertEquals(medication.getCode().getText(), body.getMedicationPrescription().getDrug().getUnstructuredDrugInformation().getUnstructuredInformation());
//        } catch (Exception e) {
//            fail("Error while asserting medicaitonUnstructuredDrugInformationType");
//        }
//    }
//
//    private void assertMedicationMerchandiseType(final MedicationMedicalRecordBodyType body, final MedicationStatement medicationStatement) {
//
//        try {
//            final Medication medication = (Medication) medicationStatement.getMedicationReference().getResource();
//            Assertions.assertCoding(body.getMedicationPrescription().getDrug().getMerchandise().getArticleNumber(), medication.getCode().getCodingFirstRep());
//        } catch (Exception e) {
//            fail("Error while assertingMedicationMerchendiseType");
//        }
//
//    }
//
//    private void assertDispensationAuthorization(final DispensationAuthorizationType dispensationAuthorization, final MedicationRequest medicationRequest) {
//        assertEquals(medicationRequest.getDispenseRequest().getValidityPeriod().getEnd().getTime(), Riv2FhirTransformationHelper.getDateForDate(dispensationAuthorization.getValidUntil()).getValue().getTime());
//        assertEquals(medicationRequest.getDispenseRequest().getExpectedSupplyDuration().getUnit(), dispensationAuthorization.getMinimumDispensationInterval().getUnit());
//        assertEquals(medicationRequest.getDispenseRequest().getExpectedSupplyDuration().getValue().doubleValue(), dispensationAuthorization.getMinimumDispensationInterval().getValue(), Double.MIN_VALUE);
//        Assertions.assertAccountableHealthcareProfessional(dispensationAuthorization.getDispensationAuthorizer(), (Practitioner) medicationRequest.getRequester().getAgent().getResource());
//        assertTrue(medicationRequest.getNote().stream().anyMatch(r -> r.getText().equals(dispensationAuthorization.getDistributionMethod())));
//        assertEquals(medicationRequest.getAuthoredOn().getTime(), Riv2FhirTransformationHelper.getDateTime(dispensationAuthorization.getDispensationAuthorizer().getAuthorTime()).getValue().getTime());
//        assertEquals(dispensationAuthorization.getPrescriptionSignatura(), medicationRequest.getDosageInstructionFirstRep().getPatientInstruction());
//        final double totalAmount = dispensationAuthorization.getTotalAmount();
//        final String packageUnit = dispensationAuthorization.getPackageUnit();
//        assertTrue(medicationRequest.getNote().stream().anyMatch(note -> note.getText().equals(String.format("%f - %s", totalAmount, packageUnit))));
//    }
//
//    private void assertMedicationGenericsType(final MedicationMedicalRecordBodyType body, final MedicationStatement medicationStatement) {
//        try {
//            final Medication medication = (Medication) medicationStatement.getMedicationReference().getResource();
//            assertEquals(medication.getForm().getText(), body.getMedicationPrescription().getDrug().getGenerics().getForm());
//            assert (medication.getIngredientFirstRep().getAmount().getNumerator()
//                    .equalsDeep(new Quantity().setUnit(body.getMedicationPrescription().getDrug().getGenerics().getStrength().getUnit())
//                            .setValue(body.getMedicationPrescription().getDrug().getGenerics().getStrength().getValue())));
//            assert (((Substance) medication.getIngredientFirstRep().getItemReference().getResource()).getCode().getCodingFirstRep().getCode()
//                    .equals(body.getMedicationPrescription()
//                            .getDrug()
//                            .getGenerics()
//                            .getSubstance()));
//        } catch (FHIRException e) {
//            fail("Error while assertingMedicationGenericsType");
//        }
//    }
//
//    private void assertDosageShortNotation(final String shortNotation, final List<Dosage> dosages) {
//        assertTrue(dosages.stream().anyMatch(dosage -> dosage.getText() != null && dosage.getText().equals(shortNotation)));
//    }
//
//    private void assertDosageUnitDose(final CVType unitDose, final List<Dosage> dosages) {
//        for (Dosage dosage : dosages) {
//            final Quantity quantity = (Quantity) dosage.getDose();
//            if (quantity != null) {
//                if (quantity.getUnit().equals(unitDose.getOriginalText())
//                        && quantity.getCode().equals(unitDose.getCode())
//                        && quantity.getSystem().equals("urn:oid:" + unitDose.getCodeSystem())) {
//
//                    return;
//                }
//            }
//        }
//        fail("Could not find UnitDose in dosages.");
//    }
//
//    private void assertMedicationDrugType(final MedicationMedicalRecordBodyType body, final MedicationStatement medicationStatement) {
//        assertNotNull(body.getMedicationPrescription().getDrug().getDrug());
//        assertIIType(body.getMedicationPrescription().getPrescriptionId(), medicationStatement.getIdentifier().get(1));
//        assertEquals(medicationStatement.getStatus(), MedicationStatement.MedicationStatementStatus.ACTIVE);
//        assertEquals(medicationStatement.getNote().get(0).getText(), "Prescription note. Treatment purpose");
//        assertPeriod(body.getMedicationPrescription().getStartOfFirstTreatment(), body.getMedicationPrescription().getEndOfTreatment(), (Period) medicationStatement.getEffective());
//        Assertions.assertCoding(body.getMedicationPrescription().getPrincipalPrescriptionReason().get(0).getReason(), (medicationStatement.getReasonCode().get(0).getCodingFirstRep()));
//        assertAdministration(body.getMedicationPrescription().getAdministration().get(0), (MedicationAdministration) medicationStatement.getDerivedFrom().get(0).getResource());
//        assertDrugAtcCoding(medicationStatement, body.getMedicationPrescription().getDrug());
//        assertPatientMapping(body.getAdditionalPatientInformation(), (Patient) medicationStatement.getSubject().getResource());
//      //  assertTrue(medicationStatement.getDosage().stream().anyMatch(dosage -> dosage.getRoute() != null && dosage.getRoute().getText().equals(body.getMedicationPrescription().getDrug().getDrug().getRouteOfAdministration().getOriginalText())));
//    }
//
//    private void assertMedicationDrugArticleType(final MedicationMedicalRecordBodyType body, final MedicationStatement medicationStatement) {
//
//        assertNotNull(body.getMedicationPrescription().getDrug().getDrugArticle());
//        assertDrugArticleNplCoding(medicationStatement, body.getMedicationPrescription().getDrug());
//    }
//
//    private MedicationMedicalRecordType getMedicationMedicalRecordType(final PatientSummaryHeaderType header, MedicationMedicalRecordBodyType body) {
//        final MedicationMedicalRecordType retval = new MedicationMedicalRecordType();
//        retval.setMedicationMedicalRecordHeader(header);
//        retval.setMedicationMedicalRecordBody(body);
//        return retval;
//    }
//
//    private void assertAdministration(final AdministrationType administrationType, final MedicationAdministration medicationAdministration) {
//        assertAdministrationIIType(administrationType.getAdministrationId(), medicationAdministration.getIdentifierFirstRep());
//        assertPeriod(administrationType.getAdministrationTime().getStart(), administrationType.getAdministrationTime().getEnd(), (Period) medicationAdministration.getEffective());
//        assertEquals(administrationType.getAdministrationComment(), medicationAdministration.getNote().get(0).getText());
//        Assertions.assertCoding(administrationType.getRouteOfAdministration(), medicationAdministration.getDosage().getRoute().getCodingFirstRep());
//    }
//
//    private void assertDrugArticleNplCoding(final MedicationStatement medicationStatement, DrugChoiceType drugChoiceType) {
//        Coding nplCoding = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrugArticle().getNplPackId()).getCodingFirstRep();
//        assert (findMedicationCoding(medicationStatement, nplCoding));
//    }
//
//    private void assertDrugAtcCoding(final MedicationStatement medicationStatement, DrugChoiceType drugChoiceType) {
//        Coding atcCoding = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrug().getAtcCode()).getCodingFirstRep();
//        assert (findMedicationCoding(medicationStatement, atcCoding));
//    }
//
//    private boolean findMedicationCoding(final MedicationStatement medicationStatement, final Coding code) {
//        try {
//            Medication medication = (Medication) medicationStatement.getMedicationReference().getResource();
//            return CodingEquals(medication.getCode().getCodingFirstRep(), code);
//        } catch (FHIRException e) {
//            fail("Error while casting MS.medication");
//            return false;
//        }
//    }
//
//    private void assertPeriod(final String start, final String end, final Period period) {
//        assertLongDateString(start, period.getStart());
//        assertLongDateString(end, period.getEnd());
//    }
//
//    private void assertAdministrationIIType(final IIType iiType, final Identifier identifier) {
//        assertEquals(iiType.getRoot(), identifier.getSystem());
//        assertEquals(iiType.getExtension(), identifier.getValue());
//    }
//
//    private void assertIIType(final IIType iiType, final Identifier identifier) {
//        assertEquals("urn:oid:" + iiType.getRoot(), identifier.getSystem());
//        assertEquals(iiType.getExtension(), identifier.getValue());
//    }
//
//    private void assertSelfMedication(final MedicationStatement medicationStatement, final MedicationMedicalRecordBodyType body) {
//        throw new NotImplementedException();
//        /*if (!medicationStatement.getBasedOn().isEmpty() && medicationStatement.getBasedOnFirstRep().getResource() != null) {
//            try {
//                MedicationRequest request = (MedicationRequest) medicationStatement.getBasedOn().get(0).getResource();
////              assert(request.getMedication() == null);
//            } catch (Exception e) {
//                e.printStackTrace();
//                fail("MS.medicationRequest cast failed");
//            }
//        }*/
//
//    }
//
//
//    private MedicationMedicalRecordBodyType createBody(final MedicationType medicationType, final boolean isSelfMedicating) {
//
//        final MedicationMedicalRecordBodyType body = new ObjectFactory().createMedicationMedicalRecordBodyType();
//        body.setAdditionalPatientInformation(createPatientInfo());
//        body.setMedicationPrescription(createMedicationPrescription(medicationType, isSelfMedicating));
//
//        return body;
//    }
//
//    private MedicationPrescriptionType createMedicationPrescription(final MedicationType medicationType, final boolean isSelfMedicating) {
//        try {
//            final MedicationPrescriptionType medicationPrescriptionType = new MedicationPrescriptionType();
//            medicationPrescriptionType.setSelfMedication(isSelfMedicating);
//            medicationPrescriptionType.setPrescriptionId((IIType) mockDataFactory.createIIType(new IIType()));
//            medicationPrescriptionType.setTypeOfPrescription(TypeOfPrescriptionEnum.I);
//            medicationPrescriptionType.setPrescriptionNote("Prescription note");
//            medicationPrescriptionType.setTreatmentPurpose("Treatment purpose");
//            medicationPrescriptionType.setStartOfFirstTreatment("20160505193000");
//            medicationPrescriptionType.setStartOfTreatment("20160605193000");
//            medicationPrescriptionType.setEndOfTreatment("20170505193000");
//            medicationPrescriptionType.getPrincipalPrescriptionReason().add(createPrescriptionReason());
//            for (int i = 0; i < MedicationType.values().length; i++) {
//                AdministrationType administration = (AdministrationType) mockDataFactory.createAdministrationType(
//                        new IIType(),
//                        new TimePeriodType(),
//                        new CVType(),
//                        new AdministrationType()
//                );
//                administration.setDrug(createDrugChoiceType(MedicationType.values()[i]));
//                medicationPrescriptionType.getAdministration().add(administration);
//            }
//            medicationPrescriptionType.setDrug(createDrugChoiceType(medicationType));
//            final CVType endOfTreatmentReason = new CVType();
//            mockDataFactory.createCVType(endOfTreatmentReason);
//            endOfTreatmentReason.setOriginalText("EndOfTreatmentReason");
//            medicationPrescriptionType.setEndOfTreatmentReason(endOfTreatmentReason);
//            medicationPrescriptionType.setDispensationAuthorization(createDispensationAthorizationType());
//            return medicationPrescriptionType;
//        } catch (Exception e) {
//            fail("Error while creating MedicationPresciption");
//            return null;
//        }
//    }
//
//    private PrescriptionReasonType createPrescriptionReason() throws Exception {
//        final PrescriptionReasonType prescriptionReasonType = new ObjectFactory().createPrescriptionReasonType();
//        prescriptionReasonType.setReason((CVType) mockDataFactory.createCVType(new CVType()));
//
//        return prescriptionReasonType;
//    }
//
//    private DrugChoiceType createDrugChoiceType(final MedicationType medicationType) throws Exception {
//        final DrugChoiceType drugChoiceType = new ObjectFactory().createDrugChoiceType();
//        switch (medicationType) {
//            case DRUG_ARTICLE:
//                drugChoiceType.setDrugArticle(createDrugArticleType());
//                break;
//            case UNSTRUCTURED_DRUG_INFORMATION:
//                UnstructuredDrugInformationType udi = new UnstructuredDrugInformationType();
//                udi.setUnstructuredInformation("UnstructuredDrugInformation " + UUID.randomUUID().toString());
//                drugChoiceType.setUnstructuredDrugInformation(udi);
//                break;
//            case MERCHANDISE:
//                drugChoiceType.setMerchandise(createMerchandiseType());
//                break;
//            case DRUG:
//                drugChoiceType.setDrug(createDrugType());
//                break;
//            case GENERICS:
//                drugChoiceType.setGenerics(createGenericsType());
//                break;
//        }
//        drugChoiceType.setComment("Comment " + UUID.randomUUID().toString().substring(0, 6));
//
//        for (int i = 0; i < 10; i++) {
//            drugChoiceType.getDosage().add(createDosageType());
//        }
//        return drugChoiceType;
//    }
//
//    private DosageType createDosageType() {
//        try {
//            final DosageType dosageType = new DosageType();
//            dosageType.setShortNotation(UUID.randomUUID().toString().substring(0, 6));
//            CVType cvType = (CVType) mockDataFactory.createCVType(new CVType());
//            cvType.setCode("Unique Code: " + UUID.randomUUID().toString().substring(0, 6));
//            dosageType.setUnitDose(cvType);
//            dosageType.setDosageInstruction("Dosage instruction: " + UUID.randomUUID().toString().substring(0, 6));
//            return dosageType;
//        } catch (Exception e) {
//            fail("Error while creating dosageType");
//            return null;
//        }
//    }
//
//    private MerchandiseType createMerchandiseType() {
//        try {
//            final MerchandiseType merchandiseType = new MerchandiseType();
//            merchandiseType.setArticleNumber((CVType) mockDataFactory.createCVType(new CVType()));
//            return merchandiseType;
//        } catch (Exception e) {
//            fail("Error while creating merchendiseType");
//            return null;
//        }
//    }
//
//    private GenericsType createGenericsType() {
//        final GenericsType genericsType = new GenericsType();
//        genericsType.setForm("Generics Form");
//        genericsType.setSubstance("Generics Substance");
//        final PQType pqType = new PQType();
//        pqType.setUnit("mg");
//        pqType.setValue(500.5);
//        genericsType.setStrength(pqType);
//        return genericsType;
//    }
//
//
//    private DrugType createDrugType() throws Exception {
//        final DrugType drugType = new ObjectFactory().createDrugType();
//        drugType.setNplId(createDrugTypeNplId());
//        drugType.setAtcCode(createDrugArticleCVType());
//        drugType.setPharmaceuticalForm("Pharma form");
//        drugType.setStrength(22.3);
//        drugType.setStrengthUnit("mg");
//        drugType.setRouteOfAdministration((CVType) mockDataFactory.createCVType(new CVType()));
//
//        return drugType;
//    }
//
//    private DrugArticleType createDrugArticleType() throws Exception {
//        final DrugArticleType drugArticleType = new ObjectFactory().createDrugArticleType();
//        drugArticleType.setNplPackId(createDrugArticleTypeNplId());
//
//        return drugArticleType;
//    }
//
//    private AdditionalPatientInformationType createPatientInfo() {
//        final AdditionalPatientInformationType patientInfo = new ObjectFactory().createAdditionalPatientInformationType();
//        patientInfo.setDateOfBirth("19121212");
//        final CVType gender = new CVType();
//        gender.setCode("M");
//        patientInfo.setGender(gender);
//
//        return patientInfo;
//    }
//
//    private boolean CodingEquals(final Coding code1, final Coding code2) {
//        try {
//            if (!Objects.equals(code1.getCode(), code2.getCode())) return false;
//            if (!Objects.equals(code1.getSystem(), code2.getSystem())) return false;
//            if (!Objects.equals(code1.getDisplay(), code2.getDisplay())) return false;
//            if (!Objects.equals(code1.getVersion(), code2.getVersion())) return false;
//        } catch (Exception e) {
//            return false;
//        }
//        return true;
//    }
//
//    private DispensationAuthorizationType createDispensationAthorizationType() {
//        try {
//            final DispensationAuthorizationType dispensationAuthorizationType = new DispensationAuthorizationType();
//            dispensationAuthorizationType.setDispensationAuthorizationId((IIType) mockDataFactory.createIIType(new IIType()));
//            dispensationAuthorizationType.setDispensationAuthorizer((HealthcareProfessionalType) mockDataFactory.createHealthcareProfessionalType(
//                    new OrgUnitType(),
//                    new CVType(),
//                    new HealthcareProfessionalType()));
//            dispensationAuthorizationType.getDispensationAuthorizer().setAuthorTime(Helpers.getStringFromLocalDateTime(LocalDateTime.now()));
//            dispensationAuthorizationType.setDispensationAuthorizerComment("DispensationAuthorization comment. " + UUID.randomUUID().toString().substring(0, 6));
//            dispensationAuthorizationType.setDistributionMethod("distributionMethod, " + UUID.randomUUID().toString().substring(0, 6));
//            dispensationAuthorizationType.setDrug(createDrugChoiceType(MedicationType.DRUG));
//            dispensationAuthorizationType.setFirstDispensationBefore("first dispensation before"); //should be date string?
//
//            PQType pqType = new PQType();
//            pqType.setUnit("week");
//            pqType.setValue(3);
//            dispensationAuthorizationType.setMinimumDispensationInterval(pqType);
//            dispensationAuthorizationType.setNonReplaceable(NonReplaceableEnum.PATIENT);
//            dispensationAuthorizationType.setPackageUnit("5mg");
//            dispensationAuthorizationType.setPrescriptionSignatura("Signatura" + UUID.randomUUID().toString().substring(0, 6));
//            dispensationAuthorizationType.setReceivingPharmacy((IIType) mockDataFactory.createIIType(new IIType()));
//            dispensationAuthorizationType.setTotalAmount(30.0);
//            dispensationAuthorizationType.setValidUntil(mockDataFactory.dateShortStr);
//            return dispensationAuthorizationType;
//
//        } catch (Exception e) {
//            fail("Error while creating dispensationAuthorizationType");
//            return null;
//        }
//    }
//
//    private CVType createDrugTypeNplId() throws Exception {
//        final CVType cvt = (CVType) mockDataFactory.createCVType(new CVType());
//        cvt.setCodeSystem(CodeSystem.NPL_ID.getValueWithoutNamespace());
//        return cvt;
//    }
//
//    private CVType createDrugArticleTypeNplId() throws Exception {
//        final CVType cvt = (CVType) mockDataFactory.createCVType(new CVType());
//        cvt.setCodeSystem(CodeSystem.NPL_PACKID.getValueWithoutNamespace());
//        return cvt;
//    }
//
//    private CVType createDrugArticleCVType() {
//        final CVType cvt = new ObjectFactory().createCVType();
//        cvt.setCode("123");
//        cvt.setCodeSystem(CodeSystem.NPL_ID.getValueWithoutNamespace());
//        cvt.setCodeSystemName("Sys 1");
//        cvt.setCodeSystemVersion("1.0");
//        cvt.setDisplayName("One Two Three");
//        cvt.setOriginalText("Original text");
//        return cvt;
//    }
//
//    private PatientSummaryHeaderType createHeader() {
//        try {
//            return (PatientSummaryHeaderType) mockDataFactory.createPatientSummaryHeaderType(
//                    new HealthcareProfessionalType(),
//                    new OrgUnitType(),
//                    new CVType(),
//                    new LegalAuthenticatorType(),
//                    new PersonIdType(),
//                    new PatientSummaryHeaderType());
//        } catch (Exception e) {
//            fail("Error while creating header");
//            return null;
//        }
//    }
//
//    private void assertPatientMapping(final AdditionalPatientInformationType patientInfo, final Patient patient) {
//        assertShortDateString(patientInfo.getDateOfBirth(), patient.getBirthDate());
//        assertEquals(patient.getGender(), Enumerations.AdministrativeGender.MALE);
//    }
//
//    private void assertShortDateString(final String dateRecorded, final Date date) {
//        assertEquals(DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(dateRecorded).toDate(), date);
//    }
//
//    private void assertLongDateString(final String dateRecorded, final Date date) {
//        assertEquals(DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(dateRecorded).toDate(), date);
//    }
}
