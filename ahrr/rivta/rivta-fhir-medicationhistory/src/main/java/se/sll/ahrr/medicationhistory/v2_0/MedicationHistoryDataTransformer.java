package se.sll.ahrr.medicationhistory.v2_0;

/*-
 * #%L
 * rivta-fhir-medicationhistory
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.activityprescription.actoutcome._2.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.substance.Category;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;



@Service
public class MedicationHistoryDataTransformer implements Riv2FhirTransformer<List<MedicationMedicalRecordType>, MedicationStatement> {


    private static final Logger log = LoggerFactory.getLogger(MedicationHistoryDataTransformer.class);

    @Override
    public Class getFHIRResourceType()
    {
        return null;
    }

    @Override
    public List<MedicationStatement> transformToFhir(List<MedicationMedicalRecordType> rivResponse) throws Exception {
        List<MedicationStatement> medicationStatements = new LinkedList<>();

        rivResponse.forEach(medicationMedicalRecordType -> {

            PatientSummaryHeaderType header = medicationMedicalRecordType.getMedicationMedicalRecordHeader();
            MedicationMedicalRecordBodyType body = medicationMedicalRecordType.getMedicationMedicalRecordBody();

            // Patient
            Patient patient = Riv2FhirTransformationHelper.getPatientFromPersonIdTypeWithAdditionalPatientInformationType(header.getPatientId(), body.getAdditionalPatientInformation());

            // Encounter
            Encounter encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId());

            /* MedicationStatement */
            MedicationStatement medicationStatement = new MedicationStatement();

            /* Legal Authenticator */
            if (medicationMedicalRecordType.getMedicationMedicalRecordHeader().getLegalAuthenticator() != null)
            {
                medicationStatement.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(medicationMedicalRecordType.getMedicationMedicalRecordHeader().getLegalAuthenticator()));
            }

            // Id
            medicationStatement.setId(Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId()));

            // Identifier
            Identifier identifier = new Identifier();
            identifier.setSystem(header.getSourceSystemHSAId());
            identifier.setValue(header.getDocumentId());
            medicationStatement.addIdentifier(identifier);

            // basedOn, medicationRequest
            MedicationRequest medicationRequest = medicationRequest(
                    body.getMedicationPrescription(),
                    header,
                    encounter,
                    patient
            );
            medicationStatement.addBasedOn(new Reference(medicationRequest));

            // partOf, medicationAdministration
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getAdministration())
            {
                body.getMedicationPrescription().getAdministration().forEach(administrationType -> {
                    medicationAdministrations(administrationType, encounter, patient).forEach(medicationAdministration -> {
                        medicationStatement.addPartOf(new Reference(medicationAdministration));
                    });
                });
            }

            // partOf, medicationDispense
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getDispensationAuthorization())
            {
                medicationStatement.addPartOf(new Reference(medicationDispense(
                        body.getMedicationPrescription(),
                        medicationRequest,
                        encounter,
                        patient
                )));
            }

            // context
            medicationStatement.setContext(new Reference(encounter));

            // status
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getTypeOfPrescription())
            {
                // Insättning
                if ("I".equalsIgnoreCase(body.getMedicationPrescription().getTypeOfPrescription().value()))
                {
                    medicationStatement.setStatus(MedicationStatement.MedicationStatementStatus.ACTIVE);
                }
                // Utsättning
                else if ("U".equalsIgnoreCase(body.getMedicationPrescription().getTypeOfPrescription().value()))
                {
                    medicationStatement.setStatus(MedicationStatement.MedicationStatementStatus.STOPPED);
                    return;
                }
            }

            // medication[x]
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getDrug())
            {
                medicationStatement.setMedication(new Reference(medication(body.getMedicationPrescription().getDrug())));
            }

            // effective[x]
            Period period = new Period();
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getStartOfTreatment())
            {
                period.setStart(Riv2FhirTransformationHelper.getDateFromTimestampType(body.getMedicationPrescription().getStartOfTreatment()));
            }
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getEndOfTreatment())
            {
                period.setEnd(Riv2FhirTransformationHelper.getDateFromTimestampType(body.getMedicationPrescription().getEndOfTreatment()));
            }
            if (null != body.getMedicationPrescription() && (null != body.getMedicationPrescription().getStartOfTreatment() || null != body.getMedicationPrescription().getEndOfTreatment()))
            {
                medicationStatement.setEffective(period);
            }

            // subject
            medicationStatement.setSubject(new Reference(patient));

            // derivedFrom
            medicationStatement.addDerivedFrom(new Reference(medicationRequest));

            // taken
            if (null != body.getMedicationPrescription() && null != body.getMedicationPrescription().getAdministration() && !body.getMedicationPrescription().getAdministration().isEmpty())
            {
                medicationStatement.setTaken(MedicationStatement.MedicationStatementTaken.Y);
            }
            else
            {
                medicationStatement.setTaken(MedicationStatement.MedicationStatementTaken.UNK);
            }

            medicationStatements.add(medicationStatement);
        });

        return medicationStatements;
    }

    public MedicationRequest medicationRequest(MedicationPrescriptionType medicationPrescriptionType, PatientSummaryHeaderType header, Encounter encounter, Patient patient)
    {
        if (null == medicationPrescriptionType)
        {
            return null;
        }

        MedicationRequest medicationRequest = new MedicationRequest();

        // Identifier
        if (null != medicationPrescriptionType.getPrescriptionId())
        {
            medicationRequest.addIdentifier(
                    Riv2FhirTransformationHelper.getIdentifierFromIIType(medicationPrescriptionType.getPrescriptionId())
            );
        }

        // groupIdentifier
        if (null != medicationPrescriptionType.getPrescriptionChainId())
        {
            medicationRequest.setGroupIdentifier(
                    Riv2FhirTransformationHelper.getIdentifierFromIIType(medicationPrescriptionType.getPrescriptionChainId())
            );
        }

        // status
        // Insättning
        if (null != medicationPrescriptionType.getTypeOfPrescription())
        {
            if ("I".equalsIgnoreCase(medicationPrescriptionType.getTypeOfPrescription().value()))
            {
                medicationRequest.setStatus(MedicationRequest.MedicationRequestStatus.ACTIVE);
            }
            // Utsättning
            else if ("U".equalsIgnoreCase(medicationPrescriptionType.getTypeOfPrescription().value()))
            {
                medicationRequest.setStatus(MedicationRequest.MedicationRequestStatus.STOPPED);
            }
        }

        // TODO DOUBLE CHECK THIS
        // intent, if we have a dispense authorization then its an order
        if (null != medicationPrescriptionType.getDispensationAuthorization())
        {
            medicationRequest.setIntent(MedicationRequest.MedicationRequestIntent.ORDER);
        }
        // if we have administrations then its an order
        else if (null != medicationPrescriptionType.getAdministration() && !medicationPrescriptionType.getAdministration().isEmpty())
        {
            medicationRequest.setIntent(MedicationRequest.MedicationRequestIntent.ORDER);
        }
        // if we dont have a dispense authorization and no administrations then its planned
        else if (null == medicationPrescriptionType.getAdministration() || medicationPrescriptionType.getAdministration().isEmpty())
        {
            medicationRequest.setIntent(MedicationRequest.MedicationRequestIntent.PLAN);
        }

        // medication
        medicationRequest.setMedication(new Reference(medication(medicationPrescriptionType.getDrug())));

        // subject
        medicationRequest.setSubject(new Reference(patient));

        // context
        medicationRequest.setContext(new Reference(encounter));

        // authoredOn
        if (null != header && null != header.getDocumentTime())
        {
            medicationRequest.setAuthoredOnElement(Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()));
        }

        // requester
        if (null != medicationPrescriptionType.getPrescriber())
        {
            medicationRequest.setRecorderTarget(
                    Riv2FhirTransformationHelper.getPractitionerFromHealthcareProfessionalType(medicationPrescriptionType.getPrescriber())
            );
        }

        // reasonCode
        for (PrescriptionReasonType prescriptionReasonType : medicationPrescriptionType.getPrincipalPrescriptionReason())
        {
            if (null != prescriptionReasonType.getReason())
            {
                medicationRequest.addReasonCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(prescriptionReasonType.getReason()));
            }
            if (null != prescriptionReasonType.getOtherReason())
            {
                medicationRequest.addReasonCode().setText(prescriptionReasonType.getOtherReason());
            }
        }

        for (PrescriptionReasonType prescriptionReasonType : medicationPrescriptionType.getAdditionalPrescriptionReason())
        {
            if (null != prescriptionReasonType.getReason())
            {
                medicationRequest.addReasonCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(prescriptionReasonType.getReason()));
            }
            if (null != prescriptionReasonType.getOtherReason())
            {
                medicationRequest.addReasonCode().setText(prescriptionReasonType.getOtherReason());
            }
        }

        // note
        if (null != medicationPrescriptionType.getPrescriptionNote())
        {
            medicationRequest.addNote().setText(medicationPrescriptionType.getPrescriptionNote());
        }

        // dosageInstruction
        if (null != medicationPrescriptionType.getDrug())
        {
            for (DosageType dosageType : medicationPrescriptionType.getDrug().getDosage())
            {
                for (Dosage dosage : dosage(medicationPrescriptionType.getDrug(), dosageType))
                {
                    medicationRequest.addDosageInstruction(dosage);
                }
            }
        }

        // substitution
        if (null != medicationPrescriptionType.getDispensationAuthorization() && null != medicationPrescriptionType.getDispensationAuthorization().getNonReplaceable())
        {
            MedicationRequest.MedicationRequestSubstitutionComponent medicationRequestSubstitutionComponent = new MedicationRequest.MedicationRequestSubstitutionComponent();

            CodeableConcept reason = new CodeableConcept();
            reason.setText(medicationPrescriptionType.getDispensationAuthorization().getNonReplaceable().value());

            medicationRequestSubstitutionComponent.setAllowed(false);
            medicationRequestSubstitutionComponent.setReason(reason);

            medicationRequest.setSubstitution(medicationRequestSubstitutionComponent);
        }

        /**
         * EXTENSIONS
         */

        // Extension precedingPrescriptionId
        if (null != medicationPrescriptionType.getPrecedingPrescriptionId())
        {
            Identifier identifier = Riv2FhirTransformationHelper.getIdentifierFromIIType(medicationPrescriptionType.getPrecedingPrescriptionId());
            Extension precedingPrescriptionIdExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-PrecedingPrescriptionId", identifier);
            medicationRequest.addExtension(precedingPrescriptionIdExtension);
        }

        // Extension succeedingPrescriptionId
        if (null != medicationPrescriptionType.getSucceedingPrescriptionId())
        {
            Identifier identifier = Riv2FhirTransformationHelper.getIdentifierFromIIType(medicationPrescriptionType.getSucceedingPrescriptionId());
            Extension succeedingPrescriptionIdExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-SucceedingPrescriptionId", identifier);
            medicationRequest.addExtension(succeedingPrescriptionIdExtension);
        }

        // Extension startOfFirstTreatment
        if (null != medicationPrescriptionType.getStartOfFirstTreatment())
        {
            DateTimeType dateTimeType = new DateTimeType();
            dateTimeType.setValue(Riv2FhirTransformationHelper.getDateFromTimestampType(medicationPrescriptionType.getStartOfFirstTreatment()));

            Extension startOfFirstTreatmentExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-StartOfFirstTreatment", dateTimeType);
            medicationRequest.addExtension(startOfFirstTreatmentExtension);
        }

        // Extension endOfTreatmentReason
        if (null != medicationPrescriptionType.getEndOfTreatmentReason())
        {
            Extension startOfFirstTreatmentExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-EndOfTreatmentReason", Riv2FhirTransformationHelper.getCodeableConceptFromCvType(medicationPrescriptionType.getEndOfTreatmentReason()));
            medicationRequest.addExtension(startOfFirstTreatmentExtension);
        }

        // Extension treatment purpose
        if (null != medicationPrescriptionType.getTreatmentPurpose())
        {
            Extension treatmentPurposeExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-TreatmentPurpose", new StringType(medicationPrescriptionType.getTreatmentPurpose()));
            medicationRequest.addExtension(treatmentPurposeExtension);
        }

        // selfMedication
        Extension selfMedicationExtention = new Extension("http://fhir.sll.se/Extension/MedicationHistory-MedicationPrescription-SelfMedication", new BooleanType(medicationPrescriptionType.isSelfMedication()));
        medicationRequest.addExtension(selfMedicationExtention);

        return medicationRequest;
    }

    // Each dosageType can contain multiple dosages as SetDosageType, MaximumDosageType, ConditionalDosageType is
    // in themselves a dosage instruction. Atleast one of the SetDosageType or ConditionalDosageType is always set
    // but they can also be set in tandem for example during rampup.
    public List<Dosage> dosage(DrugChoiceType drugChoiceType, DosageType dosageType)
    {
        List<Dosage> dosages = new LinkedList<>();

        if (null == drugChoiceType || null == dosageType)
        {
            return dosages;
        }

        Dosage dosage = new Dosage();

        // additionalInstruction
        dosage.setText(dosageType.getShortNotation());

        // patientInstruction
        dosage.setPatientInstruction(dosageType.getDosageInstruction());

        // route
        if (null != drugChoiceType.getDrug() && null != drugChoiceType.getDrug().getRouteOfAdministration())
        {
            dosage.setRoute(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrug().getRouteOfAdministration()));
        }

        if (null != dosageType.getSetDosage())
        {
            Dosage setDosage = dosage.copy();

            // Extension setDosage
            Extension setDosageType = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType", new StringType("SetDosage"));
            setDosage.addExtension(setDosageType);

            setDosageTimingAndDose(
                    setDosage,
                    dosageType.getSetDosage().getFrequencyDosage(),
                    dosageType.getSetDosage().getOccasionDosage(),
                    dosageType.getSetDosage().getPeriodDosage(),
                    dosageType.getSetDosage().getRampedDosage(),
                    dosageType.getSetDosage().getSingleDose(),
                    dosageType.getSetDosage().getUnstructuredDosageInformation()
            );

            dosages.add(setDosage);
        }

        if (null != dosageType.getMaximumDosage())
        {
            Dosage maximumDosage = dosage.copy();

            // Extension maximumDosage
            Extension maximumDosageType = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType", new StringType("MaximumDosage"));
            maximumDosage.addExtension(maximumDosageType);

            setDosageTimingAndDose(
                    maximumDosage,
                    dosageType.getMaximumDosage().getFrequencyDosage(),
                    dosageType.getMaximumDosage().getOccasionDosage(),
                    dosageType.getMaximumDosage().getPeriodDosage(),
                    dosageType.getMaximumDosage().getRampedDosage(),
                    dosageType.getMaximumDosage().getSingleDose(),
                    dosageType.getMaximumDosage().getUnstructuredDosageInformation()
            );

            dosages.add(maximumDosage);
        }

        if (null != dosageType.getConditionalDosage())
        {
            Dosage conditionalDosage = dosage.copy();

            // Extension conditionalDosage
            Extension conditionalDosageType = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DosageType", new StringType("ConditionalDosage"));
            conditionalDosage.addExtension(conditionalDosageType);

            setDosageTimingAndDose(
                    conditionalDosage,
                    dosageType.getConditionalDosage().getFrequencyDosage(),
                    dosageType.getConditionalDosage().getOccasionDosage(),
                    dosageType.getConditionalDosage().getPeriodDosage(),
                    dosageType.getConditionalDosage().getRampedDosage(),
                    dosageType.getConditionalDosage().getSingleDose(),
                    dosageType.getConditionalDosage().getUnstructuredDosageInformation()
            );

            dosages.add(conditionalDosage);
        }


        return dosages;
    }

    public void setDosageTimingAndDose(
            Dosage dosage,
            FrequencyDosageType frequencyDosageType,
            OccasionDosageType occasionDosageType,
            PeriodDosageType periodDosageType,
            RampedDosageType rampedDosageType,
            SingleDoseType singleDoseType,
            UnstructuredDosageInformationType unstructuredDosageInformationType
    ) {
        Timing timing = new Timing();

        Range range = new Range();

        SimpleQuantity high = new SimpleQuantity();
        SimpleQuantity low = new SimpleQuantity();

        // frequency dosage
        if (null != frequencyDosageType)
        {
            // Extension setDosage
            Extension frequencyDosageExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit", new StringType(frequencyDosageType.getDose().getUnit()));
            dosage.addExtension(frequencyDosageExtension);

            high.setValue(frequencyDosageType.getDose().getHigh());
            range.setHigh(high);

            low.setValue(frequencyDosageType.getDose().getLow());
            range.setLow(low);

            dosage.setDose(range);

            Timing.TimingRepeatComponent timingRepeatComponent = new Timing.TimingRepeatComponent();
            timingRepeatComponent.setFrequency((int) frequencyDosageType.getFrequency().getValue()); // TODO Double check this conversion

            // Extension setDosage
            Extension frequencyUnit = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-Frequency-Unit", new StringType(frequencyDosageType.getFrequency().getUnit()));
            timingRepeatComponent.addExtension(frequencyUnit);

            timing.setRepeat(timingRepeatComponent);

            dosage.setTiming(timing);
        }

        // occasion dosage
        else if (null != occasionDosageType)
        {
            // TODO
            log.error("Unmapped occasion dosage");
        }

        // period dosage
        else if (null != periodDosageType)
        {
            // Extension setDosage
            Extension periodDosageExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit", new StringType(periodDosageType.getDose().getUnit()));
            dosage.addExtension(periodDosageExtension);

            high.setValue(periodDosageType.getDose().getHigh());
            range.setHigh(high);

            low.setValue(periodDosageType.getDose().getLow());
            range.setLow(low);

            dosage.setDose(range);

            Timing.TimingRepeatComponent timingRepeatComponent = new Timing.TimingRepeatComponent();
            timingRepeatComponent.setPeriod(periodDosageType.getPeriod().getValue());

            try
            {
                timingRepeatComponent.setPeriodUnit(Timing.UnitsOfTime.fromCode(periodDosageType.getPeriod().getUnit()));
            }
            catch (Exception e)
            {
                log.error("Could not map UCUM unit: ".concat(periodDosageType.getPeriod().getUnit()));
            }

            timing.setRepeat(timingRepeatComponent);
            dosage.setTiming(timing);
        }

        // ramped dosage
        else if (null != rampedDosageType)
        {
            // TODO
            log.error("Unmapped ramped dosage");
        }

        // single dose
        else if (null != singleDoseType)
        {
            // Extension setDosage
            Extension singleDoseExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-DoseUnit", new StringType(singleDoseType.getDose().getUnit()));
            dosage.addExtension(singleDoseExtension);

            high.setValue(singleDoseType.getDose().getHigh());
            range.setHigh(high);

            low.setValue(singleDoseType.getDose().getLow());
            range.setLow(low);

            dosage.setDose(range);

            // Timing
            timing.addEvent(Riv2FhirTransformationHelper.getDateFromTimestampType(singleDoseType.getTime()));
            dosage.setTiming(timing);
        }

        else if (null != unstructuredDosageInformationType && null != unstructuredDosageInformationType.getText())
        {
            // Extension setDosage
            Extension unstructuredDosageInstruction = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Dosage-UnstructuredDosageInformation", new StringType(unstructuredDosageInformationType.getText()));
            dosage.addExtension(unstructuredDosageInstruction);
        }

    }

    public Medication medication(DrugChoiceType drugChoiceType)
    {
        if (null == drugChoiceType)
        {
            return null;
        }

        Medication medication = new Medication();

        // code
        if (null != drugChoiceType.getDrug())
        {
            medication.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrug().getNplId(), CodeSystem.NPL_ID.getValue()));
        }
        else if (null != drugChoiceType.getMerchandise() && null != drugChoiceType.getMerchandise().getArticleNumber())
        {
            medication.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getMerchandise().getArticleNumber(), "urn:oid: 1.2.752.129.2.2.3.1.1"));
        }
        else if (null != drugChoiceType.getUnstructuredDrugInformation())
        {
            CodeableConcept code = new CodeableConcept();
            code.setText(drugChoiceType.getUnstructuredDrugInformation().getUnstructuredInformation());
            medication.setCode(code);
        }

        // form
        CodeableConcept form = new CodeableConcept();

        if (null != drugChoiceType.getDrug() && null != drugChoiceType.getDrug().getPharmaceuticalForm())
        {
            form.setText(drugChoiceType.getDrug().getPharmaceuticalForm());
        }

        if (null != drugChoiceType.getGenerics() && null != drugChoiceType.getGenerics().getForm())
        {
            form.setText(drugChoiceType.getGenerics().getForm());
        }

        if (null != form.getText())
        {
            medication.setForm(form);
        }

        // ingredient
        if (null != drugChoiceType.getDrug() && null != drugChoiceType.getDrug().getAtcCode())
        {
            Substance substance = new Substance();
            substance.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrug().getAtcCode(), CodeSystem.ATC.getValue()));
            substance.addCategory(Helpers.getCodeableConceptFromValueSet(Category.drug));
            medication.addIngredient().setItem(new Reference(substance));
        }

        if (null != drugChoiceType.getGenerics() && null != drugChoiceType.getGenerics().getSubstance())
        {
            Substance substance = new Substance();
            substance.setCode(new CodeableConcept().setText(drugChoiceType.getGenerics().getSubstance()));
            substance.addCategory(Helpers.getCodeableConceptFromValueSet(Category.drug));
            medication.addIngredient().setItem(new Reference(substance));
        }

        // package
        if (null != drugChoiceType.getDrugArticle() && null != drugChoiceType.getDrugArticle().getNplPackId())
        {
            medication.getPackage().setContainer(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(drugChoiceType.getDrugArticle().getNplPackId()));
        }

        // comment
        if (null != drugChoiceType.getComment())
        {
            Extension commentExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-DrugChoiceType-Comment", new StringType(drugChoiceType.getComment()));
            medication.addExtension(commentExtension);
        }

        // strength
        if (null != drugChoiceType.getGenerics() && null != drugChoiceType.getGenerics().getStrength())
        {
            Extension valueExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Value", new DecimalType(drugChoiceType.getGenerics().getStrength().getValue()));
            medication.addExtension(valueExtension);

            Extension unitExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Unit", new StringType(drugChoiceType.getGenerics().getStrength().getUnit()));
            medication.addExtension(unitExtension);
        }

        if (null != drugChoiceType.getDrug() && null != drugChoiceType.getDrug().getStrength())
        {
            Extension valueExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Value", new DecimalType(drugChoiceType.getDrug().getStrength()));
            medication.addExtension(valueExtension);
        }

        if (null != drugChoiceType.getDrug() && null != drugChoiceType.getDrug().getStrengthUnit())
        {
            Extension unitExtension = new Extension("http://fhir.sll.se/Extension/MedicationHistory-Drug-Strength-Unit", new StringType(drugChoiceType.getDrug().getStrengthUnit()));
            medication.addExtension(unitExtension);
        }


        return medication;
    }

    public List<MedicationAdministration> medicationAdministrations(AdministrationType administrationType, Encounter encounter, Patient patient)
    {

        List<MedicationAdministration> medicationAdministrations = new LinkedList<>();

        if (null == administrationType)
        {
            return medicationAdministrations;
        }

        MedicationAdministration medicationAdministration = new MedicationAdministration();

        // Identifier
        if (null != administrationType.getAdministrationId())
        {
            medicationAdministration.addIdentifier(
                    Riv2FhirTransformationHelper.getIdentifierFromIIType(administrationType.getAdministrationId())
            );
        }

        // Subject
        medicationAdministration.setSubject(new Reference(patient));

        // Context
        medicationAdministration.setContext(new Reference(encounter));

        // Performer
        if (null != administrationType.getAdministeringHealthcareProfessional())
        {
            medicationAdministration.addPerformer().setActor(new Reference(Riv2FhirTransformationHelper.getPractitionerFromHealthcareProfessionalType(
                    administrationType.getAdministeringHealthcareProfessional()
            )));
        }

        // Note
        if (null != administrationType.getAdministrationComment())
        {
            medicationAdministration.addNote().setText(administrationType.getAdministrationComment());
        }

        // Dosage route
        CodeableConcept route = null;
        if (null != administrationType.getRouteOfAdministration())
        {
            route = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(administrationType.getRouteOfAdministration());
        }

        // Dosage
        MedicationAdministration.MedicationAdministrationDosageComponent dosage = new MedicationAdministration.MedicationAdministrationDosageComponent();
        if (null != route)
        {
            dosage.setRoute(route);
            medicationAdministration.setDosage(dosage);
        }

        // Medication
        Medication medication = medication(administrationType.getDrug());
        medicationAdministration.setMedication(new Reference(medication));

        // Add our medication administration to the list of medication administrations
        medicationAdministrations.add(medicationAdministration);

        // As there can be many dosages per rivta administration and only one dosage
        // per fhir administration normalize this by creating an seperate fhir medical
        // administration foreach dosage that is partOf the main administration
        if (null != administrationType.getDrug())
        {
            for (DosageType dosageType : administrationType.getDrug().getDosage()) {
                MedicationAdministration dosageAdministration = new MedicationAdministration();

                // Add the main medication administration
                dosageAdministration.addPartOf(new Reference(medicationAdministration));

                // Subject
                dosageAdministration.setSubject(new Reference(patient));

                // Context
                dosageAdministration.setContext(new Reference(encounter));

                // Medication
                dosageAdministration.setMedication(new Reference(medication));

                for (Dosage dosageInstruction : dosage(administrationType.getDrug(), dosageType))
                {
                    Extension dInstruction = new Extension(
                            "http://fhir.sll.se/Extension/DosageInstruction",
                            dosageInstruction
                    );
                    dosageAdministration.addExtension(dInstruction);
                }

                medicationAdministrations.add(dosageAdministration);
            }
        }

        return medicationAdministrations;
    }

    public MedicationDispense medicationDispense(MedicationPrescriptionType medicationPrescriptionType, MedicationRequest medicationRequest, Encounter encounter, Patient patient)
    {
        if (null == medicationPrescriptionType.getDispensationAuthorization())
        {
            return null;
        }

        MedicationDispense medicationDispense = new MedicationDispense();

        DispensationAuthorizationType dispensationAuthorizationType = medicationPrescriptionType.getDispensationAuthorization();

        // Identifier
        if (null != dispensationAuthorizationType.getDispensationAuthorizationId())
        {
            medicationDispense.addIdentifier(
                    Riv2FhirTransformationHelper.getIdentifierFromIIType(dispensationAuthorizationType.getDispensationAuthorizationId())
            );
        }

        // category
        if (null != dispensationAuthorizationType.getDistributionMethod())
        {
            CodeableConcept codeableConcept = new CodeableConcept();
            codeableConcept.setText(dispensationAuthorizationType.getDistributionMethod());
            medicationDispense.setCategory(codeableConcept);
        }

        // medication
        medicationDispense.setMedication(new Reference(medication(dispensationAuthorizationType.getDrug())));

        // subject
        medicationDispense.setSubject(new Reference(patient));

        // context
        medicationDispense.setContext(new Reference(encounter));

        // performer
        medicationDispense.addPerformer().setActor(Riv2FhirTransformationHelper.getPractitionerReferenceFromHealthcareProfessionalType(dispensationAuthorizationType.getDispensationAuthorizer()));

        // authorizingPrescription
        medicationDispense.addAuthorizingPrescription(new Reference(medicationRequest));

        // quantity, packageUnit and totalAmount is always set together.
        if (null != dispensationAuthorizationType.getPackageUnit() && null != dispensationAuthorizationType.getTotalAmount())
        {
            SimpleQuantity quantity = new SimpleQuantity();
            quantity.setValue(dispensationAuthorizationType.getTotalAmount());
            quantity.setUnit(dispensationAuthorizationType.getPackageUnit());
            medicationDispense.setQuantity(quantity);
        }

        // note
        if (null != dispensationAuthorizationType.getDispensationAuthorizerComment())
        {
            medicationDispense.addNote().setText(
                    dispensationAuthorizationType.getDispensationAuthorizerComment()
            );
        }

        // validUntil
        if (null != dispensationAuthorizationType.getValidUntil())
        {
            Extension validUntilExtension = new Extension(
                    "http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-ValidUntil",
                    Riv2FhirTransformationHelper.getDateForDate(dispensationAuthorizationType.getValidUntil())
            );
            medicationDispense.addExtension(validUntilExtension);
        }

        // receivingPharmacy
        if (null != dispensationAuthorizationType.getReceivingPharmacy())
        {
            Extension receivingPharmacyExtension = new Extension(
                    "http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-ReceivingPharmacy",
                    Riv2FhirTransformationHelper.getIdentifierFromIIType(dispensationAuthorizationType.getReceivingPharmacy())
            );
            medicationDispense.addExtension(receivingPharmacyExtension);
        }

        // minimumDispensationInterval
        if (null != dispensationAuthorizationType.getMinimumDispensationInterval())
        {
            SimpleQuantity minimumDispensationInterval = new SimpleQuantity();
            minimumDispensationInterval.setUnit(dispensationAuthorizationType.getMinimumDispensationInterval().getUnit());
            minimumDispensationInterval.setValue(dispensationAuthorizationType.getMinimumDispensationInterval().getValue());

            Extension minimumDispensation = new Extension(
                    "http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-MinimumDispensationInterval",
                    minimumDispensationInterval
            );

            medicationDispense.addExtension(minimumDispensation);
        }

        // firstDispensationBefore
        if (null != dispensationAuthorizationType.getFirstDispensationBefore())
        {
            Extension firstDispensationBeforeExtension = new Extension(
                    "http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-FirstDispensationBefore",
                    Riv2FhirTransformationHelper.getDateForDate(dispensationAuthorizationType.getFirstDispensationBefore())
            );
            medicationDispense.addExtension(firstDispensationBeforeExtension);
        }

        // prescriptionSignatura
        if (null != dispensationAuthorizationType.getPrescriptionSignatura())
        {
            Extension prescriptionSignaturaExtension = new Extension(
                    "http://fhir.sll.se/Extension/MedicationHistory-DispensationAuthorizationType-PrescriptionSignatura",
                    new StringType(dispensationAuthorizationType.getPrescriptionSignatura())
            );
            medicationDispense.addExtension(prescriptionSignaturaExtension);
        }

        return medicationDispense;
    }

}
