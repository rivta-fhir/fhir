package se.sll.ahrr.medicationhistory.v2_0.dataloader;

/*-
 * #%L
 * rivta-fhir-medicationhistory
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.ext.logging.LoggingOutInterceptor;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.activityprescription.actoutcome._2.DatePeriodType;
import riv.clinicalprocess.activityprescription.actoutcome._2.PersonIdType;
import riv.clinicalprocess.activityprescription.actoutcome.getmedicationhistory._2.rivtabp21.GetMedicationHistoryResponderInterface;
import riv.clinicalprocess.activityprescription.actoutcome.getmedicationhistory._2.rivtabp21.GetMedicationHistoryResponderService;
import riv.clinicalprocess.activityprescription.actoutcome.getmedicationhistoryresponder._2.GetMedicationHistoryResponseType;
import riv.clinicalprocess.activityprescription.actoutcome.getmedicationhistoryresponder._2.GetMedicationHistoryType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.*;

@Service
@CacheConfig(cacheNames = {"rivta.medicationhistory"})
public class MedicationHistoryDataLoader implements RivtaDataLoader<GetMedicationHistoryResponseType> {
    private final List<GetMedicationHistoryResponderInterface> ports;

    @Value("${contracts.getMedicationHistory.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public MedicationHistoryDataLoader(
        @Value("#{'${contracts.getMedicationHistory.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout,
        @Value("${ahrr.serviceConsumer:''}") String serviceConsumer,
        @Value("${ahrr.client.log.payloads:false}") Boolean logPayload
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetMedicationHistoryResponderService service = new GetMedicationHistoryResponderService();
            GetMedicationHistoryResponderInterface port = service.getGetMedicationHistoryResponderPort();

            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            if(logPayload) {
                client.getInInterceptors().add(new LoggingInInterceptor());
                client.getOutInterceptors().add(new LoggingOutInterceptor());
            }

            Map<String, List<String>> headers = new HashMap<String, List<String>>();
            headers.put("x-rivta-original-serviceconsumer-hsaid", Arrays.asList(serviceConsumer));
            client.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public GetMedicationHistoryResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetMedicationHistoryResponseType getMedicationHistoryResponseType = new GetMedicationHistoryResponseType();

            final GetMedicationHistoryType params = new GetMedicationHistoryType();
            final PersonIdType personId = new PersonIdType();
            personId.setId(clientId);
            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personId);

            /* Load the last year */
            DatePeriodType datePeriod = new DatePeriodType();
            datePeriod.setStart(DateTimeFormat.forPattern("YYYYMMdd").print(DateTime.now().minusYears(1)));
            datePeriod.setEnd(DateTimeFormat.forPattern("YYYYMMdd").print(DateTime.now()));
            params.setDatePeriod(datePeriod);

            for (GetMedicationHistoryResponderInterface port : ports)
            {
                GetMedicationHistoryResponseType response = port.getMedicationHistory(logicalAddress, params);
                getMedicationHistoryResponseType.getMedicationMedicalRecord().addAll(response.getMedicationMedicalRecord());
            }

            return getMedicationHistoryResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }



    @Override
    public Class<GetMedicationHistoryResponseType> getRivtaResponseType() {
        return GetMedicationHistoryResponseType.class;
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {
    }

}
