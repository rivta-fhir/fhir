package se.sll.ahrr.carecontact.v2_0;

/*-
 * #%L
 * rivta-fhir-carecontacts
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.BeforeClass;
import org.junit.Test;
import riv.clinicalprocess.logistics.logistics._2.*;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class TransformerTest {

    private final static Map<Integer, Encounter.EncounterStatus> rivStatusCodes = new HashMap<>();
    private final MockFactory mockFactory = new MockFactory();

    @BeforeClass
    public static void init() {
        rivStatusCodes.put(1, Encounter.EncounterStatus.PLANNED);
        rivStatusCodes.put(2, Encounter.EncounterStatus.CANCELLED);
        rivStatusCodes.put(3, Encounter.EncounterStatus.INPROGRESS);
        rivStatusCodes.put(4, Encounter.EncounterStatus.CANCELLED);
        rivStatusCodes.put(5, Encounter.EncounterStatus.FINISHED);
    }

    @Test
    public void verifyRiv2Fhir() throws Exception {

    }

    @Test
    public void verifyEncounter() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final CareContactBodyType body = createBody();
        final Encounter encounter = new CareContactDataTransformer().mapEncounter(header, body);

        //Verify header
        assertEquals(header.getSourceSystemHSAId(), encounter.getIdentifier().get(0).getSystem());
        assertEquals(Riv2FhirTransformationHelper.getId(
            header.getSourceSystemHSAId(),
            header.getDocumentId()
        ), encounter.getId());
        //assert patient mapping.
        Assertions.assertIdentifier(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId(), ((Patient) encounter.getSubject().getResource()).getIdentifierFirstRep());
        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), (Practitioner) encounter.getParticipantFirstRep().getIndividual().getResource());
        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) encounter.getServiceProvider().getResource());
        assertEquals(1, ((Practitioner) encounter.getParticipant().get(0).getIndividual().getResource()).getContained().size()); //PractitionerRole
        //Todo, temp for stu3.
        final Practitioner practitioner = (Practitioner) encounter.getParticipant().get(0).getIndividual().getResource();
        final PractitionerRole practitionerRole = (PractitionerRole) practitioner.getContained().get(0);
        Assertions.assertOrgUnitMapping(header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit(), (Organization) practitionerRole.getOrganization().getResource());

        //Verify body
        //TODO encounterClassEnum temp disabled for stu3, hapi missing constant values?
        //assertEquals(EncounterClassEnum.VIRTUAL, encounter.getClassElementElement().getValueAsEnum());
        assertEquals(body.getCareContactReason(), encounter.getReasonFirstRep().getText());
        assertTimePeriod(body.getCareContactTimePeriod(), encounter.getPeriod());
        assertEquals(Encounter.EncounterStatus.INPROGRESS, encounter.getStatusElement().getValue());
        Assertions.assertOrgUnitMapping(body.getCareContactOrgUnit(), (Organization) encounter.getServiceProvider().getResource());
    }

    @Test
    public void verifyEncounterStatusCodes() throws Exception {
        for (Integer key : rivStatusCodes.keySet()) {
            final CareContactBodyType body = createBody();
            body.setCareContactStatus(key);
            final Encounter encounter = new CareContactDataTransformer().mapEncounter(createHeader(), body);
            assertEquals(rivStatusCodes.get(key), encounter.getStatusElement().getValue());
        }
    }

    private PatientSummaryHeaderType createHeader() throws Exception {
        try {
            return (PatientSummaryHeaderType) mockFactory.createPatientSummaryHeaderType(
                new HealthcareProfessionalType(),
                new OrgUnitType(),
                new CVType(),
                new LegalAuthenticatorType(),
                new PersonIdType(),
                new PatientSummaryHeaderType());

        } catch (Exception e) {
            fail("Couldn't create header");
        }
        return null;
    }

    private CareContactBodyType createBody() throws Exception {
        final CareContactBodyType body = new ObjectFactory().createCareContactBodyType();
        body.setCareContactCode(2);
        body.setCareContactTimePeriod(createTimeperiod());
        body.setCareContactStatus(3);
        body.setCareContactOrgUnit(createOrgUnit());

        return body;
    }

    private TimePeriodType createTimeperiod() {
        try {
            return (TimePeriodType) mockFactory.createTimePeriodType(new TimePeriodType());
        } catch (Exception e) {
            fail("Error while creating timePeriod");
        }
        return null;
    }

    private OrgUnitType createOrgUnit() {
        try {
            return (OrgUnitType) mockFactory.createOrgUnit(new OrgUnitType());
        } catch (Exception e) {
            fail("Couldn't create OrgUnitType");
        }
        return null;
    }

    private void assertTimePeriod(final TimePeriodType timePeriodType, final Period period) {
        final DateTime fhirDateStart = new DateTime(DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(timePeriodType.getStart()).toDate());
        final DateTime fhirDateEnd = new DateTime(DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(timePeriodType.getEnd()).toDate());
        assertEquals(fhirDateStart.toDate(), period.getStart());
        assertEquals(fhirDateEnd.toDate(), period.getEnd());
    }
}
