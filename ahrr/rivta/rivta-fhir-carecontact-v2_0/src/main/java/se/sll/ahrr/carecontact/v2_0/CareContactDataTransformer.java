package se.sll.ahrr.carecontact.v2_0;

/*-
 * #%L
 * rivta-fhir-carecontacts
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._2.CareContactBodyType;
import riv.clinicalprocess.logistics.logistics._2.CareContactType;
import riv.clinicalprocess.logistics.logistics._2.PatientSummaryHeaderType;
import riv.clinicalprocess.logistics.logistics._2.TimePeriodType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Transformer used to transformToFhir data from RIV-TA to FHIR
 *
 * @david
 */

//TODO Encounter.class mapping disabled for stu3, cant figure out how to set it in hapi atm.
@Service("careContactDataTransformerV2_0")
public class CareContactDataTransformer implements Riv2FhirTransformer<List<CareContactType>, Encounter> {

    private static final Logger log = LoggerFactory.getLogger(CareContactDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Encounter.class;
    }

    @Override
    public List<Encounter> transformToFhir(List<CareContactType> rivResponse) throws Exception {
        final List<Encounter> encounters = new LinkedList<>();

        for (CareContactType careContactType : rivResponse) {
            if (careContactType.getCareContactHeader() != null && careContactType.getCareContactBody() != null) {
                final Encounter encounter = mapEncounter(careContactType.getCareContactHeader(), careContactType.getCareContactBody());
                encounters.add(encounter);
            }
        }
        return encounters;
    }

    public Encounter mapEncounter(final PatientSummaryHeaderType header, final CareContactBodyType body) {
        final Encounter encounter = new Encounter();

        mapContactHeader(encounter, header);
        mapContactBody(encounter, body);

        return encounter;
    }

    private void mapContactHeader(final Encounter encounter, final PatientSummaryHeaderType header) {
        final IdType id = new IdType();
        id.setValue(
                Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId())
        );

        encounter.setId(id);
        Identifier identifier = new Identifier();
        identifier.setSystem(header.getSourceSystemHSAId());
        identifier.setValue(header.getDocumentId());
        encounter.addIdentifier(identifier);


        if (header.getPatientId() != null && header.getPatientId().getId() != null) {
            encounter.setSubject(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId()));
        } else {
            log.trace("Skip mapping patient since RIV patientId was null");
        }


        if (header.getAccountableHealthcareProfessional() != null) {
            encounter.getParticipantFirstRep().setIndividual(
                    Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional())
            );
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
        } else {
            log.trace("Skip mapping practitioner since RIV accountableHealthcareProfessional was null");
        }

    }

    private void mapContactBody(final Encounter encounter, final CareContactBodyType body) {
        if (body.getCareContactCode() != null && body.getCareContactCode() == 2) {
            //TODO, hapi 3.0.0 missing constants?
            //encounter.setClassElement(EncounterClassEnum.VIRTUAL);
        } else {
            log.trace("Skip mapping class since RIV careContactCode was null or != 2");
        }
        encounter.getReasonFirstRep().setText(body.getCareContactReason());

        TimePeriodType careContactTimePeriod = body.getCareContactTimePeriod();
        if (careContactTimePeriod != null) {
            if (careContactTimePeriod.getStart() != null) {
                encounter.getPeriod().setStart(mapDateTime(careContactTimePeriod.getStart()));
            }
            if (careContactTimePeriod.getEnd() != null) {
                encounter.getPeriod().setEnd(mapDateTime(careContactTimePeriod.getEnd()));
            }
        } else {
            log.trace("Skip mapping period since RIV careContactPeriod was null");
        }

        if (body.getCareContactStatus() != null && (body.getCareContactStatus() >= 1 && body.getCareContactStatus() <= 5)) {
            encounter.setStatus(mapEncounterStatus(body.getCareContactStatus()));
        } else {
            encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);
            log.debug("Setting encounter.status to unknown since RIV careContactStatus was null or out of bounds");
        }

        if (body.getCareContactOrgUnit() != null) {

            Organization careContactOrg = (Organization) Riv2FhirTransformationHelper.getOrganizationReference(
                    body.getCareContactOrgUnit().getOrgUnitName(),
                    body.getCareContactOrgUnit().getOrgUnitHSAId(),
                    body.getCareContactOrgUnit().getOrgUnitAddress(),
                    body.getCareContactOrgUnit().getOrgUnitLocation(),
                    body.getCareContactOrgUnit().getOrgUnitTelecom(),
                    body.getCareContactOrgUnit().getOrgUnitEmail()
            ).getResource();
            if (careContactOrg != null) {
                if (encounter.getServiceProvider() != null && encounter.getServiceProvider().getResource() != null) {
                    careContactOrg.setPartOf(encounter.getServiceProvider());
                }
            }
            encounter.setServiceProvider(new Reference(careContactOrg));
        } else {
            log.trace("Skip mapping serviceProvider since RIV careContactOrgUnit was null");
        }
    }

    private Encounter.EncounterStatus mapEncounterStatus(final Integer contactStatus) {
        switch (contactStatus) {
            case 1:
                return Encounter.EncounterStatus.PLANNED;
            case 2:
                return Encounter.EncounterStatus.CANCELLED;
            case 3:
                return Encounter.EncounterStatus.INPROGRESS;
            case 4:
                return Encounter.EncounterStatus.CANCELLED;
            case 5:
                return Encounter.EncounterStatus.FINISHED;
            default:
                log.debug("contactStatus was out of bounds, returning encounterStatus.Unknown.");
                return Encounter.EncounterStatus.UNKNOWN;
        }
    }

    private java.util.Date mapDateTime(final String rivtaDate) {
        return DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(rivtaDate).toDate();
    }
}
