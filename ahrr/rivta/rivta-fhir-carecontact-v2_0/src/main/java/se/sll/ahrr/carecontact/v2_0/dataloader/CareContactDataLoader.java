package se.sll.ahrr.carecontacts.v2_0.dataloader;

/*-
 * #%L
 * rivta-fhir-carecontacts
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._2.PersonIdType;
import riv.clinicalprocess.logistics.logistics.getcarecontacts._2.rivtabp21.GetCareContactsResponderInterface;
import riv.clinicalprocess.logistics.logistics.getcarecontacts._2.rivtabp21.GetCareContactsResponderService;
import riv.clinicalprocess.logistics.logistics.getcarecontactsresponder._2.GetCareContactsResponseType;
import riv.clinicalprocess.logistics.logistics.getcarecontactsresponder._2.GetCareContactsType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.LinkedList;
import java.util.List;

@Service("careContactDataLoaderV2_0")
@CacheConfig(cacheNames = {"rivta.carecontacts"})
public class CareContactDataLoader implements RivtaDataLoader<GetCareContactsResponseType>
{
    private List<GetCareContactsResponderInterface> ports;

    @Value("${contracts.getCareContact.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public CareContactDataLoader(
        @Value("#{'${contracts.getCareContact.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetCareContactsResponderService service = new GetCareContactsResponderService();
            GetCareContactsResponderInterface port = service.getGetCareContactsResponderPort();

            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public GetCareContactsResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetCareContactsResponseType getCareContactsResponseType = new GetCareContactsResponseType();

            GetCareContactsType params = new GetCareContactsType();
            PersonIdType personId = new PersonIdType();
            personId.setId(clientId);
            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personId);

            for (GetCareContactsResponderInterface port : ports )
            {
                GetCareContactsResponseType response = port.getCareContacts(logicalAddress, params);
                getCareContactsResponseType.getCareContact().addAll(response.getCareContact());
            }

            return getCareContactsResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {

    }

    @Override
    public Class<GetCareContactsResponseType> getRivtaResponseType() {
        return GetCareContactsResponseType.class;
    }
}
