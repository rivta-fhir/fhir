package se.sll.ahrr.lookupresident.dataloader;

/*-
 * #%L
 * rivta-fhir-lookupresident
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.population.residentmaster._1.ResidentType;
import riv.population.residentmaster.lookupresidentforfullprofile._1.rivtabp21.LookupResidentForFullProfileResponderInterface;
import riv.population.residentmaster.lookupresidentforfullprofile._1.rivtabp21.LookupResidentForFullProfileResponderService;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileResponseType;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileType;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
@Qualifier("lookupResidentForFullProfileDataLoader")
@CacheConfig(cacheNames = {"rivta.lookupresident"})
public class LookupResidentForFullProfileDataLoader implements RivtaDataLoader<LookupResidentForFullProfileResponseType> {

    private List<LookupResidentForFullProfileResponderInterface> ports;

    @Value("${contracts.lookupResidentForFullProfile.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public LookupResidentForFullProfileDataLoader(
        @Value("#{'${contracts.lookupResidentForFullProfile.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            LookupResidentForFullProfileResponderService service = new LookupResidentForFullProfileResponderService();
            LookupResidentForFullProfileResponderInterface port = service.getLookupResidentForFullProfileResponderPort();
            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public LookupResidentForFullProfileResponseType loadRivtaResponse(String clientId) throws BadGatewayException {
        try
        {
            LookupResidentForFullProfileResponseType lookupResidentForFullProfileResponseType = new LookupResidentForFullProfileResponseType();

            final LookupResidentForFullProfileType params = new LookupResidentForFullProfileType();
            params.getPersonId().add(clientId);

            List<ResidentType> residents = new ArrayList<>();
            for (LookupResidentForFullProfileResponderInterface port : ports)
            {
                LookupResidentForFullProfileResponseType response = port.lookupResidentForFullProfile(logicalAddress, params);
                residents.addAll(response.getResident());
            }

            residents.sort((r1, r2) -> {
                if (r1.getSenasteAndringFolkbokforing() == null) {
                    if (r2.getSenasteAndringFolkbokforing() == null) {
                        return 0;
                    }
                    return 1;
                }
                return r2.getSenasteAndringFolkbokforing().compareTo(r1.getSenasteAndringFolkbokforing());
            });

            if (residents.size() > 0)
            {
                lookupResidentForFullProfileResponseType.getResident().add(residents.get(0));
            }

            return lookupResidentForFullProfileResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }


    @Override
    public Class<LookupResidentForFullProfileResponseType> getRivtaResponseType() {
        return LookupResidentForFullProfileResponseType.class;
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {
    }

}
