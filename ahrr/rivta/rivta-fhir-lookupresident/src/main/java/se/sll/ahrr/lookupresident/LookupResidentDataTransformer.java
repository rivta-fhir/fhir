package se.sll.ahrr.lookupresident;

/*-
 * #%L
 * rivta-fhir-lookupresident
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.dstu3.model.Address.AddressUse;
import org.hl7.fhir.dstu3.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.population.residentmaster._1.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.MaritalStatus;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Service
public class LookupResidentDataTransformer implements Riv2FhirTransformer<List<ResidentType>, Patient> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Patient.class;
    }

    @Override
    public List<Patient> transformToFhir(final List<ResidentType> rivResponse) throws Exception {
        final List<Patient> patients = new LinkedList<>();
        for (ResidentType resident : rivResponse) {

            final Patient patient = mapPatient(resident);
            if (patient != null) {
                patients.add(patient);
            }
        }
        return patients;
    }

    private Patient mapPatient(final ResidentType resident) {
        try
        {
            //resident.getSekretessmarkering(), how should we handle this?
            if (resident.getSekretessmarkering() != null && resident.getSekretessmarkering() == JaNejTYPE.J)
            {
                return null;
            }

            //resident.getSenasteAndringFolkbokforing();
            return mapPersonPost(resident.getPersonpost());
        }

        catch (Exception e)
        {
            log.error("Unexpected erro in mapPatient", e);
            return null;
        }
    }

    private Patient mapPersonPost(final PersonpostTYPE personpost)
    {
        Patient patient = new Patient();

        //personpost.getAvregistrering()
        //personpost.getDistriktskod();
        if (personpost.getPersonId() == null)
        {
            log.debug("PersonId is missing, skipping");
            return null;
        }
        patient.addIdentifier(getIDentifier(personpost.getPersonId(), Identifier.IdentifierUse.OFFICIAL));
        patient.setId(new IdType().setValue(personpost.getPersonId()));

        if (personpost.getHanvisningsPersonNr() != null) {
            /*- hänvisningspersonnummer
            För de personer som bytt personnummer anges tidigare
            personnummer/samordningsnummer. */

            patient.addIdentifier(getIDentifier(personpost.getHanvisningsPersonNr(), Identifier.IdentifierUse.SECONDARY));
        }

        if (personpost.getCivilstand() != null) {
            patient.setMaritalStatus(getMaritalStatus(personpost.getCivilstand()));
        } else {
            log.debug("Skip mapping of MaritalStatus since Civilstand was null");
        }

        //personpost.getFodelse();

        if (personpost.getFodelsetid() != null) {
            try {
                patient.setBirthDate(Riv2FhirTransformationHelper.getDateTimeForDate(personpost.getFodelsetid()).getValue());
            } catch (Exception e) {
                log.error("Error while parsing date string for Fodelsetid", e);
            }
        }


        if (personpost.getFolkbokforingsadress() != null) {
            patient.addAddress(getAddress(personpost.getFolkbokforingsadress(), AddressUse.HOME)); //home?
        }
        if (personpost.getSarskildPostadress() != null) {
            patient.addAddress(getAddress(personpost.getSarskildPostadress(), AddressUse.TEMP)); //temp?
        }

        //personpost.getKon();
        if (personpost.getKon() != null) {
            patient.setGender(getAdministrativeGender(personpost.getKon()));
        }

        if (personpost.getUtlandsadress() != null) {
            patient.addAddress(getAddress(personpost.getUtlandsadress(), AddressUse.NULL)); //AdressUse?
        }


        //personpost.getInvandring();


        if (personpost.getNamn() != null) {
            getName(personpost.getNamn()).stream().forEach(name -> patient.addName(name));
        } else {
            log.debug("Skipping name since personpost.namn was null");
        }
        //personpost.getRelationer();

        return patient;
    }

    private Address getAddress(final UtlandsadressTYPE utlandsadress, final AddressUse addressUse) {
        final Address address = getAddress(utlandsadress.getUtdelningsadress1(),
                utlandsadress.getUtdelningsadress2(),
                utlandsadress.getUtdelningsadress3(),
                "",
                "",
                "",
                utlandsadress.getLand(),
                addressUse);


        //utlandsadress.getRostrattsdatum();
        //utlandsadress.getUtlandsadressdatum()
        return address;
    }

    private Address getAddress(final SvenskAdressTYPE svenskAdress, final AddressUse addressUse) {
        return getAddress(svenskAdress.getUtdelningsadress1(),
                svenskAdress.getUtdelningsadress2(),
                "",
                svenskAdress.getCareOf(), svenskAdress.getPostort(),
                svenskAdress.getPostNr(),
                "SWE",  //SE?
                addressUse);

        //svenskAdress.getFastighetsbeteckning();
        //svenskAdress.getFolkbokforingsdatum ();
        //svenskAdress.getKommunKod();
        //svenskAdress.getLanKod();
        //svenskAdress.getSCBNyckelkod(); ?
        //svenskAdress.getForsamlingKod(); deprecated iirc
    }


    private Address getAddress(final String utdelningsAdr1, final String utdelningsAdr2, final String utdelningsAdr3, final String careOf, final String postort, final String postNr, final String countryCode, final AddressUse addressUse) {
        try {
            final Address address = new Address();

            address.setUse(addressUse);
            address.setCity(postort);
            address.setPostalCode(postNr);
            address.setCountry(countryCode);

            if (careOf != null && !careOf.isEmpty()) {
                address.addLine(careOf);
            }
            if (utdelningsAdr1 != null && !utdelningsAdr1.isEmpty()) {
                address.addLine(utdelningsAdr1);
            }
            if (utdelningsAdr2 != null && !utdelningsAdr2.isEmpty()) {
                address.addLine(utdelningsAdr2);
            }
            if (utdelningsAdr3 != null && !utdelningsAdr3.isEmpty()) {
                address.addLine(utdelningsAdr3);
            }

            if (postNr != null && !postNr.isEmpty()) {
                address.addLine(postNr);
            }
            if (postort != null && !postort.isEmpty()) {
                address.addLine(postort);
            }


            return address;
        } catch (Exception e) {
            log.error("Unexpected error in getAddress()", e);
            return new Address();
        }
    }


    private Identifier getIDentifier(final String personNr, final Identifier.IdentifierUse identifierUse) {
        Identifier identifier = new Identifier();
        identifier.setSystem(CodeSystem.PERSONID_SE.getValue());
        identifier.setValue(personNr);
        identifier.setUse(identifierUse);
        return identifier;
    }

    private List<HumanName> getName(final NamnTYPE name) {

        List<HumanName> names = new ArrayList<>();
        try {
            final HumanName officialName = new HumanName();
            officialName.setUse(HumanName.NameUse.OFFICIAL);
            if (name.getFornamn() != null) {
                Arrays.stream(name.getFornamn().split(" "))
                        .filter(nameStr -> !nameStr.isEmpty())
                        .forEach(officialName::addGiven);
            }
            if (name.getMellannamn() != null) {
                Arrays.stream(name.getMellannamn().split(" "))
                        .filter(nameStr -> !nameStr.isEmpty())
                        .forEach(officialName::addGiven);
            }
            if (name.getEfternamn() != null) {
                officialName.setFamily(name.getEfternamn());
            }
            names.add(officialName);

            if (name.getTilltalsnamnsmarkering() != null) {
                final HumanName usualName = new HumanName();
                usualName.setUse(HumanName.NameUse.USUAL);
                getTilltalsNamn(name.getFornamn(), name.getTilltalsnamnsmarkering()).forEach(usualName::addGiven);
                usualName.setFamily(name.getEfternamn());
                usualName.setText(String.format("%s %s", usualName.getGivenAsSingleString(), name.getEfternamn()));
                names.add(usualName);
            }
            //name.getAviseringsnamn();
        } catch (MalformedInputException e) {
            log.error("Mapping tilltalsnanm failed", e);
        } catch (Exception e) {
            log.error("Unexpected error in getName()", e);
        }
        return names;
    }

    private List<String> getTilltalsNamn(final String name, final Integer tilltalsnamnmarkering) throws MalformedInputException {
        if (tilltalsnamnmarkering == null) {
            throw new MalformedInputException("Tilltalsnamnmarkering was null");
        }
        if (tilltalsnamnmarkering < 10 || tilltalsnamnmarkering > 99) {
            throw new MalformedInputException(String.format("Tilltalsnamnmarkering. expected value between 10-99. actual value was: %s", tilltalsnamnmarkering));
        }
        //retain separators
        final String[] nameSplit = name.split("(?<=[-| ])|(?=[-| ])");
        int idxFirst = (Character.getNumericValue(tilltalsnamnmarkering.toString().charAt(0)) - 1) * 2;
        int idxSecond = (Character.getNumericValue(tilltalsnamnmarkering.toString().charAt(1)) - 1) * 2;

        final List<String> retGivenNames = new ArrayList<>();
        if (idxSecond < 0) {
            retGivenNames.add(nameSplit[idxFirst]);
        } else if (idxSecond == idxFirst + 2 && nameSplit[idxFirst + 1].equals("-")) {
            retGivenNames.add(String.format("%s-%s", nameSplit[idxFirst], nameSplit[idxSecond]));
        } else if (idxFirst >= 2 && idxSecond == idxFirst - 2 && nameSplit[idxFirst - 1].equals("-")) {
            retGivenNames.add(String.format("%s-%s", nameSplit[idxFirst], nameSplit[idxSecond]));
        } else {
            retGivenNames.add(nameSplit[idxFirst]);
            retGivenNames.add(nameSplit[idxSecond]);
        }
        return retGivenNames;
    }

    private AdministrativeGender getAdministrativeGender(final KonTYPE konType) {
        if (konType == null) {
            log.debug("KonType is null");
            return AdministrativeGender.NULL;
        }

        switch (konType) {
            case K:
                return AdministrativeGender.FEMALE;
            case M:
                return AdministrativeGender.MALE;
        }
        log.warn("KonTYPE is malformed");
        return AdministrativeGender.NULL;
    }

    private CodeableConcept getMaritalStatus(final CivilstandTYPE civilstand) {
        try {
            final CodeableConcept retval = new CodeableConcept();
            final Coding coding = getMaritalStatusCode(civilstand.getCivilstandKod());

            if (coding != null) {
                retval.addCoding(coding);
            }
            //todo - civilstand.getCivilstandsdatum()
            return retval;
        } catch (Exception e) {
            log.error("Parsing of CivilStandTYPE failed.", e);
            return new CodeableConcept();
        }


    }

    private Coding getMaritalStatusCode(final CivilstandKodTYPE civilstandKod) {
        try {
            switch (civilstandKod) {
                case OG: //Ogift
                    return Helpers.getCodingFromValueSet(MaritalStatus.U);
                case G://gift
                    return Helpers.getCodingFromValueSet(MaritalStatus.M);
                case A://Änka/änkling
                    return Helpers.getCodingFromValueSet(MaritalStatus.W);
                case S://skild
                    return Helpers.getCodingFromValueSet(MaritalStatus.D);
                case RP: //Registrerad Partner
                    return Helpers.getCodingFromValueSet(MaritalStatus.T);
                case SP: //Skild Partner
                    return Helpers.getCodingFromValueSet(MaritalStatus.D);
                case EP: //Efterlevande partner
                    return Helpers.getCodingFromValueSet(MaritalStatus.W);
            }
        } catch (Exception e) {
            log.error("Parsing of CivilStandKod failed.", e);
        }
        return Helpers.getCodingFromValueSet(MaritalStatus.UNK);
    }

    public class MalformedInputException extends Exception {
        public MalformedInputException() {
            super();
        }

        public MalformedInputException(String message) {
            super(message);
        }

        public MalformedInputException(String message, Throwable cause) {
            super(message, cause);
        }

        public MalformedInputException(Throwable cause) {
            super(cause);
        }

        protected MalformedInputException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
