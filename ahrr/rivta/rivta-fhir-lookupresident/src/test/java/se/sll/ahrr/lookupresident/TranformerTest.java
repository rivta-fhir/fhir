package se.sll.ahrr.lookupresident;

/*-
 * #%L
 * rivta-fhir-lookupresident
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.junit.Assert;
import org.junit.Test;
import riv.population.residentmaster._1.*;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileResponseType;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.MaritalStatus;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class TranformerTest {

    private final MockFactory mockFactory = new MockFactory();

    @Test
    public void VerifyPatient() throws Exception {
        LookupResidentForFullProfileResponseType rivResponse = createResponseType(JaNejTYPE.N, null);
        List<Patient> patients = new LookupResidentDataTransformer().transformToFhir(rivResponse.getResident());
        assertPatient(patients.get(0), rivResponse.getResident().get(0));

    }

    @Test
    public void assertMaritalStatus() {
        for (CivilstandKodTYPE civilstandKod : CivilstandKodTYPE.values()) {
            try {
                LookupResidentForFullProfileResponseType rivResponse = createResponseType(JaNejTYPE.N, civilstandKod);
                Patient patient = new LookupResidentDataTransformer().transformToFhir(rivResponse.getResident()).get(0);
                switch (civilstandKod) {
                    case OG:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.U)));
                        break;
                    case G:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.M)));
                        break;
                    case A:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.W)));
                        break;
                    case S:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.D)));
                        break;
                    case RP:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.T)));
                        break;
                    case SP:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.D)));
                        break;
                    case EP:
                        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.W)));
                        break;
                }
            } catch (Exception e) {
                assert (false);
            }
        }
    }

    @Test
    public void testTilltalsnamn() {
        List<UsualNameTesData> usualNameTestData = new ArrayList<>();
        usualNameTestData.add(new UsualNameTesData("tol van", 12, Arrays.asList(new String[]{"tol", "van"}), "tol van surname"));
        usualNameTestData.add(new UsualNameTesData("tol-van", 12, Arrays.asList(new String[]{"tol-van"}), "tol-van surname"));
        usualNameTestData.add(new UsualNameTesData("erik tol tore van", 24, Arrays.asList(new String[]{"tol", "van"}), "tol van surname"));
        usualNameTestData.add(new UsualNameTesData("van-tol", 21, Arrays.asList(new String[]{"tol-van"}), "tol-van surname"));
        usualNameTestData.add(new UsualNameTesData("Eva Mia", 20, Arrays.asList(new String[]{"Mia"}), "Mia surname"));
        usualNameTestData.add(new UsualNameTesData("Emma", 10, Arrays.asList(new String[]{"Emma"}), "Emma surname"));
        usualNameTestData.add(new UsualNameTesData("Hedwig Britt-Marie", 23, Arrays.asList(new String[]{"Britt-Marie"}), "Britt-Marie surname"));

        try {
            for (UsualNameTesData testData : usualNameTestData) {
                LookupResidentForFullProfileResponseType rivResponse = createResponseType(JaNejTYPE.N, null);
                rivResponse.getResident().get(0).getPersonpost().getNamn().setFornamn(testData.firstName);
                rivResponse.getResident().get(0).getPersonpost().getNamn().setEfternamn("surname");
                rivResponse.getResident().get(0).getPersonpost().getNamn().setTilltalsnamnsmarkering(testData.tilltalsnamnmarkering);
                List<Patient> patients = new LookupResidentDataTransformer().transformToFhir(rivResponse.getResident());
                Optional<HumanName> optName = patients.get(0).getName().stream().filter(o -> o.getUse().equals(HumanName.NameUse.USUAL)).findFirst();
                assertTrue(optName.isPresent());
                HumanName usualName = optName.get();
                for (int i = 0; i < testData.ExpectedGivenName.size(); i++) {
                    assertEquals(testData.ExpectedGivenName.get(i), usualName.getGiven().get(i).getValue());
                }
                assertEquals(testData.ExpectedText, usualName.getText());
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testNoTilltalsnamn() {
        try {
            LookupResidentForFullProfileResponseType rivResponse = createResponseType(JaNejTYPE.N, null);
            rivResponse.getResident().get(0).getPersonpost().getNamn().setTilltalsnamnsmarkering(null);
            List<Patient> patients = new LookupResidentDataTransformer().transformToFhir(rivResponse.getResident());
            assertFalse(patients.get(0).getName().stream().anyMatch(o -> o.getUse().equals(HumanName.NameUse.USUAL)));
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    private void assertPatient(final Patient patient, final ResidentType resident) {
        //resident.setSenasteAndringFolkbokforing();
        PersonpostTYPE personpost = resident.getPersonpost();
        assertIdentifiers(patient, personpost);
        assertGender(patient.getGender(), personpost.getKon());
        assertAddresses(patient, 3, personpost);
        assertEquals(patient.getBirthDate(), Riv2FhirTransformationHelper.getDateForDate(personpost.getFodelsetid()).getValue());
        assertName(patient.getName(), personpost.getNamn());
        assertTrue(patient.getMaritalStatus().equalsDeep(Helpers.getCodeableConceptFromValueSet(MaritalStatus.UNK)));

    }

    private void assertName(final List<HumanName> humanNames, final NamnTYPE nameType) {
        assertTrue(humanNames.stream().anyMatch(o -> o.getUse().equals(HumanName.NameUse.OFFICIAL)));
        assertTrue(humanNames.stream().anyMatch(o -> o.getUse().equals(HumanName.NameUse.USUAL)));
        assertEquals(2, humanNames.size());

        for (HumanName humanName : humanNames) {
            if (humanName.getUse().equals(HumanName.NameUse.OFFICIAL)) {
                for (StringType name : humanName.getGiven()) {
                    assert nameType.getFornamn().contains(name.toString()) || nameType.getMellannamn().contains(name.toString());
                }
                assertEquals(humanName.getFamily(), nameType.getEfternamn());
            } else if (humanName.getUse().equals(HumanName.NameUse.USUAL)) {
                assertEquals("Tol-Van", humanName.getGivenAsSingleString());
                assertEquals(String.format("Tol-Van %s", nameType.getEfternamn()), humanName.getText());
            } else {
                Assert.fail("Unexpected NameUse");
            }
        }

    }

    private void assertAddresses(final Patient patient, final int expectedCount, final PersonpostTYPE personpost) {
        assertEquals(patient.getAddress().size(), expectedCount);
        for (Address address : patient.getAddress()) {
            assertNotNull(address.getUse());
            switch (address.getUse()) {

                case HOME:
                    assertAddress(address, personpost.getFolkbokforingsadress());
                    break;
                case WORK:
                    assert (false);
                    break;
                case TEMP:
                    assertAddress(address, personpost.getSarskildPostadress());
                    break;
                case OLD:
                    assert (false);
                    break;
                case NULL:
                    assertAddress(address, personpost.getUtlandsadress());
                    break;
            }
        }
    }

    private void assertAddress(final Address address, final SvenskAdressTYPE svenskAdress) {
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(svenskAdress.getCareOf())));
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(svenskAdress.getUtdelningsadress1())));
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(svenskAdress.getUtdelningsadress2())));
        assertEquals(address.getCity(), svenskAdress.getPostort());
        assertEquals(address.getPostalCode(), svenskAdress.getPostNr());
        assertEquals(address.getCountry(), "SWE");
    }

    private void assertAddress(final Address address, final UtlandsadressTYPE utlandsadress) {
        assertEquals(utlandsadress.getLand(), address.getCountry());
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(utlandsadress.getUtdelningsadress1())));
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(utlandsadress.getUtdelningsadress2())));
        assertTrue(address.getLine().stream().anyMatch(o -> o.toString().equals(utlandsadress.getUtdelningsadress3())));
    }


    private void assertGender(final Enumerations.AdministrativeGender administrativeGender, final KonTYPE kon) {
        if (administrativeGender == Enumerations.AdministrativeGender.MALE) {
            assertEquals(kon, KonTYPE.M);
            return;
        }
        if (administrativeGender == Enumerations.AdministrativeGender.FEMALE) {
            assertEquals(kon, KonTYPE.K);
            return;
        }
        assert (false);
    }


    private void assertIdentifiers(final Patient patient, final PersonpostTYPE personpost) {
        assertEquals(patient.getIdentifierFirstRep().getValue(), personpost.getPersonId());
        assertEquals(patient.getIdentifierFirstRep().getSystem(), CodeSystem.PERSONID_SE.getValue());
        assertEquals(patient.getIdentifierFirstRep().getUse(), Identifier.IdentifierUse.OFFICIAL);
        assertEquals(patient.getIdentifier().get(1).getValue(), personpost.getHanvisningsPersonNr());
        assertEquals(patient.getIdentifier().get(1).getSystem(), CodeSystem.PERSONID_SE.getValue());
        assertEquals(patient.getIdentifier().get(1).getUse(), Identifier.IdentifierUse.SECONDARY);
    }

    private LookupResidentForFullProfileResponseType createResponseType(JaNejTYPE sekretess) throws Exception {
        return createResponseType(sekretess, null); //null will result in UNK marital status.
    }

    private LookupResidentForFullProfileResponseType createResponseType(JaNejTYPE sekretess, CivilstandKodTYPE civilstandKodTYPE) throws Exception {
        final LookupResidentForFullProfileResponseType responseType = new LookupResidentForFullProfileResponseType();
        final ResidentType resident = new ResidentType();
        resident.setSenasteAndringFolkbokforing(mockFactory.dateTimeStr);
        resident.setSekretessmarkering(sekretess);
        resident.setPersonpost(createPersonPost(civilstandKodTYPE));
        responseType.getResident().add(resident);
        return responseType;
    }

    private PersonpostTYPE createPersonPost(final CivilstandKodTYPE civilstandKodTYPE) throws Exception {
        final PersonpostTYPE personpost = new PersonpostTYPE();
        personpost.setFodelse(createFodelseType());
        personpost.setAvregistrering(createAvregistreringsType());
        personpost.setCivilstand(createCivilstandTYPE(civilstandKodTYPE));
        //personpost.setDistriktskod(); todo
        personpost.setFodelsetid(mockFactory.dateShortStr);

        personpost.setFolkbokforingsadress((SvenskAdressTYPE) mockFactory.createSvenskAddressType(new SvenskAdressTYPE()));
        personpost.setSarskildPostadress((SvenskAdressTYPE) mockFactory.createSvenskAddressType(new SvenskAdressTYPE()));
        personpost.setUtlandsadress(createUtlandsAddress());
        personpost.setUtlandsadress(createUtlandsAddress());
        personpost.setPersonId("191212121212");
        personpost.setHanvisningsPersonNr("191111111111");
        personpost.setKon(KonTYPE.K);
        personpost.setInvandring(createInvandringsType());
        personpost.setNamn(createNamnType());
        personpost.setRelationer(createRelationerType());
        return personpost;

    }

    private UtlandsadressTYPE createUtlandsAddress() {
        final UtlandsadressTYPE utlandsadress = new UtlandsadressTYPE();
        utlandsadress.setLand("ABC");
        utlandsadress.setRostrattsdatum(mockFactory.dateTimeStr);
        utlandsadress.setUtdelningsadress1("Utdelning1");
        utlandsadress.setUtdelningsadress2("Utdelning2");
        utlandsadress.setUtdelningsadress3("Utdelning3");
        utlandsadress.setUtlandsadressdatum(mockFactory.dateTimeStr);
        return utlandsadress;
    }

    private RelationerTYPE createRelationerType() {
        //todo - not yet mapped
        return new RelationerTYPE();
    }

    private NamnTYPE createNamnType() {
        final NamnTYPE namn = new NamnTYPE();
        namn.setFornamn("Van Tol Erik Anders Tol-Van");
        namn.setMellannamn("Elvan");
        namn.setEfternamn("Tolvansson");
        namn.setTilltalsnamnsmarkering(56);
        namn.setAviseringsnamn("aviseringsnamn");
        return namn;

    }

    private InvandringTYPE createInvandringsType() {
        //todo - not yet mapped
        return new InvandringTYPE();
    }

    private FodelseTYPE createFodelseType() {
        //todo - not yet mapped
        return new FodelseTYPE();
    }

    private AvregistreringTYPE createAvregistreringsType() {
        //todo - not yet mapped
        return new AvregistreringTYPE();
    }

    private CivilstandTYPE createCivilstandTYPE(CivilstandKodTYPE civilstandKod) {
        CivilstandTYPE civilstandTYPE = new CivilstandTYPE();
        civilstandTYPE.setCivilstandKod(civilstandKod);
        return civilstandTYPE;
    }

    private class UsualNameTesData {
        public String firstName;
        public int tilltalsnamnmarkering;
        public List<String> ExpectedGivenName;
        public String ExpectedText;

        public UsualNameTesData(String firstName, int tilltalsnamnmarkering, List<String> expectedGivenName, String expectedText) {
            this.firstName = firstName;
            this.tilltalsnamnmarkering = tilltalsnamnmarkering;
            ExpectedGivenName = expectedGivenName;
            ExpectedText = expectedText;
        }
    }
}
