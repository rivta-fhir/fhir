package se.sll.ahrr.dataloader;

/*-
 * #%L
 * rivta-log
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import riv.ehr.log._1.ResultCodeType;
import riv.ehr.log.store.storelog._1.rivtabp21.StoreLogResponderInterface;
import riv.ehr.log.store.storelog._1.rivtabp21.StoreLogResponderService;
import riv.ehr.log.store.storelogresponder._1.StoreLogRequestType;
import riv.ehr.log.store.storelogresponder._1.StoreLogResponseType;
import se.sll.ahrr.dataloader.exception.FetchDataException;

@Service
public class StoreLogDataLoader {

    private StoreLogResponderInterface port;

    @Value("${ahrr.logicalAddress:''}") String logicalAddress;

    public StoreLogDataLoader(
       @Value("${contracts.storeLog.url}") String serviceUrl,
       @Autowired TLSClientParameters tlsClientParameters,
       @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        StoreLogResponderService service = new StoreLogResponderService();
        port = service.getStoreLogResponderPort();
        Client client = ClientProxy.getClient(port);
        client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);

        // ADD SSL AND TIMEOUT CODE
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.setTlsClientParameters(tlsClientParameters);
        httpConduit.getClient().setReceiveTimeout(timeout * 1000);
    }

    public void storeLog(final StoreLogRequestType params) throws FetchDataException {
        StoreLogResponseType storeLogResponseType = port.storeLog(logicalAddress, params);
        if (storeLogResponseType.getResultType().getResultCode() != ResultCodeType.OK) {
            throw new FetchDataException(String.format("Could not register AuditEvent, result was %s", storeLogResponseType.getResultType().getResultCode()));
        }
    }
}
