package se.sll.ahrr.log;

/*-
 * #%L
 * rivta-log
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import riv.ehr.log._1.*;
import riv.ehr.log.store.storelogresponder._1.StoreLogRequestType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.AuditEventAction;
import se.sll.ahrr.codesystem.valueset.AuditEventEntityType;
import se.sll.ahrr.codesystem.valueset.AuditEventId;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.InformationType;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LogTransformer {
    private Logger log = LoggerFactory.getLogger(getClass());

    private boolean compat;
    private String systemId;

    public LogTransformer(@Value("${contracts.storeLog.compat}") boolean compat,  @Value("${contracts.storeLog.systemId}") String systemId) {
        this.compat = compat;
        this.systemId = systemId;
        if (this.compat) {
            log.info("Starting StoreLog service in RIVTA compatibility mode");
        }
    }

    /**
     * Returns a StoreLog request populated with the supplied principal
     * as the resource consumer
     *
     * @param auditEvent event
     * @param principal  the principal who performed the event
     * @param patientSSN the patient who the event is about
     * @return A StoreLog request
     */
    public StoreLogRequestType transformToRiv(AuditEvent auditEvent, CareActorPrincipal principal, String patientSSN) throws Exception {
        if (auditEvent.getEntity().isEmpty()) {
            throw new IllegalArgumentException("There is no entity to log");
        }

        LogType logEntry = new LogType();

        ActivityType activity = new ActivityType();
        ResourcesType resources = new ResourcesType();
        SystemType system = new SystemType();
        UserType user = new UserType();

        system.setSystemId(this.systemId);

        CareProviderType careProvider = new CareProviderType();
        CareUnitType careUnit = new CareUnitType();
        PatientType patient = new PatientType();
        patient.setPatientId(patientSSN);
        careProvider.setCareProviderId(principal.getCareProviderId());
        careProvider.setCareProviderName(principal.getCareProviderName());
        careUnit.setCareUnitId(principal.getCareUnitId());
        careUnit.setCareUnitName(principal.getCareUnitName());

        user.setAssignment(principal.getAssignmentName());
        user.setTitle(principal.getTitle());
        user.setCareProvider(careProvider);
        user.setCareUnit(careUnit);
        user.setPersonId(patientSSN);
        user.setUserId(principal.getHsaId());
        user.setName(principal.getCareActorGivenName() + " " + principal.getCareActorMiddleAndSurName());

        if (auditEvent.getType() == null) {
            throw new IllegalArgumentException("Missing type");
        }

        if (!CodeSystem.AUDIT_EVENT_ID.getValue().equals(auditEvent.getType().getSystem()) || !AuditEventId.PATIENT_RECORD.getCode().equals(auditEvent.getType().getCode())) {
            throw new IllegalArgumentException("Unknown type");
        }

        if (auditEvent.getAction() == null) {
            throw new IllegalArgumentException("Missing action");
        }

        AuditEventAction auditEventAction = AuditEventAction.valueOf(auditEvent.getAction().toCode());

        switch (auditEventAction) {
            case R:
                activity.setActivityType("Läsa");
                break;
            case C:
                activity.setActivityType("Skriva");
                break;
            default:
                throw new IllegalArgumentException("Unknown action");
        }

        activity.setPurpose(principal.getCommissionPurpose());

        if (auditEvent.getRecorded() == null) {
            throw new IllegalArgumentException("Missing recorded date");
        }

        activity.setStartDate(Helpers.dateToXmlGregorianCalendar(auditEvent.getRecorded()));

        resources.getResource().addAll(auditEvent.getEntity().stream()
                .map(
                        entity -> {
                            ResourceType resource = new ResourceType();
                            resource.setCareProvider(careProvider);
                            resource.setCareUnit(careUnit);
                            resource.setPatient(patient);

                            if (compat) {
                                // Lookup the RIVTA information type of this entry
                                // and use it as the resource type
                                try {
                                    AuditEventEntityType entityType = AuditEventEntityType.fromCode(entity.getType().getCode());
                                    InformationType rivtaType;
                                    switch (entityType) {
                                        case CARE_PLAN:
                                            rivtaType = InformationType.VPO;
                                            break;
                                        case COMPOSITION:
                                            rivtaType = InformationType.VOO;
                                            break;
                                        case CONDITION:
                                            rivtaType = InformationType.DIA;
                                            break;
                                        case CONSENT:
                                            rivtaType = InformationType.CONSENT;
                                            break;
                                        case DIAGNOSTIC_REPORT:
                                            rivtaType = InformationType.UND;
                                            break;
                                        case ENCOUNTER:
                                            rivtaType = InformationType.VKO;
                                            break;
                                        case FLAG:
                                            rivtaType = InformationType.UPP;
                                            break;
                                        case OBSERVATION:
                                            rivtaType = InformationType.UND;
                                            break;
                                        case IMAGING_STUDY:
                                            rivtaType = InformationType.UND;
                                            break;
                                        case IMMUNIZATION:
                                            rivtaType = InformationType.LKO;
                                            break;
                                        case MEDICATION_STATEMENT:
                                            rivtaType = InformationType.LKO;
                                            break;
                                        case REFERRAL_REQUEST:
                                            rivtaType = InformationType.UNKNOWN;
                                            break;
                                        case PATIENT:
                                            rivtaType = InformationType.PU;
                                            break;
                                        default:
                                            rivtaType = InformationType.UNKNOWN;
                                            break;
                                    }
                                    if (rivtaType == InformationType.UNKNOWN) {
                                        resource.setResourceType(entity.getType().getCode());
                                    } else {
                                        resource.setResourceType(rivtaType.name());
                                    }

                                } catch (Exception e) {
                                    // Fall back to using the supplied type if it's not mappable
                                    log.warn("Unmappable entity type {} supplied to StoreLog", entity.getType().getCode());
                                    resource.setResourceType(entity.getType().getCode());
                                }
                            } else {
                                resource.setResourceType(entity.getType().getCode());
                            }
                            return resource;
                        }
                ).collect(Collectors.toList()));

        logEntry.setLogId(UUID.randomUUID().toString());

        logEntry.setActivity(activity);
        logEntry.setResources(resources);
        logEntry.setSystem(system);
        logEntry.setUser(user);

        StoreLogRequestType storeLogRequest = new StoreLogRequestType();
        storeLogRequest.getLog().add(logEntry);

        return storeLogRequest;
    }
}
