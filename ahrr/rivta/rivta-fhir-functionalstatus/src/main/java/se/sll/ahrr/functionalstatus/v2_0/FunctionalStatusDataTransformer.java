package se.sll.ahrr.functionalstatus.v2_0;

/*-
 * #%L
 * rivta-fhir-functionalstatus
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.*;
import riv.clinicalprocess.healthcond.description.enums._2.AssessmentCategoryEnum;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class FunctionalStatusDataTransformer implements Riv2FhirTransformer<List<FunctionalStatusAssessmentType>, Observation> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Observation.class;
    }


    @Override
    public List<Observation> transformToFhir(List<FunctionalStatusAssessmentType> response) {
        return response.parallelStream()
                .map(this::getObservation)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Observation getObservation(final FunctionalStatusAssessmentType functionalStatusAssessment) {
        try {
            final Observation observation = new Observation();
            mapHeader(functionalStatusAssessment.getFunctionalStatusAssessmentHeader(), observation);
            mapBody(functionalStatusAssessment.getFunctionalStatusAssessmentBody(), observation);
            return observation;
        } catch (Exception e) {
            log.error("Unexpected error while mappint FunctionalStatusassementType -> Observation", e);
            return null;
        }
    }

    private void mapHeader(final PatientSummaryHeaderType header, final Observation observation) {

        observation.setId(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId())
        );

        observation.addIdentifier(new Identifier()
                .setSystem(header.getSourceSystemHSAId())
                .setValue(header.getDocumentId())
        );

        if (header.getDocumentTitle() != null) {
        }

        if (header.getDocumentTime() != null) {
            try {
                observation.setIssued(Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()).getValue());
            } catch (Exception e) {
                log.error("Error while parsing documentTime", e);
            }
        }

        observation.setSubject(getObservationSubject(header.getPatientId()));
        observation.setStatus(Observation.ObservationStatus.REGISTERED);

        Encounter encounter = new Encounter().setStatus(Encounter.EncounterStatus.UNKNOWN);
        if (header.getCareContactId() != null) {
            encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId()).setStatus(Encounter.EncounterStatus.UNKNOWN);
        }
        if (header.getLegalAuthenticator() != null) {
            encounter.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        }
        if (header.getAccountableHealthcareProfessional() != null) {
            observation.addPerformer(Riv2FhirTransformationHelper.getPracticionerReference(header.getAccountableHealthcareProfessional()));
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
        }
        observation.setContext(new Reference(encounter));
    }

    private void mapBody(final FunctionalStatusAssessmentBodyType body, final Observation observation) {
        /**
         * SNOMED-CT PADL Encoded Value
         * @see <a href="http://browser.ihtsdotools.org/?perspective=full&conceptId1=285592006&edition=se-edition&release=v20170531&server=https://prod-browser-exten.ihtsdotools.org/api/snomed&langRefset=46011000052107">SNOME-CT PADL</a>
         */
        if (AssessmentCategoryEnum.PAD_PAD == body.getAssessmentCategory()) {
            body.getPadl().forEach(padl -> observation.addComponent(getObservationPadlComponent(padl)));
        }

        /**
         * SNOMED-CT Disability Finding, when no other code system is provided e.g. when original is set.
         * @see <a href="http://browser.ihtsdotools.org/?perspective=full&conceptId1=21134002&edition=se-edition&release=v20170531&server=https://prod-browser-exten.ihtsdotools.org/api/snomed&langRefset=46011000052107">SNOME-CT PADL</a>
         */
        else if (AssessmentCategoryEnum.FUN_FUN == body.getAssessmentCategory() && null != body.getDisability().getDisabilityAssessment().getOriginalText()) {
            observation.setCode(getObservationDisablityCode());

            StringType value = new StringType();
            value.setValue(body.getDisability().getDisabilityAssessment().getOriginalText());
            observation.setValue(value);
        }
        /**
         * Codesystem from Rivta
         * @see <a href="http://browser.ihtsdotools.org/?perspective=full&conceptId1=21134002&edition=se-edition&release=v20170531&server=https://prod-browser-exten.ihtsdotools.org/api/snomed&langRefset=46011000052107">SNOME-CT PADL</a>
         */
        else {
            observation.setCode(getRivtaCodeFromCVType(body.getDisability().getDisabilityAssessment()));
            StringType value = new StringType();
            value.setValue(body.getDisability().getDisabilityAssessment().getOriginalText());
            observation.setValue(value);
        }
        if (body.getComment() != null) {
            observation.setComment(body.getComment());
        }
    }

    private Reference getObservationSubject(PersonIdType patientId) {
        if (null == patientId) {
            return null;
        }

        if (null == patientId.getId() || patientId.getId().isEmpty()) {
            return null;
        }

        if (null == patientId.getType() || patientId.getType().isEmpty()) {
            return null;
        }

        return Riv2FhirTransformationHelper.getPatientReferenceWithSystem(
                String.format("urn:oid:%s", patientId.getType()),
                patientId.getId()
        );
    }

    private Observation.ObservationComponentComponent getObservationPadlComponent(PADLType padl) {
        Observation.ObservationComponentComponent component = new Observation.ObservationComponentComponent();

        Coding snomedCtPadlCoding = new Coding()
                .setSystem(CodeSystem.SNOMED_CT.getValue())
                .setCode("285592006");

        CodeableConcept padlCode = new CodeableConcept()
                .addCoding(snomedCtPadlCoding)
                .setText("aktivitet relaterad till personlig vård");

        component.setCode(padlCode);

        if (null != padl.getAssessment()) {
            component.setValue(new StringType(padl.getAssessment()));
        }
        return component;
    }

    private CodeableConcept getObservationDisablityCode() {
        Coding snomedCtCoding = new Coding()
                .setSystem(CodeSystem.SNOMED_CT.getValue())
                .setCode("21134002");

        CodeableConcept disabilityCode = new CodeableConcept()
                .addCoding(snomedCtCoding)
                .setText("funktionsnedsättning");
        return disabilityCode;
    }


    private CodeableConcept getRivtaCodeFromCVType(CVType cvType) {
        Coding rivtaCoding = new Coding()
                .setSystem(CodeSystem.SNOMED_CT.getValue())
                .setCode("21134002");

        if (cvType != null && null != cvType.getCodeSystem()) {
            rivtaCoding.setSystem(String.format("urn:oid:%s", cvType.getCodeSystem()));
        }

        if (cvType != null && null != cvType.getCode()) {
            rivtaCoding.setCode(cvType.getCode());
        }

        if (cvType != null && null != cvType.getDisplayName()) {
            rivtaCoding.setDisplay(cvType.getDisplayName());
        }
        return new CodeableConcept().addCoding(rivtaCoding);
    }
}
