package se.sll.ahrr.functionalstatus.v2_0.test;

/*-
 * #%L
 * rivta-fhir-functionalstatus
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Organization;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.exceptions.FHIRException;
import org.junit.Test;
import riv.clinicalprocess.healthcond.description._2.*;
import riv.clinicalprocess.healthcond.description.enums._2.AssessmentCategoryEnum;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.functionalstatus.v2_0.FunctionalStatusDataTransformer;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class DataTransformerTest {
    private final MockFactory mockFactory = new MockFactory();

    @Test
    public void verifyPersonIdCodeSystems() {
        se.sll.ahrr.codesystem.CodeSystem[] codeSystems = {
                CodeSystem.PERSONID_SE,
                CodeSystem.PERSONID_COORDINATION_NUMBER_SE,
                CodeSystem.PERSONID_RESERVE_SE,
                CodeSystem.PERSONID_RESERVE_SLL_SE
        };

        final FunctionalStatusDataTransformer transformer = new FunctionalStatusDataTransformer();
        for (CodeSystem codeSystem : codeSystems) {
            final FunctionalStatusAssessmentType rivResponse = new FunctionalStatusAssessmentType();
            final PatientSummaryHeaderType header = createHeader();
            final FunctionalStatusAssessmentBodyType body = createBody(AssessmentCategoryEnum.PAD_PAD);

            header.getPatientId().setType(codeSystem.getValueWithoutNamespace());
            rivResponse.setFunctionalStatusAssessmentHeader(header);
            rivResponse.setFunctionalStatusAssessmentBody(body);
            Observation observation = transformer.transformToFhir(Collections.singletonList(rivResponse)).get(0);
            Assertions.assertIdentifier(codeSystem.getValue(), header.getPatientId().getId(), ((Patient)observation.getSubject().getResource()).getIdentifierFirstRep());
        }
    }
    @Test
    public void verifyObservation() {
        for(int i = 0; i < AssessmentCategoryEnum.values().length+1; i++) {
            final PatientSummaryHeaderType header = createHeader();
            final FunctionalStatusAssessmentBodyType body = createBody(i < AssessmentCategoryEnum.values().length ? AssessmentCategoryEnum.values()[i] : null);

            final FunctionalStatusAssessmentType rivResponse = new FunctionalStatusAssessmentType();
            rivResponse.setFunctionalStatusAssessmentHeader(header);
            rivResponse.setFunctionalStatusAssessmentBody(body);
            final Observation observation = new FunctionalStatusDataTransformer().transformToFhir(Collections.singletonList(rivResponse)).get(0);
            assertHeader(header, observation);
            assertBody(body, observation);
        }
    }

    private void assertBody(final FunctionalStatusAssessmentBodyType body, final Observation observation) {
        assertAssessmentCategory(body, observation);
        assertEquals(body.getComment(), observation.getComment());
    }
    private void assertAssessmentCategory(final FunctionalStatusAssessmentBodyType body, final Observation observation) {
        if(body.getAssessmentCategory() == null)
        {
            try {
               final CVType disabilityAssessment = body.getDisability().getDisabilityAssessment();
                Assertions.assertCoding("urn:oid:"+disabilityAssessment.getCodeSystem(), disabilityAssessment.getCode(),
                        disabilityAssessment.getDisplayName(), observation.getCode().getCodingFirstRep());
                assertEquals(observation.getValueStringType().toString(), body.getDisability().getDisabilityAssessment().getOriginalText());
            } catch (FHIRException e) {
                fail("AsssertAssesmentCategory failed");
            }
        } else {
            switch (body.getAssessmentCategory()) {
                case PAD_PAD:
                    body.getPadl().forEach(padl -> assertPADL(padl, observation.getComponent()));
                    break;
                case FUN_FUN:
                    assertDisability(body, observation);
                    break;
            }
        }
    }

    private void assertDisability(final FunctionalStatusAssessmentBodyType body, final Observation observation) {
        Assertions.assertCoding(CodeSystem.SNOMED_CT.getValue(), "21134002", null, observation.getCode().getCodingFirstRep());
        assertEquals("funktionsnedsättning", observation.getCode().getText());
        assertEquals(body.getDisability().getDisabilityAssessment().getOriginalText(), observation.getValue().toString());
    }


    private void assertPADL(final PADLType padl, final List<Observation.ObservationComponentComponent> obsComponents) {
        Observation.ObservationComponentComponent obsComponent = obsComponents
                .stream().filter(r -> r.getValue().toString().equals(padl.getAssessment())).collect(Collectors.toList()).get(0);
        assertNotNull(obsComponent);
        Assertions.assertCoding(CodeSystem.SNOMED_CT.getValue(), "285592006", null, obsComponent.getCode().getCodingFirstRep());
        assertEquals("aktivitet relaterad till personlig vård", obsComponent.getCode().getText());
    }


    private void assertHeader(final PatientSummaryHeaderType header, final Observation observation) {
        assertEquals(observation.getId(), Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId()));
        Assertions.assertIdentifier(header.getSourceSystemHSAId(), header.getDocumentId(), observation.getIdentifierFirstRep());

        final Encounter encounter = (Encounter) observation.getContext().getResource();
        Assertions.assertIdentifier(null, header.getCareContactId(), encounter.getIdentifierFirstRep());
        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) encounter.getServiceProvider().getResource());
        Assertions.assertLegalAuthenticator(header.getLegalAuthenticator(), encounter.getExtensionsByUrl(Riv2FhirTransformationHelper.legalAuthenticatorExtensionUrl).get(0));
        Assertions.assertIdentifier(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId(), ((Patient) observation.getSubject().getResource()).getIdentifier().get(0));
        assertEquals(observation.getIssued().getTime(), Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()).getValue().getTime());
        //header.getDocumentTitle() not mapped
    }

    private PatientSummaryHeaderType createHeader() {
        try {
            return (PatientSummaryHeaderType) mockFactory.createPatientSummaryHeaderType(
                    new HealthcareProfessionalType(),
                    new OrgUnitType(),
                    new CVType(),
                    new LegalAuthenticatorType(),
                    new PersonIdType(),
                    new PatientSummaryHeaderType()
            );
        } catch (Exception e) {
            fail("Creating header failed");
            return null;
        }
    }

    private FunctionalStatusAssessmentBodyType createBody(final AssessmentCategoryEnum assessmentCategory) {
        try {
            final FunctionalStatusAssessmentBodyType body = new FunctionalStatusAssessmentBodyType();
            for (int i = 0; i < 10; i++) {
                body.getPadl().add(createPadleType());
            }
            body.setAssessmentCategory(assessmentCategory);
            body.setDisability(createDisability());
            body.setComment("Comment");
            return body;
        } catch (Exception e) {
            fail("Creating body failed");
            return null;
        }
    }

    private DisabilityType createDisability() {
        final DisabilityType disability = new DisabilityType();
        disability.setComment("Disability comment");
        try {
            disability.setDisabilityAssessment((CVType) mockFactory.createCVType(new CVType()));
        } catch (Exception e) {
            fail("Creating disabilityType failed");
        }
        return disability;
    }

    private PADLType createPadleType() {
        final PADLType padl = new PADLType();
        padl.setAssessment(String.format("Some text describing the difficulties experiences by a patient. Unique element: %s", UUID.randomUUID()));
        try {
            padl.setTypeOfAssessment((CVType) mockFactory.createCVType(new CVType()));
        } catch (Exception e) {
            fail("Creating PADLType Failed");
        }
        return padl;
    }
}
