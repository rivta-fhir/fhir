package se.sll.ahrr.referraloutcome;

/*-
 * #%L
 * rivta-fhir-referraloutcome
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/*import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Procedure;
import ca.uhn.fhir.model.dstu2.resource.ReferralRequest;
import ca.uhn.fhir.model.primitive.IdDt;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.actoutcome._3.PatientSummaryHeaderType;
import riv.clinicalprocess.healthcond.actoutcome._3.ReferralOutcomeBodyType;
import riv.clinicalprocess.healthcond.actoutcome._3.ReferralOutcomeType;
import riv.clinicalprocess.healthcond.actoutcome.getreferraloutcomeresponder._3.GetReferralOutcomeResponseType;
import se.sll.ahrr.Riv2FhirTransformer;

import java.util.LinkedList;
import java.util.List;


//unclear, want to use Task from fhir3, use bundle type to return procedure, observation or both?
//just building observation and procedure types for the momement.
@Service
public class ReferralOutcomeDataTransformer implements Riv2FhirTransformer<GetReferralOutcomeResponseType, ReferralRequest> {

    private static final Logger log = LoggerFactory.getLogger(ReferralOutcomeDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Bundle.class;
    }

    @Override
    public List<ReferralRequest> transformToFhir(GetReferralOutcomeResponseType rivResponse) throws Exception {
        final List<ReferralRequest> referralRequests = new LinkedList<>();

        for (final ReferralOutcomeType referralOutcome : rivResponse.getReferralOutcome()) {
            referralRequests.add(mapReferralOutcome(referralOutcome.getReferralOutcomeHeader(), referralOutcome.getReferralOutcomeBody()));
        }

        return referralRequests;
    }

    public ReferralRequest mapReferralOutcome(final PatientSummaryHeaderType header, final ReferralOutcomeBodyType body) {
        final ReferralRequest referralRequest = new ReferralRequest();
        mapReferralOutcomeHeader(header, referralRequest);
        return referralRequest;
    }

    private void mapReferralOutcomeHeader(final PatientSummaryHeaderType header, final ReferralRequest referralRequest) {
        final IdDt id = new IdDt();
        id.setValue(
            Riv2FhirDstu2TransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId())
        );

        referralRequest.setId(id);
        IdentifierDt identifierDt = new IdentifierDt();
        identifierDt.setSystem(header.getSourceSystemHSAId());
        identifierDt.setValue(header.getDocumentId());
        referralRequest.addIdentifier(identifierDt);

        if (header.getPatientId() != null && header.getPatientId().getId() != null) {
            referralRequest.setPatient(Riv2FhirDstu2TransformationHelper.getPatientReference(header.getPatientId().getId()));
        } else {
            log.trace("Skip mapping patient since RIV patientId was null");
        }
    }

    //no return type until i know what im returning.
    private void mapBody(final ReferralOutcomeBodyType body) {
        Observation observation = null;
        Procedure procedure = null;

        if (body.getClinicalInformation() != null && !body.getClinicalInformation().isEmpty()) {
            observation = mapObservation(body);
        }

        if (body.getAct() != null && !body.getAct().isEmpty()) {
            procedure = mapProcedure(body);
        }

    }

    private Observation mapObservation(final ReferralOutcomeBodyType body) {

        return new Observation();
    }

    private Procedure mapProcedure(final ReferralOutcomeBodyType body) {
        return new Procedure();
    }
}*/
