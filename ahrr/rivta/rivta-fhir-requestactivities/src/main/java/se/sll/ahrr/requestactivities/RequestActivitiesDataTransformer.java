package se.sll.ahrr.requestactivities;

/*-
 * #%L
 * rivta-fhir-requestactivities
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.model.dstu2.resource.ReferralRequest;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.crm.requeststatus._1.RequestActivityType;
import riv.crm.requeststatus.getrequestactivitiesresponder._1.GetRequestActivitiesResponseType;
import se.sll.ahrr.Riv2FhirTransformer;

import java.util.LinkedList;
import java.util.List;


//Saknar mockdata.
@Service
public class RequestActivitiesDataTransformer implements Riv2FhirTransformer<GetRequestActivitiesResponseType, ReferralRequest> {

    private static final Logger log = LoggerFactory.getLogger(RequestActivitiesDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return ReferralRequest.class;
    }

    @Override
    public List<ReferralRequest> transformToFhir(GetRequestActivitiesResponseType rivResponse) throws Exception {
        final List<ReferralRequest> flags = new LinkedList<>();

        for (final RequestActivityType requestActivity : rivResponse.getRequestActivity()) {
            flags.add(mapRequestActivity(requestActivity));
        }

        return flags;
    }

    //no header?????
    public ReferralRequest mapRequestActivity(final RequestActivityType requestActivity) {
        final ReferralRequest referralRequest = new ReferralRequest();
/*
        final IdDt id = new IdDt();
        id.setValue(Riv2FhirDstu2TransformationHelper.getId(requestActivity.getLogicalSystemId(),
                        requestActivity.get,
                header.getDocumentId()
        ));
*/
        return referralRequest;
    }
}
