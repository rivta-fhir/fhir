package se.sll.ahrr.MockData;

/*-
 * #%L
 * rivta-test-helper
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.codesystem.CodeSystem;

import java.time.LocalDateTime;

public class MockFactory {


    public final String dateTimeStr = "20161201080000";
    public final String dateShortStr = "20151125";
    public final String patientIdStr = "191212121212";
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public Object createOrgUnit(Object orgUnit) throws Exception {
        return createOrgUnit("Ankeborgsvägen 35", "info@kalleanka.com", "kalleanka-vc-hsa-id",
                "Ankeborg", "Kalle Ankas Vårdcentral", "1-800-Disney", orgUnit);
    }

    public Object createOrgUnit(final String address, final String email, final String hsaId, final String location, final String name, final String telecom, final Object orgUnit) throws Exception {

        PropertyUtils.setProperty(orgUnit, "orgUnitAddress", address);
        PropertyUtils.setProperty(orgUnit, "orgUnitEmail", email);
        PropertyUtils.setProperty(orgUnit, "orgUnitHSAId", hsaId);
        PropertyUtils.setProperty(orgUnit, "orgUnitLocation", location);
        PropertyUtils.setProperty(orgUnit, "orgUnitName", name);
        PropertyUtils.setProperty(orgUnit, "orgUnitTelecom", telecom);
        return orgUnit;
    }

    public Object createHealthcareProfessionalType(final Object orgUnitTypeInstance, final Object cvTypeInstance, final Object retval) throws Exception {

        return createHealthcareProfessionalType("HSA-ID-ACTOR", "Kalle Anka", "HSAId-CareUnit", "HSAId-CareGiver", "20161201080000", createOrgUnit(orgUnitTypeInstance), createCVType(cvTypeInstance), retval);
    }

    public Object createHealthcareProfessionalType(final String hsaId, final String name, final String careUnitHSAId, final String careGiverHSAId, final String authorTime, final Object orgUnitTypeInstance, final Object cvTypeInstance, Object healthcareProfessionalInstance) throws Exception {

        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalHSAId", hsaId);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalName", name);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalOrgUnit", orgUnitTypeInstance);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalRoleCode", cvTypeInstance);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalCareUnitHSAId", careUnitHSAId);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "healthcareProfessionalCareGiverHSAId", careGiverHSAId);
        PropertyUtils.setProperty(healthcareProfessionalInstance, "authorTime", authorTime);
        return healthcareProfessionalInstance;

    }


    public Object createCVType(final String code, final String system, final String systemName, final String systemVersion, final String displayName, final String originalText, final Object retval) throws Exception {
        PropertyUtils.setProperty(retval, "code", code);
        PropertyUtils.setProperty(retval, "codeSystem", system);
        PropertyUtils.setProperty(retval, "codeSystemName", systemName);
        PropertyUtils.setProperty(retval, "codeSystemVersion", systemVersion);
        PropertyUtils.setProperty(retval, "displayName", displayName);
        PropertyUtils.setProperty(retval, "originalText", originalText);
        return retval;

    }

    public Object createCVType(final Object retval) throws Exception {
        return createCVType("123", "1.2.752.116.2.1.1", "Sys 1", "1.0", "One Two Three", "Original text", retval);
    }


    public Object createPatientSummeryHeaderType(final String documentId,
                                                 final Object accountableHealthcareProfessional,
                                                 final String sourceHsaid,
                                                 final Object legalAuthenticator,
                                                 final String careContactId,
                                                 final String documentTitle,
                                                 final String documentTime,
                                                 final Object personIdType,
                                                 final boolean approvedForPatient,
                                                 final boolean nullified,
                                                 final String nullifiedReason,
                                                 final Object retval) throws Exception {

        PropertyUtils.setProperty(retval, "documentId", documentId);
        try {
            PropertyUtils.setProperty(retval, "documentTitle", documentTitle);
        } catch (Exception e) {
            log.debug("Could not set property \"documentId\", skipping", e);
        }
        try {
            PropertyUtils.setProperty(retval, "documentTime", documentTime);
        } catch (Exception e) {
            log.debug("Could not set property \"documentTime\", skipping", e);
        }

        PropertyUtils.setProperty(retval, "patientId", personIdType);
        PropertyUtils.setProperty(retval, "accountableHealthcareProfessional", accountableHealthcareProfessional);
        try {
            PropertyUtils.setProperty(retval, "legalAuthenticator", legalAuthenticator);
        } catch (Exception e) {
            log.debug("Could not set property \"LegalAuthenticator\", skipping", e);
        }
        try {
            PropertyUtils.setProperty(retval, "careContactId", careContactId);
        } catch (Exception e) {
            log.debug("Could not set property \"legalAuthenticator\", skipping", e);
        }

        PropertyUtils.setProperty(retval, "approvedForPatient", approvedForPatient);
        PropertyUtils.setProperty(retval, "nullified", nullified);
        PropertyUtils.setProperty(retval, "nullifiedReason", nullifiedReason);
        try {
            PropertyUtils.setProperty(retval, "sourceSystemHSAId", sourceHsaid);
        } catch (Exception e) {
            //CPatientSummeryHeaderType
            PropertyUtils.setProperty(retval, "sourceSystemHSAid", sourceHsaid);
        }
        return retval;
    }

    public Object createPatientSummaryHeaderType(final Object healthcareProfessionalTypeInstance,
                                                 final Object orgUnitTypeInstance,
                                                 final Object cvTypeInstance,
                                                 final Object legalAuthernticatorInstance,
                                                 final Object personIdTypeInstance,
                                                 final Object retval) throws Exception {

        return createPatientSummeryHeaderType("123", createHealthcareProfessionalType(orgUnitTypeInstance, cvTypeInstance, healthcareProfessionalTypeInstance),
                "SourceHSAid", createLegalAuthenticatorType(legalAuthernticatorInstance),
                "Carecontact-ABB123", "Document Title", dateTimeStr,
                createPersonIdType(personIdTypeInstance), true, false, "nullified reason",
                retval);
    }

    public Object createLegalAuthenticatorType(final String signatureTime, final String hsaId, final String name, final Object retval) throws Exception {
        PropertyUtils.setProperty(retval, "signatureTime", signatureTime);
        PropertyUtils.setProperty(retval, "legalAuthenticatorHSAId", hsaId);
        PropertyUtils.setProperty(retval, "legalAuthenticatorName", name);
        return retval;


    }

    public Object createLegalAuthenticatorType(final Object retval) throws Exception {
        return createLegalAuthenticatorType("20161201080000", "HSA-123", "John", retval);

    }

    public Object createTimePeriodType(final String startTime, final String endTime, final Object retval) throws Exception {
        PropertyUtils.setProperty(retval, "start", startTime);
        PropertyUtils.setProperty(retval, "end", endTime);
        return retval;

    }

    public Object createTimePeriodType(final Object retval) throws Exception {
        String date = Helpers.getStringFromLocalDateTime(LocalDateTime.now());
        //return createTimePeriodType(date, date, retval);
        return createTimePeriodType("20160205080000", "20170205100000", retval);
    }

    public Object createAdministrationType(final Object administrationIdIIType, final Object administrationTimeTimePeriodType, final String comment, final Object routeOfAdministrationCvType, final Object retval) throws Exception {

        PropertyUtils.setProperty(retval, "administrationId", administrationIdIIType);
        PropertyUtils.setProperty(retval, "administrationTime", administrationTimeTimePeriodType);
        PropertyUtils.setProperty(retval, "administrationComment", comment);
        PropertyUtils.setProperty(retval, "routeOfAdministration", routeOfAdministrationCvType);
        return retval;


    }

    public Object createAdministrationType(final Object iiTypeInstance, final Object timePeriodTypeInstance, final Object cvTypeInstance, final Object retval) throws Exception {
        return createAdministrationType(createIIType(iiTypeInstance), createTimePeriodType(timePeriodTypeInstance), "Comment", createCVType(cvTypeInstance), retval);
    }

    public Object createIIType(final String extensionValue, final String root, final Object retval) throws Exception {

        PropertyUtils.setProperty(retval, "extension", extensionValue);
        PropertyUtils.setProperty(retval, "root", root);
        return retval;

    }

    public Object createIIType(final Object retval) throws Exception {
        return createIIType("Extensionvalue", "Rootvalue", retval);
    }

    public Object createPersonIdType(final String patientId, final String type, final Object retval) {
        try {
            PropertyUtils.setProperty(retval, "id", patientId);
        } catch (Exception e) {
            log.debug("createPersonIdType: setProperty id failed.");
        }

        try {
            PropertyUtils.setProperty(retval, "type", type);
        } catch (Exception e) {
            log.debug("createPersonIdType: setProperty type failed.");
        }

        return retval;
    }

    public Object createPersonIdType(final Object retval) throws Exception {
        return createPersonIdType(patientIdStr, CodeSystem.PERSONID_SE.getValueWithoutNamespace(), retval);
    }

    public Object createSvenskAddressType(final Object retval) throws Exception {
        //Note: scbBNyckel field isnt populated.
        PropertyUtils.setProperty(retval, "careOf", "Nisse Nilsson");
        PropertyUtils.setProperty(retval, "utdelningsadress1", "Valla gatugård");
        PropertyUtils.setProperty(retval, "utdelningsadress2", "enmansväg 44");
        PropertyUtils.setProperty(retval, "postNr", "12345");
        PropertyUtils.setProperty(retval, "postort", "postort");
        PropertyUtils.setProperty(retval, "fastighetsbeteckning", "Vapnaren 4");
        PropertyUtils.setProperty(retval, "folkbokforingsdatum", dateTimeStr);
        PropertyUtils.setProperty(retval, "kommunKod", "03");
        PropertyUtils.setProperty(retval, "lanKod", "30");
        PropertyUtils.setProperty(retval, "forsamlingKod", "forsamlingKod");
        //PropertyUtils.setProperty(retval, "scbNyckelkod", "SCBNyckelKod"); no idea why this fails.

        return retval;

    }
}
