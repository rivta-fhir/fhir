package se.sll.ahrr.fhir;

/*-
 * #%L
 * rivta-test-helper
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.beanutils.PropertyUtils;
import org.hl7.fhir.dstu3.model.*;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.OrganizationType;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.Collections;

import static org.junit.Assert.*;

public class Assertions {

    public static void assertLegalAuthenticator(final Object legalAuthenticator, final Extension legalAuthenticatorExtension) {
        try {
            final Practitioner practitioner = (Practitioner) ((Reference) legalAuthenticatorExtension.getExtensionsByUrl(Riv2FhirTransformationHelper.practitionerExtensionUrl).get(0).getValue()).getResource();
            final String legalAuhtenticatorSignatureTime = (String) PropertyUtils.getProperty(legalAuthenticator, "signatureTime");
            assertLegalAuthenticatorPractioner(legalAuthenticator, practitioner);
            assertSignatureTimeExtension(legalAuthenticatorExtension.getExtensionsByUrl(Riv2FhirTransformationHelper.signatureTimeExtensionUrl).get(0), legalAuhtenticatorSignatureTime);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertLegalAuthenticatorPractioner(final Object legalAuthenticator, final Practitioner practitioner) {
        try {
            final Identifier id = practitioner.getIdentifier().get(0);
            final String legalAuthenticatorHSAID = (String) PropertyUtils.getProperty(legalAuthenticator, "legalAuthenticatorHSAId");
            final String legalAuthenticatorName = (String) PropertyUtils.getProperty(legalAuthenticator, "legalAuthenticatorName");
            assertIdentifier(CodeSystem.HSA.getValue(), legalAuthenticatorHSAID, id);
            assertEquals(legalAuthenticatorName, practitioner.getName().get(0).getText());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertSignatureTimeExtension(final Extension signatureTimeExtension, final String rivTime) {
        try {
            final DateTimeType dateTimeType = (DateTimeType) signatureTimeExtension.getValue();
            assert (dateTimeType.equalsDeep(Riv2FhirTransformationHelper.getDateTime(rivTime)));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertAccountableHealthcareProfessional(final Object healthcareProfessionalType, final Practitioner practitioner) {
        try {
            final String hcpHsaid = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalHSAId");
            final String hcpName = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalName");
            final Object hcpRoleCode = PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalRoleCode");
            final Object hcpOrgUnit = PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalOrgUnit");

            assertIdentifier(CodeSystem.HSA.getValue(), hcpHsaid, practitioner.getIdentifierFirstRep());
            assertCoding(hcpRoleCode, ((PractitionerRole) practitioner.getContained().get(0)).getCodeFirstRep().getCoding().get(0));
            assertEquals(hcpName, practitioner.getName().get(0).getText());

            final Organization managingOrg = (Organization) ((PractitionerRole) practitioner.getContained().get(0)).getOrganization().getResource();
            assertOrgUnitMapping(hcpOrgUnit, managingOrg);

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertCareUnitAndGiverOrg(final Object healthcareProfessionalType, Organization organization) {
        try {
            final String hctCareUnit = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalCareUnitHSAId");
            final String hctCareGiver = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalCareGiverHSAId");
            CodeableConcept careGiverOrgType = new CodeableConcept();
            careGiverOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careprovider.getCodeSystem(), OrganizationType.careprovider.name(), OrganizationType.careprovider.getDisplayName())));

            CodeableConcept careUnitOrgType = new CodeableConcept();
            careUnitOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careunit.getCodeSystem(), OrganizationType.careunit.name(), OrganizationType.careunit.getDisplayName())));

            final Organization careGiverOrg = findFirstOrganizationOfType(organization, careGiverOrgType);
            final Organization careUnitOrg = findFirstOrganizationOfType(organization, careUnitOrgType);
            assert (careGiverOrg != null && careUnitOrg != null);
            assertIdentifier(CodeSystem.HSA.getValue(), hctCareUnit, careUnitOrg.getIdentifierFirstRep());
            assertIdentifier(CodeSystem.HSA.getValue(), hctCareGiver, careGiverOrg.getIdentifierFirstRep());

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private static Organization findFirstOrganizationOfType(final Organization organization, final CodeableConcept orgType) {
        Organization retval = organization;
        while (retval != null) {
            if (retval.getType().stream().anyMatch(cc -> cc.equalsDeep(orgType))) {
                return retval;
            }
            retval = (Organization) retval.getPartOf().getResource();
        }
        return null;
    }

    public static void assertAccountableHealthcareProfessionalWithoutRole(final Object healthcareProfessionalType, final Practitioner practitioner) {
        try {
            final String hctHsaid = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalHSAId");
            final String hctName = (String) PropertyUtils.getProperty(healthcareProfessionalType, "healthcareProfessionalName");

            assertIdentifier(CodeSystem.HSA.getValue(), hctHsaid, practitioner.getIdentifierFirstRep());
            assertEquals(hctName, practitioner.getName().get(0).getText());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertOrgUnitMapping(final Object orgUnitType, final Organization organization) {
        try {
            final String orgHsaId = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitHSAId");
            final String orgUnitLocation = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitLocation");
            final String orgUnitName = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitName");
            final String orgUnitAddress = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitAddress");
            final String orgTelecom = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitTelecom");
            final String orgEmail = (String) PropertyUtils.getProperty(orgUnitType, "orgUnitEmail");
            assertEquals(CodeSystem.HSA.getValue(), organization.getIdentifier().get(0).getSystem());
            assertEquals(orgHsaId, organization.getIdentifier().get(0).getValue());
            assertEquals(orgUnitLocation, organization.getAddress().get(0).getCity());
            assertEquals(orgUnitName, organization.getName());
            assertEquals(orgUnitAddress, organization.getAddress().get(0).getText());

            for (final ContactPoint cp : organization.getTelecom()) {
                if (cp.getSystemElement().getValue().equals(ContactPoint.ContactPointSystem.EMAIL)) {
                    assertEquals(orgEmail, cp.getValue());
                }
                if (cp.getSystemElement().getValue().equals(ContactPoint.ContactPointSystem.PHONE)) {
                    assertEquals(orgTelecom, cp.getValue());
                }
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public static void assertIdentifier(final String system, final String value, final Identifier identifier) {
        assertEquals(system, identifier.getSystem());
        assertEquals(value, identifier.getValue());
    }

    public static void assertCoding(final Object cvType, final Coding coding, final String system) {
        assertTrue(codingEquals(cvType, coding, system));
    }

    public static boolean codingEquals(final Object cvType, final Coding coding, final String system) {
        try {
            final String code = (String) PropertyUtils.getProperty(cvType, "code");
            final String displayName = (String) PropertyUtils.getProperty(cvType, "displayName");
            final String codeSystemVersion = (String) PropertyUtils.getProperty(cvType, "codeSystemVersion");
            if (!code.equals(coding.getCode())) {
                return false;
            }
            if (!system.equals(coding.getSystem())) {
                return false;
            }
            if (!displayName.equals(coding.getDisplay())) {
                return false;
            }
            return codeSystemVersion.equals(coding.getVersion());
        } catch (Exception e) {
            fail(e.getMessage());
            return false;
        }
    }

    public static boolean codingEquals(final Object cvType, final Coding coding) {
        try {
            final String system = (String) PropertyUtils.getProperty(cvType, "codeSystem");
            return codingEquals(cvType, coding, String.format("urn:oid:%s", system));
        } catch (Exception e) {
            fail(e.getMessage());
        }
        return false;
    }

    public static void assertCoding(final String system, final String code, final String display, final Coding coding) {
        assertEquals(system, coding.getSystem());
        assertEquals(code, coding.getCode());
        assertEquals(display, coding.getDisplay());
    }

    public static void assertCoding(final String system, final String code, final String display, final String version, final Coding coding) {
        assertCoding(system, code, display, coding);
        assertEquals(coding.getVersion(), version);
    }

    public static void assertCoding(final Object cvType, final Coding coding) {
        assertTrue(codingEquals(cvType, coding));
    }
}
