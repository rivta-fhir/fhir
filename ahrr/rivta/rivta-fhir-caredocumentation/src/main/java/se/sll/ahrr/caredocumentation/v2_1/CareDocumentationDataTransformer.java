package se.sll.ahrr.caredocumentation.v2_1;

/*-
 * #%L
 * rivta-fhir-caredocumentation
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.service.DocBookParser;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.*;
import java.util.stream.Collectors;

import static org.hl7.fhir.dstu3.model.Narrative.NarrativeStatus;

//note: multimedia entry is not mapped.
@Service
public class CareDocumentationDataTransformer implements Riv2FhirTransformer<List<CareDocumentationType>, Composition>
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private DocBookParser docBookParser = new DocBookParser();

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Composition.class;
    }

    @Override
    public List<Composition> transformToFhir(List<CareDocumentationType> rivResponse) throws Exception {
        return rivResponse.stream().map(this::mapCareDocument).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private Composition mapCareDocument(CareDocumentationType careDocument) {
        try {
            Composition document = new Composition();
            mapHeader(document, careDocument.getCareDocumentationHeader());
            mapBody(document, careDocument.getCareDocumentationBody());
            return document;
        } catch (Exception e) {
            log.error("Unexpected error while mapping careDocument", e);
            return null;
        }
    }

    private void mapBody(final Composition composition, final CareDocumentationBodyType body) {
        final String title = body.getClinicalDocumentNote().getClinicalDocumentNoteTitle();
        if (title != null && !title.isEmpty()) {
            composition.setTitle(title);
        } else {
            composition.setTitle("- Titel saknas -");
        }


        if (body.getClinicalDocumentNote().getClinicalDocumentNoteText() != null) {
            try {
                Composition.SectionComponent section = new Composition.SectionComponent();
                final Narrative narrative = new Narrative();
                narrative.setStatus(NarrativeStatus.ADDITIONAL);
                narrative.setDiv(new XhtmlNode(NodeType.Element).setValue(docBookParser.toHtml(body.getClinicalDocumentNote().getClinicalDocumentNoteText())));
                section.setText(narrative);
                composition.addSection(section);
            } catch (DocBookParser.ParsingException e) {
                log.error("Error while parsing ClinicalDocumentNoteText xml, skipping", e);
            } catch (Exception e) {
                //XhtmlNode does internal validation of xml.
                log.error("Unexpected error while setting narrative from clinicalDocumenationNoteText, xml might be malformed.", e);
            }
        }

        if (body.getClinicalDocumentNote().getDissentintOpinion() != null) {
            for (DissentingOpinionType opinion : body.getClinicalDocumentNote().getDissentintOpinion()) {
                Composition.SectionComponent section = new Composition.SectionComponent();
                final Narrative narrative = new Narrative();
                narrative.setStatus(NarrativeStatus.ADDITIONAL);

                narrative.setDiv(new XhtmlNode().setValue(String.format("Avvikande åsikt från %s med ID %s enligt: %s", opinion.getPersonName(), opinion.getPersonId().getId(), opinion.getOpinion())));

                section.setText(narrative);
                section.setTitle("Dissenting opinion");
                section.getEntry().add(Riv2FhirTransformationHelper.getPatientReference(opinion.getPersonId().getId()));
                composition.addSection(section);
            }
        }

        if (body.getClinicalDocumentNote().getClinicalDocumentTypeCode() != null) {
            composition.setType(getType(body.getClinicalDocumentNote().getClinicalDocumentTypeCode().value()));
        } else if (body.getClinicalDocumentNote().getClinicalDocumentNoteCode() != null) {
            composition.setType(getType(body.getClinicalDocumentNote().getClinicalDocumentNoteCode().value()));
        }
    }


    private void mapHeader(final Composition composition, final CPatientSummaryHeaderType header) {
        Identifier id = new Identifier();
        id.setSystem(header.getSourceSystemHSAid());
        id.setValue(header.getDocumentId());
        composition.setIdentifier(id);
        composition.setId(Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAid(), header.getDocumentId()));
          composition.setDate(Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()).getValue());
        composition.setCustodian(new Reference(getCustodian(header.getAccountableHealthcareProfessional()))); //new

        List<Reference> authors = new LinkedList<>();
        composition.setAuthor(authors);
        authors.add(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional()));

//        if (null != header.getAccountableHealthcareProfessional() && null != header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit())
//        {
//            Organization authorOrganization = new Organization();
//            authorOrganization.addIdentifier()
//                    .setSystem(CodeSystem.HSA.getValue())
//                    .setValue(header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit().getOrgUnitHSAId());
//            authorOrganization.setName(header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit().getOrgUnitName());
//            authors.add(new Reference(authorOrganization));
//        }

        if (header.getCareContactId() != null) {
            Encounter encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId());
            encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);

            if (header.getAccountableHealthcareProfessional() != null) {
                encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            }
            composition.setEncounter(new Reference(encounter));

        } else {
            log.trace("Skip mapping encounter since RIV careContactId was null");
        }


        if (header.getLegalAuthenticator() != null) {
            composition.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
            Composition.CompositionAttesterComponent attester = new Composition.CompositionAttesterComponent();
            try {
                String rivtime = header.getLegalAuthenticator().getSignatureTime();
                if (rivtime != null && !rivtime.isEmpty()) {
                    DateTimeType dateTime = Riv2FhirTransformationHelper.getDateTime(rivtime);
                    attester.setTime(dateTime.getValue());
                } else {
                    log.debug("SignatureTime is missing. skipping.");
                }
            } catch (Exception e) {
                log.error("Couldn't parse signature time", e);
            }

            attester.setParty(Riv2FhirTransformationHelper.getPractitionerReferenceFromLegalAuthenticator(header.getLegalAuthenticator()));
            attester.addMode(Composition.CompositionAttestationMode.PROFESSIONAL);
            composition.getAttester().add(attester);

        } else {
            log.debug("LegalAuthenticator was null");
        }

        if (header.getPatientId() != null) {
            composition.setSubject(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId()));
        }
        composition.setStatus(Composition.CompositionStatus.FINAL);
    }

    private Organization getCustodian(final HealthcareProfessionalType accountableHct) {
        try
        {
            final Organization organization = new Organization();

            if (null != accountableHct.getHealthcareProfessionalOrgUnit() &&
                    null != accountableHct.getHealthcareProfessionalOrgUnit().getOrgUnitHSAId())
            {
                organization.addIdentifier()
                        .setSystem(CodeSystem.HSA.getValue())
                        .setValue(accountableHct.getHealthcareProfessionalOrgUnit().getOrgUnitHSAId());

                organization.setName(accountableHct.getHealthcareProfessionalOrgUnit().getOrgUnitName());

                final Organization partOf = new Organization();
                partOf.addIdentifier()
                        .setSystem(CodeSystem.HSA.getValue())
                        .setValue(accountableHct.getHealthcareProfessionalCareGiverHSAId());

                organization.setPartOf(new Reference(partOf));
            }

            else if (null != accountableHct.getHealthcareProfessionalCareGiverHSAId())
            {
                organization.addIdentifier()
                        .setSystem(CodeSystem.HSA.getValue())
                        .setValue(accountableHct.getHealthcareProfessionalCareGiverHSAId());
            }

            return organization;
        }

        catch (Exception e)
        {
            log.error("Unexpected error while mapping custodian", e);
            return new Organization();
        }
    }

    private static Map<String, String> documentCodeMap = ImmutableMap.<String, String>builder()
            .put("utr", "Utredning")
            .put("atb", "Åtgärd/Behandling")
            .put("sam", "Sammanfattning")
            .put("sao", "Samordning")
            .put("ins", "Inskrivning")
            .put("slu", "Slutanteckning")
            .put("auf", "Anteckning utan fysiskt möte")
            .put("sva", "Slutenvårdsanteckning")
            .put("bes", "Besöksanteckning")
            .put("epi", "Epikris")
            .put("int", "Intagninganteckning")
            .put("dag", "Daganteckning")
            .put("ova", "Öppenvårdsanteckning")
            .put("ovs", "Öppenvårdssammanfattning")
            .put("ovr", "Övrigt dokument").build();


    private CodeableConcept getType(String code) {
        CodeableConcept type = new CodeableConcept();
        Coding cdt = new Coding();
        cdt.setCode(code);
        cdt.setSystem(CodeSystem.DOCUMENT_TYPE.getValueWithoutNamespace());
        cdt.setDisplay(documentCodeMap.getOrDefault(code, "Övrigt dokument"));
        type.addCoding(cdt);

        return type;
    }
}
