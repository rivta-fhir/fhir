package se.sll.ahrr.caredocumentation.v2_1.dataloader;

/*-
 * #%L
 * rivta-fhir-caredocumentation
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.PersonIdType;
import riv.clinicalprocess.healthcond.description.getcaredocumentation._2.rivtabp21.GetCareDocumentationResponderInterface;
import riv.clinicalprocess.healthcond.description.getcaredocumentation._2.rivtabp21.GetCareDocumentationResponderService;
import riv.clinicalprocess.healthcond.description.getcaredocumentationresponder._2.GetCareDocumentationResponseType;
import riv.clinicalprocess.healthcond.description.getcaredocumentationresponder._2.GetCareDocumentationType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.LinkedList;
import java.util.List;

@Service
@CacheConfig(cacheNames = {"rivta.caredocumentation"})
public class CareDocumentationDataLoader implements RivtaDataLoader<GetCareDocumentationResponseType>
{

    private List<GetCareDocumentationResponderInterface> ports;

    @Value("${contracts.getCareDocumentation.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public CareDocumentationDataLoader(
        @Value("#{'${contracts.getCareDocumentation.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout,
        @Value("${ahrr.client.log.payloads:false}") Boolean logPayload
    ) {

        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetCareDocumentationResponderService service = new GetCareDocumentationResponderService();
            GetCareDocumentationResponderInterface port = service.getGetCareDocumentationResponderPort();

            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            if(logPayload) {
                client.getInInterceptors().add(new LoggingInInterceptor());
                client.getOutInterceptors().add(new LoggingOutInterceptor());
            }

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }

    }

    @Override
    @Cacheable
    public GetCareDocumentationResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetCareDocumentationResponseType getCareDocumentationResponseType = new GetCareDocumentationResponseType();

            final GetCareDocumentationType params = new GetCareDocumentationType();
            final PersonIdType personIdType = new PersonIdType();
            personIdType.setId(clientId);
            personIdType.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personIdType);

            for (GetCareDocumentationResponderInterface port : ports)
            {
                GetCareDocumentationResponseType response = port.getCareDocumentation(logicalAddress, params);
                getCareDocumentationResponseType.getCareDocumentation().addAll(response.getCareDocumentation());
            }

            return getCareDocumentationResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {

    }

    @Override
    public Class<GetCareDocumentationResponseType> getRivtaResponseType() {
        return GetCareDocumentationResponseType.class;
    }
}
