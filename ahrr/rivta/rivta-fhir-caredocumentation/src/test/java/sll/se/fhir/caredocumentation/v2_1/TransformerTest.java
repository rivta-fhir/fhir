package sll.se.fhir.caredocumentation.v2_1;

/*-
 * #%L
 * rivta-fhir-caredocumentation
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.utilities.xhtml.NodeType;
import org.hl7.fhir.utilities.xhtml.XhtmlNode;
import org.junit.BeforeClass;
import org.junit.Test;
import riv.clinicalprocess.healthcond.description._2.*;
import riv.clinicalprocess.healthcond.description.enums._2.ClinicalDocumentNoteCodeEnum;
import riv.clinicalprocess.healthcond.description.enums._2.ClinicalDocumentTypeCodeEnum;
import riv.clinicalprocess.healthcond.description.enums._2.MediaTypeEnum;
import riv.clinicalprocess.healthcond.description.getcaredocumentationresponder._2.GetCareDocumentationResponseType;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.caredocumentation.v2_1.CareDocumentationDataTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.DocBookParser;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class TransformerTest {
    private final MockFactory mockFactory = new MockFactory();
    private final ObjectFactory rivtaObjectFactory = new ObjectFactory();

    private final static Map<String, String> documentNoteCodes = new HashMap<>();
    private final static Map<String, String> documentTypeCodes = new HashMap<>();

    @BeforeClass
    public static void init() {
        documentNoteCodes.put("utr", "Utredning");
        documentNoteCodes.put("atb", "Åtgärd/Behandling");
        documentNoteCodes.put("sam", "Sammanfattning");
        documentNoteCodes.put("sao", "Samordning");
        documentNoteCodes.put("ins", "Inskrivning");
        documentNoteCodes.put("slu", "Slutanteckning");
        documentNoteCodes.put("auf", "Anteckning utan fysiskt möte");
        documentNoteCodes.put("sva", "Slutenvårdsanteckning");
        documentNoteCodes.put("bes", "Besöksanteckning");

        documentTypeCodes.put("epi", "Epikris");
        documentTypeCodes.put("int", "Intagninganteckning");
        documentTypeCodes.put("dag", "Daganteckning");
        documentTypeCodes.put("ova", "Öppenvårdsanteckning");
        documentTypeCodes.put("ovs", "Öppenvårdssammanfattning");
        documentTypeCodes.put("ovr", "Övrigt dokument");

    }


    @Test
    public void verifyComposition() {
        try {
            final CPatientSummaryHeaderType header = createHeader();
            final CareDocumentationBodyType body = createBody();
            final GetCareDocumentationResponseType getCareDocumentationResponseType = new GetCareDocumentationResponseType();
            final CareDocumentationType careDocumentationType = new CareDocumentationType();
            careDocumentationType.setCareDocumentationHeader(header);
            careDocumentationType.setCareDocumentationBody(body);
            getCareDocumentationResponseType.getCareDocumentation().add(careDocumentationType);

            Composition composition = new CareDocumentationDataTransformer().transformToFhir(getCareDocumentationResponseType.getCareDocumentation()).get(0);
            assertNotNull(composition);
            assertHeader(header, composition);
            assertBody(body, composition);

        } catch (Exception e) {
            e.printStackTrace();
            assert (false);
        }
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void verifyDocumentationType() throws Exception {
        final CPatientSummaryHeaderType header = createHeader();
        final CareDocumentationBodyType body = createBody();
        final GetCareDocumentationResponseType getCareDocumentationResponseType = new GetCareDocumentationResponseType();
        final CareDocumentationType careDocumentationType = new CareDocumentationType();

        for (String key : documentNoteCodes.keySet()) {
            body.getClinicalDocumentNote().setClinicalDocumentNoteCode(ClinicalDocumentNoteCodeEnum.fromValue(key));
            body.getClinicalDocumentNote().setClinicalDocumentTypeCode(null);
            careDocumentationType.setCareDocumentationHeader(header);
            careDocumentationType.setCareDocumentationBody(body);
            getCareDocumentationResponseType.getCareDocumentation().add(careDocumentationType);
            Composition composition = new CareDocumentationDataTransformer().transformToFhir(getCareDocumentationResponseType.getCareDocumentation()).get(0);
            assertEquals(composition.getType().getCodingFirstRep().getDisplay(), documentNoteCodes.get(key));
            assertEquals(composition.getType().getCodingFirstRep().getCode(), key);

        }

        for (String key : documentTypeCodes.keySet()) {
            body.getClinicalDocumentNote().setClinicalDocumentNoteCode(null);
            body.getClinicalDocumentNote().setClinicalDocumentTypeCode(ClinicalDocumentTypeCodeEnum.fromValue(key));
            careDocumentationType.setCareDocumentationHeader(header);
            careDocumentationType.setCareDocumentationBody(body);
            getCareDocumentationResponseType.getCareDocumentation().add(careDocumentationType);
            Composition composition = new CareDocumentationDataTransformer().transformToFhir(getCareDocumentationResponseType.getCareDocumentation()).get(0);
            assertEquals(composition.getType().getCodingFirstRep().getDisplay(), documentTypeCodes.get(key));
            assertEquals(composition.getType().getCodingFirstRep().getCode(), key);
        }
    }

    private void assertDissentingOpinion(final CareDocumentationBodyType body, final Composition composition) {
        int opinionCount = 0;
        for (Composition.SectionComponent section : composition.getSection()) {
            if (section.getTitle() != null && section.getTitle().equalsIgnoreCase("Dissenting Opinion")) {
                final DissentingOpinionType opinion = body.getClinicalDocumentNote().getDissentintOpinion().get(opinionCount);

                final Narrative narrative = new Narrative();
                narrative.setDiv(new XhtmlNode().setValue(opinion.getOpinion()));
                narrative.setStatus(Narrative.NarrativeStatus.ADDITIONAL);
                assertEquals(section.getText().getStatus(), Narrative.NarrativeStatus.ADDITIONAL);
                assertTrue(section.getText().getDiv().toString().contains(opinion.getPersonName()));
                assertTrue(section.getText().getDiv().toString().contains(opinion.getPersonId().getId()));
                assertTrue(section.getText().getDiv().toString().contains(opinion.getOpinion()));
                assertEquals(((Patient) section.getEntry().get(0).getResource()).getIdentifierFirstRep().getValue(), opinion.getPersonId().getId());
                opinionCount++;
            }
        }
        assert (opinionCount == body.getClinicalDocumentNote().getDissentintOpinion().size());
    }

    private void assertHeader(final CPatientSummaryHeaderType header, final Composition composition) {
        Assertions.assertIdentifier(header.getSourceSystemHSAid(), header.getDocumentId(), composition.getIdentifier());
        assertEquals(header.getPatientId().getId(), ((Patient) composition.getSubject().getResource()).getIdentifier().get(0).getValue());
        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), (Practitioner) composition.getAuthor().get(0).getResource());
        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) ((Encounter) composition.getEncounter().getResource()).getServiceProvider().getResource());
        Assertions.assertLegalAuthenticatorPractioner(header.getLegalAuthenticator(), (Practitioner) composition.getAttester().get(0).getParty().getResource());
        assertEquals(composition.getAttester().get(0).getTime().getTime(), Riv2FhirTransformationHelper.getDateTime(header.getLegalAuthenticator().getSignatureTime()).getValue().getTime());
        assertEquals(Riv2FhirTransformationHelper.getDateTime(header.getLegalAuthenticator().getSignatureTime()).getValueAsString(), composition.getAttesterFirstRep().getTimeElement().getValueAsString());
        assertEquals(composition.getAttester().get(0).getMode().get(0).getValue(), Composition.CompositionAttestationMode.PROFESSIONAL);
        assertEquals(header.getCareContactId(), ((Encounter) composition.getEncounter().getResource()).getIdentifier().get(0).getValue());
        assertEquals("1.2.752.129.2.2.2.11", composition.getType().getCoding().get(0).getSystem());
        Assertions.assertIdentifier(CodeSystem.HSA.getValue(), header.getAccountableHealthcareProfessional().getHealthcareProfessionalOrgUnit().getOrgUnitHSAId(), ((Organization) composition.getCustodian().getResource()).getIdentifierFirstRep());

        assertEquals(composition.getDate(), Riv2FhirTransformationHelper.getDateTime(header.getAccountableHealthcareProfessional().getAuthorTime()).getValue());

    }

    private void assertBody(final CareDocumentationBodyType body, final Composition composition) {

        HashMap<String, String> typeAndNoteCodes = new HashMap<>();
        typeAndNoteCodes.putAll(documentNoteCodes);
        typeAndNoteCodes.putAll(documentTypeCodes);

        String codeValue = null;

        assert typeAndNoteCodes.keySet().stream().anyMatch(key -> key.equals(composition.getType().getCodingFirstRep().getCode()));
        for (String key : typeAndNoteCodes.keySet()) {
            if (key.equals(composition.getType().getCodingFirstRep().getCode())) {
                codeValue = typeAndNoteCodes.get(key);
                break;
            }
        }
        assertNotNull(codeValue);
        assertEquals(composition.getType().getCodingFirstRep().getDisplay(), codeValue);

        assertEquals("1.2.752.129.2.2.2.11", composition.getType().getCodingFirstRep().getSystem());
        assertEquals(body.getClinicalDocumentNote().getClinicalDocumentNoteTitle(), composition.getTitle());

        final DocBookParser docBookParser = new DocBookParser();
        final Narrative narrative = new Narrative();
        narrative.setStatus(Narrative.NarrativeStatus.ADDITIONAL);
        try {
            narrative.setDiv(new XhtmlNode(NodeType.Element).setValue(docBookParser.toHtml(body.getClinicalDocumentNote().getClinicalDocumentNoteText())));
        } catch (DocBookParser.ParsingException e) {
            fail("DockBookParser error");
        }

        assertEquals(composition.getSection().get(0).getText().getDiv().toString(), narrative.getDiv().toString());
        assertDissentingOpinion(body, composition);
    }

    private CPatientSummaryHeaderType createHeader() throws Exception {

        final CPatientSummaryHeaderType header = rivtaObjectFactory.createCPatientSummaryHeaderType();
        mockFactory.createPatientSummaryHeaderType(
            new HealthcareProfessionalType(),
            new OrgUnitType(),
            new CVType(),
            new LegalAuthenticatorType(),
            new PersonIdType(),
            header);
        return header;
    }

    private CareDocumentationBodyType createBody() throws Exception {
        final CareDocumentationBodyType body = rivtaObjectFactory.createCareDocumentationBodyType();
        body.setClinicalDocumentNote(createClinicalDocumentNote(ClinicalDocumentNoteCodeEnum.ATB, ClinicalDocumentTypeCodeEnum.EPI));
        return body;
    }

    private ClinicalDocumentNoteType createClinicalDocumentNote(final ClinicalDocumentNoteCodeEnum documentNoteCodeEnum, final ClinicalDocumentTypeCodeEnum documentTypeCodeEnum) throws Exception {
        final ClinicalDocumentNoteType note = rivtaObjectFactory.createClinicalDocumentNoteType();
        note.setClinicalDocumentNoteCode(documentNoteCodeEnum);
        note.setClinicalDocumentNoteText("Note text");
        note.setClinicalDocumentNoteTitle("Title text");
        note.setClinicalDocumentTypeCode(documentTypeCodeEnum);
        note.setMultimediaEntry(createMultiMediaType());
        note.getDissentintOpinion().add(createDissentingOpinionType());
        return note;
    }

    private MultimediaType createMultiMediaType() {
        final MultimediaType media = rivtaObjectFactory.createMultimediaType();
        media.setMediaType(MediaTypeEnum.APPLICATION_PDF);
        media.setReference("Reference String");
        media.setValue(ByteBuffer.allocate(Integer.BYTES).putInt(0x0C0DE101).array());
        return media;
    }

    private DissentingOpinionType createDissentingOpinionType() throws Exception {
        final DissentingOpinionType dissentingOpinionType = rivtaObjectFactory.createDissentingOpinionType();
        dissentingOpinionType.setAuthorTime(mockFactory.dateTimeStr);
        dissentingOpinionType.setOpinion("Opinion");
        dissentingOpinionType.setOpinionId((IIType) mockFactory.createIIType(new IIType()));
        dissentingOpinionType.setPersonId((PersonIdType) mockFactory.createPersonIdType(new PersonIdType()));
        dissentingOpinionType.setPersonName("PersonName");
        return dissentingOpinionType;
    }
}
