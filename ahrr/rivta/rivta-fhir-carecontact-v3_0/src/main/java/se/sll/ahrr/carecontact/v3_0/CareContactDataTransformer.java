package se.sll.ahrr.carecontact.v3_0;

/*-
 * #%L
 * rivta-fhir-carecontact-v3_0
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.model.dstu2.valueset.EncounterClassEnum;
import org.apache.coyote.ActionCode;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._3.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service("careContactDataTransformerV3_0")
public class CareContactDataTransformer implements Riv2FhirTransformer<List<CareContactType>, Encounter>
{
    private static final Logger log = LoggerFactory.getLogger(CareContactDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType()
    {
        return Encounter.class;
    }

    @Override
    public List<Encounter> transformToFhir(List<CareContactType> response) throws Exception
    {
        final List<Encounter> encounters = new LinkedList<>();

        response.forEach(careContactType -> {

            // Skip mapping the careContactType if either header or body is null
            if (null == careContactType.getCareContactHeader() || null == careContactType.getCareContactBody())
            {
                log.debug("Skip mapping careContact since header or body was null");
                return;
            }

            final Encounter encounter = new Encounter();

            if (careContactType.getCareContactHeader().getLegalAuthenticator() != null)
            {
                encounter.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(careContactType.getCareContactHeader().getLegalAuthenticator()));
            }

            /* Map header */
            PatientSummaryHeaderType header = careContactType.getCareContactHeader();

            // Id
            IdType idType = new IdType(Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId()));
            encounter.setId(idType);

            // Identifier
            Identifier identifier = new Identifier();
            identifier.setSystem(header.getSourceSystemHSAId());
            identifier.setValue(header.getDocumentId());
            encounter.addIdentifier(identifier);

            // Subject
            if (null != header.getPatientId() && null != header.getPatientId().getId())
            {
                encounter.setSubject(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId()));
            }

            // Practitioner and ServiceProvider
            if (null != header.getAccountableHealthcareProfessional())
            {
                encounter.getParticipantFirstRep().setIndividual(
                        Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional())
                );
                encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            }

            /* Map body */
            CareContactBodyType body = careContactType.getCareContactBody();

            // Reason
            encounter.getReasonFirstRep().setText(body.getCareContactReason());

            // Period
            if (null != body.getCareContactTimePeriod() && null != body.getCareContactTimePeriod().getStart())
            {
                encounter.getPeriod().setStart(transformDateTime(body.getCareContactTimePeriod().getStart()));
            }
            if (null != body.getCareContactTimePeriod() && null != body.getCareContactTimePeriod().getEnd())
            {
                encounter.getPeriod().setEnd(transformDateTime(body.getCareContactTimePeriod().getEnd()));
            }

            if (null != body.getCareContactCode())
            {
                Coding coding = new Coding();
                coding.setSystem(body.getCareContactCode().getCodeSystem());
                coding.setCode(body.getCareContactCode().getCode());

                if ("1.2.752.129.2.2.2.25".equals(body.getCareContactCode().getCodeSystem()))
                {
                    switch (body.getCareContactCode().getCode())
                    {
                        case "1":
                            coding.setDisplay("Besök");
                            break;
                        case "2":
                            coding.setDisplay("Telefon");
                            break;
                        case "3":
                            coding.setDisplay("Vårdtillfälle");
                            break;
                        case "4":
                            coding.setDisplay("Dagsjukvård");
                            break;
                        case "5":
                            coding.setDisplay("Annan");
                            break;
                        default:
                            break;
                    }
                }

                encounter.addType().addCoding(coding);
            }

            // Status
            // SnomedCT - 53761000052103
            switch (body.getCareContactStatus().getCode())
            {
                case "avbruten vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.CANCELLED);
                    break;

                case "avslutad vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.FINISHED);
                    break;

                case "inställd vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.CANCELLED);
                    break;

                case "makulerad vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.CANCELLED);
                    break;

                case "patient på väntelista":
                    encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);
                    break;

                case "permission från vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.ONLEAVE);
                    break;

                case "pågående vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.INPROGRESS);
                    break;

                case "tidbokad vårdkontakt":
                    encounter.setStatus(Encounter.EncounterStatus.PLANNED);
                    break;

                // Safeguard, should never happen
                default:
                    encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);
                    break;
            }

            if (null != body.getCareContactOrgUnit())
            {
                Organization organization = (Organization) Riv2FhirTransformationHelper.getOrganizationReference(
                        body.getCareContactOrgUnit().getOrgUnitName(),
                        body.getCareContactOrgUnit().getOrgUnitHSAId(),
                        body.getCareContactOrgUnit().getOrgUnitAddress(),
                        body.getCareContactOrgUnit().getOrgUnitLocation(),
                        body.getCareContactOrgUnit().getOrgUnitTelecom(),
                        body.getCareContactOrgUnit().getOrgUnitEmail()
                ).getResource();

                encounter.setServiceProvider(new Reference(organization));
            }

            // Patient Date of birth
            if (null != body.getAdditionalPatientInformation() && null != body.getAdditionalPatientInformation().getDateOfBirth() && null != encounter.getSubject())
            {
                try
                {
                    Patient patient = (Patient) encounter.getSubject().getResource();
                    PartialDateType dateOfBirth = body.getAdditionalPatientInformation().getDateOfBirth();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Date birthDate = sdf.parse(dateOfBirth.getValue());
                    patient.setBirthDate(birthDate);
                }
                catch (Exception e)
                {
                    log.debug("Could not parse birthdate");
                }
            }

            // Patient Gender
            if (null != body.getAdditionalPatientInformation() && null != body.getAdditionalPatientInformation().getGender() && null != encounter.getSubject())
            {
                Patient patient = (Patient) encounter.getSubject().getResource();
                switch (body.getAdditionalPatientInformation().getGender().getCode())
                {
                    case "0":
                        patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
                        break;

                    case "1":
                        patient.setGender(Enumerations.AdministrativeGender.MALE);
                        break;

                    case "2":
                        patient.setGender(Enumerations.AdministrativeGender.FEMALE);
                        break;

                    case "9":
                        patient.setGender(Enumerations.AdministrativeGender.NULL);
                        break;

                    default:
                        patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
                        break;
                }

            }

                // Add our encounter to the collection of encounters
            encounters.add(encounter);
        });

        return encounters;
    }

    public java.util.Date transformDateTime(final String rivtaDate)
    {
        return DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(rivtaDate).toDate();
    }
}
