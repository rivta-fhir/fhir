package se.sll.ahrr.carecontact.v3_0;

/*-
 * #%L
 * rivta-fhir-carecontact-v3_0
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import riv.clinicalprocess.logistics.logistics._3.*;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class CareContactDataTransformerTest {

    private final MockFactory mockFactory = new MockFactory();

    private CareContactDataTransformer careContactDataTransformer;

    @Before
    public void init()
    {
        careContactDataTransformer = new CareContactDataTransformer();
    }

    private CareContactType baseCareContactType()
    {
        CareContactType careContactType = new CareContactType();

        /* Header */
        careContactType.setCareContactHeader(new PatientSummaryHeaderType());
        PatientSummaryHeaderType header = careContactType.getCareContactHeader();

        // Document id
        header.setDocumentId("DOCUMENT ID");

        // Source system id
        header.setSourceSystemHSAId("SOURCE SYSTEM ID");

        // Person id
        PersonIdType person = new PersonIdType();
        person.setId("SOME PERSON ID");
        person.setType(CodeSystem.PERSONID_SE.getValue());
        header.setPatientId(person);

        /* Body */
        careContactType.setCareContactBody(new CareContactBodyType());
        CareContactBodyType body = careContactType.getCareContactBody();

        // Reason
        body.setCareContactReason("SOME REASON FOR THE CARE CONTACT");

        // Care contact code
        CVType cvTypeForCode = new CVType();
        cvTypeForCode.setCode("SOME UNKNOWN CODE");
        body.setCareContactCode(cvTypeForCode);

        // Care contact status
        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("SOME UNKNOWN STATUS");
        body.setCareContactStatus(cvTypeForStatus);

        // Care contact org unit
        OrgUnitType organization = new OrgUnitType();
        organization.setOrgUnitName("SOME ORGANISATION NAME");
        organization.setOrgUnitHSAId("SOME ORGANIZATION HSAID");
        organization.setOrgUnitAddress("SOME ORGANIZATION ADDRESS");
        organization.setOrgUnitLocation("SOME ORGANIZATION UNIT LOCATION");
        organization.setOrgUnitTelecom("SOME ORGANIZATION PHONE");
        organization.setOrgUnitEmail("SOME ORGANIZATION EMAIL");
        body.setCareContactOrgUnit(organization);

        // AdditionalPatientInformation
        AdditionalPatientInformationType additionalPatientInformationType = new AdditionalPatientInformationType();
        CVType gender = new CVType();
        gender.setCode("SOME CODE THAT SHOULD PARSE TO UNKNOWN");
        additionalPatientInformationType.setGender(gender);
        body.setAdditionalPatientInformation(additionalPatientInformationType);

        return careContactType;
    }

    @Test
    public void transformToFhirBaseCareContactTypeTest() throws Exception
    {
        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType())).get(0);

        Patient patient = (Patient) encounter.getSubject().getResource();
        Organization organization = (Organization) encounter.getServiceProvider().getResource();

        // Document
        assert "SOURCE SYSTEM ID".equals(encounter.getIdentifier().get(0).getSystem());
        assert "DOCUMENT ID".equals(encounter.getIdentifier().get(0).getValue());

        assert CodeSystem.PERSONID_SE.getValue().equals(patient.getIdentifier().get(0).getSystem());
        assert "SOME PERSON ID".equals(patient.getIdentifier().get(0).getValue());

        assert "SOME REASON FOR THE CARE CONTACT".equals(encounter.getReasonFirstRep().getText());

        // Organization
        assert "SOME ORGANISATION NAME".equals(organization.getName());
        assert CodeSystem.HSA.getValue().equals(organization.getIdentifier().get(0).getSystem());
        assert "SOME ORGANIZATION HSAID".equals(organization.getIdentifier().get(0).getValue());
        organization.getTelecom().forEach(contactPoint -> {
            if (ContactPoint.ContactPointSystem.EMAIL == contactPoint.getSystem())
            {
                assert "SOME ORGANIZATION EMAIL".equals(contactPoint.getValue());
            }
            else if (ContactPoint.ContactPointSystem.PHONE == contactPoint.getSystem())
            {
                assert "SOME ORGANIZATION PHONE".equals(contactPoint.getValue());
            }
        });
        assert "SOME ORGANIZATION ADDRESS".equals(organization.getAddress().get(0).getText());
        assert "SOME ORGANIZATION UNIT LOCATION".equals(organization.getAddress().get(0).getCity());

        assert Encounter.EncounterStatus.UNKNOWN == encounter.getStatus();

        assert Enumerations.AdministrativeGender.UNKNOWN.getDisplay().equals(patient.getGender().getDisplay());
        assert Enumerations.AdministrativeGender.UNKNOWN.getSystem().equals(patient.getGender().getSystem());
        assert Enumerations.AdministrativeGender.UNKNOWN.getDefinition().equals(patient.getGender().getDefinition());


        assert null == patient.getBirthDate();
    }

    @Test
    public void transformToFhirAdditionalPatientInformationGenderUnknown() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().getGender().setCode("0");
        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        Patient patient = (Patient) encounter.getSubject().getResource();

        assert Enumerations.AdministrativeGender.UNKNOWN.getDisplay().equals(patient.getGender().getDisplay());
        assert Enumerations.AdministrativeGender.UNKNOWN.getSystem().equals(patient.getGender().getSystem());
        assert Enumerations.AdministrativeGender.UNKNOWN.getDefinition().equals(patient.getGender().getDefinition());
    }

    @Test
    public void transformToFhirAdditionalPatientInformationGenderMale() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().getGender().setCode("1");
        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        Patient patient = (Patient) encounter.getSubject().getResource();

        assert Enumerations.AdministrativeGender.MALE.getDisplay().equals(patient.getGender().getDisplay());
        assert Enumerations.AdministrativeGender.MALE.getSystem().equals(patient.getGender().getSystem());
        assert Enumerations.AdministrativeGender.MALE.getDefinition().equals(patient.getGender().getDefinition());
    }

    @Test
    public void transformToFhirAdditionalPatientInformationGenderFemale() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().getGender().setCode("2");
        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        Patient patient = (Patient) encounter.getSubject().getResource();

        assert Enumerations.AdministrativeGender.FEMALE.getDisplay().equals(patient.getGender().getDisplay());
        assert Enumerations.AdministrativeGender.FEMALE.getSystem().equals(patient.getGender().getSystem());
        assert Enumerations.AdministrativeGender.FEMALE.getDefinition().equals(patient.getGender().getDefinition());
    }


    @Test
    public void transformToFhirAdditionalPatientInformationGenderNull() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().getGender().setCode("9");
        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        Patient patient = (Patient) encounter.getSubject().getResource();

        assert Enumerations.AdministrativeGender.NULL.getDisplay().equals(patient.getGender().getDisplay());
        assert Enumerations.AdministrativeGender.NULL.getSystem().equals(patient.getGender().getSystem());
        assert Enumerations.AdministrativeGender.NULL.getDefinition().equals(patient.getGender().getDefinition());
    }


    @Test
    public void transformToFhirAdditionalPatientInformationBirthdate() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        PartialDateType partialDateType = new PartialDateType();
        partialDateType.setValue("20050505");

        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().setDateOfBirth(partialDateType);


        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);
        Patient patient = (Patient) encounter.getSubject().getResource();

        patient.getBirthDate().compareTo(new Date(2005, 5, 5));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        assert 0 == patient.getBirthDate().compareTo(sdf.parse("20050505"));
    }

    @Test
    public void transformToFhirAdditionalPatientInformationBirthdateFailedToParse() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        PartialDateType partialDateType = new PartialDateType();
        partialDateType.setValue("200505");
        baseCareContactType.getCareContactBody().getAdditionalPatientInformation().setDateOfBirth(partialDateType);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);
        Patient patient = (Patient) encounter.getSubject().getResource();
        
        assert null == patient.getBirthDate();
    }

    @Test
    public void transformToFhirBaseStatusAvbrutenVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("avbruten vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.CANCELLED == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusAvslutadVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("avslutad vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.FINISHED == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusInstalldVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("inställd vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.CANCELLED == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusMakuleradVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("makulerad vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.CANCELLED == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusPatientPaVantelista() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("patient på väntelista");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.UNKNOWN == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusPermissionFranVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("permission från vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.ONLEAVE == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusPagaendeVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("pågående vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.INPROGRESS == encounter.getStatus();
    }

    @Test
    public void transformToFhirBaseStatusTidsbokadVardkontakt() throws Exception
    {
        CareContactType baseCareContactType = baseCareContactType();

        CVType cvTypeForStatus = new CVType();
        cvTypeForStatus.setCode("tidbokad vårdkontakt");
        baseCareContactType.getCareContactBody().setCareContactStatus(cvTypeForStatus);

        Encounter encounter = careContactDataTransformer.transformToFhir(Collections.singletonList(baseCareContactType)).get(0);

        assert Encounter.EncounterStatus.PLANNED == encounter.getStatus();
    }


    @Test
    public void transformToFhirWithCareContactTimeFullPeriod() throws Exception
    {
        CareContactType careContactType = baseCareContactType();

        TimePeriodType timePeriodType = new TimePeriodType();
        timePeriodType.setStart("20010101010101");
        timePeriodType.setEnd("20020202020202");
        careContactType.getCareContactBody().setCareContactTimePeriod(timePeriodType);

        List<CareContactType> careContactTypeList = new LinkedList<>();
        careContactTypeList.add(careContactType);

        List<Encounter> encounters = careContactDataTransformer.transformToFhir(careContactTypeList);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

        Date start = encounters.get(0).getPeriod().getStart();
        assert 0 == start.compareTo(sdf.parse("20010101-01:01:01"));

        Date end = encounters.get(0).getPeriod().getEnd();
        assert 0 == end.compareTo(sdf.parse("20020202-02:02:02"));
    }

    @Test
    public void transformToFhirWithCareContactTimeStartPeriod() throws Exception
    {
        CareContactType careContactType = baseCareContactType();

        TimePeriodType timePeriodType = new TimePeriodType();
        timePeriodType.setStart("20030303030303");

        careContactType.getCareContactBody().setCareContactTimePeriod(timePeriodType);

        List<CareContactType> careContactTypeList = new LinkedList<>();
        careContactTypeList.add(careContactType);

        List<Encounter> encounters = careContactDataTransformer.transformToFhir(careContactTypeList);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

        Date start = encounters.get(0).getPeriod().getStart();
        assert 0 == start.compareTo(sdf.parse("20030303-03:03:03"));
        assert null == encounters.get(0).getPeriod().getEnd();
    }

    @Test
    public void transformToFhirWithCareContactTimeEndPeriod() throws Exception
    {
        CareContactType careContactType = baseCareContactType();

        TimePeriodType timePeriodType = new TimePeriodType();
        timePeriodType.setEnd("20040404040404");

        careContactType.getCareContactBody().setCareContactTimePeriod(timePeriodType);

        List<CareContactType> careContactTypeList = new LinkedList<>();
        careContactTypeList.add(careContactType);

        List<Encounter> encounters = careContactDataTransformer.transformToFhir(careContactTypeList);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");

        assert null == encounters.get(0).getPeriod().getStart();

        Date end = encounters.get(0).getPeriod().getEnd();
        assert 0 == end.compareTo(sdf.parse("20040404-04:04:04"));
    }
}
