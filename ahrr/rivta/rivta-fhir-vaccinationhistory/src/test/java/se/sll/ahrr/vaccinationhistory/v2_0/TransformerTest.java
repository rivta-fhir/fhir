package se.sll.ahrr.vaccinationhistory.v2_0;

/*-
 * #%L
 * rivta-fhir-vaccinationhistory
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.Bean;
import riv.clinicalprocess.activityprescription.actoutcome._2.*;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import javax.xml.crypto.dsig.keyinfo.PGPData;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import static org.hl7.fhir.dstu3.model.ContactPoint.ContactPointSystem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by marcus on 2017-02-03.
 */

public class TransformerTest {

    private VaccinationHistoryDataTransformer vaccinationHistoryDataTransformer;

    @Before
    public void init() {
        vaccinationHistoryDataTransformer = new VaccinationHistoryDataTransformer();
    }

    private VaccinationMedicalRecordType baseVaccinationMedicalRecordType()
    {
        VaccinationMedicalRecordType vaccinationMedicalRecordType = new VaccinationMedicalRecordType();

        /* Header */
        vaccinationMedicalRecordType.setVaccinationMedicalRecordHeader(new PatientSummaryHeaderType());
        PatientSummaryHeaderType header = vaccinationMedicalRecordType.getVaccinationMedicalRecordHeader();

        // Document id
        header.setDocumentId("DOCUMENT ID");

        // Source system id
        header.setSourceSystemHSAId("SOURCE SYSTEM ID");

        // Person id
        PersonIdType person = new PersonIdType();
        person.setId("SOME PERSON ID");
        person.setType(CodeSystem.PERSONID_SE.getValue());
        header.setPatientId(person);

        // Accountable
        HealthcareProfessionalType accountableHealthcareProfessional = new HealthcareProfessionalType();
        accountableHealthcareProfessional.setHealthcareProfessionalHSAId("ACCOUNTABLE HEALTHCARE PROFESSIONAL HSAID");
        accountableHealthcareProfessional.setHealthcareProfessionalName("SOME ACCOUNTABLE HEALTHCARE PROFESSIONAL");
        header.setAccountableHealthcareProfessional(accountableHealthcareProfessional);

        /* Body */
        vaccinationMedicalRecordType.setVaccinationMedicalRecordBody(new VaccinationMedicalRecordBodyType());
        VaccinationMedicalRecordBodyType vaccinationMedicalRecordBodyType = vaccinationMedicalRecordType.getVaccinationMedicalRecordBody();

        /* AdministrationRecordType */
        AdministrationRecordType administrationRecordType = new AdministrationRecordType();
        vaccinationMedicalRecordBodyType.getAdministrationRecord().add(administrationRecordType);

        // Anatomical site
        CVType anatomicalSite = new CVType();
        anatomicalSite.setCode("RA");
        anatomicalSite.setCodeSystem("2.16.840.1.113883.5.1052");
        administrationRecordType.setAnatomicalSite(anatomicalSite);

        // Comment administation
        administrationRecordType.setCommentAdministration("SOME COMMENT ADMINISTRATION");

        // Comment prescription
        administrationRecordType.setCommentPrescription("SOME COMMENT PRESCRIPTION");

        // Dose
        DoseType doseType = new DoseType();
        PQType pqType = new PQType();
        pqType.setUnit("ml");
        pqType.setValue(1000);
        doseType.setDisplayName("SOME DOSE DISPLAY");
        doseType.setQuantity(pqType);

        // Is dose complete
        administrationRecordType.setIsDoseComplete(true);

        // Performer
        ActorType performer = new ActorType();
        performer.setHsaid("SOME PERFOMER HSAID");
        administrationRecordType.setPerformer(performer);

        // Prescriber
        ActorType prescriber = new ActorType();
        prescriber.setHsaid("SOME PRESCRIBER HSAID");
        administrationRecordType.setPrescriberPerson(prescriber);

        // Route
        CVType route = new CVType();
        route.setCode("SOME ROUTE CODE");
        route.setCodeSystem("SOME ROUTE CODE SYSTEM");
        administrationRecordType.setRoute(route);

        // Source description
        administrationRecordType.setSourceDescription("SOME SOURCE DESCRIPTION");

        // Type of vaccine
        CVType typeOfVaccine = new CVType();
        typeOfVaccine.setCode("SOME VACCINE CODE");
        typeOfVaccine.setCodeSystem("SOME VACCINE SYSTEM");
        administrationRecordType.setTypeOfVaccine(typeOfVaccine);

        // Vaccine batch id
        administrationRecordType.setVaccineBatchId("SOME BATCH-12345");

        // Manufacturer
        administrationRecordType.setVaccineManufacturer("SOME VACCINE MANUFACTURER");

        // Vaccine name
        CVType vaccineName = new CVType();
        vaccineName.setOriginalText("SOME VACCINE NAME");
        administrationRecordType.setVaccineName(vaccineName);

        /* RegistrationRecordType */
        vaccinationMedicalRecordBodyType.setRegistrationRecord(new RegistrationRecordType());
        RegistrationRecordType registrationRecordType = vaccinationMedicalRecordBodyType.getRegistrationRecord();

        // Care giver contact
        ActorType careGiverContact = new ActorType();
        careGiverContact.setHsaid("SOME HSAID");
        registrationRecordType.setCareGiverContact(careGiverContact);

        // Care giver org
        OrgUnitType orgUnitType = new OrgUnitType();
        orgUnitType.setOrgUnitHSAId("SOME ORGUNIT HSA ID");
        registrationRecordType.setCareGiverOrg(orgUnitType);

        registrationRecordType.setVaccinationUnstructuredNote("SOME UNSTRUCTURED NOTE");

        registrationRecordType.setDate("20110101");

        return vaccinationMedicalRecordType;
    }

    @Test
    public void transformToFhir() throws Exception
    {
        Immunization immunization = vaccinationHistoryDataTransformer.transformToFhir(Collections.singletonList(baseVaccinationMedicalRecordType())).get(0);

        // identifier
        assert immunization.getIdentifierFirstRep().getSystem().equals("SOURCE SYSTEM ID");
        assert immunization.getIdentifierFirstRep().getValue().equals("DOCUMENT ID");

        // status
        assert Immunization.ImmunizationStatus.COMPLETED == immunization.getStatus();

        // notGiven
        assert false == immunization.getNotGiven();

        // vaccineCode
        immunization.getVaccineCode().getText().equals("SOME VACCINE NAME");

        // patient
        assert ((Patient) immunization.getPatient().getResource()).getIdentifierFirstRep().getSystem().equals("urn:oid:1.2.752.129.2.1.3.1");
        assert ((Patient) immunization.getPatient().getResource()).getIdentifierFirstRep().getValue().equals("SOME PERSON ID");

        // date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        assert 0 == immunization.getDate().compareTo(sdf.parse("20110101"));

        // manufacturer
        assert ((Organization) immunization.getManufacturer().getResource()).getName().equals("SOME VACCINE MANUFACTURER");

        // lotNumber
        assert immunization.getLotNumber().equals("SOME BATCH-12345");

        // site
        assert immunization.getSite().getCodingFirstRep().getSystem().equals("urn:oid:2.16.840.1.113883.5.1052");
        assert immunization.getSite().getCodingFirstRep().getCode().equals("RA");

        // route
        assert immunization.getRoute().getCodingFirstRep().getSystem().equals("urn:oid:SOME ROUTE CODE SYSTEM");
        assert immunization.getRoute().getCodingFirstRep().getCode().equals("SOME ROUTE CODE");

        // practitionare
        assert 2 == immunization.getPractitioner().size();

        // note
        assert 4 == immunization.getNote().size();
        assert immunization.getNote().get(0).getText().equals("SOME UNSTRUCTURED NOTE");
        assert immunization.getNote().get(1).getText().equals("SOME SOURCE DESCRIPTION");
        assert immunization.getNote().get(2).getText().equals("SOME COMMENT PRESCRIPTION");
        assert immunization.getNote().get(3).getText().equals("SOME COMMENT ADMINISTRATION");

        return;
    }

}
