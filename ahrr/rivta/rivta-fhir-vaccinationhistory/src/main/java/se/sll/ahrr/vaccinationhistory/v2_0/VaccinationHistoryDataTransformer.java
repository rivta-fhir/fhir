package se.sll.ahrr.vaccinationhistory.v2_0;

/*-
 * #%L
 * rivta-fhir-vaccinationhistory
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.activityprescription.actoutcome._2.PatientSummaryHeaderType;
import riv.clinicalprocess.activityprescription.actoutcome._2.RegistrationRecordType;
import riv.clinicalprocess.activityprescription.actoutcome._2.VaccinationMedicalRecordBodyType;
import riv.clinicalprocess.activityprescription.actoutcome._2.VaccinationMedicalRecordType;
import se.sll.ahrr.Riv2FhirTransformer;

import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

@Service
public class VaccinationHistoryDataTransformer implements Riv2FhirTransformer<List<VaccinationMedicalRecordType>, Immunization> {

    @Override
    public Class getFHIRResourceType()
    {
        return null;
    }

    @Override
    public List<Immunization> transformToFhir(List<VaccinationMedicalRecordType> rivResponse) throws Exception {
        final List<Immunization> immunizations = new LinkedList<>();

        rivResponse.forEach(vaccinationMedicalRecord -> {
            vaccinationMedicalRecord.getVaccinationMedicalRecordBody().getAdministrationRecord().forEach(administrationRecord -> {
                Immunization immunization = new Immunization();

                Encounter encounter = new Encounter();
                immunization.setEncounter(new Reference(encounter));

                PatientSummaryHeaderType header = vaccinationMedicalRecord.getVaccinationMedicalRecordHeader();
                VaccinationMedicalRecordBodyType body = vaccinationMedicalRecord.getVaccinationMedicalRecordBody();
                RegistrationRecordType registration = body.getRegistrationRecord();

                /* Legal Authenticator */
                if (vaccinationMedicalRecord.getVaccinationMedicalRecordHeader().getLegalAuthenticator() != null)
                {
                    immunization.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(vaccinationMedicalRecord.getVaccinationMedicalRecordHeader().getLegalAuthenticator()));
                }

                // Id
                immunization.setId(
                        Riv2FhirTransformationHelper.getId(
                                header.getSourceSystemHSAId(),
                                header.getDocumentId()
                        )
                );

                // Identifier
                Identifier identifier = immunization.addIdentifier();
                identifier.setSystem(header.getSourceSystemHSAId());
                identifier.setValue(header.getDocumentId());

                // Status
                if (null != administrationRecord.isIsDoseComplete() && true == administrationRecord.isIsDoseComplete())
                {
                    immunization.setStatus(Immunization.ImmunizationStatus.COMPLETED);
                }

                // notGiven
                immunization.setNotGiven(false);

                // vaccineCode
                if (null != administrationRecord.getVaccineName())
                {
                    CodeableConcept vaccineCode = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(administrationRecord.getVaccineName());

                    if (null != administrationRecord.getTypeOfVaccine() && null != administrationRecord.getTypeOfVaccine().getDisplayName())
                    {
                        vaccineCode.setText(administrationRecord.getTypeOfVaccine().getDisplayName());
                    }

                    immunization.setVaccineCode(vaccineCode);
                }

                // patient
                if (null != header.getPatientId())
                {
                    Patient patient = Riv2FhirTransformationHelper.getPatient(header.getPatientId().getId());
                    immunization.setPatient(new Reference(patient));

                    Encounter.EncounterParticipantComponent participantComponent = encounter.addParticipant();
                    participantComponent.setIndividual(new Reference(patient));
                }

                // encounter
                encounter.setServiceProvider(
                    Riv2FhirTransformationHelper.getOrganizationReferenceFromOrgUnitType(registration.getCareGiverOrg())
                );

                if (null != registration.getCareGiverContact())
                {
                    encounter.addParticipant().setIndividual(
                            Riv2FhirTransformationHelper.getPractitionerReferenceFromRivtaActor(registration.getCareGiverContact())
                    );
                }

                if (null != header.getCareContactId())
                {
                    encounter.addIdentifier().setValue(header.getCareContactId());
                }

                // date
                immunization.setDate(Riv2FhirTransformationHelper.getDateTimeForDate(registration.getDate()).getValue());

                // manufacturer
                if (null != administrationRecord.getVaccineManufacturer())
                {
                    Organization manufacturer = new Organization();
                    manufacturer.setName(administrationRecord.getVaccineManufacturer());
                    immunization.setManufacturer(new Reference(manufacturer));
                }

                // lotNumber
                if (null != administrationRecord.getVaccineBatchId())
                {
                    immunization.setLotNumber(administrationRecord.getVaccineBatchId());
                }

                // site
                if (null != administrationRecord.getAnatomicalSite())
                {
                    immunization.setSite(
                            Riv2FhirTransformationHelper.getCodeableConceptFromCvType(administrationRecord.getAnatomicalSite())
                    );
                }

                // route
                if (null != administrationRecord.getRoute())
                {
                    immunization.setRoute(
                            Riv2FhirTransformationHelper.getCodeableConceptFromCvType(administrationRecord.getRoute())
                    );
                }

                // doseQuantity
                if (null != administrationRecord.getDose() && null != administrationRecord.getDose().getQuantity()) {
                    SimpleQuantity simpleQuantity = new SimpleQuantity();

                    simpleQuantity.setUnit(administrationRecord.getDose().getQuantity().getUnit());
                    simpleQuantity.setValue(administrationRecord.getDose().getQuantity().getValue());

                    immunization.setDoseQuantity(simpleQuantity);
                }

                // practictioner
                if (null != administrationRecord.getPrescriberPerson())
                {
                    Immunization.ImmunizationPractitionerComponent practitionerComponent = immunization.addPractitioner();
                    practitionerComponent.setRole(getOrderingProviderRole());
                    practitionerComponent.setActor(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivtaActor(administrationRecord.getPrescriberPerson()));
                }

                Practitioner accountableHealthcareProfessional = Riv2FhirTransformationHelper.getPractitionerFromHealthcareProfessionalType(header.getAccountableHealthcareProfessional());

                if (null != accountableHealthcareProfessional)
                {
                    Immunization.ImmunizationPractitionerComponent immunizationPractitionerComponent = immunization.addPractitioner();
                    immunizationPractitionerComponent.setRole(getAdministeringProviderRole());
                    immunizationPractitionerComponent.setActor(new Reference(accountableHealthcareProfessional));

                    Encounter.EncounterParticipantComponent participantComponent = encounter.addParticipant();
                    participantComponent.setIndividual(new Reference(accountableHealthcareProfessional));
                }

                if (
                    null != administrationRecord.getPerformer()
                    && null != administrationRecord.getPerformer().getHsaid()
                    && null != header.getAccountableHealthcareProfessional().getHealthcareProfessionalHSAId()
                    && administrationRecord.getPerformer().getHsaid().equalsIgnoreCase(header.getAccountableHealthcareProfessional().getHealthcareProfessionalHSAId())
                )
                {
                    Reference actorReference = Riv2FhirTransformationHelper.getPractitionerReferenceFromHealthcareProfessionalType(administrationRecord.getPerformer());

                    Immunization.ImmunizationPractitionerComponent immunizationPractitionerComponent = immunization.addPractitioner();
                    immunizationPractitionerComponent.setRole(getAdministeringProviderRole());
                    immunizationPractitionerComponent.setActor(actorReference);

                    Encounter.EncounterParticipantComponent participantComponent = encounter.addParticipant();
                    participantComponent.setIndividual(actorReference);
                }


                // note
                if (null != body.getRegistrationRecord() && null != body.getRegistrationRecord().getVaccinationUnstructuredNote())
                {
                    Annotation annotation = new Annotation();
                    annotation.setText(body.getRegistrationRecord().getVaccinationUnstructuredNote());
                    immunization.addNote(annotation);
                }

                if (null != administrationRecord.getSourceDescription())
                {
                    Annotation annotation = new Annotation();
                    annotation.setText(administrationRecord.getSourceDescription());
                    immunization.addNote(annotation);
                }

                if (null != administrationRecord.getCommentPrescription())
                {
                    Annotation annotation = new Annotation();
                    annotation.setText(administrationRecord.getCommentPrescription());
                    immunization.addNote(annotation);
                }

                if (null != administrationRecord.getCommentAdministration())
                {
                    Annotation annotation = new Annotation();
                    annotation.setText(administrationRecord.getCommentAdministration());
                    immunization.addNote(annotation);
                }

                // reaction
                if (null != administrationRecord.getPatientAdverseEffect())
                {
                    administrationRecord.getPatientAdverseEffect().forEach(cvType -> {
                        Immunization.ImmunizationReactionComponent reaction = new Immunization.ImmunizationReactionComponent();
                        Observation observation = new Observation();
                        observation.setComment(cvType.getDisplayName());
                        reaction.setDetail(new Reference(observation));
                        immunization.addReaction(reaction);
                    });
                }

                immunizations.add(immunization);
            });
        });


        return immunizations;
    }


    public CodeableConcept getAdministeringProviderRole()
    {
        CodeableConcept codeableConcept = new CodeableConcept();

        // Coding
        Coding coding = new Coding();
        coding.setSystem("urn:oid:2.16.840.1.113883.4.642.3.295");
        coding.setCode("AP");
        coding.setDisplay("Administering Provider");

        codeableConcept.addCoding(coding);

        return codeableConcept;
    }

    public CodeableConcept getOrderingProviderRole()
    {
        CodeableConcept codeableConcept = new CodeableConcept();

        // Coding
        Coding coding = new Coding();
        coding.setSystem("urn:oid:2.16.840.1.113883.4.642.3.295");
        coding.setCode("OP");
        coding.setDisplay("Ordering Provider");

        codeableConcept.addCoding(coding);

        return codeableConcept;
    }


}
