package se.sll.ahrr.alertinformation.v2_0;

/*-
 * #%L
 * rivta-fhir-alertinformation
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.model.primitive.IdDt;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceVerificationStatus;
import org.hl7.fhir.dstu3.model.Encounter.EncounterStatus;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import riv.clinicalprocess.healthcond.description._2.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.valueset.flag.Category;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class AlertInformationDataTransformer implements Riv2FhirTransformer<List<AlertInformationType>, Flag> {

    private static final Logger log = LoggerFactory.getLogger(AlertInformationDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Flag.class;
    }

    @Override
    public List<Flag> transformToFhir(List<AlertInformationType> rivResponse) throws Exception {
        final List<Flag> flags = new LinkedList<>();

        for (final AlertInformationType alertInformation : rivResponse) {
            flags.add(mapAlertInformation(alertInformation.getAlertInformationHeader(), alertInformation.getAlertInformationBody()));
        }
        return flags;
    }

    private Flag mapAlertInformation(final PatientSummaryHeaderType header, final AlertInformationBodyType body) {

        //flag.code is mandatory. Is not always set atm. todo
        final Flag flag = new Flag();
        mapAlertInformationHeader(flag, header);
        mapAlertInformationBody(flag, body);
        return flag;
    }

    @SuppressWarnings("Duplicates")
    private void mapAlertInformationHeader(final Flag flag, final PatientSummaryHeaderType header) {
        final IdDt id = new IdDt();
        id.setValue(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId()
        ));
        flag.setId(id);

        final Identifier identifier = new Identifier();
        identifier.setSystem(header.getSourceSystemHSAId());
        identifier.setValue(header.getDocumentId());
        flag.addIdentifier(identifier);
        if (header.getPatientId() != null) {
            flag.setSubject(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(String.format("urn:oid:%s", header.getPatientId().getType()), header.getPatientId().getId()));
        } else {
            log.trace("Skip mapping patient since RIV patientId was null");
        }

        Encounter encounter;
        if (header.getCareContactId() != null) {
            encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId()).setStatus(EncounterStatus.UNKNOWN);
        } else {
            encounter = new Encounter().setStatus(EncounterStatus.UNKNOWN);
            log.debug("Skip mapping encounter since RIV careContactId was null");
        }

        if (header.getLegalAuthenticator() != null) {
            encounter.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        }

        if (header.getAccountableHealthcareProfessional() != null) {
            flag.setAuthor(Riv2FhirTransformationHelper.getPracticionerReference(header.getAccountableHealthcareProfessional()));
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
        }
        flag.setEncounter(new Reference(encounter));

    }

    private void mapBodyCommons(final Flag flag, final AlertInformationBodyType body) {
        try {
            final Period period = new Period();
            if (body.getValidityTimePeriod() != null) {
                if (StringUtils.hasText(body.getValidityTimePeriod().getStart())) {
                    period.setStart(Riv2FhirTransformationHelper.getDateTime(body.getValidityTimePeriod().getStart()).getValue());
                }
                if (StringUtils.hasText(body.getValidityTimePeriod().getEnd())) {
                    period.setEnd(Riv2FhirTransformationHelper.getDateTime(body.getValidityTimePeriod().getEnd()).getValue());
                }
            }
            if (StringUtils.hasText(body.getObsoleteTime())) {
                period.setEnd(Riv2FhirTransformationHelper.getDateTime(body.getObsoleteTime()).getValue());
            }
            final String typeOfAlertInformation = getTypeOfAlertInformationText(body.getTypeOfAlertInformation());
            if (typeOfAlertInformation.equals("Vårdbegränsning")) {
                flag.setCategory(Helpers.getCodeableConceptFromValueSet(Category.contact));
            } else if (typeOfAlertInformation.equals("Smittsam sjukdom")) {
                flag.setCategory(Helpers.getCodeableConceptFromValueSet(Category.communicableDisease));
            }
            flag.setPeriod(period);

        } catch (Exception e) {
            log.error("Unexpected error while mapping body commons", e);
        }
    }

    private void mapAlertInformationBody(final Flag flag, final AlertInformationBodyType body) {
        mapBodyCommons(flag, body);

        if (body.getHypersensitivity() != null) {
            mapHyperSensitivity(flag, body.getHypersensitivity());
        } else if (body.getSeriousDisease() != null) {
            mapSeriousDisease(flag, body.getSeriousDisease());
        } else if (body.getTreatment() != null) {
            mapTreatment(flag, body.getTreatment());
        } else if (body.getCommunicableDisease() != null) {
            mapCommunicableDisease(flag, body.getCommunicableDisease());
        } else if (body.getRestrictionOfCare() != null) {
            mapRestrictionOfCare(flag, body.getRestrictionOfCare());
        } else if (body.getUnstructuredAlertInformation() != null) {
            mapUnstructuredAlertInformation(flag, body.getUnstructuredAlertInformation());
        }

        if (flag.getCode() == null) {
            if (body.getTypeOfAlertInformation() != null) {
                flag.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(body.getTypeOfAlertInformation()));
                flag.getCode().setText(getTypeOfAlertInformationText(body.getTypeOfAlertInformation()));
            }
        } else {
            if (flag.getCode().getText() != null) {
                flag.getCode().setText(getTypeOfAlertInformationText(body.getTypeOfAlertInformation()));
            } else {
                flag.getCode().setText(String.format("%s %s", flag.getCode().getText(), getTypeOfAlertInformationText(body.getTypeOfAlertInformation()).trim()));
            }
        }
        //debug   System.out.println(String.format("Display: %s OriginalText: %s, Code: %s", body.getTypeOfAlertInformation().getDisplayName(), body.getTypeOfAlertInformation().getOriginalText(), body.getTypeOfAlertInformation().getCode()));
    }

    private void mapCommunicableDisease(final Flag flag, final CommunicableDiseaseType communicableDisease) {
        final Condition condition = new Condition();
        condition.setId(UUID.randomUUID().toString());
        condition.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(communicableDisease.getCommunicableDiseaseCode()));

        if (communicableDisease.getRouteOfTransmission() != null) {
            condition.addExtension(new Extension(Riv2FhirTransformationHelper.routeOfTransmissionExtensionUrl)
                    .setValue(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(communicableDisease.getRouteOfTransmission())));
        }

        addContainedData(flag, condition);
    }

    private void mapUnstructuredAlertInformation(final Flag flag, final UnstructuredAlertInformationType unstructuredAlertInformation) {
        CodeableConcept codeableConcept = new CodeableConcept();
        codeableConcept.setText(unstructuredAlertInformation.getUnstructuredAlertInformationHeading() + ": "
                + unstructuredAlertInformation.getUnstructuredAlertInformationContent());
        flag.setCode(codeableConcept);
    }

    //treatmentType.getPharmaceuticalTreatment() -> Fhir.medication. sub resource of Procedure or flag?
    private void mapTreatment(final Flag flag, final TreatmentType treatment) {

        final Procedure procedure = new Procedure();
        procedure.setId(UUID.randomUUID().toString());

        final Annotation note = new Annotation();
        note.setText(treatment.getTreatmentDescription());
        procedure.addNote(note);

        if (treatment.getTreatmentCode() != null) {
            procedure.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(treatment.getTreatmentCode()));
        }

        for (CVType pharmaceuticalTreatment : treatment.getPharmaceuticalTreatment()) {
            final Medication medication = new Medication();
            medication.setId(UUID.randomUUID().toString());
            medication.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(pharmaceuticalTreatment));
            addContainedData(flag, medication);
        }

        addContainedData(flag, procedure);
        CodeableConcept flagCode = new CodeableConcept();
        flagCode.setText("Treatment FlagCode Placeholder"); //todo

    }

    //grund, mappning ej klar
    private void mapSeriousDisease(final Flag flag, final SeriousDiseaseType seriousDisease) {
        Condition condition = new Condition();
        condition.setId(UUID.randomUUID().toString());

        //temp godtycklig mappning:
        condition.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(seriousDisease.getDisease()));
        addContainedData(flag, condition);

        CodeableConcept flagCode = new CodeableConcept();
        String flagCodeStr = seriousDisease.getDisease().getDisplayName();
        if (flagCodeStr == null || flagCodeStr.equals("")) {
            flagCodeStr = "."; //?
        }
        flagCode.setText(flagCodeStr);
        flag.setCode(flagCode);
    }

    //Severity done, remaining fields have yet to be mapped.
    private void mapHyperSensitivity(final Flag flag, final HyperSensitivityType hyperSensitivity) {
        final AllergyIntolerance allergyIntolerance = new AllergyIntolerance();
        allergyIntolerance.setId(UUID.randomUUID().toString());
        final AllergyIntolerance.AllergyIntoleranceReactionComponent reaction = new AllergyIntolerance.AllergyIntoleranceReactionComponent();

        //might need to be moved, we have no event tied to the reaction.
        if (hyperSensitivity.getDegreeOfSeverity() != null && hyperSensitivity.getDegreeOfSeverity().getDisplayName() != null) {
            final String severityCode = hyperSensitivity.getDegreeOfSeverity().getDisplayName();
            if (severityCode.equals("Livshotande")) {
                reaction.setSeverity(AllergyIntolerance.AllergyIntoleranceSeverity.SEVERE);
            } else if (severityCode.equals("Skadande") || severityCode.equals("Skadlig")) {
                reaction.setSeverity(AllergyIntolerance.AllergyIntoleranceSeverity.MODERATE);
            } else if (severityCode.equals("Besvärande")) {
                reaction.setSeverity(AllergyIntolerance.AllergyIntoleranceSeverity.MILD);
            } else {
                log.debug("Unexpected Severity code in hypersensitivity: " + severityCode);
            }
        }

        final PharmaceuticalHypersensitivityType pharmaHyperSensitivity = hyperSensitivity.getPharmaceuticalHypersensitivity();
        final OtherHypersensitivityType otherHypersensitivity = hyperSensitivity.getOtherHypersensitivity();
        CodeableConcept substance = null;
        if (otherHypersensitivity != null) {
            substance = new CodeableConcept();
            if (otherHypersensitivity.getHypersensitivityAgentCode() != null) {
                substance = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(otherHypersensitivity.getHypersensitivityAgentCode());
            }
            if (StringUtils.hasText(otherHypersensitivity.getHypersensitivityAgent())) {
                substance.setText(otherHypersensitivity.getHypersensitivityAgent());
            }
        } else if (pharmaHyperSensitivity != null) {
            allergyIntolerance.addCategory(AllergyIntolerance.AllergyIntoleranceCategory.MEDICATION);
            flag.setCategory(Helpers.getCodeableConceptFromValueSet(Category.drug));
            if (pharmaHyperSensitivity.getAtcSubstance() != null) {
                substance = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(pharmaHyperSensitivity.getAtcSubstance());
            } else if (StringUtils.hasText(pharmaHyperSensitivity.getNonATCSubstance())) {
                substance = new CodeableConcept();
                substance.setText(pharmaHyperSensitivity.getNonATCSubstance());

                if (StringUtils.hasText(pharmaHyperSensitivity.getNonATCSubstanceComment())) {
                    reaction.addNote(new Annotation().setText(pharmaHyperSensitivity.getNonATCSubstanceComment()));
                }
            }
        }

        //if pharam set category drug.

        if (pharmaHyperSensitivity != null && pharmaHyperSensitivity.getPharmaceuticalProductId() != null) {
            //Potentially incomplete list of medications might do more harm than good here - skipping
        }
        reaction.setSubstance(substance);

        allergyIntolerance.addReaction(reaction);

        if (hyperSensitivity.getDegreeOfCertainty() != null && hyperSensitivity.getDegreeOfCertainty().getCode() != null) {
            final String code = hyperSensitivity.getDegreeOfCertainty().getCode();
            switch (code.toUpperCase()) {
                //kodverk, se ineras kodverkslista + kodverk och oider i npo version.
                case "K": //konstaterad
                    allergyIntolerance.setVerificationStatus(AllergyIntoleranceVerificationStatus.CONFIRMED);
                    break;
                case "B": //bekräftad
                    allergyIntolerance.setVerificationStatus(AllergyIntoleranceVerificationStatus.CONFIRMED);
                    break;
                case "T": //trolig
                    allergyIntolerance.setVerificationStatus(AllergyIntoleranceVerificationStatus.UNCONFIRMED);
                    break;
                case "M": //misstänkt
                    allergyIntolerance.setVerificationStatus(AllergyIntoleranceVerificationStatus.UNCONFIRMED);
                    break;
                default:
                    allergyIntolerance.setVerificationStatus(AllergyIntoleranceVerificationStatus.NULL);
                    log.debug("Unrecognized degreeOfCertainty code");
                    break;
            }
        }

        if (hyperSensitivity.getTypeOfHypersensitivity() != null) {
            allergyIntolerance.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(hyperSensitivity.getTypeOfHypersensitivity()));
        }
        addContainedData(flag, allergyIntolerance);
    }

    private void mapRestrictionOfCare(final Flag flag, final RestrictionOfCareType restrictionOfCare) {
        if (restrictionOfCare == null) {
            return;
        }
        //flag.setCategory(Helpers.getCodeableConceptFromValueSet(Category.contact));
        final CodeableConcept description = new CodeableConcept();
        description.setText(restrictionOfCare.getRestrictionOfCareComment());
        flag.setCode(description);
    }

    //Adds resource to Flags contained resources and sets a flag-detail extension reference.
    private void addContainedData(final Flag flag, final Resource resource) {
        final Extension extension = new Extension("http://hl7.org/fhir/StructureDefinition/flag-detail");
        final Reference resourceReference = new Reference("#" + resource.getId());
        extension.setValue(resourceReference);
        flag.addExtension(extension);
        flag.addContained(resource);
    }

    private String getTypeOfAlertInformationText(final CVType typeOfAlertInformation) {
        if (typeOfAlertInformation == null) {
            return "";
        }
        if (StringUtils.hasText(typeOfAlertInformation.getDisplayName())) {
            return typeOfAlertInformation.getDisplayName();
        }
        if (StringUtils.hasText(typeOfAlertInformation.getOriginalText())) {
            return typeOfAlertInformation.getOriginalText();
        }
        return "";
    }
}
