package se.sll.ahrr.alertinformation.v2_0;

/*-
 * #%L
 * rivta-fhir-alertinformation
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.junit.Test;
import riv.clinicalprocess.healthcond.description._2.*;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.Collections;
import java.util.List;

import static org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceSeverity;
import static org.hl7.fhir.dstu3.model.AllergyIntolerance.AllergyIntoleranceVerificationStatus;
import static org.junit.Assert.*;

public class TransformerTest {

    private MockFactory mockFactory = new MockFactory();
    //private static final String _treatmentDescriptionText = "Treatment Description";
 /*
  *             V-TIM - [Livshotande, Skadlig, Besvärande]
  *
  *          NPÖ - [Livshotande, Skadande, Besvärande
  */


    private enum HyperSensitivitySeverity {
        Livshotande,
        Besvärande,
        Skadlig,
        Skadande
    }

    private enum AlertInformationSubTypes {
        Hypersensitivity,
        SeriousDisease,
        Treatment,
        CommunicableDisease,
        RestrictionOfCare,
        UnstructuredAlertInformation
    }

    @Test
    public void verifyRiv2Fhir() throws Exception {

    }

    @Test
    public void verifyFlagHypersensitivityType() throws Exception {
        assertHyperSensitivitySeverities();
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.Hypersensitivity);
        Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);

        //Verify header
        assertCommonHeaders(header, flag);

        //verify body:
        Resource resource = flag.getContained().get(0);
        assert (resource instanceof AllergyIntolerance);
        AllergyIntolerance allergyIntolerance = (AllergyIntolerance) resource;
        assertEquals(allergyIntolerance.getReaction().get(0).getSeverity(), AllergyIntoleranceSeverity.MODERATE);
        Assertions.assertCoding(body.getHypersensitivity().getTypeOfHypersensitivity(), allergyIntolerance.getCode().getCoding().get(0));
        assertNotNull(flag.getCode());

        for (String code : "kbtmZ".split("")) {
            body.getHypersensitivity().getDegreeOfCertainty().setCode(code);
            flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
            allergyIntolerance = (AllergyIntolerance) flag.getContained().get(0);
            switch (code.toUpperCase()) {
                case "K":
                    assertEquals(allergyIntolerance.getVerificationStatus(), AllergyIntoleranceVerificationStatus.CONFIRMED);
                    break;
                case "B":
                    assertEquals(allergyIntolerance.getVerificationStatus(), AllergyIntoleranceVerificationStatus.CONFIRMED);
                    break;
                case "T":
                    assertEquals(allergyIntolerance.getVerificationStatus(), AllergyIntoleranceVerificationStatus.UNCONFIRMED);
                    break;
                case "M":
                    assertEquals(allergyIntolerance.getVerificationStatus(), AllergyIntoleranceVerificationStatus.UNCONFIRMED);
                    break;
                case "Z": //not valid
                    assertEquals(allergyIntolerance.getVerificationStatus(), AllergyIntoleranceVerificationStatus.NULL);
                    break;
                default:
                    fail("Unrecognized code in test set");
                    break;
            }
        }

    }

    @Test
    public void verifyFlagSeriousDiseaseType() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.SeriousDisease);
        final Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        assertCommonHeaders(header, flag);
        Resource resource = flag.getContained().get(0);
        assert (resource instanceof Condition);
        Condition condition = (Condition) resource;
        Assertions.assertCoding(body.getSeriousDisease().getDisease(), condition.getCode().getCoding().get(0));
        //assert(flag.code)
    }

    @Test
    public void verifyFlagTreatmentType() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.Treatment);
        final Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        assertCommonHeaders(header, flag);

        for (Resource resource : flag.getContained()) {
            if (resource instanceof Procedure) {
                Procedure procedure = (Procedure) resource;
                assert (body.getTreatment().getTreatmentDescription().equals(procedure.getNote().get(0).getText()));
                Assertions.assertCoding(body.getTreatment().getTreatmentCode(), procedure.getCode().getCoding().get(0));
                //todo
            } else if (resource instanceof Medication) {
                Medication medication = (Medication) resource;
                assert (medication.getCode() != null);
                boolean medicationFound = false;
                for (CVType pharmaceuticalTreatment : body.getTreatment().getPharmaceuticalTreatment()) {
                    if (Assertions.codingEquals(pharmaceuticalTreatment, medication.getCode().getCoding().get(0))) {
                        medicationFound = true;
                        break;
                    }
                }
                assert (medicationFound);
            } else {
                assert (false); //unexptected type.
            }
        }
        //assert(flag.getCode() != null); //? todo
    }

    @Test
    public void verifyFlagCommunicableDiseaseType() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.CommunicableDisease);
        final Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        assertCommonHeaders(header, flag);

        Resource resource = flag.getContained().get(0);
        assert (resource instanceof Condition);
        Condition condition = (Condition) resource;
        Assertions.assertCoding(body.getCommunicableDisease().getCommunicableDiseaseCode(), condition.getCode().getCoding().get(0));

//        assert(condition.getCategory().getText().equals("communicable disease")); //not sure if it should be in text segment.
        //assert(Flag.getCode) //todo

    }

    @Test
    public void verifyFlagRestrictionOfCareType() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.RestrictionOfCare);
        final Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        assertCommonHeaders(header, flag);

//todo        assertEquals(body.getRestrictionOfCare().getRestrictionOfCareComment(), flag.getCode().getText());
//todo        assertTrue(flag.getCategory().equalsDeep(Helpers.getCodeableConceptFromValueSet(Category.contact))); //restrictionOfCareMapping
    }

    @Test
    public void verifyFlagUnstructuredAlerinformationType() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final AlertInformationBodyType body = createBody(AlertInformationSubTypes.UnstructuredAlertInformation);
        final Flag flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        assertCommonHeaders(header, flag);

        String descriptiveText = body.getUnstructuredAlertInformation().getUnstructuredAlertInformationHeading() + ": "
                + body.getUnstructuredAlertInformation().getUnstructuredAlertInformationContent();
      //todo  assertEquals(descriptiveText, flag.getCode().getText());
    }

    private List<AlertInformationType> getAlertInformationTypeSingletonList(final PatientSummaryHeaderType header, final AlertInformationBodyType body) {
        final AlertInformationType retval = new AlertInformationType();
        retval.setAlertInformationHeader(header);
        retval.setAlertInformationBody(body);
        return Collections.singletonList(retval);
    }

    private void assertCommonHeaders(final PatientSummaryHeaderType header, final Flag flag) {
        assertEquals(header.getSourceSystemHSAId(), flag.getIdentifier().get(0).getSystem());
        assertEquals(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId()
        ), flag.getId());

        final Encounter encounter = (Encounter) flag.getEncounter().getResource();
        assertEquals(encounter.getStatus(), Encounter.EncounterStatus.UNKNOWN);
        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), (Practitioner) flag.getAuthor().getResource());
        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) encounter.getServiceProvider().getResource());
        Assertions.assertLegalAuthenticator(header.getLegalAuthenticator(), encounter.getExtensionsByUrl(Riv2FhirTransformationHelper.legalAuthenticatorExtensionUrl).get(0));
        Assertions.assertIdentifier(CodeSystem.PERSONID_SE.getValue(), header.getPatientId().getId(), ((Patient) flag.getSubject().getResource()).getIdentifierFirstRep());

        assertEquals(header.getCareContactId(), ((Encounter) flag.getEncounter().getResource()).getIdentifier().get(0).getValue());

    }

    //tries parsing all valid severity strings.
    private void assertHyperSensitivitySeverities() throws Exception {
        PatientSummaryHeaderType header = createHeader();
        AlertInformationBodyType body = new AlertInformationBodyType();

        AllergyIntolerance allergyIntolerance;
        Flag flag;

        body.setHypersensitivity(createHyperSensitivty(HyperSensitivitySeverity.Besvärande));
        flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        allergyIntolerance = (AllergyIntolerance) flag.getContained().get(0);
        assert (allergyIntolerance.getReaction().get(0).getSeverity().equals(AllergyIntoleranceSeverity.MILD));

        body.setHypersensitivity(createHyperSensitivty(HyperSensitivitySeverity.Livshotande));
        flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        allergyIntolerance = (AllergyIntolerance) flag.getContained().get(0);
        assert (allergyIntolerance.getReaction().get(0).getSeverity().equals(AllergyIntoleranceSeverity.SEVERE));

        body.setHypersensitivity(createHyperSensitivty(HyperSensitivitySeverity.Skadande));
        flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        allergyIntolerance = (AllergyIntolerance) flag.getContained().get(0);
        assert (allergyIntolerance.getReaction().get(0).getSeverity().equals(AllergyIntoleranceSeverity.MODERATE));

        body.setHypersensitivity(createHyperSensitivty(HyperSensitivitySeverity.Skadlig));
        flag = new AlertInformationDataTransformer().transformToFhir(getAlertInformationTypeSingletonList(header, body)).get(0);
        allergyIntolerance = (AllergyIntolerance) flag.getContained().get(0);
        assert (allergyIntolerance.getReaction().get(0).getSeverity().equals(AllergyIntoleranceSeverity.MODERATE));
    }

    private PatientSummaryHeaderType createHeader() {
        try {
            return (PatientSummaryHeaderType) mockFactory.createPatientSummaryHeaderType(
                    new HealthcareProfessionalType(),
                    new OrgUnitType(),
                    new CVType(),
                    new LegalAuthenticatorType(),
                    new PersonIdType(),
                    new PatientSummaryHeaderType());

        } catch (Exception e) {
            fail("Couldnt create header");
        }
        return null;
    }

    private PersonIdType createPatient() {
        PersonIdType personIdType = new ObjectFactory().createPersonIdType();
        personIdType.setId("191212121212");

        return personIdType;
    }

    private UnstructuredAlertInformationType createUnstructuredAlertInformation() {
        final UnstructuredAlertInformationType unstructuredAlertInformationType = new UnstructuredAlertInformationType();
        unstructuredAlertInformationType.setUnstructuredAlertInformationContent("header");
        unstructuredAlertInformationType.setUnstructuredAlertInformationContent("content");
        return unstructuredAlertInformationType;
    }

    private AlertInformationBodyType createBody(final AlertInformationSubTypes subType) {
        final AlertInformationBodyType body = new ObjectFactory().createAlertInformationBodyType();

        switch (subType) {
            case Hypersensitivity:
                body.setHypersensitivity(createHyperSensitivty(HyperSensitivitySeverity.Skadande));
                break;
            case SeriousDisease:
                body.setSeriousDisease(createSeriousDisease());
                break;
            case Treatment:
                body.setTreatment(createTreatmentType());
                break;
            case CommunicableDisease:
                body.setCommunicableDisease(createCommunicableDisease());
                break;
            case RestrictionOfCare:
                body.setRestrictionOfCare(createRestrictionOfCare());
                break;
            case UnstructuredAlertInformation:
                body.setUnstructuredAlertInformation(createUnstructuredAlertInformation());
                break;
        }
        return body;
    }

    private SeriousDiseaseType createSeriousDisease() {
        try {

            final SeriousDiseaseType seriousDisease = new SeriousDiseaseType();
            final CVType seriousDiseaseCVtype = (CVType) mockFactory.createCVType(new CVType());
            seriousDisease.setDisease(seriousDiseaseCVtype);
            return seriousDisease;
        } catch (Exception e) {
            fail("Error while creating serious disease");
            return null;
        }
    }

    private CommunicableDiseaseType createCommunicableDisease() {
        try {
            final CommunicableDiseaseType communicableDisease = new CommunicableDiseaseType();
            final CVType cvType = (CVType) mockFactory.createCVType(new CVType());
            communicableDisease.setCommunicableDiseaseCode(cvType);
            communicableDisease.setRouteOfTransmission(cvType);
            return communicableDisease;
        } catch (Exception e) {
            fail("Error while creating communicableDisease");
            return null;
        }

    }

    private HyperSensitivityType createHyperSensitivty(HyperSensitivitySeverity severity) {
        try {
            final HyperSensitivityType hyperSensitivityType = new HyperSensitivityType();
            final CVType severityCVtype = new CVType();
            severityCVtype.setDisplayName(severity.toString());
            hyperSensitivityType.setDegreeOfSeverity(severityCVtype);

            final CVType certainty = (CVType) mockFactory.createCVType(new CVType());
            certainty.setCode("k");
            hyperSensitivityType.setDegreeOfCertainty(certainty);
            final CVType typeOfAlertInformation = (CVType) mockFactory.createCVType(new CVType());
            hyperSensitivityType.setTypeOfHypersensitivity(typeOfAlertInformation);

            //remaining fields havent been mapped yet, todo.
            return hyperSensitivityType;
        } catch (Exception e) {
            fail("Error while creating hypersensitivity");
            return null;
        }
    }


    private TreatmentType createTreatmentType() {
        try {
            final TreatmentType treatment = new TreatmentType();
            final CVType cvType = (CVType) mockFactory.createCVType(new CVType());
            treatment.setTreatmentCode(cvType);
            treatment.setTreatmentDescription("Treatment Description Text");
            treatment.getPharmaceuticalTreatment().add((cvType));
            return treatment;
        } catch (Exception e) {
            fail("Error while creating treatmentTYpe");
            return null;
        }
    }

    private RestrictionOfCareType createRestrictionOfCare() {
        RestrictionOfCareType restrictionOfCare = new RestrictionOfCareType();
        restrictionOfCare.setRestrictionOfCareComment("RestrictionOfCare comment TEST");
        return restrictionOfCare;

    }
}
