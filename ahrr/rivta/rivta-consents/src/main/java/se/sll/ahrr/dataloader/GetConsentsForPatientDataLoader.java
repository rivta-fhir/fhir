package se.sll.ahrr.dataloader;

/*-
 * #%L
 * rivta-consents
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.ehr.patientconsent.querying.getconsentsforpatient._1.rivtabp21.GetConsentsForPatientResponderInterface;
import riv.ehr.patientconsent.querying.getconsentsforpatient._1.rivtabp21.GetConsentsForPatientResponderService;
import riv.ehr.patientconsent.querying.getconsentsforpatientresponder._1.GetConsentsForPatientRequestType;
import riv.ehr.patientconsent.querying.getconsentsforpatientresponder._1.GetConsentsForPatientResponseType;

@Service
@CacheConfig(cacheNames = "rivta.consents")
public class GetConsentsForPatientDataLoader {

    private GetConsentsForPatientResponderInterface port;

    @Value("${contracts.getConsentsForPatient.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public GetConsentsForPatientDataLoader(
        @Value("${contracts.getConsentsForPatient.url}") String serviceUrl,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        GetConsentsForPatientResponderService service = new GetConsentsForPatientResponderService();
        port = service.getGetConsentsForPatientResponderPort();
        Client client = ClientProxy.getClient(port);
        client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);

        // ADD SSL AND TIMEOUT CODE
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.setTlsClientParameters(tlsClientParameters);
        httpConduit.getClient().setReceiveTimeout(timeout * 1000);
    }


    @Cacheable(sync = true)
    public GetConsentsForPatientResponseType loadRivtaResponse(String clientId, String careProviderId) {
        final GetConsentsForPatientRequestType params = new GetConsentsForPatientRequestType();
        params.setCareProviderId(careProviderId);
        params.setPatientId(clientId);
        return port.getConsentsForPatient(logicalAddress, params);
    }

    @CacheEvict
    public void clearCache(String clientId, String careProviderId) {
    }
}
