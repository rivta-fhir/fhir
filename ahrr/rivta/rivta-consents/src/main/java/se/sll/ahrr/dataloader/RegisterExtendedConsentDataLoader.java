package se.sll.ahrr.dataloader;

/*-
 * #%L
 * rivta-consents
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import riv.ehr.patientconsent.administration.registerextendedconsent._1.rivtabp21.RegisterExtendedConsentResponderInterface;
import riv.ehr.patientconsent.administration.registerextendedconsent._1.rivtabp21.RegisterExtendedConsentResponderService;
import riv.ehr.patientconsent.administration.registerextendedconsentresponder._1.RegisterExtendedConsentRequestType;


@Service
public class RegisterExtendedConsentDataLoader {

    private RegisterExtendedConsentResponderInterface port;
    private GetConsentsForPatientDataLoader getConsentsForPatientDataLoader;

    @Value("${contracts.registerExtendedConsent.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public RegisterExtendedConsentDataLoader(
            GetConsentsForPatientDataLoader getConsentsForPatientDataLoader,
            @Value("${contracts.registerExtendedConsent.url}") String serviceUrl,
            @Autowired TLSClientParameters tlsClientParameters,
            @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        RegisterExtendedConsentResponderService service = new RegisterExtendedConsentResponderService();
        port = service.getRegisterExtendedConsentResponderPort();
        Client client = ClientProxy.getClient(port);
        client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
        this.getConsentsForPatientDataLoader = getConsentsForPatientDataLoader;

        // ADD SSL AND TIMEOUT CODE
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.setTlsClientParameters(tlsClientParameters);
        httpConduit.getClient().setReceiveTimeout(timeout * 1000);
    }


    /**
     * Registers a new consent and clears consent caches
     *
     * @param params consent to register
     * @return the id of the consent
     */
    public String registerConsent(final RegisterExtendedConsentRequestType params) {
        port.registerExtendedConsent(logicalAddress, params);

        getConsentsForPatientDataLoader.clearCache(params.getPatientId(), params.getCareProviderId());

        return params.getAssertionId();
    }
}
