package se.sll.ahrr.dataloader;

/*-
 * #%L
 * rivta-consents
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import riv.ehr.patientconsent._1.AccessingActorType;
import riv.ehr.patientconsent.accesscontrol.checkconsent._1.rivtabp21.CheckConsentResponderInterface;
import riv.ehr.patientconsent.accesscontrol.checkconsent._1.rivtabp21.CheckConsentResponderService;
import riv.ehr.patientconsent.accesscontrol.checkconsentresponder._1.CheckConsentRequestType;
import riv.ehr.patientconsent.accesscontrol.checkconsentresponder._1.CheckConsentResponseType;


//public CareDocumentationDataLoader(
//  @Value("#{'${contracts.careContact.urls}'.split(',')}") List<String> serviceUrls,
//  @Autowired TLSClientParameters tlsClientParameters,
//  @Value("${ahrr.client.timeout:60}") Long timeout
//) {

@Service
public class CheckConsentsDataLoader {

    private CheckConsentResponderInterface port;

    @Value("${contracts.checkConsent.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public CheckConsentsDataLoader(
        @Value("${contracts.checkConsent.url}") String serviceUrl,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        CheckConsentResponderService service = new CheckConsentResponderService();
        port = service.getCheckConsentResponderPort();
        Client client = ClientProxy.getClient(port);
        client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);

        // ADD SSL AND TIMEOUT CODE
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.setTlsClientParameters(tlsClientParameters);
        httpConduit.getClient().setReceiveTimeout(timeout * 1000);
    }

    public CheckConsentResponseType loadRivtaResponse(String clientId, String careProviderId, String careUnitId, String employeeId) {
        final CheckConsentRequestType params = new CheckConsentRequestType();
        params.setPatientId(clientId);
        AccessingActorType accessingActorType = new AccessingActorType();
        accessingActorType.setCareProviderId(careProviderId);
        accessingActorType.setCareUnitId(careUnitId);
        accessingActorType.setEmployeeId(employeeId);
        params.setAccessingActor(accessingActorType);
        return port.checkConsent(logicalAddress, params);
    }
}
