package se.sll.ahrr.consents;

/*-
 * #%L
 * rivta-consents
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import riv.ehr.patientconsent._1.*;
import riv.ehr.patientconsent.administration.registerextendedconsentresponder._1.RegisterExtendedConsentRequestType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.*;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.domain.Actor;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;
import java.util.stream.Collectors;

import static se.sll.ahrr.common.FhirUtilities.getResource;

@Service
public class ConsentDataTransformer implements Riv2FhirTransformer<List<PDLAssertionType>, Consent> {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Consent.class;
    }

    @Override
    public List<Consent> transformToFhir(List<PDLAssertionType> rivResponse) throws Exception {
        return rivResponse.stream()
            .map(r -> {
                    Consent consent = new Consent();

                    final String careGiverHsaId = r.getCareProviderId();
                    final String careUnitHsaId = r.getCareUnitId();
                    final Identifier careGiverId = new Identifier();
                    final Identifier careUnitId = new Identifier();

                    final Organization careGiverOrg = new Organization();
                    final Organization careUnitOrg = new Organization();

                    CodeableConcept careGiverOrgType = new CodeableConcept();
                    careGiverOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careprovider.getCodeSystem(), OrganizationType.careprovider.name(), OrganizationType.careprovider.getDisplayName())));

                    CodeableConcept careUnitOrgType = new CodeableConcept();
                    careUnitOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careunit.getCodeSystem(), OrganizationType.careunit.name(), OrganizationType.careunit.getDisplayName())));

                    careGiverId.setValue(careGiverHsaId);
                    careGiverId.setSystem(CodeSystem.HSA.getValue());
                    careGiverOrg.setType(Collections.singletonList(careGiverOrgType));
                    careUnitId.setValue(careUnitHsaId);
                    careUnitId.setSystem(CodeSystem.HSA.getValue());
                    careUnitOrg.setType(Collections.singletonList(careUnitOrgType));

                    careGiverOrg.addIdentifier(careGiverId);
                    careUnitOrg.addIdentifier(careUnitId);

                    careUnitOrg.setPartOf(new Reference(careGiverOrg));

                    consent.setOrganization(Arrays.asList(new Reference(careGiverOrg), new Reference(careUnitOrg)));
                    consent.setId(r.getAssertionId());
                    Reference patient = Riv2FhirTransformationHelper.getPatientReference(r.getPatientId());
                    consent.setPatient(patient);
                    consent.setConsentingParty(Collections.singletonList(patient));
                    consent.setPurpose(Collections.singletonList(new Coding(ConsentType.sjf.getCodeSystem(), ConsentType.sjf.name(), ConsentType.sjf.getDisplayName())));

                    CodeableConcept actors = new CodeableConcept();
                    actors.addCoding(new Coding(RoleClass.PROV.getCodeSystem(), RoleClass.PROV.name(), RoleClass.PROV.getDisplayName()));

                    if (!StringUtils.isEmpty(r.getEmployeeId())) {
                        // This is a personal consent
                        final Practitioner practitioner = new Practitioner();

                        final Identifier id = new Identifier();
                        id.setValue(r.getEmployeeId());
                        id.setSystem(CodeSystem.HSA.getValue());
                        practitioner.addIdentifier(id);

                        consent.addActor(new Consent.ConsentActorComponent(actors, new Reference(practitioner)));
                    } else {
                        // This is a consent for the whole care unit
                        consent.addActor(new Consent.ConsentActorComponent(actors, new Reference(careUnitOrg)));
                    }
                    CodeableConcept consentType = new CodeableConcept();
                    if (r.getAssertionType() == AssertionTypeType.EMERGENCY) {
                        consentType.addCoding(new Coding(ActConsentType.EMRGONLY.getCodeSystem(), ActConsentType.EMRGONLY.name(), ActConsentType.EMRGONLY.getDisplayName()));
                    } else {
                        consentType.addCoding(new Coding(ActConsentType.ACD.getCodeSystem(), ActConsentType.ACD.name(), ActConsentType.ACD.getDisplayName()));
                    }
                    consent.setCategory(Collections.singletonList(consentType));

                    CodeableConcept action = new CodeableConcept();
                    action.addCoding().setSystem(ConsentAction.access.getCodeSystem()).setCode(ConsentAction.access.name()).setDisplay(ConsentAction.access.getDisplayName());
                    consent.setAction(Collections.singletonList(action));
                    Period consentPeriod = new Period();

                    if (r.getStartDate() != null) {
                        consentPeriod.setStart(Helpers.xmlGregorianCalendarToDate(r.getStartDate()));
                    }

                    if (r.getEndDate() != null) {
                        consentPeriod.setEnd(Helpers.xmlGregorianCalendarToDate(r.getEndDate()));
                    }

                    consent.setPeriod(consentPeriod);
                    return consent;
                }
            ).collect(Collectors.toList());
    }

    public RegisterExtendedConsentRequestType transformToRiv(List<Consent> fhir, Actor actor) throws Exception {
        if (fhir.size() > 1) {
            throw new IllegalArgumentException("Cannot handle multiple consents at once");
        }

        if (fhir.size() == 0) {
            throw new IllegalArgumentException("No consent received");
        }
        Consent c = fhir.get(0);

        RegisterExtendedConsentRequestType consentRequestType = new RegisterExtendedConsentRequestType();

        consentRequestType.setAssertionId(UUID.randomUUID().toString());
        consentRequestType.setPatientId(getResource(c.getPatient(), Patient.class)
            .orElseThrow(() -> new IllegalArgumentException("Missing patient in consent"))
            .getIdentifierFirstRep().getValue()
        );

        if (c.getCategory().size() != 1) {
            throw new IllegalArgumentException("Missing consent type");
        } else {
            Coding category = c.getCategory().get(0).getCodingFirstRep();
            if (!CodeSystem.ACT_CONSENT_TYPE.getValue().equals(category.getSystem())) {
                throw new IllegalArgumentException("Unknown category code system " + category.getSystem());
            }
            {
                ActConsentType consentType = ActConsentType.valueOf(category.getCode());
                if (ActConsentType.ACD == consentType) {
                    consentRequestType.setAssertionType(AssertionTypeType.CONSENT);
                } else if (ActConsentType.EMRGONLY == consentType) {
                    consentRequestType.setAssertionType(AssertionTypeType.EMERGENCY);
                } else {
                    throw new IllegalArgumentException("Unknown code " + consentType.name());
                }
            }
        }

        if (c.getAction() == null) {
            throw new IllegalArgumentException("Consent action must be defined");
        }

        c.getAction().forEach(
            a -> {
                Coding action = a.getCodingFirstRep();
                CodeSystem codeSystem = CodeSystem.fromCode(action.getSystem());
                if (CodeSystem.CONSENT_ACTION != codeSystem) {
                    throw new IllegalArgumentException("Cannot handle code system " + codeSystem.name() + " as consent action");
                }
                ConsentAction consentAction = ConsentAction.valueOf(action.getCode());
                if (consentAction != ConsentAction.access) {
                    throw new IllegalArgumentException("Can only handle consents with action " + ConsentAction.access.name());
                }
            }
        );

        c.getOrganization().stream()
            .map(o -> getResource(o, Organization.class).orElseThrow(() -> new IllegalArgumentException("Could not get organization " + o)))
            .forEach(
                o ->
                    o.getType().forEach(
                        t -> t.getCoding()
                            .forEach(tc -> {
                                try {
                                    if (CodeSystem.fromCode(tc.getSystem()) == CodeSystem.ORGANIZATION_TYPE) {
                                        if (OrganizationType.valueOf(tc.getCode()) == OrganizationType.careunit) {
                                            o.getIdentifier().forEach(
                                                identifier -> {
                                                    try {
                                                        if (CodeSystem.fromCode(identifier.getSystem()) == CodeSystem.HSA) {
                                                            consentRequestType.setCareUnitId(identifier.getValue());
                                                        }
                                                    } catch (IllegalArgumentException e) {
                                                        log.debug("Unknown organization id type", e);
                                                    }
                                                }
                                            );
                                        } else if (OrganizationType.valueOf(tc.getCode()) == OrganizationType.careprovider) {
                                            o.getIdentifier().forEach(
                                                identifier -> {
                                                    try {
                                                        if (CodeSystem.fromCode(identifier.getSystem()) == CodeSystem.HSA) {
                                                            consentRequestType.setCareProviderId(identifier.getValue());
                                                        }
                                                    } catch (IllegalArgumentException e) {
                                                        log.debug("Unknown organization id type", e);
                                                    }
                                                }
                                            );
                                        }
                                    }
                                } catch (IllegalArgumentException e) {
                                    log.debug("Unknown organization type", e);
                                }
                            })
                    )
            );

        if (StringUtils.isEmpty(consentRequestType.getCareProviderId())) {
            throw new IllegalArgumentException("Missing care provider");
        }
        if (StringUtils.isEmpty(consentRequestType.getCareUnitId())) {
            throw new IllegalArgumentException("Missing care unit");
        }

        consentRequestType.setScope(ScopeType.NATIONAL_LEVEL);

        ActionType actionType = new ActionType();

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);

        actionType.setRegistrationDate(now);
        actionType.setRequestDate(now);

        ActorType registrar = new ActorType();
        registrar.setAssignmentId(actor.getAssignmentId());
        registrar.setAssignmentName(actor.getAssignmentName());
        registrar.setEmployeeId(actor.getHsaId());

        final Resource actorResource = (Resource)c.getActorFirstRep().getReference().getResource();
        if("practitioner".equals(actorResource.fhirType().toLowerCase())) {
            final Practitioner practitioner = (Practitioner) actorResource;
            validateId(practitioner.getIdentifierFirstRep());
            consentRequestType.setEmployeeId(practitioner.getIdentifierFirstRep().getValue());
        }

        actionType.setRegisteredBy(registrar);
        actionType.setRequestedBy(registrar);
        consentRequestType.setRegistrationAction(actionType);

        if (c.getPeriod() != null) {
            if (c.getPeriod().getStart() != null) {
                GregorianCalendar g = new GregorianCalendar();
                g.setTime(c.getPeriod().getStart());
                consentRequestType.setStartDate(datatypeFactory.newXMLGregorianCalendar(g));
            }
            if (c.getPeriod().getEnd() != null) {
                GregorianCalendar g = new GregorianCalendar();
                g.setTime(c.getPeriod().getEnd());
                consentRequestType.setEndDate(datatypeFactory.newXMLGregorianCalendar(g));
            }
        }

        return consentRequestType;
    }

    private void validateId(Identifier identifierFirstRep) {
        if (!identifierFirstRep.getSystem().equals(CodeSystem.HSA.getValue())) {
            throw new IllegalArgumentException("Requested actor not identified with HSA-ID");
        }
    }
}
