package se.sll.ahrr.dataloader;

/*-
 * #%L
 * rivta-blocks
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.ehr.blocking.querying.getallblocksforpatient._2.rivtabp21.GetAllBlocksForPatientResponderInterface;
import riv.ehr.blocking.querying.getallblocksforpatient._2.rivtabp21.GetAllBlocksForPatientResponderService;
import riv.ehr.blocking.querying.getallblocksforpatientresponder._2.GetAllBlocksForPatientRequestType;
import riv.ehr.blocking.querying.getallblocksforpatientresponder._2.GetAllBlocksForPatientResponseType;

@Service
@CacheConfig(cacheNames = "rivta.blocks")
public class GetAllBlocksForPatientDataLoader {

    private GetAllBlocksForPatientResponderInterface port;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${contracts.getAllBlocksForPatient.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public GetAllBlocksForPatientDataLoader(
        @Value("${contracts.getAllBlocksForPatient.url}") String serviceUrl,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        GetAllBlocksForPatientResponderService service = new GetAllBlocksForPatientResponderService();
        port = service.getGetAllBlocksForPatientResponderPort();

        Client client = ClientProxy.getClient(port);
        client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);

        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
        httpConduit.setTlsClientParameters(tlsClientParameters);
        httpConduit.getClient().setReceiveTimeout(timeout * 1000);
    }

    @Cacheable(sync = true)
    public GetAllBlocksForPatientResponseType loadRivtaResponse(String patientId) {
        {
            try {
                final GetAllBlocksForPatientRequestType params = new GetAllBlocksForPatientRequestType();
                params.setPatientId(patientId);
                return port.getAllBlocksForPatient(logicalAddress, params);
            } catch (Exception e) {
                log.error("Unexpected error during soap request", e);
                return null;
            }
        }
    }
}
