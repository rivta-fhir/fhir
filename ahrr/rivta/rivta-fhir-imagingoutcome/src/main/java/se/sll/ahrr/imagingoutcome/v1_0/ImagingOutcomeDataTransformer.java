package se.sll.ahrr.imagingoutcome.v1_0;

/*-
 * #%L
 * rivta-fhir-imagingoutcome
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import riv.clinicalprocess.healthcond.actoutcome._3.ImageRecordingType;
import riv.clinicalprocess.healthcond.actoutcome._3.ImagingBodyType;
import riv.clinicalprocess.healthcond.actoutcome._3.ImagingOutcomeType;
import riv.clinicalprocess.healthcond.actoutcome._3.PatientSummaryHeaderType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

import static org.hl7.fhir.dstu3.model.ImagingStudy.ImagingStudySeriesComponent;

//WIP
@Service
public class ImagingOutcomeDataTransformer implements Riv2FhirTransformer<List<ImagingOutcomeType>, ImagingStudy> {

    private static final Logger log = LoggerFactory.getLogger(ImagingOutcomeDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return ImagingStudy.class;
    }

    @Override
    public List<ImagingStudy> transformToFhir(List<ImagingOutcomeType> rivResponse) throws Exception {
        final List<ImagingStudy> imagingStudies = new LinkedList<>();

        for (final ImagingOutcomeType imagingOutcome : rivResponse) {
            imagingStudies.add(mapImagingOutcome(imagingOutcome.getImagingOutcomeHeader(), imagingOutcome.getImagingOutcomeBody()));
        }

        return imagingStudies;
    }

    public ImagingStudy mapImagingOutcome(final PatientSummaryHeaderType header, final ImagingBodyType body) {
        final ImagingStudy imagingStudy = new ImagingStudy();
        mapHeader(imagingStudy, header);
        mapBody(imagingStudy, body);
        return imagingStudy;
    }

    private void mapHeader(final ImagingStudy imagingStudy, final PatientSummaryHeaderType header) {
        final IdType id = new IdType();
        id.setValue(
                Riv2FhirTransformationHelper.getId(header.getSourceSystemHSAId(), header.getDocumentId())
        );

        imagingStudy.setId(id);

        Identifier identifierDt = new Identifier();
        identifierDt.setSystem(header.getSourceSystemHSAId());
        identifierDt.setValue(header.getDocumentId());
        imagingStudy.addIdentifier(identifierDt);

        if (header.getPatientId() != null && header.getPatientId().getId() != null && header.getPatientId().getType() != null) {
            imagingStudy.setPatient(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(header.getPatientId().getType(), header.getPatientId().getId()));
        } else {
            log.trace("Skip mapping patient since RIV patientId was null");
        }


        if (header.getCareContactId() != null) {
            final Encounter encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId());
            encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);
            imagingStudy.setContext(new Reference(encounter));
        } else {
            imagingStudy.setContext(new Reference(new Encounter().setStatus(Encounter.EncounterStatus.UNKNOWN)));
        }

        //noinspection Duplicates
        if (header.getAccountableHealthcareProfessional() != null) {
            final Encounter encounter = (Encounter) imagingStudy.getContext().getResource();
            final Encounter.EncounterParticipantComponent participantComponent = new Encounter.EncounterParticipantComponent();
            participantComponent.setIndividual(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.addParticipant(participantComponent);
        }

        if (header.getLegalAuthenticator() != null) {
            imagingStudy.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        }
    }

    private void mapBody(final ImagingStudy imagingStudy, final ImagingBodyType body) {
        if (body.getReferral() != null) {
            imagingStudy.setReferrer(Riv2FhirTransformationHelper.getPracticionerReference(body.getReferral().getAccountableHealthcareProfessional()));
            if (StringUtils.hasText(body.getReferral().getReferralReason())) {
                imagingStudy.setReason(new CodeableConcept().setText(body.getReferral().getReferralReason()));
            }
        }

        if (body.getImageRecording() != null && !body.getImageRecording().isEmpty()) {
            for (ImageRecordingType imageRecordingType : body.getImageRecording()) {
                imagingStudy.addSeries(getSeries(imageRecordingType));
            }
        }
    }

    private ImagingStudySeriesComponent getSeries(final ImageRecordingType imageRecordingType) {
        final ImagingStudySeriesComponent seriesComponent = new ImagingStudySeriesComponent();
        if (imageRecordingType.getRecordingId() != null && StringUtils.hasText(imageRecordingType.getRecordingId().getRoot())) {
            seriesComponent.setUid(imageRecordingType.getRecordingId().getRoot());
        }

        if (imageRecordingType.getImageStructuredData() != null) {

        }
        return seriesComponent;
    }
}

