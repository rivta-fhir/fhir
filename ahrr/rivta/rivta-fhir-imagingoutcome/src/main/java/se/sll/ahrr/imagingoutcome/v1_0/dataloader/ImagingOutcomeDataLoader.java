package se.sll.ahrr.imagingoutcome.v1_0.dataloader;

/*-
 * #%L
 * rivta-fhir-imagingoutcome
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.actoutcome._3.PersonIdType;
import riv.clinicalprocess.healthcond.actoutcome.getimagingoutcome._1.rivtabp21.GetImagingOutcomeResponderInterface;
import riv.clinicalprocess.healthcond.actoutcome.getimagingoutcome._1.rivtabp21.GetImagingOutcomeResponderService;
import riv.clinicalprocess.healthcond.actoutcome.getimagingoutcomeresponder._1.GetImagingOutcomeResponseType;
import riv.clinicalprocess.healthcond.actoutcome.getimagingoutcomeresponder._1.GetImagingOutcomeType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.LinkedList;
import java.util.List;

@Service
@CacheConfig(cacheNames = {"rivta.imagingOutcome"})
public class ImagingOutcomeDataLoader implements RivtaDataLoader<GetImagingOutcomeResponseType> {
    private List<GetImagingOutcomeResponderInterface> ports;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${contracts.getImagingOutcome.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public ImagingOutcomeDataLoader(
        @Value("#{'${contracts.getImagingOutcome.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetImagingOutcomeResponderService service = new GetImagingOutcomeResponderService();
            GetImagingOutcomeResponderInterface port = service.getGetImagingOutcomeResponderPort();

            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public GetImagingOutcomeResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetImagingOutcomeResponseType getImagingOutcomeResponseType = new GetImagingOutcomeResponseType();

            final GetImagingOutcomeType params = new GetImagingOutcomeType();
            final PersonIdType personId = new PersonIdType();
            personId.setId(clientId);
            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personId);

            for (GetImagingOutcomeResponderInterface port : ports)
            {
                GetImagingOutcomeResponseType response = port.getImagingOutcome(logicalAddress, params);
                getImagingOutcomeResponseType.getImagingOutcome().addAll(response.getImagingOutcome());
            }

            return getImagingOutcomeResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }
//
//    @Override
//    @Cacheable
//    public GetImagingOutcomeResponseType loadRivtaResponse(String clientId) {
//        try {
//            final GetImagingOutcomeType params = new GetImagingOutcomeType();
//            final PersonIdType personId = new PersonIdType();
//            personId.setId(clientId);
//            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
//            params.setPatientId(personId);
//            return port.getImagingOutcome("", params);
//        } catch (Exception e) {
//            log.error("Error while loading GetImagingOutcome", e);
//            return null;
//        }
//    }

    @Override
    public Class<GetImagingOutcomeResponseType> getRivtaResponseType() {
        return GetImagingOutcomeResponseType.class;
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {
    }

}
