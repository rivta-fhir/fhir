package se.sll.ahrr.laboratoryorderoutcome.v3_1;

/*-
 * #%L
 * rivta-fhir-laboratoryorderoutcome
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import riv.clinicalprocess.healthcond.actoutcome._3.*;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static org.hl7.fhir.dstu3.model.DiagnosticReport.DiagnosticReportPerformerComponent;
import static org.hl7.fhir.dstu3.model.DiagnosticReport.DiagnosticReportStatus;
import static org.hl7.fhir.dstu3.model.Observation.ObservationReferenceRangeComponent;
import static org.hl7.fhir.dstu3.model.Observation.ObservationStatus;

@Service
public class LaboratoryOutcomeDataTransformer implements Riv2FhirTransformer<List<LaboratoryOrderOutcomeType>, DiagnosticReport> {

    private static final Logger log = LoggerFactory.getLogger(LaboratoryOutcomeDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return DiagnosticReport.class;
    }

    @Override
    public List<DiagnosticReport> transformToFhir(List<LaboratoryOrderOutcomeType> rivResponse) throws Exception {
        final List<DiagnosticReport> diagnosticReports = new LinkedList<>();

        for (LaboratoryOrderOutcomeType labOutcomeType : rivResponse) {
            if (labOutcomeType.getLaboratoryOrderOutcomeHeader() != null && labOutcomeType.getLaboratoryOrderOutcomeBody() != null) {
                final DiagnosticReport diagnosticReport = mapDiagnosticReport(labOutcomeType.getLaboratoryOrderOutcomeHeader(), labOutcomeType.getLaboratoryOrderOutcomeBody());
                diagnosticReports.add(diagnosticReport);
            }
        }

        return diagnosticReports;
    }

    public DiagnosticReport mapDiagnosticReport(final PatientSummaryHeaderType header, final LaboratoryOrderOutcomeBodyType body) {
        final DiagnosticReport diagnosticReport = new DiagnosticReport();

        mapDiagnosticReportHeader(diagnosticReport, header);
        mapDiagnosticReportBody(diagnosticReport, body, header);

        return diagnosticReport;
    }


    @SuppressWarnings("Duplicates")
    private void mapDiagnosticReportHeader(final DiagnosticReport diagnosticReport, final PatientSummaryHeaderType header) {
        final IdType id = new IdType();
        id.setValue(
                Riv2FhirTransformationHelper.getId(
                        header.getSourceSystemHSAId(),
                        header.getDocumentId()
                )
        );
        diagnosticReport.setId(id);

        Identifier identifier = new Identifier();
        identifier.setSystem(header.getSourceSystemHSAId());
        identifier.setValue(header.getDocumentId());
        diagnosticReport.addIdentifier(identifier);

        if (header.getPatientId() != null) {
            Reference patientReference = Riv2FhirTransformationHelper.getPatientReference(header.getPatientId().getId());
            diagnosticReport.setSubject(patientReference);
        } else {
            log.trace("Skip mapping subject since RIV patientId was null");
        }

        if (header.getCareContactId() != null) {
            Encounter encounterIdentifier = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId()).setStatus(Encounter.EncounterStatus.UNKNOWN);
            diagnosticReport.setContext(new Reference(encounterIdentifier));
        } else {
            log.debug("RIV careContactId was null");
            diagnosticReport.setContext(new Reference(new Encounter().setStatus(Encounter.EncounterStatus.UNKNOWN)));

        }

        if (header.getAccountableHealthcareProfessional() != null) {

            final Encounter encounter = (Encounter) diagnosticReport.getContext().getResource();
            final Encounter.EncounterParticipantComponent participantComponent = new Encounter.EncounterParticipantComponent();
            participantComponent.setIndividual(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.addParticipant(participantComponent);
        }

        if (header.getLegalAuthenticator() != null) {
            diagnosticReport.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        } else {
            log.trace("LegalAuthenticator was null");
        }

        diagnosticReport.setCode(new CodeableConcept().addCoding(new Coding().setSystem(CodeSystem.SNOMED_CT_SE.getValue()).setCode("SCT=65981005").setDisplay("Laboratoriemedicinsk konsultation för tolkning av analys")));
    }

    private void mapDiagnosticReportBody(final DiagnosticReport diagnosticReport, final LaboratoryOrderOutcomeBodyType body, final PatientSummaryHeaderType header) {
        diagnosticReport.setStatus(mapResult(body.getResultType()));
        diagnosticReport.setCategory(
                new CodeableConcept().setText("Klinisk kemi"));

        if (body.getResultReport() != null) {
            diagnosticReport.setConclusion(body.getResultReport());
        }
        diagnosticReport.addBasedOn(getBasedOn(body.getOrder(), diagnosticReport.getSubject()));

        if (body.getAccountableHealthcareProfessional() != null) {
            try {
                DiagnosticReportPerformerComponent performer = new DiagnosticReportPerformerComponent();
                performer.setActor(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(body.getAccountableHealthcareProfessional()));
                diagnosticReport.addPerformer(performer);
            } catch (Exception e) {
                log.error("Couldn't map legalAuthenticator to performer", e);
            }
        }

        final String registrationTime = body.getRegistrationTime();
        if (registrationTime != null && !registrationTime.isEmpty()) {
            try {
                diagnosticReport.setIssued(Riv2FhirTransformationHelper.getDateTime(registrationTime).getValue());
            } catch (Exception e) {
                log.error("Couldn't parse registration time", e);
            }
        } else {
            log.trace("Registration time is missing.");
        }
        diagnosticReport.setResult(mapResult(diagnosticReport, body, header));
        for (Reference ref : diagnosticReport.getResult()) {
            if (ref.getResource() instanceof Observation) {
                final Observation observation = (Observation) ref.getResource();
                diagnosticReport.addSpecimen(observation.getSpecimen());
            }
        }
    }

    private Reference getBasedOn(final OrderType order, final Reference patientRef) {
        final ReferralRequest refRequest = new ReferralRequest();
        refRequest.setStatus(ReferralRequest.ReferralRequestStatus.UNKNOWN);
        refRequest.setIntent(ReferralRequest.ReferralCategory.ORDER);
        if (order != null) {
            if (order.getOrderReason() != null) {
                refRequest.setDescription(order.getOrderReason());
            }
            if (order.getOrderId() != null) {
                refRequest.addIdentifier().setValue(order.getOrderId());
            }
            if (patientRef != null && patientRef.getResource() != null) {
                refRequest.setSubject(patientRef);
            } else {
                log.debug("Patient reference was null, LaboratoryOutcomeDataTransformer::getBasedOn()");
            }

        }
        return new Reference(refRequest);
    }

    private List<Reference> mapResult(final DiagnosticReport diagnosticReport, final LaboratoryOrderOutcomeBodyType body, final PatientSummaryHeaderType header) {
        final List<AnalysisType> analysisTypes = body.getAnalysis();
        final List<Reference> resourceReferenceDtList = new LinkedList<>();

        if (analysisTypes != null && !analysisTypes.isEmpty()) {
            for (AnalysisType analysisType : analysisTypes) {
                try {
                    resourceReferenceDtList.add(getObservationRefFromAnalysis(analysisType, diagnosticReport, body, header));
                } catch (Exception e) {
                    log.error("Unexpeced error while mapping observation", e);
                }
            }
        }

        return resourceReferenceDtList;
    }

    private Reference getObservationRefFromAnalysis(final AnalysisType analysisType, final DiagnosticReport diagnosticReport, final LaboratoryOrderOutcomeBodyType body, final PatientSummaryHeaderType header) {
        final Observation observation = new Observation();
        final OrderType order = body.getOrder();

        observation.addBasedOn(getBasedOn(order, diagnosticReport.getSubject()));
        observation.setSubject(diagnosticReport.getSubject());
        observation.setContext(diagnosticReport.getContext());
        observation.addCategory(new CodeableConcept().addCoding(new Coding().setDisplay("Laboratory").setCode("laboratory")));


        if (analysisType.getAnalysisId() != null) {
            observation.getIdentifier().add(mapAnalysisId(analysisType.getAnalysisId()));
        } else {
            log.trace("Skip mapping analysisIdentifier since RIV analysisId was null");
        }

        if (analysisType.getAnalysisCode() != null) {
            observation.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(analysisType.getAnalysisCode(), CodeSystem.NPU_CODE.getValue()));
            if (observation.getCode() == null) {
                observation.setCode(new CodeableConcept().setText(""));
            }
            if (observation.getCode().getCodingFirstRep().getCode() == null || observation.getCode().getCodingFirstRep().getCode().isEmpty()) {
                if (StringUtils.hasText(analysisType.getAnalysisText())) {
                    observation.setCode(new CodeableConcept().setText(analysisType.getAnalysisText()));
                }
            }
        } else if (StringUtils.hasText(analysisType.getAnalysisText())) {
            observation.setCode(new CodeableConcept().setText(analysisType.getAnalysisText()));
        } else {
            log.trace("Skip mapping analysisCode since RIV analysisCode was null. Added empty CodeableConcept isntead.");
            observation.setCode(new CodeableConcept().setText(""));
        }

        if (analysisType.getAnalysisStatus() != null) {
            observation.setStatus(mapStatus(analysisType.getAnalysisStatus()));
        } else {
            observation.setStatus(ObservationStatus.UNKNOWN);
            log.trace("Skip mapping analysisStatus since RIV analysisStatus was null");
        }

        try {
            final String observationTime = analysisType.getAnalysisOutcome().getObservationTime();
            if (observationTime != null && !observationTime.isEmpty()) {
                observation.setEffective(new DateTimeType().setValue(Riv2FhirTransformationHelper.getDateTime(analysisType.getAnalysisOutcome().getObservationTime()).getValue()));
            } else {
                log.trace("Skipping observation.EffectiveDateTime since observationTime was null or empty");
            }
        } catch (Exception e) {
            log.error("Unexpected error while parsing observationTime string", e);
        }

        if (header.getDocumentTime() != null && !header.getDocumentTime().isEmpty()) {
            try {
                final Date documentTime = Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()).getValue();
                observation.setIssued(documentTime);
            } catch (Exception e) {
                log.error("Unexpected error while parsing documentTime string", e);
            }
        }

        try {
            if (diagnosticReport.getPerformer() != null && diagnosticReport.getPerformer().size() > 0) {
                observation.addPerformer(diagnosticReport.getPerformer().get(0).getActor());
            }
        } catch (Exception e) {
            log.error("Unexpected error while mapping performer to observation", e);
        }

        if (analysisType.getAttested() != null) {
            observation.addExtension(Riv2FhirTransformationHelper.getPractitionerExtensionFromAttested(analysisType.getAttested()));
        }

        if (analysisType.getAnalysisOutcome() != null && analysisType.getAnalysisOutcome().isPathologicalFlag()) {
            observation.setInterpretation(new CodeableConcept().setText("Pathological"));
        }

        if (analysisType.getAnalysisComment() != null) {
            observation.setComment(analysisType.getAnalysisComment() + "\n" + analysisType.getAnalysisOutcome().getOutcomeDescription());
        }

        if (analysisType.getMethod() != null) {
            observation.setMethod(new CodeableConcept().setText(analysisType.getMethod()));
        }
        mapObservationValue(analysisType, observation);
        //todo should specimen be populated at all if Analysis.specimen isnt provided?
        observation.setSpecimen(new Reference(getSpecimen(diagnosticReport, analysisType)));

        final ObservationReferenceRangeComponent refRange = new ObservationReferenceRangeComponent();
        if (analysisType.getAnalysisOutcome() != null && analysisType.getAnalysisOutcome().getReferenceInterval() != null) {
            refRange.setText(analysisType.getAnalysisOutcome().getReferenceInterval());
        }
        if (analysisType.getAnalysisOutcome() != null && analysisType.getAnalysisOutcome().getReferencePopulation() != null) {
            refRange.addAppliesTo(new CodeableConcept().setText(analysisType.getAnalysisOutcome().getReferencePopulation()));
        }
        observation.addReferenceRange(refRange);

        return new Reference(observation);
    }

    private void mapObservationValue(final AnalysisType analysisType, final Observation observation) {
        try {
            if (analysisType.getAnalysisOutcome() == null) {
                return;
            }
            final String outcomeValue = analysisType.getAnalysisOutcome().getOutcomeValue();
            final String outcomeUnit = analysisType.getAnalysisOutcome().getOutcomeUnit();
            try {
                final Quantity quantity = new Quantity();
                NumberFormat nf = NumberFormat.getInstance(new Locale("sv", "SE"));
                Number num = nf.parse(outcomeValue);
                quantity.setValue(num.doubleValue());
                if (outcomeUnit != null && !outcomeUnit.isEmpty()) {
                    quantity.setUnit(outcomeUnit);
                }
                observation.setValue(quantity);

            } catch (ParseException e) {
                log.debug("OutcomeValue parse failed, treating as string");

                String valueText = "";
                if (outcomeValue != null && !outcomeValue.isEmpty()) {
                    valueText = outcomeValue;
                }
                if (outcomeUnit != null && !outcomeUnit.isEmpty()) {
                    valueText += "; " + outcomeUnit;
                }
                observation.setValue(new StringType(valueText.trim()));
            }

        } catch (Exception e) {
            log.error("Unexpected error while mapping Observation.value", e);
        }
    }

    private Specimen getSpecimen(final DiagnosticReport diagnosticReport, final AnalysisType analysisType) {
        final Specimen specimen = new Specimen();
        try {
            if (analysisType.getSpecimen() != null) {
                specimen.getType().setText(analysisType.getSpecimen());
            }
            if (diagnosticReport.getSubject() != null) {
                specimen.setSubject(diagnosticReport.getSubject());
            }

            final String analysisTimeStart = analysisType.getAnalysisTime().getStart();
            final String analysisTimeEnd = analysisType.getAnalysisTime().getEnd();
            specimen.setCollection(new Specimen.SpecimenCollectionComponent());
            specimen.getCollection().setCollected(new Period());

            if (analysisTimeStart != null && !analysisTimeStart.isEmpty()) {
                try {
                    specimen.getCollection().getCollectedPeriod().setStart(Riv2FhirTransformationHelper.getDateTime(analysisTimeStart).getValue());
                } catch (Exception e) {
                    log.error("Error while setting specimen.collectedPeriod.Start", e);
                }
            }

            if (analysisTimeEnd != null && !analysisTimeEnd.isEmpty()) {
                try {
                    specimen.getCollection().getCollectedPeriod().setEnd(Riv2FhirTransformationHelper.getDateTime(analysisTimeEnd).getValue());
                } catch (Exception e) {
                    log.error("Error while setting specimen.collectedPeriod.End", e);
                }
            }
        } catch (Exception e) {
            log.error("Unexpected error while mapping specimen", e);
        }
        return specimen;
    }

    private Identifier mapAnalysisId(final IIType iiType) {
        Identifier identifier = new Identifier();
        identifier.setSystem("urn:oid:" + iiType.getRoot());
        identifier.setValue(iiType.getExtension());

        return identifier;
    }

    private ObservationStatus mapStatus(final String status) {
        if (status.equalsIgnoreCase("PLANERAD")) {
            return ObservationStatus.REGISTERED;
        }
        if (status.equalsIgnoreCase("PÅGÅENDE")) {
            return ObservationStatus.PRELIMINARY;
        }
        if (status.equalsIgnoreCase("AVKLARAD")) {
            return ObservationStatus.FINAL;
        }
        return ObservationStatus.UNKNOWN;
    }

    private DiagnosticReportStatus mapResult(final String resultType) {
        switch (resultType.toUpperCase()) {
            case "DEF":
                return DiagnosticReportStatus.FINAL;
            case "TILL":
                return DiagnosticReportStatus.APPENDED;
            default:
                return null;
        }
    }
}
