package se.sll.ahrr.laboratoryorderoutcome.v3_1;

/*-
 * #%L
 * rivta-fhir-laboratoryorderoutcome
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.dstu3.model.DiagnosticReport.DiagnosticReportStatus;
import org.hl7.fhir.exceptions.FHIRException;
import org.junit.Test;
import riv.clinicalprocess.healthcond.actoutcome._3.*;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.Assert.*;

public class TransformerTest {

    private final MockFactory mockFactory = new MockFactory();
    private final ObjectFactory rivObjectFactory = new ObjectFactory();

    private static class AnalysisStatusStrings {
        static String Planerad = "Planerad";
        static String Pagaende = "Pågående";
        static String Avklarad = "Avklarad";
    }

    @Test
    public void verifyDiagnosticReport() throws Exception {
        final PatientSummaryHeaderType header = createHeader();
        final LaboratoryOrderOutcomeBodyType body = createBody();
        DiagnosticReport diagnosticReport = new LaboratoryOutcomeDataTransformer().mapDiagnosticReport(header, body);
        assertNotNull(diagnosticReport);
        assertHeader(header, diagnosticReport);
        assertBody(body, header, diagnosticReport);
    }

    @Test
    public void verifyDiagnosticReportStatus() throws Exception {
        final LaboratoryOutcomeDataTransformer laboratoryOutcomeDataTransformer = new LaboratoryOutcomeDataTransformer();
        final PatientSummaryHeaderType header = createHeader();
        final LaboratoryOrderOutcomeBodyType body = createBody();
        body.setResultType("DEF");
        DiagnosticReport diagnosticReport = laboratoryOutcomeDataTransformer.mapDiagnosticReport(header, body);
        assertTrue(diagnosticReport.getStatus().toCode().equalsIgnoreCase(DiagnosticReportStatus.FINAL.toCode()));
        body.setResultType("TILL");
        diagnosticReport = new LaboratoryOutcomeDataTransformer().mapDiagnosticReport(header, body);
        assertTrue(diagnosticReport.getStatus().toCode().equalsIgnoreCase(DiagnosticReportStatus.APPENDED.toCode()));
    }

    private void assertHeader(final PatientSummaryHeaderType header, final DiagnosticReport diagnosticReport) {
        assertEquals(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId()
        ), diagnosticReport.getId());

        final Identifier identifier = diagnosticReport.getIdentifier().get(0);
        Assertions.assertIdentifier(header.getSourceSystemHSAId(), header.getDocumentId(), identifier);
        assertEquals(((Patient) diagnosticReport.getSubject().getResource()).getIdentifier().get(0).getValue(), header.getPatientId().getId());

        final Encounter encounter = (Encounter) diagnosticReport.getContext().getResource();
        final Practitioner practitioner = (Practitioner) encounter.getParticipant().get(0).getIndividual().getResource();
        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), practitioner);
        Assertions.assertCareUnitAndGiverOrg(header.getAccountableHealthcareProfessional(), (Organization) ((Encounter) diagnosticReport.getContext().getResource()).getServiceProvider().getResource());
        assertEquals(((Encounter) diagnosticReport.getContext().getResource()).getIdentifier().get(0).getValue(), header.getCareContactId());
        assertEquals(((Encounter) diagnosticReport.getContext().getResource()).getStatus(), Encounter.EncounterStatus.UNKNOWN);

        Assertions.assertLegalAuthenticator(header.getLegalAuthenticator(), diagnosticReport.getExtensionsByUrl(Riv2FhirTransformationHelper.legalAuthenticatorExtensionUrl).get(0));
        Assertions.assertCoding(CodeSystem.SNOMED_CT_SE.getValue(), "SCT=65981005", "Laboratoriemedicinsk konsultation för tolkning av analys", diagnosticReport.getCode().getCodingFirstRep());

    }

    private void assertBody(final LaboratoryOrderOutcomeBodyType body, final PatientSummaryHeaderType header, final DiagnosticReport diagnosticReport) throws FHIRException {
        final Date registrationTime = Riv2FhirTransformationHelper.getDateTime(body.getRegistrationTime()).getValue();
        assertEquals(diagnosticReport.getIssued(), registrationTime);

        Assertions.assertAccountableHealthcareProfessional(body.getAccountableHealthcareProfessional(), (Practitioner) diagnosticReport.getPerformer().get(0).getActor().getResource());

        assertBasedOn((ReferralRequest) diagnosticReport.getBasedOn().get(0).getResource(), diagnosticReport.getSubject(), body);
        //Assertions.assertCoding("http://hl7.org/fhir/v2/0074", "CH", "Chemistry", diagnosticReport.getCategory().getCodingFirstRep());
        assertEquals("Klinisk kemi", diagnosticReport.getCategory().getText());
        assertTrue(diagnosticReport.getStatus().toCode().equalsIgnoreCase(DiagnosticReportStatus.FINAL.toCode()));
        assertEquals(diagnosticReport.getConclusion(), body.getResultReport());
        assertAnalysisTypes(body.getAnalysis(), header, body, diagnosticReport);
        diagnosticReport.getSpecimen().forEach(specimenRef -> assertTrue(resultContainsSpecimen(diagnosticReport.getResult(), (Specimen) specimenRef.getResource())));

    }

    private boolean resultContainsSpecimen(final List<Reference> results, final Specimen specimen) {
        for (Reference ref : results) {
            final Observation observation = (Observation) ref.getResource();
            if (observation.getSpecimen().getResource().equals(specimen)) return true;
        }
        return false;
    }


    private void assertObservation(final List<AnalysisType> analysisTypes, final DiagnosticReport diagnosticReport, final LaboratoryOrderOutcomeBodyType body, final PatientSummaryHeaderType header, final Observation observation) throws FHIRException, ParseException {
        final AnalysisType analysis = findMatchingAnalysis(analysisTypes, observation);
        assertNotNull(analysis);
        assertTrue(analysisIdEqualsIdentifier(analysis.getAnalysisId(), observation.getIdentifier().get(0)));
        assertBasedOn((ReferralRequest) observation.getBasedOn().get(0).getResource(), diagnosticReport.getSubject(), body);
        Assertions.assertCoding(null, "laboratory", "Laboratory", observation.getCategory().get(0).getCodingFirstRep());
        assertEquals(((Patient) diagnosticReport.getSubject().getResource()).getIdentifier().get(0).getValue(), ((Patient) observation.getSubject().getResource()).getIdentifier().get(0).getValue());

        assertEquals(diagnosticReport.getContext(), observation.getContext());

        assertEquals(observation.getCode().getCodingFirstRep().getSystem(), CodeSystem.NPU_CODE.getValue());

        assertEquals(observation.getEffectiveDateTimeType().getValue(), Riv2FhirTransformationHelper.getDateTime(analysis.getAnalysisOutcome().getObservationTime()).getValue());
        assertEquals(observation.getIssued(), Riv2FhirTransformationHelper.getDateTime(header.getDocumentTime()).getValue());
        assertEquals(observation.getPerformer().get(0), diagnosticReport.getPerformer().get(0).getActor());
        if (observation.getValue() instanceof Quantity) {
            assertEquals(observation.getValueQuantity().getUnit(), analysis.getAnalysisOutcome().getOutcomeUnit());
            NumberFormat nf = NumberFormat.getInstance(new Locale("sv", "SE"));
            Number outcomeValueNum = nf.parse(analysis.getAnalysisOutcome().getOutcomeValue());
            assertEquals(observation.getValueQuantity().getValue(), outcomeValueNum);
        } else if (observation.getValue() instanceof StringType) {
            assert (observation.getValueStringType().toString().contains(analysis.getAnalysisOutcome().getOutcomeUnit()));
            assert (observation.getValueStringType().toString().contains(analysis.getAnalysisOutcome().getOutcomeValue()));
        } else {
            fail("Observation.value test failed.");
        }

        if (analysis.getAnalysisOutcome().isPathologicalFlag()) {
            assertEquals(observation.getInterpretation().getText(), "Pathological");
        } else {
            assertNull(observation.getInterpretation());
        }
        assertEquals(observation.getComment(), analysis.getAnalysisComment() + "\n" + analysis.getAnalysisOutcome().getOutcomeDescription());
        assertEquals(observation.getMethod().getText(), analysis.getMethod());

        final Specimen observationSpecimen = (Specimen) observation.getSpecimen().getResource();
        assertEquals(observationSpecimen.getType().getText(), analysis.getSpecimen());
        assertEquals(observationSpecimen.getSubject(), diagnosticReport.getSubject());
        assertEquals(observationSpecimen.getCollection().getCollectedPeriod().getStart().getTime(), Riv2FhirTransformationHelper.getDateTime(analysis.getAnalysisTime().getStart()).getValue().getTime());
        assertEquals(observationSpecimen.getCollection().getCollectedPeriod().getEnd().getTime(), Riv2FhirTransformationHelper.getDateTime(analysis.getAnalysisTime().getEnd()).getValue().getTime());
        assertEquals(observation.getReferenceRange().get(0).getText(), analysis.getAnalysisOutcome().getReferenceInterval());
        assertEquals(observation.getReferenceRange().get(0).getAppliesTo().get(0).getText(), analysis.getAnalysisOutcome().getReferencePopulation());
        assertAttested(analysis, observation);


        //DR.result.code.identifier/value (NPU-kod - OID 1.2.752.108.1 - om finns, annars analysisText
        CodeableConcept codeableConcept = Riv2FhirTransformationHelper.getCodeableConceptFromCvType(analysis.getAnalysisCode(), CodeSystem.NPU_CODE.getValue());
        if (observation.getCode().getCodingFirstRep().getCode() == null || observation.getCode().getCodingFirstRep().getCode().isEmpty()) {
            assertTrue(codeableConcept.getCodingFirstRep().getCode() == null || codeableConcept.getCodingFirstRep().getCode().isEmpty());
            assertEquals(observation.getCode().getText(), analysis.getAnalysisText());
        } else {
            Assertions.assertCoding(analysis.getAnalysisCode(), observation.getCode().getCodingFirstRep(), CodeSystem.NPU_CODE.getValue());
            assertTrue(observation.getCode().getText() == null || observation.getCode().getText().isEmpty());
        }

        assertAnalysisStatus(analysis, observation);

    }

    private void assertAttested(final AnalysisType analysisType, final Observation observation) {
        final Extension legalAuthenticatorExtension = observation.getExtensionsByUrl(Riv2FhirTransformationHelper.legalAuthenticatorExtensionUrl).get(0);
        final Practitioner practitioner = (Practitioner) ((Reference) legalAuthenticatorExtension.getExtensionsByUrl(Riv2FhirTransformationHelper.practitionerExtensionUrl).get(0).getValue()).getResource();
        final DateTimeType attestedTimeDtType = (DateTimeType) legalAuthenticatorExtension.getExtensionsByUrl(Riv2FhirTransformationHelper.signatureTimeExtensionUrl).get(0).getValue();

        assert (attestedTimeDtType.equalsDeep(Riv2FhirTransformationHelper.getDateTime(analysisType.getAttested().getAttestedTime())));
        Assertions.assertIdentifier(CodeSystem.HSA.getValue(), analysisType.getAttested().getAttesterHSAId(), practitioner.getIdentifierFirstRep());
        assertEquals(analysisType.getAttested().getAttesterName(), practitioner.getNameFirstRep().getText());


    }

    private void assertAnalysisTypes(final List<AnalysisType> analysisTypes, final PatientSummaryHeaderType header, final LaboratoryOrderOutcomeBodyType body, final DiagnosticReport diagnosticReport) throws FHIRException {
        int testCounter = 0;
        for (Reference resource : diagnosticReport.getResult()) {
            if (resource.getResource().getClass().isAssignableFrom(Observation.class)) {
                Observation observation = (Observation) resource.getResource();

                try {
                    assertObservation(analysisTypes, diagnosticReport, body, header, observation);
                } catch (ParseException e) {
                    fail("Observation  test failed");
                }
                testCounter++;
            }
        }
        assertTrue(testCounter == analysisTypes.size());
    }

    private void assertAnalysisStatus(final AnalysisType analysisType, final Observation observation) {
        if (analysisType.getAnalysisStatus().equalsIgnoreCase("Planerad")) {
            assertTrue(observation.getStatus().getDisplay().equals("Registered"));
        } else if (analysisType.getAnalysisStatus().equalsIgnoreCase("Pågående")) {
            assertTrue(observation.getStatus().getDisplay().equals("Preliminary"));
        } else if (analysisType.getAnalysisStatus().equalsIgnoreCase("Avklarad")) {
            assertTrue(observation.getStatus().getDisplay().equals("Final"));
        } else if (analysisType.getAnalysisStatus().equalsIgnoreCase("")) {
            assertTrue(observation.getStatus().getDisplay().equals("Unknown"));
        } else {
            assert (false);
        }

    }

    private AnalysisType findMatchingAnalysis(final List<AnalysisType> analysisTypes, final Observation observation) {
        for (AnalysisType analysisType : analysisTypes) {
            if (analysisIdEqualsIdentifier(analysisType.getAnalysisId(), observation.getIdentifier().get(0))) {
                return analysisType;
            }
        }
        return null;
    }

    private boolean analysisIdEqualsIdentifier(final IIType iiType, final Identifier identifier) {
        if (!identifier.getSystem().equals("urn:oid:" + iiType.getRoot())) return false;
        //noinspection RedundantIfStatement
        if (!identifier.getValue().equals(iiType.getExtension())) return false;
        return true;
    }

    private OrderType createOrderType() {
        final OrderType orderType = rivObjectFactory.createOrderType();
        orderType.setOrderId("OrderId");
        orderType.setOrderReason("Order reason");
        return orderType;
    }

    private AnalysisType createAnalysisType(final String status) throws Exception {

        final AnalysisType analysisType = rivObjectFactory.createAnalysisType();
        analysisType.setAnalysisCode((CVType) mockFactory.createCVType(new CVType()));
        analysisType.setAnalysisComment("Analysis comment.");

        IIType analysisId = (IIType) mockFactory.createIIType(new IIType());
        analysisId.setRoot(UUID.randomUUID().toString());
        analysisId.setExtension(UUID.randomUUID().toString());
        analysisType.setAnalysisId(analysisId);

        analysisType.setAnalysisOutcome(createAnalysisOutcomeType());
        analysisType.setAnalysisStatus(status);
        analysisType.setAnalysisText("Analysis text");
        analysisType.setAnalysisTime((TimePeriodType) mockFactory.createTimePeriodType(new TimePeriodType()));
        analysisType.setAttested(createAttested());
        analysisType.setMethod("Method");
        analysisType.setSpecimen("Specimen");
        return analysisType;

    }

    private Attested createAttested() {
        final Attested attested = rivObjectFactory.createAttested();
        attested.setAttestedTime(mockFactory.dateTimeStr);
        attested.setAttesterHSAId("HSAId-attested");
        attested.setAttesterName("Attested name");
        return attested;
    }

    private AnalysisOutcomeType createAnalysisOutcomeType() {
        final AnalysisOutcomeType analysisOutcomeType = rivObjectFactory.createAnalysisOutcomeType();
        analysisOutcomeType.setObservationTime(mockFactory.dateTimeStr);
        analysisOutcomeType.setOutcomeDescription("Analysis outcome description");
        analysisOutcomeType.setOutcomeUnit("Outcome unit");
        analysisOutcomeType.setOutcomeValue("Outcome value");
        analysisOutcomeType.setPathologicalFlag(true);
        analysisOutcomeType.setReferenceInterval("42");
        analysisOutcomeType.setReferencePopulation("42");
        return analysisOutcomeType;
    }


    private PatientSummaryHeaderType createHeader() throws Exception {
        final PatientSummaryHeaderType header = new PatientSummaryHeaderType();
        mockFactory.createPatientSummaryHeaderType(
                new HealthcareProfessionalType(),
                new OrgUnitType(),
                new CVType(),
                new LegalAuthenticatorType(),
                new PersonIdType(),
                header);
        return header;
    }

    private LaboratoryOrderOutcomeBodyType createBody() throws Exception {
        LaboratoryOrderOutcomeBodyType body = new LaboratoryOrderOutcomeBodyType();
        body.setAccountableHealthcareProfessional((HealthcareProfessionalType) mockFactory.createHealthcareProfessionalType(new OrgUnitType(), new CVType(), new HealthcareProfessionalType()));
        body.setDiscipline("Klinisk kemi");
        body.setOrder(createOrderType());
        body.setRegistrationTime(mockFactory.dateTimeStr);
        body.setResultComment("Result Comment");
        body.setResultReport("Result Report");
        body.getAnalysis().add(createAnalysisType(AnalysisStatusStrings.Pagaende));
        body.getAnalysis().add(createAnalysisType(AnalysisStatusStrings.Avklarad));
        body.getAnalysis().add(createAnalysisType(AnalysisStatusStrings.Planerad));
        body.getAnalysis().add(createAnalysisType(""));
        body.setResultType("DEF");
        return body;
    }

    private void assertBasedOn(final ReferralRequest refRequest, final Reference patientRef, final LaboratoryOrderOutcomeBodyType body) {
        assertEquals(refRequest.getStatus(), ReferralRequest.ReferralRequestStatus.UNKNOWN);
        assertEquals(refRequest.getIntent(), ReferralRequest.ReferralCategory.ORDER);
        assert (patientRef.equalsDeep(refRequest.getSubject()));
        assertEquals(refRequest.getDescription(), body.getOrder().getOrderReason());
        assertEquals(refRequest.getIdentifierFirstRep().getValue(), body.getOrder().getOrderId());
    }

}
