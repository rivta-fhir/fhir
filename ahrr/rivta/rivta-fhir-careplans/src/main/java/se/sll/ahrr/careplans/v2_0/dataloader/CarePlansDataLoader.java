package se.sll.ahrr.careplans.v2_0.dataloader;

/*-
 * #%L
 * rivta-fhir-careplans
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._3.PersonIdType;
import riv.clinicalprocess.logistics.logistics.getcareplans._2.rivtabp21.GetCarePlansResponderInterface;
import riv.clinicalprocess.logistics.logistics.getcareplans._2.rivtabp21.GetCarePlansResponderService;
import riv.clinicalprocess.logistics.logistics.getcareplansresponder._2.GetCarePlansResponseType;
import riv.clinicalprocess.logistics.logistics.getcareplansresponder._2.GetCarePlansType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.LinkedList;
import java.util.List;

@Service
@CacheConfig(cacheNames = {"rivta.careplan"})
public class CarePlansDataLoader implements RivtaDataLoader<GetCarePlansResponseType> {

    private List<GetCarePlansResponderInterface> ports;

    @Value("${contracts.getCarePlan.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public CarePlansDataLoader(
        @Value("#{'${contracts.getCarePlan.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetCarePlansResponderService service = new GetCarePlansResponderService();
            GetCarePlansResponderInterface port = service.getGetCarePlansResponderPort();

            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            // ADD SSL AND TIMEOUT CODE
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public GetCarePlansResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetCarePlansResponseType getCarePlansResponseType = new GetCarePlansResponseType();

            final GetCarePlansType params = new GetCarePlansType();
            final PersonIdType personId = new PersonIdType();
            personId.setId(clientId);
            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personId);

            for (GetCarePlansResponderInterface port : ports)
            {
                GetCarePlansResponseType response = port.getCarePlans(logicalAddress, params);
                getCarePlansResponseType.getCarePlan().addAll(response.getCarePlan());
            }

            return getCarePlansResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }

    @Override
    public Class<GetCarePlansResponseType> getRivtaResponseType() {
        return GetCarePlansResponseType.class;
    }

    @Override
    @CacheEvict
    public void clearCache(String clientId) {
    }

}
