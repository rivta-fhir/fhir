package se.sll.ahrr.careplans.v2_0;

/*-
 * #%L
 * rivta-fhir-careplans
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import riv.clinicalprocess.logistics.logistics._3.CarePlanBodyType;
import riv.clinicalprocess.logistics.logistics._3.CarePlanType;
import riv.clinicalprocess.logistics.logistics._3.MultimediaType;
import riv.clinicalprocess.logistics.logistics._3.PatientSummaryHeaderType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

@Service
public class CarePlanDataTransformer implements Riv2FhirTransformer<List<CarePlanType>, CarePlan> {

    private static final Logger log = LoggerFactory.getLogger(CarePlanDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return CarePlan.class;
    }

    @Override
    public List<CarePlan> transformToFhir(List<CarePlanType> rivResponse) throws Exception {
        final List<CarePlan> CarePlans = new LinkedList<>();

        for (final CarePlanType carePlan : rivResponse) {
            try {
                CarePlans.add(mapCarePlan(carePlan.getCarePlanHeader(), carePlan.getCarePlanBody()));
            } catch (Exception e) {
                log.error("Unexpected error while mapping CarePlan", e);
            }
        }
        return CarePlans;
    }

    private CarePlan mapCarePlan(final PatientSummaryHeaderType header, final CarePlanBodyType body) {
        final CarePlan carePlan = new CarePlan();
        mapCarePlanHeader(carePlan, header);
        mapCarePlanBody(carePlan, body);
        return carePlan;
    }

    private void mapCarePlanHeader(final CarePlan carePlan, final PatientSummaryHeaderType header) {

        final Identifier identifier = new Identifier();
        identifier.setSystem(header.getSourceSystemHSAId());
        identifier.setValue(header.getDocumentId());
        carePlan.addIdentifier(identifier);
        carePlan.setId(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId()));


        if (header.getPatientId() != null) {
            carePlan.setSubject(Riv2FhirTransformationHelper.getPatientReferenceWithSystem(String.format("urn:oid:%s", header.getPatientId().getType()), header.getPatientId().getId()));
        } else {
            log.debug("Skip mapping patient since RIV patientId was null");
        }

        if (header.getCareContactId() != null) {
            final Encounter encounter = Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId());
            encounter.setStatus(Encounter.EncounterStatus.UNKNOWN);
            carePlan.setContext(new Reference(encounter));
        } else {
            carePlan.setContext(new Reference(new Encounter().setStatus(Encounter.EncounterStatus.UNKNOWN)));
            log.debug("Skip mapping CareContact since RIV careContactId was null");
        }

        //noinspection Duplicates
        if (header.getAccountableHealthcareProfessional() != null) {
            final Encounter encounter = (Encounter) carePlan.getContext().getResource();
            final Encounter.EncounterParticipantComponent participantComponent = new Encounter.EncounterParticipantComponent();
            participantComponent.setIndividual(Riv2FhirTransformationHelper.getPractitionerReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.setServiceProvider(Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional()));
            encounter.addParticipant(participantComponent);
        }

        if (header.getLegalAuthenticator() != null) {
            carePlan.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        } else {
            log.debug("Skipped mapping legalAuthenticator since it was null");
        }

        if (StringUtils.hasText(header.getDocumentTitle())) {
            carePlan.setTitle(header.getDocumentTitle());
        }
    }


    private void mapCarePlanBody(final CarePlan carePlan, final CarePlanBodyType body) {

        /*Typ av vård- och omsorgsplan, "SIP" för Samordnad individuell plan,
        "SPLPTLRV" för Samordnad plan enligt LPT och LRV,
        "SPU" för Samordnad plan vid utskrivning,
        "VP" för Vårdplan,
        "HP" för Habiliteringsplan,
        "RP" för Rehabiliteringsplan,
        "GP" för Genomförande plan,
        "SVP" Standardiserad vårdplan.
        Termerna är tagna ifrån Socialstyrelsens termbank.*/

        carePlan.setIntent(CarePlan.CarePlanIntent.PLAN);

        if (body.getTypeOfCarePlanEnum() != null) {
            carePlan.addCategory(new CodeableConcept().setText(body.getTypeOfCarePlanEnum().value()));//temp doesnt map well to intent.
        }

        for (final MultimediaType media : body.getContent()) {
            if (media.getValue() != null && media.getMediaType() != null) {
                final Binary attachment = new Binary();
                attachment.setContentType(media.getMediaType().value());
                attachment.setContent(media.getValue());
                carePlan.addSupportingInfo(new Reference(attachment));
            }
        }
    }
}
