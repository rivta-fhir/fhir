/*-
 * #%L
 * rivta-fhir-careplans
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.*;
import org.junit.Test;
import riv.clinicalprocess.logistics.logistics._3.*;
import riv.clinicalprocess.logistics.logistics.enums._3.MediaTypeEnum;
import riv.clinicalprocess.logistics.logistics.enums._3.TypeOfCarePlanEnum;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.careplans.v2_0.CarePlanDataTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TransformerTest {

    private final MockFactory mockFactory = new MockFactory();
    @Test
    public void verifyCarePlan() {
        try {
            final PatientSummaryHeaderType header = createHeader();
            final CarePlanBodyType body = createBody();
            final CarePlanType carePlanType = new CarePlanType();
            carePlanType.setCarePlanHeader(header);
            carePlanType.setCarePlanBody(body);
            final CarePlan carePlan = new CarePlanDataTransformer().transformToFhir(Collections.singletonList(carePlanType)).get(0);
            assertHeader(header, carePlan);

            //verify body:
            assert(carePlan.getCategory().get(0).getText().equals(body.getTypeOfCarePlanEnum().toString())); //move to CarePlan.intent
            final List<Binary> attachments = carePlan.getSupportingInfo().stream().map(ref -> (Binary) ref.getResource()).collect(Collectors.toList());
            for(MultimediaType media : body.getContent())
            {
                try {
                    final Binary attachment = attachments.stream().filter(obj -> obj.getContentType().equals(media.getMediaType().value())).findFirst().get();
                    assertEquals(media.getValue(), attachment.getContent());
                }catch(Exception e)                {
                    throw new AssertionError();
                }
            }

        } catch (Exception e) {
            fail("CarePlan transformer threw an exception");
        }
    }

    private void assertHeader(final PatientSummaryHeaderType header, final CarePlan carePlan) {
        assertEquals(header.getSourceSystemHSAId(), carePlan.getIdentifier().get(0).getSystem());
        assertEquals(Riv2FhirTransformationHelper.getId(
                header.getSourceSystemHSAId(),
                header.getDocumentId()
        ), carePlan.getId());

        assertPatientMapping(header.getPatientId(), (Patient) carePlan.getSubject().getResource());
        final Encounter encounter = (Encounter) carePlan.getContext().getResource();
        assertEquals(header.getCareContactId(), encounter.getIdentifier().get(0).getValue());
        assertEquals(encounter.getStatus(), Encounter.EncounterStatus.UNKNOWN);
        Assertions.assertAccountableHealthcareProfessional(header.getAccountableHealthcareProfessional(), (Practitioner) encounter.getParticipantFirstRep().getIndividual().getResource());
        assertEquals(header.getDocumentTitle(), carePlan.getTitle());
        Assertions.assertLegalAuthenticator(header.getLegalAuthenticator(), carePlan.getExtensionsByUrl(Riv2FhirTransformationHelper.legalAuthenticatorExtensionUrl).get(0));
    }

    private void assertPatientMapping(final PersonIdType personIdType, final Patient patient) {
        Assertions.assertIdentifier(CodeSystem.PERSONID_SE.getValue(), personIdType.getId(), patient.getIdentifierFirstRep());
    }

    private CarePlanBodyType createBody() {
        final CarePlanBodyType carePlanBody = new CarePlanBodyType();
        carePlanBody.setTypeOfCarePlanEnum(TypeOfCarePlanEnum.HP);
        for(MediaTypeEnum mediaType : MediaTypeEnum.values()) {
            carePlanBody.getContent().add(createContent(mediaType));
        }
        return carePlanBody;
    }

    private MultimediaType createContent(MediaTypeEnum mediaType) {
        final MultimediaType multimedia = new MultimediaType();
        multimedia.setMediaType(mediaType);
        multimedia.setValue(ByteBuffer.allocate(Integer.BYTES).putInt(new Random().nextInt()).array());
        return multimedia;
    }

    private PatientSummaryHeaderType createHeader() {
        try {
            return (PatientSummaryHeaderType) mockFactory.createPatientSummaryHeaderType(
                    new HealthcareProfessionalType(),
                    new OrgUnitType(),
                    new CVType(),
                    new LegalAuthenticatorType(),
                    new PersonIdType(),
                    new PatientSummaryHeaderType()
            );
        } catch (Exception e) {
            fail("Couldn't create header");
            return null;
        }
    }
}
