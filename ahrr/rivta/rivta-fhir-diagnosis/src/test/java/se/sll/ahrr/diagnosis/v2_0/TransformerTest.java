package se.sll.ahrr.diagnosis.v2_0;

/*-
 * #%L
 * rivta-fhir-diagnosis
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.joda.time.format.DateTimeFormat;
import org.junit.Test;
import riv.clinicalprocess.healthcond.description._2.*;
import riv.clinicalprocess.healthcond.description.enums._2.DiagnosisTypeEnum;
import se.sll.ahrr.MockData.Helpers;
import se.sll.ahrr.MockData.MockFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.condition.Category;
import se.sll.ahrr.fhir.Assertions;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;
import static se.sll.ahrr.common.FhirUtilities.getResource;

public class TransformerTest {

    private final MockFactory mockFactory = new MockFactory();

    @Test
    public void verifyCondition() throws Exception {
        final PatientSummaryHeaderType header = createHeader();

        assertThat(header, notNullValue());

        final DiagnosisBodyType body = createBody();
        final Condition condition = new DiagnosisDataTransformer().mapCondition(header, body);

        //Verify header
        assertEquals(Riv2FhirTransformationHelper.getId(
            header.getSourceSystemHSAId(),
            header.getDocumentId()
        ), condition.getId());

        assertEquals(header.getSourceSystemHSAId(), condition.getIdentifier().get(0).getSystem());
        assertEquals(header.getDocumentId(), condition.getIdentifier().get(0).getValue());
        assertEquals(Condition.ConditionVerificationStatus.CONFIRMED, condition.getVerificationStatus());
        assertEquals(header.getCareContactId(), ((Encounter) condition.getContext().getResource()).getIdentifier().get(0).getValue());

        assertNotNull(getResource(condition.getContext(), Encounter.class).orElse(new Encounter()).getStatus());

        assertPatientMapping(header.getPatientId(), (Patient) condition.getSubject().getResource());
        assertActorMapping(header.getAccountableHealthcareProfessional(), ((Practitioner) condition.getAsserter().getResource()));

        //Verify body
        Assertions.assertCoding(CodeSystem.ICD10_SE.getValue(), body.getDiagnosisCode().getCode(), body.getDiagnosisCode().getDisplayName(), body.getDiagnosisCode().getCodeSystemVersion(), condition.getCode().getCodingFirstRep());
        final Encounter encounter = (Encounter) condition.getContext().getResource();
        body.getRelatedDiagnosis().forEach(relatedDiagnosis -> assertTrue(collectionContainsRelatedDiagnosis(encounter.getDiagnosis(), relatedDiagnosis.getDocumentId())));
        assertDateString(body.getDiagnosisTime(), condition.getAssertedDate());
    }

    @Test
    public void verifyConditionCategories() {
        final PatientSummaryHeaderType header = createHeader();
        DiagnosisBodyType body = createBody(true, DiagnosisTypeEnum.HUVUDDIAGNOS);
        Condition condition = new DiagnosisDataTransformer().mapCondition(header, body);

        assertTrue(condition.getCategory().stream().anyMatch(cat -> cat.equalsDeep(se.sll.ahrr.common.Helpers.getCodeableConceptFromValueSet(Category.HUVUDDIAGNOS))));
        assertTrue(condition.getCategory().stream().anyMatch(cat -> cat.equalsDeep(se.sll.ahrr.common.Helpers.getCodeableConceptFromValueSet(Category.PROBLEM_LIST_ITEM))));

        body = createBody(false, DiagnosisTypeEnum.BIDIAGNOS);
        condition = new DiagnosisDataTransformer().mapCondition(header, body);

        assertTrue(condition.getCategory().stream().anyMatch(cat -> cat.equalsDeep(se.sll.ahrr.common.Helpers.getCodeableConceptFromValueSet(Category.BIDIAGNOS))));
        assertTrue(condition.getCategory().stream().anyMatch(cat -> cat.equalsDeep(se.sll.ahrr.common.Helpers.getCodeableConceptFromValueSet(Category.ENCOUNTER_DIAGNOSIS))));

    }

    @Test
    public void verifyEncounterServiceProvider() {
        final PatientSummaryHeaderType header = createHeader();
        DiagnosisBodyType body = createBody();
        Condition condition = new DiagnosisDataTransformer().mapCondition(header, body);
        final Encounter context = getResource(condition.getContext(), Encounter.class).orElseThrow(() -> new AssertionError("No valid context"));
        final Organization serviceProvider = getResource(context.getServiceProvider(), Organization.class).orElseThrow(() -> new AssertionError("No valid serviceProvider"));

        assertThat(serviceProvider.getIdentifierFirstRep().getValue(), is("kalleanka-vc-hsa-id"));

        final Organization careUnit = getResource(serviceProvider.getPartOf(), Organization.class).orElseThrow(() -> new AssertionError("No valid careUnit"));
        assertThat(careUnit.getIdentifierFirstRep().getValue(), is("HSAId-CareUnit"));

        final Organization careGiver = getResource(careUnit.getPartOf(), Organization.class).orElseThrow(() -> new AssertionError("No valid careGiver"));
        assertThat(careGiver.getIdentifierFirstRep().getValue(), is("HSAId-CareGiver"));
    }


    private PatientSummaryHeaderType createHeader() {
        try {
            return (PatientSummaryHeaderType) mockFactory.createPatientSummaryHeaderType(
                new HealthcareProfessionalType(),
                new OrgUnitType(),
                new CVType(),
                new LegalAuthenticatorType(),
                new PersonIdType(),
                new PatientSummaryHeaderType());

        } catch (Exception e) {
            fail("Couldn't create header");
            return null;
        }
    }

    private boolean collectionContainsRelatedDiagnosis(final List<Encounter.DiagnosisComponent> diagnosisConditions, final String id) {
        for (Encounter.DiagnosisComponent component : diagnosisConditions) {
            if (((Condition) component.getCondition().getResource()).getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    private DiagnosisBodyType createBody(boolean isChronical, DiagnosisTypeEnum diagnosisType) {
        final DiagnosisBodyType body = new ObjectFactory().createDiagnosisBodyType();
        body.setDiagnosisCode(createCVType());
        body.setDiagnosisTime(Helpers.getStringFromLocalDateTime(LocalDateTime.now()));
        body.setChronicDiagnosis(isChronical);
        body.setTypeOfDiagnosis(diagnosisType);
        for (int i = 0; i < 7; i++) {
            final RelatedDiagnosisType rDiag = new RelatedDiagnosisType();
            rDiag.setDocumentId(String.format("RelatedDiagnosisId-%s", i));
            body.getRelatedDiagnosis().add(rDiag);
        }
        return body;
    }

    private DiagnosisBodyType createBody() {
        return createBody(true, DiagnosisTypeEnum.HUVUDDIAGNOS);
    }

    private CVType createCVType() {
        try {
            return (CVType) mockFactory.createCVType(new CVType());
        } catch (Exception e) {
            fail("Couldn't create CVType");
            return null;
        }
    }


    private void assertActorMapping(final HealthcareProfessionalType careActor, final Practitioner practitioner) {
        Assertions.assertAccountableHealthcareProfessionalWithoutRole(careActor, practitioner);
    }

    private void assertPatientMapping(final PersonIdType personIdType, final Patient patient) {
        Assertions.assertIdentifier(CodeSystem.PERSONID_SE.getValue(), personIdType.getId(), patient.getIdentifierFirstRep());
    }

    private void assertDateString(final String dateRecorded, final Date date) {
        assertEquals(DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(dateRecorded).toDate(), date);
    }
}
