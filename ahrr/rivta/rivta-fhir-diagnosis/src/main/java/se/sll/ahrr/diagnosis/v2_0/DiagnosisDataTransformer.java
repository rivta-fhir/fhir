package se.sll.ahrr.diagnosis.v2_0;

/*-
 * #%L
 * rivta-fhir-diagnosis
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.DiagnosisBodyType;
import riv.clinicalprocess.healthcond.description._2.DiagnosisType;
import riv.clinicalprocess.healthcond.description._2.PatientSummaryHeaderType;
import riv.clinicalprocess.healthcond.description._2.RelatedDiagnosisType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.condition.Category;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.service.Riv2FhirTransformationHelper;

import java.util.LinkedList;
import java.util.List;

@Service
public class DiagnosisDataTransformer implements Riv2FhirTransformer<List<DiagnosisType>, Condition> {

    private static final Logger log = LoggerFactory.getLogger(DiagnosisDataTransformer.class);

    @Override
    public Class<? extends IBaseResource> getFHIRResourceType() {
        return Condition.class;
    }

    @Override
    public List<Condition> transformToFhir(List<DiagnosisType> rivResponse) throws Exception {
        final List<Condition> conditions = new LinkedList<>();

        for (DiagnosisType diagnosisType : rivResponse) {
            if (diagnosisType.getDiagnosisHeader() != null && diagnosisType.getDiagnosisBody() != null) {
                final Condition condition = mapCondition(diagnosisType.getDiagnosisHeader(), diagnosisType.getDiagnosisBody());
                conditions.add(condition);
            }
        }
        return conditions;
    }

    public Condition mapCondition(final PatientSummaryHeaderType header, final DiagnosisBodyType body) {
        final Condition condition = new Condition();

        mapDiagnosisHeader(condition, header);
        mapDiagnosisBody(condition, body);

        return condition;
    }

    private void mapDiagnosisHeader(final Condition condition, final PatientSummaryHeaderType header) {
        final IdType id = new IdType();
        id.setValue(
                Riv2FhirTransformationHelper.getId(
                        header.getSourceSystemHSAId(),
                        header.getDocumentId()
                )
        );
        condition.setId(id);

        Identifier identifier = new Identifier();
        identifier.setSystem(header.getSourceSystemHSAId());
        identifier.setValue(header.getDocumentId());
        condition.addIdentifier(identifier);

        condition.setVerificationStatus(Condition.ConditionVerificationStatus.CONFIRMED);

        final Reference organizations = Riv2FhirTransformationHelper.getOrganizationHierarcyReferenceFromRivta(header.getAccountableHealthcareProfessional());

        if (header.getCareContactId() != null) {
            condition.setContext(new Reference(
                    Riv2FhirTransformationHelper.getEncounterIdentifier(header.getCareContactId())
                            .setStatus(Encounter.EncounterStatus.UNKNOWN)
                            .setServiceProvider(organizations)
            ));
        } else {
            condition.setContext(new Reference(new Encounter().setStatus(Encounter.EncounterStatus.UNKNOWN).setServiceProvider(organizations)));
            log.trace("Skip mapping encounter since RIV careContactId was null");
        }


        if (header.getPatientId() != null && header.getPatientId().getId() != null) {
            condition.setSubject(Riv2FhirTransformationHelper.getPatientReference(header.getPatientId().getId()));
        } else {
            log.debug("Skip mapping patient since RIV patientId was null");
        }

        if (header.getAccountableHealthcareProfessional() != null) {
            condition.setAsserter(Riv2FhirTransformationHelper.getPracticionerReference(header.getAccountableHealthcareProfessional(), false));

        } else {
            log.debug("Skip mapping practitioner since RIV accountableHealthcareProfessional was null");
        }

        if (header.getLegalAuthenticator() != null) {
            condition.addExtension(Riv2FhirTransformationHelper.getLegalAuthenticatorExtension(header.getLegalAuthenticator()));
        } else {
            log.debug("Skipped mapping legalAuthenticator since it was null.");
        }

    }

    private void mapDiagnosisBody(final Condition condition, final DiagnosisBodyType body) {
        if (body.getDiagnosisTime() != null) {
            try {
                condition.setAssertedDate(Riv2FhirTransformationHelper.getDateTime(body.getDiagnosisTime()).getValue());
            } catch (Exception e) {
                log.error("Unexpected error in mapDiagnosisBody()", e);
            }
        } else {
            log.trace("Skip DiagnosticReport since riv diagnosisTime was null");
        }

        if (Boolean.TRUE.equals(body.isChronicDiagnosis())) {
            condition.addCategory(Helpers.getCodeableConceptFromValueSet(Category.PROBLEM_LIST_ITEM));
        } else {
            condition.addCategory(Helpers.getCodeableConceptFromValueSet(Category.ENCOUNTER_DIAGNOSIS));
        }

        switch (body.getTypeOfDiagnosis()) {
            case HUVUDDIAGNOS:
                condition.addCategory(Helpers.getCodeableConceptFromValueSet(Category.HUVUDDIAGNOS));
                break;
            case BIDIAGNOS:
                condition.addCategory(Helpers.getCodeableConceptFromValueSet(Category.BIDIAGNOS));
                break;
        }

        condition.setCode(Riv2FhirTransformationHelper.getCodeableConceptFromCvType(body.getDiagnosisCode(), CodeSystem.ICD10_SE.getValue()));
        mapRelatedDiagnosis(body, condition);


    }

    private void mapRelatedDiagnosis(final DiagnosisBodyType body, final Condition condition) {
        try {
            for (RelatedDiagnosisType relatedDiagnosis : body.getRelatedDiagnosis()) {
                final Encounter.DiagnosisComponent diagnosis = new Encounter.DiagnosisComponent();
                final Condition diagCondition = new Condition();
                diagCondition.setId(relatedDiagnosis.getDocumentId());
                diagnosis.setCondition(new Reference(diagCondition));
                ((Encounter) condition.getContext().getResource()).addDiagnosis(diagnosis);

            }
        } catch (Exception e) {
            log.error("Unexpected error while mapping relatedDiagnosis", e);
        }
    }
}
