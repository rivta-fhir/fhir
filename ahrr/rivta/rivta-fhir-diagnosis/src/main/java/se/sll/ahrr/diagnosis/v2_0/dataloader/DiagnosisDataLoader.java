package se.sll.ahrr.diagnosis.v2_0.dataloader;

/*-
 * #%L
 * rivta-fhir-diagnosis
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.PersonIdType;
import riv.clinicalprocess.healthcond.description.getdiagnosis._2.rivtabp21.GetDiagnosisResponderInterface;
import riv.clinicalprocess.healthcond.description.getdiagnosis._2.rivtabp21.GetDiagnosisResponderService;
import riv.clinicalprocess.healthcond.description.getdiagnosisresponder._2.GetDiagnosisResponseType;
import riv.clinicalprocess.healthcond.description.getdiagnosisresponder._2.GetDiagnosisType;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.dataloader.ContentTypeFixInterceptor;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

import java.util.LinkedList;
import java.util.List;

@Service
@CacheConfig(cacheNames = {"rivta.diagnosis"})
public class DiagnosisDataLoader implements RivtaDataLoader<GetDiagnosisResponseType> {

    private List<GetDiagnosisResponderInterface> ports;

    @Value("${contracts.getDiagnosis.logicalAddress:${ahrr.logicalAddress:}}") String logicalAddress;

    public DiagnosisDataLoader(
        @Value("#{'${contracts.getDiagnosis.urls}'.split(',')}") List<String> serviceUrls,
        @Autowired TLSClientParameters tlsClientParameters,
        @Value("${ahrr.client.timeout:60}") Long timeout
    ) {
        ports = new LinkedList<>();
        for (String serviceUrl : serviceUrls)
        {
            GetDiagnosisResponderService service = new GetDiagnosisResponderService();
            GetDiagnosisResponderInterface port = service.getGetDiagnosisResponderPort();

            // Client
            Client client = ClientProxy.getClient(port);
            client.getRequestContext().put(Message.ENDPOINT_ADDRESS, serviceUrl);
            client.getInInterceptors().add(new ContentTypeFixInterceptor());

            // SSL and Timeout
            HTTPConduit httpConduit = (HTTPConduit) client.getConduit();
            httpConduit.setTlsClientParameters(tlsClientParameters);
            httpConduit.getClient().setReceiveTimeout(timeout * 1000);

            ports.add(port);
        }
    }

    @Override
    @Cacheable
    public GetDiagnosisResponseType loadRivtaResponse(String clientId) throws BadGatewayException
    {
        try
        {
            GetDiagnosisResponseType getDiagnosisResponseType = new GetDiagnosisResponseType();

            final GetDiagnosisType params = new GetDiagnosisType();
            PersonIdType personId = new PersonIdType();
            personId.setId(clientId);
            personId.setType(CodeSystem.PERSONID_SE.getValueWithoutNamespace());
            params.setPatientId(personId);

            for (GetDiagnosisResponderInterface port : ports)
            {
                GetDiagnosisResponseType response = port.getDiagnosis(logicalAddress, params);
                getDiagnosisResponseType.getDiagnosis().addAll(response.getDiagnosis());
            }

            return getDiagnosisResponseType;
        }
        catch (Exception e)
        {
            throw new BadGatewayException(String.format("Error while fetching data, %s", getRivtaResponseType()), e);
        }
    }


    @Override
    @CacheEvict
    public void clearCache(String clientId) {

    }

    @Override
    public Class<GetDiagnosisResponseType> getRivtaResponseType() {
        return GetDiagnosisResponseType.class;
    }
}
