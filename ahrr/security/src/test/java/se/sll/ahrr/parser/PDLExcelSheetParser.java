package se.sll.ahrr.parser;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import se.sll.ahrr.domain.PDLTestCase;
import se.sll.ahrr.security.domain.Block;
import se.sll.ahrr.security.domain.Consent;
import se.sll.ahrr.security.domain.TemporaryRevoke;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

@Service
public class PDLExcelSheetParser {

    private Map<String, Block> blockMap = new HashMap<>();
    private Map<String, TemporaryRevoke> temporaryRevokeMap = new HashMap<>();
    private Map<String, Consent> consentMap = new HashMap<>();
    List<PDLTestCase> testCases = new ArrayList<>();

    @FunctionalInterface
    private interface CallbackForTestCase {
        void run(PDLTestCase pdlTestCase);
    }

    private PDLTestCase parse(Iterator<Cell> cellIterator) {
        PDLTestCase pdlTestCase = new PDLTestCase();

        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();

            String value;

            switch (cell.getColumnIndex()) {
                case 0:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setTestcase(value);
                    break;
                case 1:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setRowid(value);
                    break;
                case 2:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setPatientid(value);
                    break;
                case 3:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setInformationTime(value);
                    break;
                case 4:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setInfotype(value);
                    break;
                case 5:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setInformationCareProviderId(value);
                    break;
                case 6:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setInformationCareUnitId(value);
                    break;
                case 7:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setResult(value);
                    break;
                case 8:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setReason_code(value);
                    break;
                case 9:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setReason_text(value);
                    break;
                case 10:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_rowid(value);
                    break;
                case 11:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_careGiver(value);
                    break;
                case 12:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_careUnitHsaIdentity(value);
                    break;
                case 13:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_hsaIdentityPerson(value);
                    break;
                case 14:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_miuPurpose(value);
                    break;
                case 15:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_miuRights_aktivitet(value);
                    break;
                case 16:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_miuRights_infotype(value);
                    break;
                case 17:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setUser_miuRights_omf(value);
                    break;
                case 18:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_consents(value);
                    break;
                case 19:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_rowid(value);
                    break;
                case 20:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_AssertionType(value);
                    break;
                case 21:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_CareUnitId(value);
                    break;
                case 22:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_EmployeeId(value);
                    break;
                case 23:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_EndDate(value);
                    break;
                case 24:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setConsent_PatientId(value);
                    break;
                case 25:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_blocks(value);
                    break;
                case 26:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_rowid(value);
                    break;
                case 27:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_BlockType(value);
                    break;
                case 28:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_ExcludedInformationTypes(value);
                    break;
                case 29:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_InformationCareProviderId(value);
                    break;
                case 30:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_InformationCareUnitId(value);
                    break;
                case 31:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_InformationEndDate(value);
                    break;
                case 32:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_InformationStartDate(value);
                    break;
                case 33:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_PatientId(value);
                    break;
                case 34:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_TemporaryRevokes(value);
                    break;
                case 35:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_EndDate(value);
                    break;
                case 36:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_RevokedForCareUnitId(value);
                    break;
                case 37:
                    value = cell.getStringCellValue();
                    if (value.equals("null")) {
                        value = null;
                    }
                    pdlTestCase.setBlock_RevokedForEmployeeId(value);
                    break;
            }
        }
        return pdlTestCase;
    }

    private void initializeMapsWith(PDLTestCase pdlTestCase) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        if (!StringUtils.isEmpty(pdlTestCase.getBlock_blocks()) && !pdlTestCase.getBlock_blocks().contains(",")) {
            blockMap.computeIfAbsent(pdlTestCase.getBlock_blocks(), b -> {
                    List<String> excludedInformationTypes = new ArrayList<>();
                    if (!StringUtils.isEmpty(pdlTestCase.getBlock_ExcludedInformationTypes())) {
                        excludedInformationTypes.addAll(Arrays.asList(pdlTestCase.getBlock_ExcludedInformationTypes().split(",")));
                    }

                    return Block.builder()
                        .id(pdlTestCase.getBlock_rowid())
                        .informationCareProviderId(pdlTestCase.getBlock_InformationCareProviderId())
                        .informationCareUnitId(pdlTestCase.getBlock_InformationCareUnitId())
                        .informationStartDate(pdlTestCase.getBlock_InformationStartDate() != null ? LocalDate.parse(pdlTestCase.getBlock_InformationStartDate(), dateTimeFormatter).atStartOfDay() : null)
                        .informationEndDate(pdlTestCase.getBlock_InformationEndDate() != null ? LocalDate.parse(pdlTestCase.getBlock_InformationEndDate(), dateTimeFormatter).atStartOfDay() : null)
                        .patientId(pdlTestCase.getBlock_PatientId())
                        .type("inner".equalsIgnoreCase(pdlTestCase.getBlock_BlockType()) ? Block.Type.INNER : Block.Type.OUTER)
                        .excludedInformationTypes(excludedInformationTypes)
                        .build();
                }
            );
        }

        if (!StringUtils.isEmpty(pdlTestCase.getConsent_consents()) && !pdlTestCase.getConsent_consents().contains(",")) {
            consentMap.computeIfAbsent(pdlTestCase.getConsent_consents(), c ->
                Consent.builder()
                    .careUnitId(pdlTestCase.getConsent_CareUnitId())
                    .employeeId(pdlTestCase.getConsent_EmployeeId())
                    .patientId(pdlTestCase.getConsent_PatientId())
                    .endDate(pdlTestCase.getConsent_EndDate() != null ? LocalDate.parse(pdlTestCase.getConsent_EndDate(), dateTimeFormatter).atStartOfDay() : null)
                    .type("CONSENT".equalsIgnoreCase(pdlTestCase.getConsent_AssertionType()) ? Consent.Type.CONSENT : Consent.Type.EMERGENCY)
                    .build()
            );
        }

        if (!StringUtils.isEmpty(pdlTestCase.getBlock_TemporaryRevokes()) && !pdlTestCase.getBlock_TemporaryRevokes().contains(",")) {
            temporaryRevokeMap.computeIfAbsent(pdlTestCase.getBlock_TemporaryRevokes(), br ->
                TemporaryRevoke.builder()
                    .endDate(pdlTestCase.getBlock_EndDate() != null ? LocalDate.parse(pdlTestCase.getBlock_EndDate(), dateTimeFormatter).atStartOfDay() : null)
                    .revokedForCareUnitId(pdlTestCase.getBlock_RevokedForCareUnitId())
                    .revokedForEmployeeId(pdlTestCase.getBlock_RevokedForEmployeeId())
                    .build()
            );
        }
    }

    private void postProcess(PDLTestCase pdlTestCase) {
        if (!StringUtils.isEmpty(pdlTestCase.getBlock_blocks())) {
            Stream.of(pdlTestCase.getBlock_blocks().split(","))
                .map(String::trim)
                .forEach(
                    bid -> pdlTestCase.getBlocks().add(blockMap.get(bid))
                );
        }

        if (!StringUtils.isEmpty(pdlTestCase.getConsent_consents())) {
            Stream.of(pdlTestCase.getConsent_consents().split(","))
                .map(String::trim)
                .forEach(
                    cid -> pdlTestCase.getConsents().add(consentMap.get(cid))
                );
        }

        if (!StringUtils.isEmpty(pdlTestCase.getBlock_TemporaryRevokes())) {
            Stream.of(pdlTestCase.getBlock_TemporaryRevokes().split(","))
                .map(String::trim)
                .forEach(
                    brid -> pdlTestCase.getBlocks().forEach(
                        b -> b.getTemporaryRevokes().add(temporaryRevokeMap.get(brid))
                    )
                );
        }

        // Only test EMR data, skip LKM
        if (!"LKM".equalsIgnoreCase(pdlTestCase.getInfotype()) && !"PAT".equalsIgnoreCase(pdlTestCase.getInfotype())) {
            testCases.add(pdlTestCase);
        }
    }

    private void runForEachLineInSheet(CallbackForTestCase callback) throws Exception {
        InputStream excelStream = getClass().getResourceAsStream("/access_control_test_cases.xls");

        Workbook workbook = new HSSFWorkbook(excelStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        // Skip the header line
        iterator.next();

        while (iterator.hasNext()) {

            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();

            PDLTestCase pdlTestCase = parse(cellIterator);

            callback.run(pdlTestCase);

        }
        workbook.close();
    }

    public List<PDLTestCase> parse() throws Exception {
        runForEachLineInSheet(this::initializeMapsWith);
        runForEachLineInSheet(this::postProcess);
        return testCases;
    }
}
