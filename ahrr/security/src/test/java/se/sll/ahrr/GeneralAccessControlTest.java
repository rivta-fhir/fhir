package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.domain.Principal;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessDeniedReason;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;
import se.sll.ahrr.security.service.accesscontrol.TestContext;
import se.sll.ahrr.support.TestObjectFactory;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestContext.class)
public class GeneralAccessControlTest {

    @Autowired
    private AccessControl accessControl;

    @Autowired
    TestContext.Mocks mocks;

    @Before
    public void init() throws Exception {
        mocks.resetMocks(accessControl);
    }

    private class InvalidPrincipal extends Principal {
        @Override
        public String getUsername() {
            return "Invalid";
        }
    }

    @Test
    public void invalidPrincipal() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            TestObjectFactory.getPatientDataEntry("191212121212"),
            new InvalidPrincipal()
        );

        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.INVALID_PRINCIPAL, accessControlledResult.getAccessDeniedReason());
    }
}
