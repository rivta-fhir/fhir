package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileResponseType;
import se.sll.ahrr.dataloader.GetAllBlocksForPatientDataLoader;
import se.sll.ahrr.dataloader.GetConsentsForPatientDataLoader;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.security.service.accesscontrol.BlockService;

@RunWith(SpringRunner.class)
@SpringBootTest
@SpringBootConfiguration
@ComponentScan(basePackages = "se.sll.ahrr.security.service.accesscontrol")
@TestPropertySource(properties = {
    "blockcheck.disabled=true",
})
public class DisabledBlocksAccessControlTest {

    @Bean
    GetAllBlocksForPatientDataLoader getAllBlocksForPatientDataLoader() {
        return Mockito.mock(GetAllBlocksForPatientDataLoader.class);
    }

    @Bean
    GetConsentsForPatientDataLoader getConsentsForPatientDataLoader() {
        return Mockito.mock(GetConsentsForPatientDataLoader.class);
    }

    @Bean(name = "lookupResidentForFullProfileDataLoader")
    RivtaDataLoader<LookupResidentForFullProfileResponseType> lookupResidentForFullProfileDataLoader() {
        return Mockito.mock(RivtaDataLoader.class);
    }

    @Autowired
    BlockService blockService;

    @Test
    public void disabledBlockCheck() throws Exception {
        Assert.assertEquals(0, blockService.getAllBlocksForPatient("191212121212").size());
    }

}
