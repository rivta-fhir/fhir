package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.util.AopTestUtils;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TestContext {

    public class Mocks {
        private BlockService blockService;
        private ConsentService consentService;
        private PatientService patientService;

        public Mocks(BlockService blockService, ConsentService consentService, PatientService patientService) {
            this.blockService = blockService;
            this.consentService = consentService;
            this.patientService = patientService;
        }

        public void resetMocks(AccessControl accessControl) throws Exception {
            AccessControl accessControlTarget = AopTestUtils.getUltimateTargetObject(accessControl);
            ReflectionTestUtils.setField(accessControlTarget, "blockService", blockService);
            ReflectionTestUtils.setField(accessControlTarget, "consentService", consentService);
            ReflectionTestUtils.setField(accessControlTarget, "patientService", patientService);
            when(patientService.hasSecuredIdentity(anyString())).thenReturn(false);
            when(blockService.getAllBlocksForPatient(anyString())).thenReturn(Collections.emptyList());
            when(consentService.getConsentsForPatient(anyString(), anyString())).thenReturn(Collections.emptyList());
        }

        public BlockService getBlockService() {
            return blockService;
        }

        public ConsentService getConsentService() {
            return consentService;
        }

        public PatientService getPatientService() {
            return patientService;
        }
    }

    @Bean
    Mocks mocks() {
        return new Mocks(blockService(), consentService(), patientService());
    }

    @Bean
    BlockService blockService() {
        return mock(BlockService.class);
    }

    @Bean
    ConsentService consentService() {
        return mock(ConsentService.class);
    }

    @Bean
    PatientService patientService() {
        return mock(PatientService.class);
    }

    @Bean
    public CareProviderAccessControl careProviderAccessControl() {
        CareProviderAccessControl careProviderAccessControl = spy(CareProviderAccessControl.class);

        Mockito.when(careProviderAccessControl.now()).thenReturn(LocalDateTime.of(
            2014,
            12,
            24,
            0,
            0
        ));
        return careProviderAccessControl;
    }

    @Bean
    CitizenAccessControl citizenAccessControl() {
        return new CitizenAccessControl();
    }

    @Bean
    public AccessControl accessControl() {
        return new AccessControl();
    }
}
