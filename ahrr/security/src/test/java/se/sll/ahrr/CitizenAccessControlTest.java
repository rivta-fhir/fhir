package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessDeniedReason;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;
import se.sll.ahrr.security.service.accesscontrol.TestContext;

import static org.junit.Assert.assertEquals;
import static se.sll.ahrr.support.TestObjectFactory.getCitizenPrincipalFor;
import static se.sll.ahrr.support.TestObjectFactory.getPatientDataEntry;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestContext.class)
public class CitizenAccessControlTest {
    @Autowired
    private AccessControl accessControl;

    @Autowired
    TestContext.Mocks mocks;

    @Before
    public void init() throws Exception {
        mocks.resetMocks(accessControl);
    }


    @Test
    public void allowedForPatientEntry() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "191212121212",
                true
            ),
            getCitizenPrincipalFor("191212121212")
        );
        assertEquals(AccessResult.ACCESS_GRANTED, accessControlledResult.getResult());
    }

    @Test
    public void notAllowedForPatientEntry() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "191212121212",
                false
            ),
            getCitizenPrincipalFor("191212121212")
        );
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.NOT_ALLOWED_FOR_PATIENT, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void otherPatientsEntry() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "191212121212",
                true
            ),
            getCitizenPrincipalFor("197705232382")
        );
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.BELONGS_TO_WRONG_PATIENT, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void entryWithNoPatient() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "",
                true
            ),
            getCitizenPrincipalFor("191212121212")
        );
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.MISSING_PATIENT, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void principalWithNoId() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "191212121212",
                true
            ),
            getCitizenPrincipalFor("")
        );
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.INVALID_PRINCIPAL, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void principalWithNullId() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry(
                "191212121212",
                true
            ),
            getCitizenPrincipalFor(null)
        );
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.INVALID_PRINCIPAL, accessControlledResult.getAccessDeniedReason());
    }

}
