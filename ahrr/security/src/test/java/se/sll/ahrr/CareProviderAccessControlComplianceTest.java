package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.domain.PDLTestCase;
import se.sll.ahrr.parser.PDLExcelSheetParser;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;
import se.sll.ahrr.security.service.accesscontrol.TestContext;
import se.sll.ahrr.support.TestCaseParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static se.sll.ahrr.support.TestCaseParser.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestContext.class})
public class CareProviderAccessControlComplianceTest {
    @Autowired
    private AccessControl accessControl;

    @Autowired
    TestContext.Mocks mocks;

    @Before
    public void init() throws Exception {
        mocks.resetMocks(accessControl);
    }


    @Test
    public void testCompliance() throws Exception {
        PDLExcelSheetParser excelSheetParser = new PDLExcelSheetParser();
        excelSheetParser.parse().forEach(
            this::runTest
        );
    }

    public void runTest(PDLTestCase testCase) {
        try {
            when(mocks.getBlockService().getAllBlocksForPatient(anyString())).thenReturn(getBlocksFromTestCase(testCase));
            when(mocks.getConsentService().getConsentsForPatient(anyString(), anyString())).thenReturn(getConsentsFromTestCase(testCase));
        } catch (Exception e) {
            fail(e.getMessage());
        }

        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntryFromTestCase(testCase),
            getPrincipalFromTestCase(testCase)
        );

        assertEquals(
            String.format("Failed for %s\n\nShould deny for reason: %s\nWas denied for reason: %s\n", testCase.toString(), testCase.getReason_text(), accessControlledResult.getAccessDeniedReason()) +
                String.format("\n\nExpected output: %s\n\nTestcase: %s\n\nPrincipal: %s\n\nBlockService: %s\nConsents: %s\n\n-------------------------------\n",
                testCase.getResult(),
                getPatientDataEntryFromTestCase(testCase),
                getPrincipalFromTestCase(testCase),
                getBlocksFromTestCase(testCase),
                getConsentsFromTestCase(testCase)),
            TestCaseParser.getExpectedResultForTestCase(testCase),
            accessControlledResult.getResult()
        );
    }
}
