package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessDeniedReason;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;
import se.sll.ahrr.security.service.accesscontrol.TestContext;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static se.sll.ahrr.support.TestObjectFactory.getCareActorPrincipal;
import static se.sll.ahrr.support.TestObjectFactory.getPatientDataEntry;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestContext.class)
public class CareProviderAccessControlTest {
    @Autowired
    private AccessControl accessControl;

    @Autowired
    TestContext.Mocks mocks;

    @Before
    public void init() throws Exception {
        mocks.resetMocks(accessControl);
    }

    @Test
    public void nullPrincipal() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(getPatientDataEntry("191212121212"), null);
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.INVALID_PRINCIPAL, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void missingBlocks() throws Exception {
        when(mocks.getBlockService().getAllBlocksForPatient(anyString())).thenReturn(null);
        when(mocks.getConsentService().getConsentsForPatient(anyString(), anyString())).thenReturn(Collections.emptyList());
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry("191212121212"),
            getCareActorPrincipal());
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.DATA_MISSING_BLOCKS, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void missingConsent() throws Exception {
        when(mocks.getBlockService().getAllBlocksForPatient(anyString())).thenReturn(Collections.emptyList());
        when(mocks.getConsentService().getConsentsForPatient(anyString(), anyString())).thenReturn(null);
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            getPatientDataEntry("191212121212"),
            getCareActorPrincipal());
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.DATA_MISSING_CONSENTS, accessControlledResult.getAccessDeniedReason());

    }

    @Test
    public void missingInformationTime() throws Exception {
        when(mocks.getBlockService().getAllBlocksForPatient(anyString())).thenReturn(Collections.emptyList());
        when(mocks.getConsentService().getConsentsForPatient(anyString(), anyString())).thenReturn(Collections.emptyList());
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(
            PatientDataEntry.builder()
                .careProviderId("SE165567766992-17J5")
                .careUnitId("SE165567766992-17J5")
                .patientId("191212121212")
                .infoType(InformationType.VKO)
                .build(),
            getCareActorPrincipal());
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.NO_CONSENT, accessControlledResult.getAccessDeniedReason());

    }
}
