package se.sll.ahrr.support;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.domain.*;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.domain.Block;
import se.sll.ahrr.security.domain.Consent;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestCaseParser {

    public static List<Block> getBlocksFromTestCase(PDLTestCase testCase) {
        return testCase.getBlocks();
    }

    public static List<Consent> getConsentsFromTestCase(PDLTestCase testCase) {
        return testCase.getConsents();
    }

    public static PatientDataEntry<Object> getPatientDataEntryFromTestCase(PDLTestCase testCase) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        return PatientDataEntry.builder()
            .timestamp(LocalDate.parse(testCase.getInformationTime(), dateTimeFormatter).atStartOfDay())
            .careProviderId(testCase.getInformationCareProviderId())
            .careUnitId(testCase.getInformationCareUnitId())
            .patientId(testCase.getPatientid())
            .infoType(InformationType.fromString(testCase.getInfotype()))
            .build();
    }


    private static List<CommissionRight> getCommissionRightsFor(String scope, String permission, String... infotypes) {
        return Stream.of(infotypes).map(i -> CommissionRight.builder().infoType(i).permission(permission).scope(scope).build()).collect(Collectors.toList());
    }

    private static List<CommissionRight> getCommissionRightsFromTestCase(PDLTestCase testCase) {
        if (testCase.getUser_miuRights_infotype() == null) {
            return Collections.emptyList();
        }
        if (testCase.getUser_miuRights_infotype().equals("ALL")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "DIA", "PAD", "FUN", "VOT", "VKO", "LKO", "LKF", "UPP", "UND", "VBE", "VPO", "VOO", "PAT", "LKM");
        }
        if (testCase.getUser_miuRights_infotype().equals("ALL_BUT_LKM")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "DIA", "PAD", "FUN", "VOT", "VKO", "LKO", "LKF", "UPP", "UND", "VBE", "VPO", "VOO", "PAT");
        }
        if (testCase.getUser_miuRights_infotype().equals("alla")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "ALLA");
        }
        if (testCase.getUser_miuRights_infotype().equals("invalid")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "invalid");
        }
        if (testCase.getUser_miuRights_infotype().equals("SOME1")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "FUN", "VKO", "VBE", "DIA", "PAD", "VOT", "UND", "VPO");
        }
        if (testCase.getUser_miuRights_infotype().equals("SOME2")) {
            return getCommissionRightsFor(testCase.getUser_miuRights_omf(), testCase.getUser_miuRights_aktivitet(), "FUN", "VKO", "VBE", "VOO", "LKF", "UPP");
        }
        return Collections.emptyList();
    }


    public static CareActorPrincipal getPrincipalFromTestCase(PDLTestCase testCase) {
        return CareActorPrincipal.builder()
            .hsaId(testCase.getUser_hsaIdentityPerson())
            .careActorGivenName("")
            .careActorMiddleAndSurName("")
            .careProviderId(testCase.getUser_careGiver())
            .careUnitId(testCase.getUser_careUnitHsaIdentity())
            .commissionPurpose(testCase.getUser_miuPurpose())
            .commissionRights(getCommissionRightsFromTestCase(testCase))
            .build();
    }

    public static AccessResult getExpectedResultForTestCase(PDLTestCase testCase) {
        if (testCase.getResult().equals("Grant")) {
            return AccessResult.ACCESS_GRANTED;
        }
        return AccessResult.ACCESS_DENIED;
    }
}
