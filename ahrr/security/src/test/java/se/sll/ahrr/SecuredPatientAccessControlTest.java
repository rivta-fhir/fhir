package se.sll.ahrr;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessDeniedReason;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;
import se.sll.ahrr.security.service.accesscontrol.TestContext;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static se.sll.ahrr.support.TestObjectFactory.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestContext.class)
public class SecuredPatientAccessControlTest {
    @Autowired
    private AccessControl accessControl;

    @Autowired
    TestContext.Mocks mocks;

    @Before
    public void init() throws Exception {
        mocks.resetMocks(accessControl);
        when(mocks.getPatientService().hasSecuredIdentity(anyString())).thenReturn(true);
    }

    @Test
    public void securedPatientCitizenPrincipal() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(getPatientDataEntry("191212121212"), getCitizenPrincipalFor("191212121212"));
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.SECURED_PATIENT, accessControlledResult.getAccessDeniedReason());
    }

    @Test
    public void securedPatientCareGiverPrincipal() throws Exception {
        AccessControlledResult accessControlledResult = accessControl.performAccessControl(getPatientDataEntry("191212121212"), getCareActorPrincipal());
        assertEquals(AccessResult.ACCESS_DENIED, accessControlledResult.getResult());
        assertEquals(AccessDeniedReason.SECURED_PATIENT, accessControlledResult.getAccessDeniedReason());
    }
}
