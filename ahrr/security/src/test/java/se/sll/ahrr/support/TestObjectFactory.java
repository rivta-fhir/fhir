package se.sll.ahrr.support;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.domain.*;

import java.time.LocalDateTime;
import java.util.Collections;

public class TestObjectFactory {

    static public PatientDataEntry<Object> getPatientDataEntry(String patientId, boolean allowedForPatient) {
        return PatientDataEntry.builder()
            .timestamp(LocalDateTime.now())
            .careProviderId("SE165567766992-17J1")
            .careUnitId("SE165567766992-17J1")
            .patientId(patientId)
            .infoType(InformationType.VKO)
            .allowedForPatient(allowedForPatient)
            .build();
    }

    static public PatientDataEntry<Object> getPatientDataEntry(String patientId) {
        return getPatientDataEntry(patientId, true);
    }

    static public PatientDataEntry<Object> getPatientDataEntry() {
        return getPatientDataEntry("191212121212");
    }

    static public CitizenPrincipal getCitizenPrincipalFor(String patientId) {
        return new CitizenPrincipal(patientId, "Given-Name", "Middle-And-Surname");
    }

    static public CareActorPrincipal getCareActorPrincipal() {
        return CareActorPrincipal.builder()
            .careActorGivenName("Test")
            .careActorMiddleAndSurName("Testsson")
            .hsaId("SE165567766992-17J2")
            .careProviderId("SE165567766992-17J1")
            .careProviderName("SE165567766992-17J1")
            .careUnitId("SE165567766992-17J1")
            .careUnitName("SE165567766992-17J1")
            .commissionPurpose("Vård och behandling")
            .commissionRights(Collections.singletonList(CommissionRight.builder().permission("läsa").infoType("ALLA").scope("sjf").build()))
            .build();
    }
}
