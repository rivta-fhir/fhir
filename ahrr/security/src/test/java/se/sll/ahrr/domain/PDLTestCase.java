package se.sll.ahrr.domain;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.Data;
import se.sll.ahrr.security.domain.Block;
import se.sll.ahrr.security.domain.Consent;

import java.util.ArrayList;
import java.util.List;

public @Data
class PDLTestCase {
    private String testcase;
    private String rowid;
    private String patientid;
    private String informationTime;
    private String infotype;
    private String informationCareProviderId;
    private String informationCareUnitId;
    private String result;
    private String reason_code;
    private String reason_text;
    private String user_rowid;
    private String user_careGiver;
    private String user_careUnitHsaIdentity;
    private String user_hsaIdentityPerson;
    private String user_miuPurpose;
    private String user_miuRights_aktivitet;
    private String user_miuRights_infotype;
    private String user_miuRights_omf;
    private String consent_consents;
    private String consent_rowid;
    private String consent_AssertionType;
    private String consent_CareUnitId;
    private String consent_EmployeeId;
    private String consent_EndDate;
    private String consent_PatientId;
    private String block_blocks;
    private String block_rowid;
    private String block_BlockType;
    private String block_ExcludedInformationTypes;
    private String block_InformationCareProviderId;
    private String block_InformationCareUnitId;
    private String block_InformationEndDate;
    private String block_InformationStartDate;
    private String block_PatientId;
    private String block_TemporaryRevokes;
    private String block_EndDate;
    private String block_RevokedForCareUnitId;
    private String block_RevokedForEmployeeId;

    private List<Block> blocks = new ArrayList<>();
    private List<Consent> consents = new ArrayList<>();

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PDLTestCase{");
        sb.append("\n  testcase='").append(testcase).append('\'');
        sb.append("\n  rowid='").append(rowid).append('\'');
        sb.append("\n  patientid='").append(patientid).append('\'');
        sb.append("\n  informationTime='").append(informationTime).append('\'');
        sb.append("\n  infotype='").append(infotype).append('\'');
        sb.append("\n  informationCareProviderId='").append(informationCareProviderId).append('\'');
        sb.append("\n  informationCareUnitId='").append(informationCareUnitId).append('\'');
        sb.append("\n  result='").append(result).append('\'');
        sb.append("\n  reason_code='").append(reason_code).append('\'');
        sb.append("\n  reason_text='").append(reason_text).append('\'');
        sb.append("\n  user_rowid='").append(user_rowid).append('\'');
        sb.append("\n  user_careGiver='").append(user_careGiver).append('\'');
        sb.append("\n  user_careUnitHsaIdentity='").append(user_careUnitHsaIdentity).append('\'');
        sb.append("\n  user_hsaIdentityPerson='").append(user_hsaIdentityPerson).append('\'');
        sb.append("\n  user_miuPurpose='").append(user_miuPurpose).append('\'');
        sb.append("\n  user_miuRights_aktivitet='").append(user_miuRights_aktivitet).append('\'');
        sb.append("\n  user_miuRights_infotype='").append(user_miuRights_infotype).append('\'');
        sb.append("\n  user_miuRights_omf='").append(user_miuRights_omf).append('\'');
        sb.append("\n  consent_consents='").append(consent_consents).append('\'');
        sb.append("\n  consent_rowid='").append(consent_rowid).append('\'');
        sb.append("\n  consent_AssertionType='").append(consent_AssertionType).append('\'');
        sb.append("\n  consent_CareUnitId='").append(consent_CareUnitId).append('\'');
        sb.append("\n  consent_EmployeeId='").append(consent_EmployeeId).append('\'');
        sb.append("\n  consent_EndDate='").append(consent_EndDate).append('\'');
        sb.append("\n  consent_PatientId='").append(consent_PatientId).append('\'');
        sb.append("\n  block_blocks='").append(block_blocks).append('\'');
        sb.append("\n  block_rowid='").append(block_rowid).append('\'');
        sb.append("\n  block_BlockType='").append(block_BlockType).append('\'');
        sb.append("\n  block_ExcludedInformationTypes='").append(block_ExcludedInformationTypes).append('\'');
        sb.append("\n  block_InformationCareProviderId='").append(block_InformationCareProviderId).append('\'');
        sb.append("\n  block_InformationCareUnitId='").append(block_InformationCareUnitId).append('\'');
        sb.append("\n  block_InformationEndDate='").append(block_InformationEndDate).append('\'');
        sb.append("\n  block_InformationStartDate='").append(block_InformationStartDate).append('\'');
        sb.append("\n  block_PatientId='").append(block_PatientId).append('\'');
        sb.append("\n  block_TemporaryRevokes='").append(block_TemporaryRevokes).append('\'');
        sb.append("\n  block_EndDate='").append(block_EndDate).append('\'');
        sb.append("\n  block_RevokedForCareUnitId='").append(block_RevokedForCareUnitId).append('\'');
        sb.append("\n  block_RevokedForEmployeeId='").append(block_RevokedForEmployeeId).append('\'');
        sb.append("\n}");
        return sb.toString();
    }
}
