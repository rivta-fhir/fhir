package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.domain.Principal;
import se.sll.ahrr.security.domain.*;

import java.util.List;

/*
 * This class has been dumbed-down to disable access control completely
 */
public class AccessControl {

    private Logger log = LoggerFactory.getLogger(getClass());

    public AccessControl() {
    }

    public AccessControlledResult performAccessControl(PatientDataEntry patientDataEntry, Principal principal) {
        return AccessControlledResult.builder().result(AccessResult.ACCESS_GRANTED).patientDataEntry(patientDataEntry).build();
    }
}
