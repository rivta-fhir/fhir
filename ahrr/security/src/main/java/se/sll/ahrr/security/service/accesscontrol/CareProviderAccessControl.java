package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.CommissionRight;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.security.domain.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CareProviderAccessControl {

    private Set<InformationType> excludedInformationTypes = new HashSet<>(Arrays.asList(InformationType.PU, InformationType.CONSENT));

    private Logger log = LoggerFactory.getLogger(getClass());

    private boolean isRevoked(CareActorPrincipal principal, List<TemporaryRevoke> temporaryRevokes) {
        if (temporaryRevokes == null || temporaryRevokes.isEmpty()) {
            return false;
        }

        if (temporaryRevokes.stream()
            .filter(r -> principal.getHsaId().equalsIgnoreCase(r.getRevokedForEmployeeId()))
            .filter(r -> r.getEndDate() == null || now().isBefore(r.getEndDate()))
            .count() > 0) {
            log.debug("Block is revoked for employee");
            return true;
        }

        if (temporaryRevokes.stream()
            .filter(r -> StringUtils.isEmpty(r.getRevokedForEmployeeId()))
            .filter(r -> principal.getCareUnitId().equalsIgnoreCase(r.getRevokedForCareUnitId()))
            .filter(r -> r.getEndDate() == null || now().isBefore(r.getEndDate()))
            .count() > 0) {
            log.debug("Block is revoked for care unit");
            return true;
        }

        return false;
    }

    private AccessResult checkBlocks(PatientDataEntry patientDataEntry, CareActorPrincipal principal, List<Block> blocks) {
        if (blocks.stream()
            .filter(b -> !patientDataEntry.getPatientId().equalsIgnoreCase(b.getPatientId()))
            .count() > 0
            ) {
            log.debug("Block contains the wrong patient");
            return AccessResult.ACCESS_DENIED;
        }

        if (blocks.stream()
            .filter(b -> StringUtils.isEmpty(b.getInformationCareProviderId()))
            .count() > 0
            ) {
            log.debug("Block is missing information care provider");
            return AccessResult.ACCESS_DENIED;
        }

        if (!principal.getCareProviderId().equalsIgnoreCase(patientDataEntry.getCareProviderId()) && blocks.stream()
            .filter(b -> b.getType() == Block.Type.OUTER)
            .filter(b -> b.getInformationStartDate() == null || b.getInformationStartDate().isBefore(patientDataEntry.getTimestamp()))
            .filter(b -> b.getInformationEndDate() == null || b.getInformationEndDate().isAfter(patientDataEntry.getTimestamp()))
            .filter(b -> patientDataEntry.getCareProviderId().equalsIgnoreCase(b.getInformationCareProviderId()))
            .filter(b -> b.getExcludedInformationTypes() == null || !b.getExcludedInformationTypes().contains(patientDataEntry.getInfoType()))
            .filter(b -> !isRevoked(principal, b.getTemporaryRevokes()))
            .count() > 0
            ) {
            log.debug("Outer block");
            return AccessResult.ACCESS_DENIED;
        }

        if (blocks.stream()
            .filter(b -> b.getType() == Block.Type.INNER)
            .filter(b -> StringUtils.isEmpty(b.getInformationCareUnitId()))
            .count() > 0
            ) {
            log.debug("Inner block is missing information care unit");
            return AccessResult.ACCESS_DENIED;
        }

        if (!principal.getCareUnitId().equalsIgnoreCase(patientDataEntry.getCareUnitId()) && blocks.stream()
            .filter(b -> b.getType() == Block.Type.INNER)
            .filter(b -> b.getInformationStartDate() == null || b.getInformationStartDate().isBefore(patientDataEntry.getTimestamp()))
            .filter(b -> b.getInformationEndDate() == null || b.getInformationEndDate().isAfter(patientDataEntry.getTimestamp()))
            .filter(b -> patientDataEntry.getCareUnitId().equalsIgnoreCase(b.getInformationCareUnitId()))
            .filter(b -> b.getExcludedInformationTypes() != null && !b.getExcludedInformationTypes().contains(patientDataEntry.getInfoType()))
            .filter(b -> !isRevoked(principal, b.getTemporaryRevokes()))
            .count() > 0
            ) {
            log.debug("Inner block");
            return AccessResult.ACCESS_DENIED;
        }

        return AccessResult.ACCESS_GRANTED;
    }

    private AccessResult checkConsents(PatientDataEntry patientDataEntry, CareActorPrincipal principal, List<Consent> consents) {
        for (Consent c : consents) {
            if (c.getPatientId() == null) {
                return AccessResult.ACCESS_DENIED;
            }

            if (!c.getPatientId().equalsIgnoreCase(patientDataEntry.getPatientId())) {
                return AccessResult.ACCESS_DENIED;
            }
        }

        if (patientDataEntry.getCareProviderId().equalsIgnoreCase(principal.getCareProviderId())) {
            return AccessResult.ACCESS_GRANTED;
        }

        if (patientDataEntry.getTimestamp() == null) {
            return AccessResult.ACCESS_DENIED;
        }

        if (StringUtils.isEmpty(principal.getHsaId())) {
            return AccessResult.ACCESS_DENIED;
        }

        if (consents.stream()
            .filter(c -> principal.getHsaId().equalsIgnoreCase(c.getEmployeeId()))
            .filter(c -> c.getEndDate() == null || c.getEndDate().isAfter(now()))
            .count() > 0) {
            return AccessResult.ACCESS_GRANTED;
        }

        if (consents.stream()
            .filter(c -> c.getType() == Consent.Type.CONSENT)
            .filter(c -> principal.getCareUnitId().equalsIgnoreCase(c.getCareUnitId()))
            .filter(c -> c.getEndDate() == null || c.getEndDate().isAfter(now()))
            .count() > 0) {
            return AccessResult.ACCESS_GRANTED;
        }


        return AccessResult.ACCESS_DENIED;
    }

    private boolean hasRight(List<CommissionRight> commissionRights, CommissionRight.Scope scope, CommissionRight.Permission permission, InformationType infoType) {
        return commissionRights
            .stream()
            .filter(r -> r.getPermission() == permission)
            .filter(r -> r.getScope() == scope)
            .filter(r -> r.getInfoType() != InformationType.UNKNOWN)
            .filter(r -> infoType == r.getInfoType() || InformationType.ALLA == r.getInfoType())
            .count() > 0;
    }

    private AccessResult checkUserRights(PatientDataEntry patientDataEntry, CareActorPrincipal principal) {

        if (StringUtils.isEmpty(principal.getCareUnitId())) {
            return AccessResult.ACCESS_DENIED;
        }

        if (StringUtils.isEmpty(principal.getCareProviderId())) {
            return AccessResult.ACCESS_DENIED;
        }

        // Give access if the user has READ access for national entries
        if (hasRight(principal.getCommissionRights(),
            CommissionRight.Scope.NATIONAL,
            CommissionRight.Permission.READ,
            patientDataEntry.getInfoType())) {
            return AccessResult.ACCESS_GRANTED;
        }

        // Give access if the user has READ access inside care provider
        // and the record is from the same care provider
        if (principal.getCareProviderId().equalsIgnoreCase(patientDataEntry.getCareProviderId()) && hasRight(principal.getCommissionRights(),
            CommissionRight.Scope.CARE_PROVIDER,
            CommissionRight.Permission.READ,
            patientDataEntry.getInfoType())) {
            return AccessResult.ACCESS_GRANTED;
        }

        // Give access if the user has READ access inside care unit
        // and the record is from the same care unit
        if (principal.getCareUnitId().equalsIgnoreCase(patientDataEntry.getCareUnitId()) && hasRight(principal.getCommissionRights(),
            CommissionRight.Scope.CARE_UNIT,
            CommissionRight.Permission.READ,
            patientDataEntry.getInfoType())) {
            return AccessResult.ACCESS_GRANTED;
        }

        // Give access if the user has access to a specific
        // care organization and the record is from the same care unit or care provider
        if (principal.getCommissionRights()
            .parallelStream()
            .filter(r -> r.getPermission() == CommissionRight.Permission.READ)
            .filter(r -> r.getScope() == CommissionRight.Scope.SPECIFIC_ORGANIZATIONAL_UNIT)
            .filter(r -> r.getInfoType() != InformationType.UNKNOWN)
            .filter(r -> patientDataEntry.getCareProviderId().equalsIgnoreCase(r.getSpecificOrganizationId()) || patientDataEntry.getCareUnitId().equalsIgnoreCase(r.getSpecificOrganizationId()))
            .filter(r -> patientDataEntry.getInfoType() == r.getInfoType() || InformationType.ALLA == r.getInfoType())
            .count() > 0) {
            return AccessResult.ACCESS_GRANTED;
        }

        return AccessResult.ACCESS_DENIED;
    }

    AccessControlledResult performAccessControl(PatientDataEntry patientDataEntry, CareActorPrincipal principal, List<Block> blocks, List<Consent> consents) {
        if (StringUtils.isEmpty(patientDataEntry.getInfoType())) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_MISSING_INFO_TYPE)
                .build();
        }

        if (excludedInformationTypes.contains(patientDataEntry.getInfoType())) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_GRANTED)
                .build();
        }

        if (!"Vård och behandling".equalsIgnoreCase(principal.getCommissionPurpose())) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.WRONG_COMMISSION_PURPOSE)
                .build();
        }

        if (blocks == null) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_MISSING_BLOCKS)
                .build();
        }

        if (consents == null) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_MISSING_CONSENTS)
                .build();
        }

        if (StringUtils.isEmpty(patientDataEntry.getCareProviderId())) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_MISSING_CARE_PROVIDER)
                .build();
        }

        if (StringUtils.isEmpty(patientDataEntry.getCareUnitId())) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_MISSING_CARE_UNIT)
                .build();
        }

        if (checkUserRights(patientDataEntry, principal) == AccessResult.ACCESS_DENIED) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.INSUFFICIENT_RIGHTS)
                .build();
        }

        if (checkBlocks(patientDataEntry, principal, blocks) == AccessResult.ACCESS_DENIED) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.DATA_BLOCKED)
                .build();
        }

        if (checkConsents(patientDataEntry, principal, consents) == AccessResult.ACCESS_DENIED) {
            return AccessControlledResult.builder()
                .patientDataEntry(patientDataEntry)
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.NO_CONSENT)
                .build();
        }

        return AccessControlledResult.builder()
            .patientDataEntry(patientDataEntry)
            .result(AccessResult.ACCESS_GRANTED)
            .build();
    }

    LocalDateTime now() {
        return LocalDateTime.now();
    }
}
