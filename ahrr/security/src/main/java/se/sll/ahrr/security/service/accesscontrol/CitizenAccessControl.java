package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.stereotype.Service;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessDeniedReason;
import se.sll.ahrr.security.domain.AccessResult;

@Service
public class CitizenAccessControl {

    AccessControlledResult performAccessControl(PatientDataEntry patientDataEntry, CitizenPrincipal principal) {
        if (patientDataEntry.getPatientId() == null || patientDataEntry.getPatientId().isEmpty()) {
            return AccessControlledResult.builder()
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.MISSING_PATIENT)
                .build();
        }

        if (principal.getSubjectSerialNumber() == null || principal.getSubjectSerialNumber().isEmpty()) {
            return AccessControlledResult.builder()
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.INVALID_PRINCIPAL)
                .build();
        }

        if (!patientDataEntry.getPatientId().equals(principal.getSubjectSerialNumber())) {
            return AccessControlledResult.builder()
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.BELONGS_TO_WRONG_PATIENT)
                .build();
        }
        if (!patientDataEntry.isAllowedForPatient()) {
            return AccessControlledResult.builder()
                .result(AccessResult.ACCESS_DENIED)
                .accessDeniedReason(AccessDeniedReason.NOT_ALLOWED_FOR_PATIENT)
                .build();
        }

        return AccessControlledResult.builder()
            .result(AccessResult.ACCESS_GRANTED)
            .build();
    }
}
