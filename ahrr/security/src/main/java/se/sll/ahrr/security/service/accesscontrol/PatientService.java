package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import riv.population.residentmaster._1.JaNejTYPE;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileResponseType;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;

@Service
public class PatientService {
    private Logger log = LoggerFactory.getLogger(getClass());
    private RivtaDataLoader<LookupResidentForFullProfileResponseType> lookupResidentForFullProfileResponseTypeRivtaDataLoader;

    public PatientService(@Qualifier("lookupResidentForFullProfileDataLoader") RivtaDataLoader<LookupResidentForFullProfileResponseType> lookupResidentForFullProfileResponseTypeRivtaDataLoader) {
        this.lookupResidentForFullProfileResponseTypeRivtaDataLoader = lookupResidentForFullProfileResponseTypeRivtaDataLoader;
    }

    public boolean hasSecuredIdentity(final String clientId) {
        try {
            return lookupResidentForFullProfileResponseTypeRivtaDataLoader.loadRivtaResponse(clientId).getResident().stream()
                .anyMatch(residentType -> residentType.getSekretessmarkering() == JaNejTYPE.J);
        } catch (BadGatewayException e) {
            log.error("Could not fetch patient for secured identity check", e);
            return true;
        }
    }
}
