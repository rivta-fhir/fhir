package se.sll.ahrr.security.domain;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public enum AccessDeniedReason {
    INVALID_PRINCIPAL,
    NOT_ALLOWED_FOR_PATIENT,
    BELONGS_TO_WRONG_PATIENT,
    MISSING_PATIENT,
    WRONG_COMMISSION_PURPOSE,
    DATA_MISSING_BLOCKS,
    DATA_MISSING_CONSENTS,
    DATA_MISSING_CARE_UNIT,
    DATA_MISSING_CARE_PROVIDER,
    DATA_MISSING_INFO_TYPE,
    DATA_BLOCKED,
    NO_CONSENT,
    INSUFFICIENT_RIGHTS,
    SECURED_PATIENT,
    PATIENT_DATA_ENTRY_MISSING,
    INTERNAL_ERROR
}
