package se.sll.ahrr.security.domain;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import se.sll.ahrr.domain.InformationType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@ToString
public class Block {

    private String id;
    @NonNull private Type type;
    private String patientId;
    private LocalDateTime informationStartDate;
    private LocalDateTime informationEndDate;
    private String informationCareUnitId;
    private String informationCareProviderId;
    private List<InformationType> excludedInformationTypes;
    private List<TemporaryRevoke> temporaryRevokes;

    public static class BlockBuilder {

        public BlockBuilder() {
            this.temporaryRevokes = new ArrayList<>();
        }

        /**
         * Add a list of excluded info types
         *
         * @param excludedInformationTypes only lak and UPP are valid values, others will be ignored.
         * @see <a href=https://bitbucket.org/rivta-domains/riv.ehr.blocking/raw/ehr_blocking_3.2.2/docs/TKB_ehr_blocking_3.2.2.docx>TKB</a>
         */
        public BlockBuilder excludedInformationTypes(List<String> excludedInformationTypes) {
            if (this.excludedInformationTypes == null) {
                this.excludedInformationTypes = new ArrayList<>();
            }
            excludedInformationTypes.forEach(
                infoType -> {
                    if ("lak".equalsIgnoreCase(infoType)) {
                        this.excludedInformationTypes.add(InformationType.LKF);
                        this.excludedInformationTypes.add(InformationType.LKO);
                    }
                    if ("UPP".equalsIgnoreCase(infoType)) {
                        this.excludedInformationTypes.add(InformationType.UPP);
                    }
                }
            );
            return this;
        }
    }

    public enum Type {
        INNER,
        OUTER
    }
}
