package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import riv.ehr.blocking._2.BlockType;
import riv.ehr.blocking._2.InformationTypeType;
import riv.ehr.blocking._2.TemporaryRevokeType;
import riv.ehr.blocking.querying.getallblocksforpatientresponder._2.GetAllBlocksForPatientResponseType;
import se.sll.ahrr.dataloader.GetAllBlocksForPatientDataLoader;
import se.sll.ahrr.security.domain.Block;
import se.sll.ahrr.security.domain.TemporaryRevoke;
import se.sll.ahrr.security.exception.BlockRetrievalException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static se.sll.ahrr.common.Helpers.xmlGregorianCalendarToDateTime;

@Service
public class BlockService {

    private GetAllBlocksForPatientDataLoader dataLoader;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private boolean blocksDisabled;

    public BlockService(GetAllBlocksForPatientDataLoader dataLoader, @Value("${blockcheck.disabled:#{false}}") boolean blocksDisabled) {
        this.dataLoader = dataLoader;
        this.blocksDisabled = blocksDisabled;

        if (blocksDisabled) {
            log.warn("\n" +
                "---------------------------------------------------\n" +
                "--------------------- WARNING ---------------------\n" +
                "---------------------------------------------------\n" +
                "-- Block checks are disabled, if this setting is --\n" +
                "-- used with a health care system that releases  --\n" +
                "-- blocked information this setting breaks       --\n" +
                "-- Swedish regulations                           --\n" +
                "---------------------------------------------------\n" +
                "---------------------------------------------------\n" +
                "---------------------------------------------------");
        }
    }

    public List<Block> getAllBlocksForPatient(final String patientId) throws BlockRetrievalException {
        if (blocksDisabled) {
            return Collections.emptyList();
        }

        try {
            final GetAllBlocksForPatientResponseType response = dataLoader.loadRivtaResponse(patientId);
            if (response == null) {
                throw new BlockRetrievalException("Response was null");
            }
            final List<Block> retval = new ArrayList<>();
            for (BlockType blockType : response.getBlockHeaderType().getBlocks()) {
                retval.add(getBlock(blockType));
            }
            return retval;

        } catch (BlockRetrievalException e) {
            throw e;
        } catch (Exception e) {
            throw new BlockRetrievalException("Unexpected error while getting blocks", e);
        }
    }

    private Block getBlock(final BlockType blockType) throws BlockRetrievalException {
        try {
            final String id = blockType.getBlockId();
            final Block.Type type = blockType.getBlockType().value().equalsIgnoreCase("Outer") ? Block.Type.OUTER : Block.Type.INNER;
            final String ownerId = blockType.getOwnerId();
            final String careProviderId = blockType.getInformationCareProviderId();
            final String careUnitId = blockType.getInformationCareUnitId();
            final List<String> excludedInformationTypes = blockType.getExcludedInformationTypes().stream().map(InformationTypeType::getInfoTypeId).collect(Collectors.toList());
            final String patientId = blockType.getPatientId();

            Block.BlockBuilder builder = Block.builder()
                .informationCareProviderId(careProviderId)
                .informationCareUnitId(careUnitId)
                .type(type)
                .id(id)
                .patientId(patientId)
                .excludedInformationTypes(excludedInformationTypes);

            if (blockType.getInformationStartDate() != null) {
                builder.informationStartDate(xmlGregorianCalendarToDateTime(blockType.getInformationStartDate()));
            }
            if (blockType.getInformationEndDate() != null) {
                builder.informationEndDate(xmlGregorianCalendarToDateTime(blockType.getInformationEndDate()));
            }
            if (blockType.getTemporaryRevokes() != null) {
                builder.temporaryRevokes(getTemporaryRevokes(blockType.getTemporaryRevokes()));
            }
            return builder.build();
        } catch (Exception e) {
            throw new BlockRetrievalException("Error while parsing BlockType", e);
        }
    }

    private List<TemporaryRevoke> getTemporaryRevokes(final List<TemporaryRevokeType> temporaryRevokeTypes) {
        return temporaryRevokeTypes.stream().map(this::getTemporaryRevoke).collect(Collectors.toList());
    }

    private TemporaryRevoke getTemporaryRevoke(final TemporaryRevokeType temporaryRevokeType) {
        return TemporaryRevoke.builder()
            .endDate(xmlGregorianCalendarToDateTime(temporaryRevokeType.getEndDate()))
            .revokedForCareUnitId(temporaryRevokeType.getRevokedForCareUnitId())
            .revokedForEmployeeId(temporaryRevokeType.getRevokedForEmployeeId()).build();
    }
}
