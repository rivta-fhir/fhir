package se.sll.ahrr.security.service.accesscontrol;

/*-
 * #%L
 * security
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.stereotype.Service;
import riv.ehr.patientconsent._1.PDLAssertionType;
import riv.ehr.patientconsent.querying.getconsentsforpatientresponder._1.GetConsentsForPatientResponseType;
import se.sll.ahrr.common.Helpers;
import se.sll.ahrr.dataloader.GetConsentsForPatientDataLoader;
import se.sll.ahrr.dataloader.exception.BadGatewayException;
import se.sll.ahrr.security.domain.Consent;
import se.sll.ahrr.security.exception.ConsentRetrievalException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConsentService {

    private GetConsentsForPatientDataLoader dataloader;

    public ConsentService(GetConsentsForPatientDataLoader dataloader) {
        this.dataloader = dataloader;
    }

    public List<Consent> getConsentsForPatient(final String careProviderId, final String patientId) throws ConsentRetrievalException, BadGatewayException {
        return getConsent(dataloader.loadRivtaResponse(patientId, careProviderId));
    }

    private List<Consent> getConsent(final GetConsentsForPatientResponseType response) throws ConsentRetrievalException {
        try {
            return response.getGetConsentsResultType().getPdlAssertions().parallelStream().map(this::getConsent).collect(Collectors.toList());
        } catch (Exception e) {
            throw new ConsentRetrievalException("Unexpected exception while parsing CONSENT", e);
        }
    }

    private Consent getConsent(final PDLAssertionType pdlAssertion) {
        Consent.ConsentBuilder builder = Consent.builder()
            .patientId(pdlAssertion.getPatientId())
            .careUnitId(pdlAssertion.getCareUnitId())
            .employeeId(pdlAssertion.getEmployeeId())
            .type(Consent.Type.valueOf(pdlAssertion.getAssertionType().name().toUpperCase()));

        if (pdlAssertion.getEndDate() != null) {
            builder.endDate(Helpers.xmlGregorianCalendarToDateTime(pdlAssertion.getEndDate()));
        }
        return builder.build();

    }

}
