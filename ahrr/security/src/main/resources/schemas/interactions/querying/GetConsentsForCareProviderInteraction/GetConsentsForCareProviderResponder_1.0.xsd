<?xml version="1.0" encoding="UTF-8" ?>
<!-- 
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements. See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership. Inera AB licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License. You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied. See the License for the
 specific language governing permissions and limitations
 under the License.
 -->	
<xs:schema xmlns:xs='http://www.w3.org/2001/XMLSchema' 
	xmlns:tns='urn:riv:ehr:patientconsent:querying:GetConsentsForCareProviderResponder:1'				
	xmlns:patientconsent='urn:riv:ehr:patientconsent:1'
	targetNamespace='urn:riv:ehr:patientconsent:querying:GetConsentsForCareProviderResponder:1'
	elementFormDefault='qualified'
	attributeFormDefault='unqualified'
	version='1.0'>

	<xs:import schemaLocation='../../../core_components/ehr_patientconsent_1.0.xsd' namespace='urn:riv:ehr:patientconsent:1' />


	<xs:element name='GetConsentsForCareProviderRequest' type='tns:GetConsentsForCareProviderRequestType'/>

	<xs:complexType name='GetConsentsForCareProviderRequestType'>
		<xs:annotation>
		    <xs:documentation xml:lang='sv'>
		        Tjänst som läser alla giltiga samtyckesintyg för en viss vårdgivare med grundinformation.        
		        Med giltiga samtyckesintyg avses de samtyckesintyg, alternativt intyg om nödsituation, som används som underlag vid en kontroll av åtkomst (CheckConsents).
		        Det är valbart om makulerade och återkallade samtyckesintyg som ej är utgångna (giltigt t o m har passerats) skall returneras. 
		        Utgångna samtyckesintyg (giltigt t o m har passerats) returneras ej oavsett makulering eller återkallning.
		        Det går även att ange en tidpunkt (CreatedOnOrAfter) från när man önskar inhämta uppgifter och på så sätt undvika att inhämta data som redan hämtats vid ett tidigare tillfälle. Här avses tidpunkten då samtycket lagrades i tjänsten.         
		        Tjänsten tillåts att dela upp listan av samtyckesintyg i mindre delar för att minska på belastningen på systemet. Om detta sker kommer flaggan HasMore att vara satt om det finns fler samtyckesintyg att hämta. De resterande samtyckesintygen skall i så fall hämtas med ytterligare anrop till tjänsten tills flaggan HashMore ej längre är satt (false). 
		        Tjänsten returnerar en ny tidpunkt (CreatedOnOrAfter) som anger från och med nästa tidpunkt som samtyckesintygen ej har hämtats. Detta värde kan användas som inparameter i ytterligare anrop till tjänsten för att hämta nästa sekvens av samtyckesintyg.                
		        Tjänsten kan användas i ett integrationsmönster där vårdsystemet med visst intervall inhämtar alla samtycken det behöver utifrån de vårdgivare som systemet hanterar information från, för att sedan vid behov utföra intern kontroll mot underlaget av samtycken och nödsituationsintyg.        
		        Viktigt att kontrollera att alla samtycken är hämtade genom att kontrollera värdet på flaggan HasMore.
		    </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="CareProviderId" type="patientconsent:HsaId"/>
			<xs:element name="CreatedOnOrAfter" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="GetCancelledFlag" type="xs:boolean"/>
			<xs:any maxOccurs="unbounded" minOccurs="0" namespace="##other" processContents="lax"/>
		</xs:sequence>
	</xs:complexType>

	<xs:element name='GetConsentsForCareProviderResponse' type='tns:GetConsentsForCareProviderResponseType'/>

	<xs:complexType name='GetConsentsForCareProviderResponseType'>
		<xs:annotation>
		    <xs:documentation xml:lang='sv'>
		        Tjänst som läser alla giltiga samtyckesintyg för en viss vårdgivare med grundinformation.        
		        Med giltiga samtyckesintyg avses de samtyckesintyg, alternativt intyg om nödsituation, som används som underlag vid en kontroll av åtkomst (CheckConsents).
		        Det är valbart om makulerade och återkallade samtyckesintyg som ej är utgångna (giltigt t o m har passerats) skall returneras. 
		        Utgångna samtyckesintyg (giltigt t o m har passerats) returneras ej oavsett makulering eller återkallning.
		        Det går även att ange en tidpunkt (CreatedOnOrAfter) från när man önskar inhämta uppgifter och på så sätt undvika att inhämta data som redan hämtats vid ett tidigare tillfälle. Här avses tidpunkten då samtycket lagrades i tjänsten.         
		        Tjänsten tillåts att dela upp listan av samtyckesintyg i mindre delar för att minska på belastningen på systemet. Om detta sker kommer flaggan HasMore att vara satt om det finns fler samtyckesintyg att hämta. De resterande samtyckesintygen skall i så fall hämtas med ytterligare anrop till tjänsten tills flaggan HashMore ej längre är satt (false). 
		        Tjänsten returnerar en ny tidpunkt (CreatedOnOrAfter) som anger från och med nästa tidpunkt som samtyckesintygen ej har hämtats. Detta värde kan användas som inparameter i ytterligare anrop till tjänsten för att hämta nästa sekvens av samtyckesintyg.                
		        Tjänsten kan användas i ett integrationsmönster där vårdsystemet med visst intervall inhämtar alla samtycken det behöver utifrån de vårdgivare som systemet hanterar information från, för att sedan vid behov utföra intern kontroll mot underlaget av samtycken och nödsituationsintyg.        
		        Viktigt att kontrollera att alla samtycken är hämtade genom att kontrollera värdet på flaggan HasMore.
		    </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="GetAllAssertionsResultType" type="patientconsent:GetAllAssertionsResultType"/>
			<xs:any maxOccurs="unbounded" minOccurs="0" namespace="##other" processContents="lax"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
