package se.sll.ahrr.codesystem.valueset.flag;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.ValueSet;

public enum Category implements ValueSet {
    diet("Diet", CodeSystem.FLAG_CATEGORY),
    drug("Drug", CodeSystem.FLAG_CATEGORY),
    lab("Lab", CodeSystem.FLAG_CATEGORY),
    admin("Administrative", CodeSystem.FLAG_CATEGORY),
    contact("Subject contact", CodeSystem.FLAG_CATEGORY),
    communicableDisease("Communicable disease", CodeSystem.FLAG_CATEGORY_EXTENSION, "communicable disease");

    private String displayName;
    private CodeSystem codeSystem;
    private String code;

    Category(final String displayName, final CodeSystem codeSystem) {
        this.displayName = displayName;
        this.codeSystem = codeSystem;
        this.code = this.name();
    }

    Category(final String displayName, final CodeSystem codeSystem, final String code) {
        this.displayName = displayName;
        this.codeSystem = codeSystem;
        this.code = code;
    }

    @Override
    public String getCodeSystem() {
        return codeSystem.getValue();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getCode() {
        return code;
    }
}
