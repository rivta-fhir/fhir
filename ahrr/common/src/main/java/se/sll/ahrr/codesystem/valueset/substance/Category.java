package se.sll.ahrr.codesystem.valueset.substance;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.ValueSet;

public enum Category implements ValueSet {
    allergen("Allergen"),
    biological("Biological Substance"),
    body("Body Substance"),
    chemical("Chemical"),
    food("Dietary Substance"),
    drug("Drug or Medicament"),
    material("Material");

    private final String display;

    Category(final String display) {
        this.display = display;
    }

    @Override
    public String getCodeSystem() {
        return CodeSystem.SUBSTANCE_CATEGORY.getValue();
    }

    @Override
    public String getDisplayName() {
        return display;
    }

    @Override
    public String getCode() {
        return this.name();
    }
}
