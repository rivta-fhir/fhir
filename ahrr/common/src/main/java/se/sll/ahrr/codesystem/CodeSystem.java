package se.sll.ahrr.codesystem;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.stream.Stream;

public enum CodeSystem {

    ACT_CONSENT_TYPE("urn:oid:", "2.16.840.1.113883.1.11.19897"),
    AUDIT_EVENT_ID("urn:oid:", "2.16.840.1.113883.4.642.3.455"),
    AUDIT_EVENT_ACTION("urn:oid:", "2.16.840.1.113883.4.642.3.445"),
    AUDIT_EVENT_ENTITY_TYPE("urn:oid:", "2.16.840.1.113883.4.642.3.894"),
    CONSENT_ACTION("urn:oid:", "2.16.840.1.113883.4.642.3.737"),
    CONSENT_TYPE("uri:", "se.sll.ahrr.consenttype"),
    HSA("urn:oid:", "1.2.752.129.2.1.4.1"),
    ORGANIZATION_TYPE("uri:", "se.sll.fhir.organizationtype"),
    ROLE_CLASS("urn:oid:", "2.16.840.1.113883.5.110"),
    DIAGNOSIS_TYPE("uri:", "se.sll.ahrr.diagnosistype"),
    FLAG_CATEGORY_EXTENSION("uri:", "se.sll.ahrr.flagcategoryextension"),
    ICD10_SE("urn:oid:", "1.2.752.116.1.1.1.1.3"),
    PERSONID_SE("urn:oid:", "1.2.752.129.2.1.3.1"),
    PERSONID_RESERVE_SE("urn:oid:", "1.2.752.129.2.1.3.2"),
    PERSONID_RESERVE_SLL_SE("urn:oid:", "1.2.752.97.3.1.3"),
    PERSONID_COORDINATION_NUMBER_SE("urn:oid:", "1.2.752.129.2.1.3.3"), //Samordningsnummer
    CONDITION_CATEGORY("urn:oid:", "2.16.840.1.113883.4.642.3.153"),
    MARITAL_STATUS("", "http://hl7.org/fhir/ValueSet/marital-status"),
    RULE_SET("urn:", "se.sll.ahrr.ruleset"),
    NULL_FLAVOR("", "http://hl7.org/fhir/v3/NullFlavor"),
    SNOMED_CT_SE("urn:oid:", "1.2.752.116.2.1.1"),
    SNOMED_CT("uri:", "http://snomed.info/sct"),
    NPU_CODE("urn:oid:", "1.2.752.108.1"),
    ATC("urn:oid:", "1.2.752.129.2.2.3.1.1"),
    SUBSTANCE_CATEGORY("", "http://hl7.org/fhir/substance-category"),
    NPL_ID("urn:oid:", "1.2.752.129.2.1.5.1"),
    NPL_PACKID("urn:oid:", "1.2.752.129.2.1.5.2"),
    DOCUMENT_TYPE("urn:oid:", "1.2.752.129.2.2.2.11"),
    FLAG_CATEGORY("uri:", "http://hl7.org/fhir/ValueSet/flag-category");

    private String value;
    private String namespace;

    CodeSystem(String namespace, String value) {
        this.value = value;
        this.namespace = namespace;
    }

    public static CodeSystem fromCode(String code) {
        return Stream.of(CodeSystem.values()).filter(
                c -> c.getValue().equals(code)
        ).findFirst().orElseThrow(() -> new IllegalArgumentException("No code " + code + " exists"));
    }

    public String getValue() {
        return namespace + value;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getValueWithoutNamespace() {
        return value;
    }

}
