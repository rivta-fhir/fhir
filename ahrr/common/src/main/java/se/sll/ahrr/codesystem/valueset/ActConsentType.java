package se.sll.ahrr.codesystem.valueset;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;

public enum ActConsentType implements ValueSet {
    /**
     * Any instructions, written or given verbally by a patient to a health care provider in anticipation of potential need for medical treatment. [2005 Honor My Wishes]
     */
    ACD("advance directive"),
    /**
     * Opt-in to disclosure of health information for emergency only consent directive.
     * Comment: This general consent directive specifically limits disclosure of health
     * information for purpose of emergency treatment. Additional parameters may further
     * limit the disclosure to specific users, roles, duration, types of information, and
     * impose uses obligations. [ActConsentDirective (2.16.840.1.113883.1.11.20425)]
     */
    EMRGONLY("emergency only");

    private String displayName;

    ActConsentType(String displayName) {
        this.displayName = displayName;
    }


    @Override
    public String getCodeSystem() {
        return CodeSystem.ACT_CONSENT_TYPE.getValue();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getCode() {
        return name();
    }


}
