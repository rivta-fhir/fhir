package se.sll.ahrr.service;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.HtmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class DocBookParser {
    private Logger log = LoggerFactory.getLogger(getClass());
    private Map<String, XmlElement> xmlElements;

    public DocBookParser() {
        xmlElements = new HashMap<>();
        xmlElements.put("article", new XmlElement()
                .setPre("<div class=\"article\">")
                .setPost("</div>")
                .setOutputContents(false)
        );

        xmlElements.put("info", new XmlElement()
                .setPre("<div class=\"info\">")
                .setPost("</div>")
                .setOutputContents(false)
        );

        xmlElements.put("section", new XmlElement()
                .setPre("<div class=\"section\">")
                .setPost("</div>")
                .setOutputContents(false)
        );

        xmlElements.put("para", new XmlElement()
                .setPre("<p>")
                .setPost("</p>")
                .setOutputContents(true)
        );
        xmlElements.put("title", new XmlElement()
                .setPre("<div class=\"title\">")
                .setPost("</div>")
                .setOutputContents(true)
        );
        xmlElements.put("bibliography", new XmlElement()
                .setPre("<p class=\"bibliography\"><i>")
                .setPost("</i></p>")
                .setOutputContents(true)
        );
    }

    public boolean isDocBook(final String xml) {
        if (xml == null) {
            return false;
        }

        return xml.indexOf("<?xml") == 0;
    }

    public String toHtml(final String docBookXml) throws ParsingException {
        try
        {
            String unescapedXml = docBookXml.replace("&lt;", "LESS_THEN_REPLACEMENT");
            unescapedXml = unescapedXml.replace("&gt;", "GREATER_THEN_REPLACEMENT");
            unescapedXml = unescapedXml.replaceAll("&amp;", "AND_REPLACEMENT");
            unescapedXml = StringEscapeUtils.unescapeXml(unescapedXml);
            unescapedXml = unescapedXml.replace("LESS_THEN_REPLACEMENT", "&lt;");
            unescapedXml = unescapedXml.replace("GREATER_THEN_REPLACEMENT", "&gt;");
            unescapedXml = unescapedXml.replace("AND_REPLACEMENT", "&amp;");

            String html;
            if (isDocBook(unescapedXml)) {
                html = parseXml(unescapedXml);
            } else {
                html = parsePlainText(unescapedXml);
            }
            return String.format("<div>%s</div>", html);
        }
        catch (Exception e)
        {
            throw new ParsingException("DocBookParser::toHtml. String couldn't be parsed", e);
        }
    }

    private String parseXml(final String input) throws Exception {
        Document dom = loadXMLFromString(input);
        Element rootElement = dom.getDocumentElement();
        NodeList nodes = rootElement.getChildNodes();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < nodes.getLength(); i++) {
            final Node node = nodes.item(i);
            builder.append(parseNode(node));
        }
        return builder.toString();
    }

    private String parseNode(final Node node) {
        StringBuilder strBuilder = new StringBuilder();
        if (xmlElements.containsKey(node.getNodeName())) {
            XmlElement xmlElement = xmlElements.get(node.getNodeName());
            strBuilder.append(xmlElement.getPre());
            if (xmlElement.isOutputContents()) {
                strBuilder.append(HtmlUtils.htmlEscape(node.getTextContent(), "UTF-8"));
            } else {
                NodeList childNodes = node.getChildNodes();
                for (int i = 0; i < childNodes.getLength(); i++) {
                    strBuilder.append(parseNode(childNodes.item(i)));
                }
            }
            strBuilder.append(xmlElement.getPost());
        } else {
            log.debug(String.format("Docbook parser, unrecognized tag: %s", node.getNodeName()));
        }
        return strBuilder.toString();
    }

    private Document loadXMLFromString(String xml) throws Exception {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();


            InputSource is = new InputSource(new StringReader(xml));
            return builder.parse(is);
        } catch (Exception e) {
            log.error("Couldn't load xml", e);
            throw e;
        }
    }

    private String parsePlainText(final String input) {
        String[] paragraphs = input.split("\\n{2}");
        StringBuilder strBuilder = new StringBuilder();
        for (String paragraph : paragraphs) {
            strBuilder.append(String.format("<p>%s</p>", paragraph.replaceAll("\\r?\\n", "<br />")));
        }
        return strBuilder.toString();
    }

    private class XmlElement {
        private String pre;
        private String post;
        private boolean outputContents;

        public String getPre() {
            return pre;
        }

        public XmlElement setPre(String pre) {
            this.pre = pre;
            return this;
        }

        public String getPost() {
            return post;
        }

        public XmlElement setPost(String post) {
            this.post = post;
            return this;
        }

        public boolean isOutputContents() {
            return outputContents;
        }

        public XmlElement setOutputContents(boolean outputContents) {
            this.outputContents = outputContents;
            return this;
        }
    }

    public class ParsingException extends Exception {
        public ParsingException() {
            super();
        }

        public ParsingException(String message) {
            super(message);
        }

        public ParsingException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}



