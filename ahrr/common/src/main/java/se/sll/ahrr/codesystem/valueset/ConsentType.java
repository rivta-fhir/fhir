package se.sll.ahrr.codesystem.valueset;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;

public enum ConsentType implements ValueSet {
    sjf("National ehr access");

    private String displayName;

    ConsentType(String displayName) {
        this.displayName = displayName;
    }


    @Override
    public String getCodeSystem() {
        return CodeSystem.CONSENT_TYPE.getValue();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getCode() {
        return name();
    }
}
