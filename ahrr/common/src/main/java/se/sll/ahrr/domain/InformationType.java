package se.sll.ahrr.domain;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum InformationType {
    /**
     * Access to all information types
     */
    ALLA,
    /**
     * Diagnoses
     */
    DIA,
    /**
     * Functional status
     */
    FUN,
    /**
     * Drug prescription (förskrivning)
     */
    LKF,
    /**
     * Retreived drugs from eHälsomyndigheten
     */
    LKM,
    /**
     * Drug prescription (ordination)
     */
    LKO,
    /**
     * Daily activities (PADL)
     */
    PAD,
    /**
     * Patient informations
     */
    PAT,
    /**
     * Examination results (imaging outcome, ecg, referral outcome, laboratory outcome)
     */
    UND,
    /**
     * Alert information
     */
    UPP,
    /**
     * Health care request
     */
    VBE,
    /**
     * Care contact
     */
    VKO,
    /**
     * Care documentation
     */
    VOO,
    /**
     * Care service
     */
    VOT,
    /**
     * Care plans
     */
    VPO,
    /**
     * Person national registration
     */
    PU,
    /**
     * PDL CONSENT
     */
    CONSENT,
    UNKNOWN;

    private static Logger logger = LoggerFactory.getLogger(InformationType.class);

    public static InformationType fromString(final String infoType) {
        if (infoType == null) {
            return null;
        }
        try {
            return InformationType.valueOf(infoType.toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.debug("Could not parse information type", e);
            return UNKNOWN;
        }
    }
}
