package se.sll.ahrr.service;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.beanutils.PropertyUtils;
import org.hl7.fhir.dstu3.model.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.OrganizationType;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import static org.apache.commons.codec.digest.DigestUtils.sha256Hex;
import static se.sll.ahrr.common.FhirUtilities.getResource;


public class Riv2FhirTransformationHelper {
    public final static String legalAuthenticatorExtensionUrl = "se.sll.fhir/Extensions/RivtaLegalAuthentiator";
    public final static String signatureTimeExtensionUrl = legalAuthenticatorExtensionUrl + "/attestedTimeExtension";
    public final static String practitionerExtensionUrl = legalAuthenticatorExtensionUrl + "/practitioner";
    public final static String routeOfTransmissionExtensionUrl = "se.sll.fhir/Extensions/routeOfTransmission";
    private static Logger log = LoggerFactory.getLogger(Riv2FhirTransformationHelper.class);

    private Riv2FhirTransformationHelper() {
    }

    public static String getId(final String sourceSystemHsaId, final String documentId) {
        if (sourceSystemHsaId == null) {
            log.debug("Entry is missing sourceSystemHsaId");
        }
        if (documentId == null) {
            log.debug("Entry is missing documentId");
        }
        return sha256Hex(String.format("%s::%s", sourceSystemHsaId, documentId));
    }

    public static Patient getPatient(final String patientId) {
        Patient patient = new Patient();
        Identifier patientIdentifier = new Identifier();
        patientIdentifier.setSystem(CodeSystem.PERSONID_SE.getValue());
        patientIdentifier.setValue(patientId);
        patient.addIdentifier(patientIdentifier);

        return patient;
    }

    @Deprecated
    public static Reference getPatientReference(final String patientId) {
        return new Reference(getPatient(patientId));
    }

    public static Reference getPatientReferenceWithSystem(final String system, final String patientId) {
        Patient patient = new Patient();
        Identifier patientIdentifier = new Identifier();
        patientIdentifier.setSystem(system);
        patientIdentifier.setValue(patientId);
        patient.addIdentifier(patientIdentifier);
        Reference reference = new Reference();
        reference.setResource(patient);
        return reference;
    }

    public static Reference getPractitionerReference(
            final String hsaId,
            final String name,
            final CodeableConcept practitionerRole,
            final String organizationName,
            final String organizationHsaId,
            final String address,
            final String city,
            final String phone,
            final String email,
            final boolean includeRole) {
        final Practitioner practitioner = new Practitioner();

        final Identifier id = new Identifier();
        id.setValue(hsaId);
        id.setSystem(CodeSystem.HSA.getValue());
        practitioner.addIdentifier(id);

        final HumanName humanName = new HumanName();
        humanName.setText(name);
        practitioner.addName(humanName);

        if (includeRole) {
            final PractitionerRole pr = new PractitionerRole();
            pr.setOrganization(getOrganizationReference(organizationName, organizationHsaId, address, city, phone, email));
            pr.addCode(practitionerRole);
            practitioner.addContained(pr);
        }

        return new Reference(practitioner);
    }

    //todo mapping still uncertain.
    public static Reference getPractitionerReferenceFromLegalAuthenticator(final Object legalAuthenticator) {
        try {
            Practitioner practitioner = new Practitioner();
            //final String signatureTime = (String) PropertyUtils.getProperty(legalAuthenticator, "signatureTime");
            final String authenticatorHSAID = (String) PropertyUtils.getProperty(legalAuthenticator, "legalAuthenticatorHSAId");
            final String name = (String) PropertyUtils.getProperty(legalAuthenticator, "legalAuthenticatorName");
            //final Object roleCodeCvType = PropertyUtils.getPropertyType(legalAuthenticator, "legalAuthenticatorRoleCode");

            final Identifier id = new Identifier();
            id.setValue(authenticatorHSAID);
            id.setSystem(CodeSystem.HSA.getValue()); //unsuru.
            practitioner.addIdentifier(id);

            final HumanName humanName = new HumanName();
            humanName.setText(name);
            practitioner.addName(humanName);

            return new Reference(practitioner);
        } catch (Exception e) {
            log.error("Unexpected error while mapping Practioner from LegalAuthenticator", e);
            return null;
        }
    }

    public static Reference getPractitionerReferenceFromAttested(final Object attested) {

        try {
            final Practitioner practitioner = new Practitioner();
            final String hsaid = (String) PropertyUtils.getProperty(attested, "attesterHSAId");
            final String name = (String) PropertyUtils.getProperty(attested, "attesterName");
            //how should we map time to practitioner?
            final String time = (String) PropertyUtils.getProperty(attested, "attestedTime");
            //system should be same as legalAuthenticator?
            practitioner.addIdentifier(new Identifier().setSystem(CodeSystem.HSA.getValue()).setValue(hsaid));
            practitioner.addName(new HumanName().setText(name));
            return new Reference(practitioner);

        } catch (Exception e) {
            log.error("Unexpected error while mapping riv Attester to Practitioner", e);
            return null;
        }
    }

    @SuppressWarnings("Duplicates")
    public static Extension getPractitionerExtensionFromAttested(final Object attested) {
        final Extension legalAuthenticatorExt = new Extension();
        try {
            legalAuthenticatorExt.setUrl(legalAuthenticatorExtensionUrl);
            Reference practitionerRef = getPractitionerReferenceFromAttested(attested);
            if (practitionerRef != null) {
                final Extension practitionerExt = new Extension();
                practitionerExt.setUrl(practitionerExtensionUrl);
                practitionerExt.setValue(practitionerRef);
                legalAuthenticatorExt.addExtension(practitionerExt);

            }
            final String time = (String) PropertyUtils.getProperty(attested, "attestedTime");
            if (time != null) {
                legalAuthenticatorExt.addExtension(getSignatureTimeExtension(Riv2FhirTransformationHelper.getDateTime(time)));
            }
        } catch (Exception e) {
            log.error("Unexpected error in getLegalAuthenticatorExtension", e);
        }
        return legalAuthenticatorExt;
    }

    @SuppressWarnings("Duplicates")
    public static Extension getLegalAuthenticatorExtension(final Object legalAuthenticator) {
        final Extension legalAuthenticatorExt = new Extension();
        try {
            legalAuthenticatorExt.setUrl(legalAuthenticatorExtensionUrl);
            Reference practitionerRef = getPractitionerReferenceFromLegalAuthenticator(legalAuthenticator);
            if (practitionerRef != null) {
                final Extension practitionerExt = new Extension();
                practitionerExt.setUrl(practitionerExtensionUrl);
                practitionerExt.setValue(practitionerRef);
                legalAuthenticatorExt.addExtension(practitionerExt);
            }

            final String signatureTime = (String) PropertyUtils.getProperty(legalAuthenticator, "signatureTime");

            if (signatureTime != null) {

                legalAuthenticatorExt.addExtension(getSignatureTimeExtension(Riv2FhirTransformationHelper.getDateTime(signatureTime)));
            }
        } catch (Exception e) {
            log.error("Unexpected error in getLegalAuthenticatorExtension", e);
        }
        return legalAuthenticatorExt;
    }

    public static Extension getSignatureTimeExtension(final DateTimeType dateTime) {
        Extension attestedExt = new Extension();
        attestedExt.setUrl(signatureTimeExtensionUrl);
        attestedExt.setValue(dateTime);
        return attestedExt;
    }

    public static Reference getOrganizationReference(
            final String name,
            final String hsaId,
            final String address,
            final String city,
            final String phone,
            final String email) {
        return new Reference(getOrganization(name, hsaId, address, city, phone, email));
    }

    public static Organization getOrganization(
            final String name,
            final String hsaId,
            final String address,
            final String city,
            final String phone,
            final String email) {

        final Organization organization = new Organization();
        organization.setName(name);

        final Identifier orgId = new Identifier();
        orgId.setValue(hsaId);
        orgId.setSystem(CodeSystem.HSA.getValue());
        organization.addIdentifier(orgId);

        final ContactPoint contactPhone = new ContactPoint();
        contactPhone.setSystem(ContactPoint.ContactPointSystem.PHONE);
        contactPhone.setValue(phone);

        organization.addTelecom(contactPhone);

        final ContactPoint contactEmail = new ContactPoint();
        contactEmail.setSystem(ContactPoint.ContactPointSystem.EMAIL);
        contactEmail.setValue(email);

        organization.addTelecom(contactEmail);

        final Address contactAddress = new Address();
        contactAddress.setText(address);
        contactAddress.setCity(city);
        organization.addAddress(contactAddress);
        organization.setName(name);
        return organization;
    }




    public static CodeableConcept getCodeableConceptFromCvType(final Object cvType, final String codeSystemOverride) {
        try {
            final String originalText = (String) PropertyUtils.getSimpleProperty(cvType, "originalText");
            final String code = (String) PropertyUtils.getSimpleProperty(cvType, "code");

            // TODO Remove this! As an override of the codesystem automamticly overrides the codeSystemVersion.
            final String codeSystemVersion = (String) PropertyUtils.getSimpleProperty(cvType, "codeSystemVersion");

            final String displayName = (String) PropertyUtils.getSimpleProperty(cvType, "displayName");

            final CodeableConcept cc = new CodeableConcept();

            if (code != null) {
                final Coding coding = new Coding();

                coding.setCode(code);
                coding.setSystem(codeSystemOverride);

                if (displayName != null) {
                    coding.setDisplay(displayName);
                }

                if (null != codeSystemVersion)
                {
                    coding.setVersion(codeSystemVersion);
                }

                cc.addCoding(coding);
            } else {
                cc.setText(originalText);
            }

            return cc;
        } catch (Exception e) {
            log.error("Unexpected error while mapping CodeableConcept from CvType", e);
            return null;
        }
    }

    public static CodeableConcept getCodeableConceptFromCvType(final Object cvType) {
        try {
            final String originalText = (String) PropertyUtils.getSimpleProperty(cvType, "originalText");
            final String code = (String) PropertyUtils.getSimpleProperty(cvType, "code");
            final String codeSystem = (String) PropertyUtils.getSimpleProperty(cvType, "codeSystem");
            final String codeSystemVersion = (String) PropertyUtils.getSimpleProperty(cvType, "codeSystemVersion");
            final String displayName = (String) PropertyUtils.getSimpleProperty(cvType, "displayName");

            final CodeableConcept cc = new CodeableConcept();

            if (code != null && codeSystem != null) {
                final Coding coding = new Coding();

                coding.setCode(code);
                coding.setVersion(codeSystemVersion);
                coding.setSystem("urn:oid:" + codeSystem);

                if (displayName != null) {
                    coding.setDisplay(displayName);
                }

                cc.addCoding(coding);
            } else {
                cc.setText(originalText);
            }

            return cc;
        } catch (Exception e) {
            log.error("Unexpected error while mapping CodeableConcept from CvType", e);
            return null;
        }
    }

    public static Reference getPracticionerReference(Object healthCareProfessional) {
        return getPracticionerReference(healthCareProfessional, true);
    }

    public static Reference getPracticionerReference(Object healthCareProfessional, boolean includeRole) {
        try {
            Object role = PropertyUtils.getSimpleProperty(healthCareProfessional, "healthcareProfessionalRoleCode");
            Object orgUnit = PropertyUtils.getSimpleProperty(healthCareProfessional, "healthcareProfessionalOrgUnit");
            return Riv2FhirTransformationHelper.getPractitionerReference(
                    (String) PropertyUtils.getSimpleProperty(healthCareProfessional, "healthcareProfessionalHSAId"),
                    (String) PropertyUtils.getSimpleProperty(healthCareProfessional, "healthcareProfessionalName"),
                    role != null ? Riv2FhirTransformationHelper.getCodeableConceptFromCvType(role) : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitName") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitHSAId") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitAddress") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitLocation") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitTelecom") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitEmail") : null,
                    includeRole
            );
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.debug("Could not get Practicioner reference", e);
            return null;
        }
    }

    public static Reference getPractitionerReferenceFromRivta(Object healthCareProfessional) {
        try {

            final Practitioner practitioner = getResource(getPracticionerReference(healthCareProfessional), Practitioner.class)
                    .orElseThrow(() -> new RuntimeException("Failed to get Practitioner from healtCareProfessional"));

            return new Reference(practitioner);

        } catch (Exception e) {
            log.error("Could not map healthcareprofessional", e);

        }
        return null;
    }

    public static Reference getPractitionerReferenceFromRivtaActor(Object actor)
    {
        Practitioner practitioner = getPractitionerFromActorType(actor);

        if (null != practitioner)
        {
            return new Reference(practitioner);
        }

        return null;
    }

    public static Practitioner getPractitionerFromActorType(Object actorType)
    {
        try
        {
            Practitioner practitioner = new Practitioner();

            String hsaId = (String) PropertyUtils.getSimpleProperty(actorType, "hsaid");
            if (null != hsaId)
            {
                Identifier identifier = practitioner.addIdentifier();
                identifier.setSystem(CodeSystem.HSA.getValue());
                identifier.setValue(hsaId);
            }

            String personName = (String) PropertyUtils.getSimpleProperty(actorType, "personName");
            if (null != personName)
            {
                practitioner.addName().setText(personName);
            }

            String personAddress = (String) PropertyUtils.getSimpleProperty(actorType, "personAddress");
            if (null != personAddress)
            {
                Address address = practitioner.addAddress();
                address.setText(personAddress);
                address.setType(Address.AddressType.PHYSICAL);
            }

            String personTelecom = (String) PropertyUtils.getSimpleProperty(actorType, "personTelecom");
            if (null != personTelecom)
            {
                ContactPoint contactPoint = practitioner.addTelecom();
                contactPoint.setSystem(ContactPoint.ContactPointSystem.PHONE);
                contactPoint.setValue(personTelecom);
            }

            String personEmail = (String) PropertyUtils.getSimpleProperty(actorType, "personEmail");
            if (null != personEmail)
            {
                ContactPoint contactPoint = practitioner.addTelecom();
                contactPoint.setSystem(ContactPoint.ContactPointSystem.EMAIL);
                contactPoint.setValue(personTelecom);
            }

            // Discard empty practitioner
            if (null == hsaId && null == personName && null == personAddress && null == personTelecom && null == personEmail)
            {
                return null;
            }

            return practitioner;
        }
        catch (Exception e)
        {
            log.error("Could not map health care professional", e);
        }

        return null;
    }


    public static Reference getOrganizationReferenceFromRivta(Object orgUnit) {
        Organization result = getOrganizationFromRivta(orgUnit);
        return result == null ? null : new Reference(result);
    }

    public static Organization getOrganizationFromRivta(Object orgUnit) {
        try {
            return getOrganization(
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitName") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitHSAId") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitAddress") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitLocation") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitTelecom") : null,
                    orgUnit != null ? (String) PropertyUtils.getSimpleProperty(orgUnit, "orgUnitEmail") : null);
        } catch (Exception e) {
            log.error("Unexpected error while mapping Organization from OrgUnit", e);
            return null;
        }
    }

    public static Reference getOrganizationHierarcyReferenceFromRivta(Object healthcareProfessional) {

        try {
            Organization result = getOrganizationFromRivta(PropertyUtils.getProperty(healthcareProfessional, "healthcareProfessionalOrgUnit"));

            if (result != null) {
                CodeableConcept careGiverOrgType = new CodeableConcept();
                careGiverOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careprovider.getCodeSystem(), OrganizationType.careprovider.name(), OrganizationType.careprovider.getDisplayName())));

                CodeableConcept careUnitOrgType = new CodeableConcept();
                careUnitOrgType.setCoding(Collections.singletonList(new Coding(OrganizationType.careunit.getCodeSystem(), OrganizationType.careunit.name(), OrganizationType.careunit.getDisplayName())));

                final String careGiverHsaId = (String) PropertyUtils.getProperty(healthcareProfessional, "healthcareProfessionalCareGiverHSAId");
                final String careUnitHsaId = (String) PropertyUtils.getProperty(healthcareProfessional, "healthcareProfessionalCareUnitHSAId");

                final Organization careGiverOrg = new Organization();
                careGiverOrg.addIdentifier().setValue(careGiverHsaId).setSystem(CodeSystem.HSA.getValue());
                careGiverOrg.addType(careGiverOrgType);

                final Organization careUnitOrg = new Organization();
                careUnitOrg.addIdentifier().setValue(careUnitHsaId).setSystem(CodeSystem.HSA.getValue());
                careUnitOrg.addType(careUnitOrgType);
                careUnitOrg.setPartOf(new Reference(careGiverOrg));
                result.setPartOf(new Reference(careUnitOrg));

            } else {
                return null;
            }

            return new Reference(result);

        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            log.debug("Could not access healthcareProfessionalCareGiverHSAId or healthcareProfessionalCareUnitHSAId", e);
            return null;
        }
    }

    public static DateTimeType getDateTimeForDate(final String rivtaDate) {
        final DateTime dt = DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(rivtaDate);
        final DateTimeType fhirDate = new DateTimeType();
        fhirDate.setValue(dt.toDate());

        return fhirDate;
    }

    public static DateTimeType getDateTime(final String rivtaDate) {
        final DateTime dt = DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(rivtaDate);
        final DateTimeType fhirDate = new DateTimeType();
        fhirDate.setValue(dt.toDate());

        return fhirDate;
    }

    public static DateType getDate(final String rivtaDate) {
        final DateTime dt = DateTimeFormat.forPattern("yyyyMMddHHmmss").parseDateTime(rivtaDate);
        final DateType fhirDate = new DateType();
        fhirDate.setValue(dt.toDate());

        return fhirDate;
    }

    public static DateType getDateForDate(final String rivtaDate) {
        final DateTime dt = DateTimeFormat.forPattern("yyyyMMdd").parseDateTime(rivtaDate);
        final DateType fhirDate = new DateType();
        fhirDate.setValue(dt.toDate());

        return fhirDate;
    }

    public static Encounter getEncounterIdentifier(final String careContactId) {
        Encounter encounter = new Encounter();
        Identifier encounterId = new Identifier();
        encounterId.setValue(careContactId);
        encounter.addIdentifier(encounterId);
        return encounter;
    }

    public static Reference getPractitionerReferenceFromHealthcareProfessionalType(Object healthcareProfessionalType)
    {
        Practitioner practitioner = getPractitionerFromHealthcareProfessionalType(healthcareProfessionalType);

        if (null != practitioner)
        {
            return new Reference(practitioner);
        }

        return null;
    }

    public static Practitioner getPractitionerFromHealthcareProfessionalType(Object healthcareProfessionalType)
    {
        try
        {
            Practitioner practitioner = new Practitioner();

            String healthcareProfessionalHSAId = (String) PropertyUtils.getSimpleProperty(healthcareProfessionalType, "healthcareProfessionalHSAId");
            if (null != healthcareProfessionalHSAId)
            {
                practitioner.addIdentifier(getIdentifierFromHSAId(healthcareProfessionalHSAId));
            }

            String healthcareProfessionalName = (String) PropertyUtils.getSimpleProperty(healthcareProfessionalType, "healthcareProfessionalName");
            if (null != healthcareProfessionalName)
            {
                practitioner.addName().setText(healthcareProfessionalName);
            }

            // Discard empty practitioner
            if (null == healthcareProfessionalHSAId && null == healthcareProfessionalName)
            {
                return null;
            }

            return practitioner;
        }
        catch (Exception e)
        {
            log.error("Could not map healthcareProfessionalType", e);
        }

        return null;
    }


    public static Reference getOrganizationReferenceFromOrgUnitType(Object orgUnitType)
    {
        Organization oraganization = getOrganizationFromOrgUnitType(orgUnitType);

        if (null != oraganization)
        {
            return new Reference(oraganization);
        }

        return null;
    }

    public static Organization getOrganizationFromOrgUnitType(Object orgUnitType)
    {
        try
        {
            Organization organization = new Organization();

            String orgUnitHSAId = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitHSAId");
            if (null != orgUnitHSAId)
            {
                organization.addIdentifier(getIdentifierFromHSAId(orgUnitHSAId));
            }

            String orgUnitName = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitName");
            if (null != orgUnitName)
            {
                organization.setName(orgUnitName);
            }

            String orgUnitTelecom = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitTelecom");
            if (null != orgUnitTelecom)
            {
                ContactPoint contactPoint = organization.addTelecom();
                contactPoint.setSystem(ContactPoint.ContactPointSystem.PHONE);
                contactPoint.setValue(orgUnitTelecom);
            }

            String orgUnitEmail = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitEmail");
            if (null != orgUnitEmail)
            {
                ContactPoint contactPoint = organization.addTelecom();
                contactPoint.setSystem(ContactPoint.ContactPointSystem.EMAIL);
                contactPoint.setValue(orgUnitEmail);
            }

            String orgUnitAddress = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitAddress");
            String orgUnitLocation = (String) PropertyUtils.getSimpleProperty(orgUnitType, "orgUnitLocation");
            if (null != orgUnitAddress && null != orgUnitLocation)
            {
                Address address = organization.addAddress();

                if (null != orgUnitAddress)
                {
                    address.setType(Address.AddressType.PHYSICAL);
                    address.setText(orgUnitAddress);
                }

                if (null != orgUnitLocation)
                {
                    address.setCity(orgUnitLocation);
                }
            }

            // Discard empty organization
            if (null == orgUnitHSAId && null == orgUnitName && null == orgUnitTelecom && null == orgUnitEmail && null == orgUnitAddress && null == orgUnitLocation)
            {
                return null;
            }

            return organization;
        }
        catch (Exception e)
        {
            log.error("Could not map orgUnitType", e);
        }

        return null;
    }


    public static Identifier getIdentifierFromHSAId(String hsaId)
    {
        Identifier identifier = new Identifier();
        identifier.setSystem(CodeSystem.HSA.getValue());
        identifier.setValue(hsaId);

        return identifier;
    }

    @SuppressWarnings("Duplicates")
    public static Patient getPatientFromPersonIdType(Object personIdType) {
        try
        {
            Patient patient = new Patient();

            String id = (String) PropertyUtils.getSimpleProperty(personIdType, "id");
            if (null != id)
            {
                Identifier identifier = patient.addIdentifier();
                identifier.setValue(id);

                String type = (String) PropertyUtils.getSimpleProperty(personIdType, "type");
                if (null != type)
                {
                    identifier.setSystem("urn:oid:".concat(type));
                }
            }

            // Discard empty patient
            if (null == id)
            {
                return null;
            }

            return patient;
        }
        catch (Exception e)
        {
            log.error("Could not map personIdType", e);
        }

        return null;
    }

    @SuppressWarnings("Duplicates")
    public static Patient getPatientFromPersonIdTypeWithAdditionalPatientInformationType(Object personIdType, Object additionalPatientInformation)
    {
        try
        {
            Patient patient = new Patient();

            String id = (String) PropertyUtils.getSimpleProperty(personIdType, "id");
            if (null != id)
            {
                Identifier identifier = patient.addIdentifier();
                identifier.setValue(id);

                String type = (String) PropertyUtils.getSimpleProperty(personIdType, "type");
                if (null != type)
                {
                    identifier.setSystem("urn:oid:".concat(type));
                }
            }

            if (null != additionalPatientInformation)
            {
                String dateOfBirth = (String) PropertyUtils.getSimpleProperty(additionalPatientInformation, "dateOfBirth");
                if (null != dateOfBirth)
                {
                    try
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        patient.setBirthDate(sdf.parse(dateOfBirth));
                    }
                    catch (Exception e) {}
                }

                Object gender = PropertyUtils.getSimpleProperty(additionalPatientInformation, "gender");
                if (null != gender)
                {
                    String code = (String) PropertyUtils.getSimpleProperty(gender, "code");
                    String codeSystem = (String) PropertyUtils.getSimpleProperty(gender, "codeSystem");
                    if (null != code && codeSystem.contains("1.2.752.129.2.2.1.1"))
                    {
                        switch (code)
                        {
                            case "1":
                                patient.setGender(Enumerations.AdministrativeGender.MALE);
                                break;

                            case "2":
                                patient.setGender(Enumerations.AdministrativeGender.FEMALE);
                                break;

                            case "9":
                                patient.setGender(Enumerations.AdministrativeGender.NULL);
                                break;

                            default:
                                patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
                                break;
                        }
                    }
                    else
                    {
                        patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
                    }
                }
            }

            // Discard empty patient
            if (null == id && null == patient.getGender() && null == patient.getBirthDate())
            {
                return null;
            }

            return patient;
        }
        catch (Exception e)
        {
            log.error("Could not map personIdType with additionalPatientInformation", e);
        }

        return null;
    }


    public static Identifier getIdentifierFromIIType(Object iiType)
    {
        try
        {
            Identifier identifier = new Identifier();

            String root = (String) PropertyUtils.getSimpleProperty(iiType, "root");
            String extension = (String) PropertyUtils.getSimpleProperty(iiType, "extension");

            if (null == extension)
            {
                identifier.setValue(root);
            }
            else
            {
                identifier.setSystem(root);
                identifier.setValue(extension);
            }

            if (null == root && null == extension)
            {
                return null;
            }

            return identifier;
        }
        catch (Exception e)
        {
            log.error("Could not map iiType to identifier", e);
            return null;
        }
    }

    public static Date getDateFromTimestampType(Object timestampType)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            return sdf.parse((String) timestampType);
        }
        catch (Exception e)
        {
            log.error("Could not map timestamp to date", e);
            return null;
        }
    }


}