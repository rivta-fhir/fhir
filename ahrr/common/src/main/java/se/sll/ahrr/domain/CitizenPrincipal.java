package se.sll.ahrr.domain;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CitizenPrincipal extends Principal {
    private String subjectSerialNumber;
    private String name;
    private String middleAndSurName;

    @JsonIgnore
    @Override
    public String getUsername() {
        return this.subjectSerialNumber;
    }

    public CitizenPrincipal() {
    }

    public CitizenPrincipal(String subjectSerialNumber, String name, String middleAndSurName) {
        this.subjectSerialNumber = subjectSerialNumber;
        this.name = name;
        this.middleAndSurName = middleAndSurName;
    }

    public String getSubjectSerialNumber() {
        return subjectSerialNumber;
    }

    public String getName() {
        return name;
    }

    public String getMiddleAndSurName() {
        return middleAndSurName;
    }
}
