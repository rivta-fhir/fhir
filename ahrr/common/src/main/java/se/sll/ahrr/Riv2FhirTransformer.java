package se.sll.ahrr;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;

public interface Riv2FhirTransformer<RIV, FHIR> {

    /**
     * The FHIR resource type we are transforming to
     *
     * @return
     */
    Class getFHIRResourceType();

    /**
     * Transform RIV-TA data to FHIR
     *
     * @param rivResponse
     * @return
     * @throws Exception
     */
    List<FHIR> transformToFhir(final RIV rivResponse) throws Exception;
}
