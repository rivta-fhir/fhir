package se.sll.ahrr.common;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import se.sll.ahrr.codesystem.valueset.ValueSet;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Helpers {

    public static XMLGregorianCalendar dateToXmlGregorianCalendar(Date d) throws Exception {
        GregorianCalendar g = new GregorianCalendar();
        g.setTime(d);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(g);
    }

    public static LocalDateTime xmlGregorianCalendarToDateTime(XMLGregorianCalendar xmlGregorianCalendar) {
        if (xmlGregorianCalendar == null) {
            return null;
        }
        final ZonedDateTime zonedDateTime = ZonedDateTime.of(xmlGregorianCalendar.getYear(),
                xmlGregorianCalendar.getMonth(),
                xmlGregorianCalendar.getDay(),
                xmlGregorianCalendar.getHour(),
                xmlGregorianCalendar.getMinute(),
                xmlGregorianCalendar.getSecond(),
                xmlGregorianCalendar.getMillisecond() * 1000000,
                xmlGregorianCalendar.getTimeZone(xmlGregorianCalendar.getTimezone()).toZoneId());
        return zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date xmlGregorianCalendarToDate(XMLGregorianCalendar xmlGregorianCalendar) {
        if (xmlGregorianCalendar == null) {
            return null;
        }
        final ZonedDateTime zonedDateTime = ZonedDateTime.of(xmlGregorianCalendar.getYear(),
                xmlGregorianCalendar.getMonth(),
                xmlGregorianCalendar.getDay(),
                xmlGregorianCalendar.getHour(),
                xmlGregorianCalendar.getMinute(),
                xmlGregorianCalendar.getSecond(),
                xmlGregorianCalendar.getMillisecond() * 1000000,
                xmlGregorianCalendar.getTimeZone(xmlGregorianCalendar.getTimezone()).toZoneId());
        return Date.from(zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toInstant());
    }

    public static Coding getCodingFromValueSet(final ValueSet valueSet) {
        final Coding coding = new Coding();
        coding.setSystem(valueSet.getCodeSystem());
        coding.setCode(valueSet.getCode());
        coding.setDisplay(valueSet.getDisplayName());
        return coding;
    }

    public static CodeableConcept getCodeableConceptFromValueSet(final ValueSet valueSet) {
        return new CodeableConcept().addCoding(getCodingFromValueSet(valueSet));
    }

    public static boolean codingListContains(String codeSystem, String code, List<Coding> codings) {
        return codings.stream()
                .filter(c -> c.getSystem().equals(codeSystem))
                .filter(c -> c.getCode().equals(code))
                .count() > 0;
    }

    public static boolean codeableConceptContainsCode(String codeSystem, String code, CodeableConcept codeableConcept) {
        return codingListContains(codeSystem, code, codeableConcept.getCoding());
    }
}
