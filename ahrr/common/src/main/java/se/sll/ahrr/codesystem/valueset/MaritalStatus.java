package se.sll.ahrr.codesystem.valueset;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;

public enum MaritalStatus implements ValueSet {
    A("Annulled"),
    D("Divorced"),
    I("Interlocutory"),
    L("Legally Separated"),
    M("Married"),
    P("Polygamous"),
    S("Never Married"),
    T("Domestic partner"),
    U("unmarried"), //is lowercase here https://www.hl7.org/fhir/v3/MaritalStatus/cs.html
    W("Widowed"),
    UNK("unknown", CodeSystem.NULL_FLAVOR);

    private CodeSystem codeSystem;
    private String displayName;


    MaritalStatus(final String displayName) {
        this.displayName = displayName;
        this.codeSystem = CodeSystem.MARITAL_STATUS;
    }

    MaritalStatus(final String displayName, final CodeSystem codeSystem) {
        this.displayName = displayName;
        this.codeSystem = codeSystem;
    }

    @Override
    public String getCodeSystem() {
        return codeSystem.getValue();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getCode() {
        return name();
    }
}
