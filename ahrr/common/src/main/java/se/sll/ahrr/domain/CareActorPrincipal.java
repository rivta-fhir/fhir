package se.sll.ahrr.domain;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CareActorPrincipal extends Principal {
    private final String hsaId;
    private final String careActorGivenName;
    private final String careActorMiddleAndSurName;
    private final String careUnitId;
    private final String careUnitName;
    private final String careProviderId;
    private final String careProviderName;
    private final String title;
    private final String assignmentId;
    private final String assignmentName;
    private final String commissionPurpose;
    private final List<CommissionRight> commissionRights;

    @Override
    public String getUsername() {
        return hsaId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CareActorPrincipal{");
        sb.append("hsaId='").append(hsaId).append('\'');
        sb.append("\n  careActorGivenName='").append(careActorGivenName).append('\'');
        sb.append("\n  careActorMiddleAndSurName='").append(careActorMiddleAndSurName).append('\'');
        sb.append("\n  careUnitId='").append(careUnitId).append('\'');
        sb.append("\n  careUnitName='").append(careUnitName).append('\'');
        sb.append("\n  careProviderId='").append(careProviderId).append('\'');
        sb.append("\n  careProviderName='").append(careProviderName).append('\'');
        sb.append("\n  title='").append(title).append('\'');
        sb.append("\n  assignmentId='").append(assignmentId).append('\'');
        sb.append("\n  assignmentName='").append(assignmentName).append('\'');
        sb.append("\n  commissionPurpose='").append(commissionPurpose).append('\'');
        sb.append("\n  commissionRights=").append(commissionRights);
        sb.append('}');
        return sb.toString();
    }
}
