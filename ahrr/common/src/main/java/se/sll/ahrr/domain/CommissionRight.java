package se.sll.ahrr.domain;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Builder
@ToString
public class CommissionRight implements Serializable {
    private final InformationType infoType;
    private Permission permission;
    private Scope scope;
    private String specificOrganizationId;

    public enum Scope {
        NATIONAL,
        CARE_PROVIDER,
        CARE_UNIT,
        SPECIFIC_ORGANIZATIONAL_UNIT
    }

    public enum Permission {
        READ
    }

    public static class CommissionRightBuilder {
        public CommissionRightBuilder scope(String scope) {
            if (scope == null) {
                return this;
            }

            if (scope.equalsIgnoreCase("sjf")) {
                this.scope = Scope.NATIONAL;
            } else if (scope.equalsIgnoreCase("vg")) {
                this.scope = Scope.CARE_PROVIDER;
            } else if (scope.equalsIgnoreCase("ve")) {
                this.scope = Scope.CARE_UNIT;
            } else {
                this.scope = Scope.SPECIFIC_ORGANIZATIONAL_UNIT;
                this.specificOrganizationId = scope;
            }

            return this;
        }

        public CommissionRightBuilder permission(String permission) {
            if (permission == null) {
                return this;
            }
            if (permission.equalsIgnoreCase("Läsa")) {
                this.permission = Permission.READ;
            }

            return this;
        }

        public CommissionRightBuilder infoType(String infoType) {
            if (infoType == null) {
                return this;
            }
            this.infoType = InformationType.fromString(infoType);

            return this;
        }
    }
}
