package se.sll.ahrr.codesystem.valueset.condition;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.ValueSet;

public enum Category implements ValueSet {
    PROBLEM_LIST_ITEM("Problem List Item", "problem-list-item", CodeSystem.CONDITION_CATEGORY),
    ENCOUNTER_DIAGNOSIS("Encounter Diagnosis", "encounter-diagnosis", CodeSystem.CONDITION_CATEGORY),
    HUVUDDIAGNOS("", "Huvuddiagnos", CodeSystem.DIAGNOSIS_TYPE),
    BIDIAGNOS("", "Bidiagnos", CodeSystem.DIAGNOSIS_TYPE);

    private String displayName;
    private String code;
    private CodeSystem codeSystem;

    Category(String displayName, String code, CodeSystem codeSystem) {
        this.displayName = displayName;
        this.code = code;
        this.codeSystem = codeSystem;
    }

    @Override
    public String getCodeSystem() {
        return codeSystem.getValue();
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getCode() {
        return code;
    }
}
