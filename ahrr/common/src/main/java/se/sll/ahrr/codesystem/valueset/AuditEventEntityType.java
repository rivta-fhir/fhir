package se.sll.ahrr.codesystem.valueset;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import se.sll.ahrr.codesystem.CodeSystem;

import java.util.Arrays;

public enum AuditEventEntityType implements ValueSet {
    CARE_PLAN("CarePlan"),
    COMPOSITION("Composition"),
    CONDITION("Condition"),
    CONSENT("Consent"),
    DIAGNOSTIC_REPORT("DiagnosticReport"),
    ENCOUNTER("Encounter"),
    FLAG("Flag"),
    OBSERVATION("Observation"),
    IMAGING_STUDY("ImagingStudy"),
    IMMUNIZATION("Immunization"),
    MEDICATION_STATEMENT("MedicationStatement"),
    REFERRAL_REQUEST("ReferralRequest"),
    PATIENT("Patient");


    private String code;

    AuditEventEntityType(String code) {
        this.code = code;
    }

    @Override
    public String getCodeSystem() {
        return CodeSystem.AUDIT_EVENT_ENTITY_TYPE.getValue();
    }

    @Override
    public String getDisplayName() {
        // This codeset uses the code as the displayname
        return code;
    }

    @Override
    public String getCode() {
        return code;
    }

    public static AuditEventEntityType fromCode(final String code) {
        return Arrays.stream(AuditEventEntityType.values())
                .filter(e -> e.code.equalsIgnoreCase(code))
                .findFirst().orElse(null);
    }
}
