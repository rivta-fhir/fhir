package se.sll.ahrr.common;

/*-
 * #%L
 * common
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Reference;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.Optional;

public class FhirUtilities {
    public static <T extends IBaseResource> Optional<T> getResource(Reference reference, Class<T> type) {
        if (reference == null) {
            return Optional.empty();
        }
        try {
            return Optional.of(type.cast(reference.getResource()));
        } catch (ClassCastException e) {
            return Optional.empty();
        }
    }

    public static CodeableConcept codeableConceptWith(Coding coding) {
        CodeableConcept codeableConcept = new CodeableConcept();
        codeableConcept.addCoding(coding);
        return codeableConcept;
    }
}
