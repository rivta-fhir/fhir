package se.sll.ahrr.config.ssl;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration("sslConfiguration")
@ConfigurationProperties(prefix = "ahrr.client.ssl")
public class SSLConfiguration {

    private CnCheck cnCheck = new CnCheck();

    private Store truststore = new Store();
    private Store keystore = new Store();

    private String protocol;
    private boolean enabled = false;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public CnCheck getCnCheck() {
        return cnCheck;
    }

    public void setCnCheck(CnCheck cnCheck) {
        this.cnCheck = cnCheck;
    }

    public Store getTruststore() {
        return truststore;
    }

    public void setTruststore(Store truststore) {
        this.truststore = truststore;
    }

    public Store getKeystore() {
        return keystore;
    }

    public void setKeystore(Store keystore) {
        this.keystore = keystore;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public static class CnCheck {
        private boolean disable = false;
        public boolean getDisable() { return disable; }
        public void setDisable(boolean disable) { this.disable = disable; }
    }

    public static class Store {
        private String file;
        private String type = "JKS";
        private String password;
        private String certAlias;
        public String getFile() { return file; }
        public void setFile(String file) { this.file = file; }
        public String getType() { return type; }
        public void setType(String type) { this.type = type; }
        public String getPassword() { return password; }
        public void setPassword(String password) { this.password = password; }
        public String getCertAlias() { return certAlias; }
        public void setCertAlias(String certAlias) { this.certAlias = certAlias; }
    }

}
