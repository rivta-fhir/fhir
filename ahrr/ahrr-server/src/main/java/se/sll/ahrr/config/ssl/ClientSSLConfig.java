package se.sll.ahrr.config.ssl;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.FiltersType;
import org.apache.log4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.Resource;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Optional;

@Configuration
@ConditionalOnProperty(prefix = "ahrr.client.ssl", name = "enabled", havingValue = "true")
public class ClientSSLConfig implements ClientTLSConfig {

    private final Logger log = Logger.getLogger(getClass());

    @Resource
    private SSLConfiguration sslConfiguration;

    @Resource
    private ResourceLoader resourceLoader;

    @Override
    @Bean(name = "soapClientTLSClientParameters")
    public TLSClientParameters tlsClientParameters() throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        TLSClientParameters tlsParams = new TLSClientParameters();
        tlsParams.setDisableCNCheck(sslConfiguration.getCnCheck().getDisable());
        tlsParams.setKeyManagers(createKeyManagers());
        tlsParams.setTrustManagers(createTrustManagers());
        tlsParams.setCipherSuitesFilter(createCipherSuitesFilter());
        tlsParams.setCertAlias(sslConfiguration.getKeystore().getCertAlias());
        tlsParams.setSecureSocketProtocol(sslConfiguration.getProtocol());
        return tlsParams;
    }

    private TrustManager[] createTrustManagers() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore trustStore = KeyStore.getInstance(sslConfiguration.getTruststore().getType());

        getFile(sslConfiguration.getTruststore().getFile()).ifPresent(f -> {
            try (InputStream is = f) {
                trustStore.load(is, sslConfiguration.getTruststore().getPassword().toCharArray());
            } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
                log.warn(String.format("Could not read truststore file %s", sslConfiguration.getTruststore().getFile()), e);
            }
        });
        TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustFactory.init(trustStore);
        return trustFactory.getTrustManagers();
    }

    private KeyManager[] createKeyManagers() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        KeyStore keyStore = KeyStore.getInstance(sslConfiguration.getKeystore().getType());

        getFile(sslConfiguration.getKeystore().getFile()).ifPresent(f -> {
            try (InputStream is = f) {
                keyStore.load(is, sslConfiguration.getKeystore().getPassword().toCharArray());
            } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
                log.warn(String.format("Could not read keystore file %s", sslConfiguration.getKeystore().getFile()), e);
            }
        });

        KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyFactory.init(keyStore, sslConfiguration.getKeystore().getPassword().toCharArray());
        return keyFactory.getKeyManagers();
    }

    private Optional<InputStream> getFile(String path) {
        if(path == null) {
            return Optional.empty();
        }

        Optional<InputStream> file = Optional.empty();

        if(path.startsWith("classpath:")) {

            org.springframework.core.io.Resource resource = resourceLoader.getResource(path);
            //org.springframework.core.io.Resource resource = new ClassPathResource("/");

            try {
                file = Optional.ofNullable(resource.getInputStream());
            } catch (IOException e) {
                log.error(String.format("Failed to fetch file %s from jar filesystem", path), e);
            }
        } else {
            try {
                file = Optional.of(new FileInputStream(Paths.get(path).toFile()));
            } catch (FileNotFoundException e) {
                log.error(String.format("Failed to fetch file %s from local filesystem", path), e);
            }
        }

        return file;
    }

    /**
     * these filters ensure that a ciphersuite with
     * export-suitable or null encryption is used,
     * but exclude anonymous Diffie-Hellman key change as
     * this is vulnerable to man-in-the-middle attacks
     */
    private FiltersType createCipherSuitesFilter() {
        FiltersType filter = new FiltersType();
        filter.getInclude().add(".*_EXPORT_.*");
        filter.getInclude().add(".*_EXPORT1024_.*");
        filter.getInclude().add(".*_WITH_DES_.*");
        filter.getInclude().add(".*_WITH_AES_.*");
        filter.getInclude().add(".*_WITH_NULL_.*");
        filter.getExclude().add(".*_DH_anon_.*");
        return filter;
    }
}
