package se.sll.ahrr.config.ssl;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 - 2018 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

@Configuration
@ConditionalOnProperty(prefix = "ahrr.client.ssl", name = "enabled", havingValue = "false", matchIfMissing = true)
public class ClientNoSSLConfig implements ClientTLSConfig
{
    @Override
    @Bean(name = "soapClientTLSClientParameters")
    public TLSClientParameters tlsClientParameters() throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException
    {
        // @TODO: Check with Petur about why this is necessery and not in the ehr-writer
        //return new TLSClientParameters();

        return null;
    }
}
