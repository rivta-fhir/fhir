package se.sll.ahrr.config;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.cache.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@EnableCaching
@Configuration
public class CacheConfig {

    private Logger log = LoggerFactory.getLogger(getClass());

    public class AhrrCacheManager extends SimpleCacheManager {
        @Override
        protected org.springframework.cache.Cache getMissingCache(String name) {
            log.debug("Creating new guava cache {}", name);
            return new GuavaCache(name,
                CacheBuilder.newBuilder()
                    .maximumSize(2000)
                    .expireAfterWrite(30, TimeUnit.SECONDS)
                    .build());
        }
    }

    @Bean
    public CacheManager cacheManager() {
        return new AhrrCacheManager();
    }
}
