package se.sll.ahrr.integration;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.context.FhirContext;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.Ahrr;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ahrr.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CareActorIT {

    @Value("${local.server.port}")
    protected int localPort;

    private GetRequest get(final String path, final String patientId) throws Exception {
        GetRequest request = Unirest.get(String.format("http://localhost:%d/fhir/%s", localPort, path));

        Map<String, String> headers = new HashMap<>();
        headers.put("X-PRINCIPAL-HSAID", "SE165567766992-17J2");
        headers.put("X-PRINCIPAL-GIVENNAME", "Tolvan");
        headers.put("X-PRINCIPAL-MIDDLEANDSURNAME", "Tolvansson");
        headers.put("X-PRINCIPAL-CAREUNIT-ID", "TNT4477663322-100R");
        headers.put("X-PRINCIPAL-CAREUNIT-NAME", "Chorus AB");
        headers.put("X-PRINCIPAL-CAREPROVIDER-ID", "TNT4477663322-0002");
        headers.put("X-PRINCIPAL-CAREPROVIDER-NAME", "Chorus AB(2)");
        headers.put("X-PRINCIPAL-ASSIGNMENT-ID", "developer");
        headers.put("X-PRINCIPAL-ASSIGNMENT-NAME", "Utvecklare");
        headers.put("X-PRINCIPAL-COMMISSION-PURPOSE", "Vård och behandling");
        headers.put("X-PRINCIPAL-COMMISSION-RIGHTS", "Läsa;alla;SJF");
        headers.put("Accept", "application/json");
        headers.put("X-SSN", patientId);

        headers.forEach(
            request::header
        );

        return request;
    }

    public static <T extends IBaseResource> T parse(HttpResponse<JsonNode> json, Class<T> type) {
        return type.cast(FhirContext.forDstu3().newJsonParser().parseResource(json.getBody().toString()));
    }

    @Test
    public void testPatient() throws Exception {
        HttpResponse<JsonNode> response = get("Patient", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(1, parse(response, Bundle.class).getTotal());

        response = get("Patient/193601286499", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("193601286499", parse(response, Patient.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testFlag() throws Exception {
        HttpResponse<JsonNode> response = get("Flag", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(11, parse(response, Bundle.class).getTotal());

        response = get("Flag/f50139b1a5fe271db585a8d9cb2c239ebb6350babefce860833b39a3e36c0292", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GAI-02-01", parse(response, Flag.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testCarePlan() throws Exception {
        HttpResponse<JsonNode> response = get("CarePlan", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(10, parse(response, Bundle.class).getTotal());

        response = get("CarePlan/dd72c01b534644548db9a68fb191c7aaf1e03e4f15972050ca66a56a51ad53c1", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GCP-02-01", parse(response, CarePlan.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testComposition() throws Exception {
        HttpResponse<JsonNode> response = get("Composition", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(16, parse(response, Bundle.class).getTotal());

        response = get("Composition/e3bce827b140e892b8a30ed8aa6d1efaf2752300e7c2a061ef66a73f96178e47", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GCD-02-01", parse(response, Composition.class).getIdentifier().getValue());
    }

    @Test
    public void testCondition() throws Exception {
        HttpResponse<JsonNode> response = get("Condition", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(6, parse(response, Bundle.class).getTotal());

        response = get("Condition/659b452ad83b14a6a53d0e3894ac6724fc6ff9680644778da9121f7d032db215", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GD-02-01", parse(response, Condition.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testConsent() throws Exception {
        HttpResponse<JsonNode> response = get("Consent", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(1, parse(response, Bundle.class).getTotal());

        response = get("Consent/0.4762436408433961", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("Consent/0.4762436408433961", parse(response, Consent.class).getId());
    }

    @Test
    public void testDiagnosticReport() throws Exception {
        HttpResponse<JsonNode> response = get("DiagnosticReport", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(5, parse(response, Bundle.class).getTotal());

        response = get("DiagnosticReport/f3aa66f47bcd0a16737dd038a3ba84380d80bdf2863e1f902f148ffc67398a61", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GLOO-02-01", parse(response, DiagnosticReport.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testEncounter() throws Exception {
        HttpResponse<JsonNode> response = get("Encounter", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(10, parse(response, Bundle.class).getTotal());

        response = get("Encounter/47039c1c7bcebfcb15b8f52cee515bed13315eecdd2f7f3cc854db54dd08b02b", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GCC2-02-01", parse(response, Encounter.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testImagingStudy() throws Exception {
        HttpResponse<JsonNode> response = get("ImagingStudy", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(5, parse(response, Bundle.class).getTotal());

        response = get("ImagingStudy/8cc59b5dcfd65049d44ef1ebcfd494c088a1be25720bb463af6d2f8280cd6651", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GIO-02-01", parse(response, ImagingStudy.class).getIdentifierFirstRep().getValue());
    }

    @Test
    public void testMedicationStatement() throws Exception {
        HttpResponse<JsonNode> response = get("MedicationStatement", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(6, parse(response, Bundle.class).getTotal());

        response = get("MedicationStatement/faddd392af268d59d80bfd0c5be4ca72a0c943d7758b22dec3c4c09abbfd3888", "193601286499").asJson();
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals("JOL-MOCK-GMH-02-01", parse(response, MedicationStatement.class).getIdentifierFirstRep().getValue());
    }
}
