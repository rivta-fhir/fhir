package se.sll.ahrr;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.CommissionRight;
import se.sll.ahrr.domain.InformationType;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ahrr.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("dev")
public class SecurityTest {

    @Before
    public void init() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Value("${local.server.port}")
    protected int localPort;

    private HttpResponse<CitizenPrincipal> getCitizenPrincipal(Map<String, String> headers) throws Exception {
        GetRequest request = Unirest.get(String.format("http://localhost:%d/me", localPort));

        headers.forEach(
            request::header
        );

        return request.asObject(CitizenPrincipal.class);
    }

    private HttpResponse<CareActorPrincipal> getCareActorPrincipal(Map<String, String> headers) throws Exception {
        GetRequest request = Unirest.get(String.format("http://localhost:%d/me", localPort));

        headers.forEach(
            request::header
        );

        return request.asObject(CareActorPrincipal.class);
    }

    @Test
    public void testCitizenPrincipal() throws Exception {
        CitizenPrincipal principal = getCitizenPrincipal(Collections.singletonMap("X-PRINCIPAL-SSN", "191212121212")).getBody();
        assertEquals("191212121212", principal.getSubjectSerialNumber());
    }

    @Test
    public void testBareBoneCareActorPrincipal() throws Exception {
        CareActorPrincipal principal = getCareActorPrincipal(Collections.singletonMap("X-PRINCIPAL-HSAID", "SE165567766992-17J2")).getBody();
        assertEquals("SE165567766992-17J2", principal.getHsaId());
    }

    @Test
    public void testFullCareActorPrincipal() throws Exception {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-PRINCIPAL-HSAID", "SE165567766992-17J2");
        headers.put("X-PRINCIPAL-GIVENNAME", "Tolvan");
        headers.put("X-PRINCIPAL-MIDDLEANDSURNAME", "Tolvansson");
        headers.put("X-PRINCIPAL-CAREUNIT-ID", "SE165567766992-17J1");
        headers.put("X-PRINCIPAL-CAREUNIT-NAME", "Chorus AB");
        headers.put("X-PRINCIPAL-CAREPROVIDER-ID", "SE165567766992-17J1(2)");
        headers.put("X-PRINCIPAL-CAREPROVIDER-NAME", "Chorus AB(2)");
        headers.put("X-PRINCIPAL-ASSIGNMENT-ID", "developer");
        headers.put("X-PRINCIPAL-ASSIGNMENT-NAME", "Utvecklare");
        headers.put("X-PRINCIPAL-COMMISSION-PURPOSE", "Vård och behandling");
        headers.put("X-PRINCIPAL-COMMISSION-RIGHTS", "Läsa;alla;SJF");
        headers.put("X-PRINCIPAL-TITLE", "Läkare");

        CareActorPrincipal principal = getCareActorPrincipal(headers).getBody();
        assertEquals("SE165567766992-17J2", principal.getUsername());
        assertEquals("Tolvan", principal.getCareActorGivenName());
        assertEquals("Tolvansson", principal.getCareActorMiddleAndSurName());
        assertEquals("SE165567766992-17J1", principal.getCareUnitId());
        assertEquals("Chorus AB", principal.getCareUnitName());
        assertEquals("SE165567766992-17J1(2)", principal.getCareProviderId());
        assertEquals("Chorus AB(2)", principal.getCareProviderName());
        assertEquals("developer", principal.getAssignmentId());
        assertEquals("Utvecklare", principal.getAssignmentName());
        assertEquals("Vård och behandling", principal.getCommissionPurpose());
        assertEquals("Läkare", principal.getTitle());
        assertEquals(InformationType.ALLA, principal.getCommissionRights().get(0).getInfoType());
        assertEquals(CommissionRight.Permission.READ, principal.getCommissionRights().get(0).getPermission());
        assertEquals(CommissionRight.Scope.NATIONAL, principal.getCommissionRights().get(0).getScope());
    }


    @Test
    public void testNoPrincipal() throws Exception {
        assertEquals(403, getCitizenPrincipal(Collections.emptyMap()).getStatus());
        assertEquals(403, getCareActorPrincipal(Collections.emptyMap()).getStatus());
    }

    @Test
    public void testDoublePrincipal() throws Exception {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-PRINCIPAL-HSAID", "SE165567766992-17J2");
        headers.put("X-PRINCIPAL-SSN", "191212121212");

        assertEquals(403, getCitizenPrincipal(headers).getStatus());
    }
}
