package se.sll.ahrr.integration;

/*-
 * #%L
 * ahrr-server
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.google.common.cache.Cache;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import riv.ehr.patientconsent.administration.registerextendedconsentresponder._1.RegisterExtendedConsentRequestType;
import se.sll.ahrr.Ahrr;
import se.sll.ahrr.carecontact.v3_0.dataloader.CareContactDataLoader;
import se.sll.ahrr.dataloader.CheckConsentsDataLoader;
import se.sll.ahrr.dataloader.GetConsentsForPatientDataLoader;
import se.sll.ahrr.dataloader.RegisterExtendedConsentDataLoader;
import se.sll.ahrr.dataloader.RivtaDataLoader;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ahrr.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
public class CacheIT {

    @Autowired
    private CacheManager cacheManager;
    @Autowired
    private Collection<RivtaDataLoader> RivtaDataLoaders;

    @Autowired
    GetConsentsForPatientDataLoader getConsentsForPatientDataLoader;
    @Autowired
    CheckConsentsDataLoader checkConsentsDataLoader;
    @Autowired
    RegisterExtendedConsentDataLoader registerExtendedConsentDataLoader;

    private String getCacheName(Class<?> dataLoader) {
        CacheConfig config = dataLoader.getAnnotation(CacheConfig.class);
        String[] names = config.cacheNames();
        assertEquals(1, names.length);
        return names[0];
    }

    private Cache getCache(Class<?> dataLoader) throws Exception{
        CacheConfig config = dataLoader.getAnnotation(CacheConfig.class);
        if (config == null) {
            throw new IllegalStateException(String.format("%s cache is null, is it missing annotations?", dataLoader.getSimpleName()));
        }
        assertNotNull(config.cacheNames());
        // We use a Guava cache, cast the spring cache
        // to allow for introspection
        return (Cache) cacheManager.getCache(config.cacheNames()[0]).getNativeCache();
    }

    private void testDataLoaderCache(RivtaDataLoader dataLoader, Class<?> dataLoaderClass) throws Exception {
        Cache cache = getCache(dataLoaderClass);
        assertEquals(String.format("%s cache is not empty when beginning", dataLoaderClass.getSimpleName()), 0, cache.size());
        try {
            dataLoader.loadRivtaResponse("191212121212");
        } catch (Exception e) {
            fail(e.getMessage());
        }
        assertEquals(String.format("%s cache did not increase in size after fetching data", dataLoaderClass.getSimpleName()), 1, cache.size());
        dataLoader.clearCache("191212121212");
        assertEquals(String.format("%s cache did not clear data", dataLoaderClass.getSimpleName()), 0, cache.size());
    }

    @Test
    public void testDataCache() throws Exception {
        Set<String> cacheNames = new HashSet<>();

        RivtaDataLoaders
                .stream()
                .filter(rivtaDataLoader -> rivtaDataLoader.getClass() != CareContactDataLoader.class) //Skip GCC v3 as it's not being called
                .forEach(
            rivtaDataLoader -> {
                Class<?> dataLoaderClass = AopUtils.getTargetClass(rivtaDataLoader);
                try {
                    testDataLoaderCache(rivtaDataLoader, dataLoaderClass);
                } catch (Exception e) {
                    fail(e.getMessage());
                }

                String cacheName = getCacheName(dataLoaderClass);
                assertFalse(String.format("Cache name %s is used multiple times", cacheName), cacheNames.contains(cacheName));
                cacheNames.add(cacheName);
            }
        );
    }

    @Test
    public void testConsentCache() throws Exception{
        // Consent operations don't use RivtaDataloaders as they
        // require custom parameters.
        Class targetClass = AopUtils.getTargetClass(getConsentsForPatientDataLoader);
        Cache cache = getCache(targetClass);

        boolean wasCheckConsentCaching = true;

        try {
            getCache(AopUtils.getTargetClass(checkConsentsDataLoader));
        } catch (IllegalStateException e) {
            wasCheckConsentCaching = false;
        }

        assertFalse("CheckConsent is not allowed to use caching", wasCheckConsentCaching);

        assertEquals(String.format("%s cache is not empty when beginning", targetClass), 0, cache.size());
        try {
            getConsentsForPatientDataLoader.loadRivtaResponse("191212121212", "SE165567766992-17J1");
            checkConsentsDataLoader.loadRivtaResponse("191212121212", "SE165567766992-17J1", "SE165567766992-17J1", "SE165567766992-17J2");
        } catch (Exception e) {
            fail(e.getMessage());
        }
        assertEquals(String.format("%s cache did not increase in size after fetching data", targetClass), 1, cache.size());

        // Registering a new consent should clear the cache
        RegisterExtendedConsentRequestType registerExtendedConsentRequestType = new RegisterExtendedConsentRequestType();
        registerExtendedConsentRequestType.setCareProviderId("SE165567766992-17J1");
        registerExtendedConsentRequestType.setPatientId("191212121212");
        registerExtendedConsentDataLoader.registerConsent(registerExtendedConsentRequestType);

        assertEquals(String.format("%s cache did not clear data", targetClass), 0, cache.size());
    }
}
