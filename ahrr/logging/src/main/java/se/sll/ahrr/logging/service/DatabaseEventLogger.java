package se.sll.ahrr.logging.service;

/*-
 * #%L
 * logging
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.eventbus.Subscribe;
import org.springframework.stereotype.Service;
import se.sll.ahrr.codesystem.valueset.RuleSet;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.event.ReleaseOfDataEvent;
import se.sll.ahrr.listener.EventListener;
import se.sll.ahrr.logging.entity.Log;
import se.sll.ahrr.logging.repository.LogRepository;

@Service
public class DatabaseEventLogger implements EventListener {
    private LogRepository logRepository;

    public DatabaseEventLogger(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @Subscribe
    public void releaseOfData(ReleaseOfDataEvent releaseOfDataEvent) {
        PatientDataEntry patientDataEntry = releaseOfDataEvent.getEntry();

        Log.LogBuilder logEntityBuilder = Log.builder()
            .allowedForPatient(patientDataEntry.isAllowedForPatient())
            .careProviderId(patientDataEntry.getCareProviderId())
            .careUnitId(patientDataEntry.getCareUnitId())
            .informationId(patientDataEntry.getId())
            .sourceSystemId(patientDataEntry.getSourceSystemId())
            .infoType(patientDataEntry.getInfoType())
            .patientId(patientDataEntry.getPatientId())
            .informationTimestamp(patientDataEntry.getTimestamp())
            .timestamp(releaseOfDataEvent.timestamp());

        if (releaseOfDataEvent.getPrincipal() instanceof CareActorPrincipal) {
            CareActorPrincipal careActorPrincipal = (CareActorPrincipal) releaseOfDataEvent.getPrincipal();
            logEntityBuilder.loggedInCareActorHsaId(careActorPrincipal.getHsaId())
                .loggedInCareActorGivenName(careActorPrincipal.getCareActorGivenName())
                .loggedInCareActorMiddleAndSurName(careActorPrincipal.getCareActorMiddleAndSurName())
                .loggedInCareActorCareUnitId(careActorPrincipal.getCareUnitId())
                .loggedInCareActorCareUnitName(careActorPrincipal.getCareUnitName())
                .loggedInCareActorCareProviderId(careActorPrincipal.getCareProviderId())
                .loggedInCareActorCareProviderName(careActorPrincipal.getCareProviderName())
                .loggedInCareActorAssignmentId(careActorPrincipal.getAssignmentId())
                .loggedInCareActorAssignmentName(careActorPrincipal.getAssignmentName())
                .loggedInCareActorCommissionPurpose(careActorPrincipal.getCommissionPurpose());

            logEntityBuilder.ruleset(RuleSet.SJF);
        } else if (releaseOfDataEvent.getPrincipal() instanceof CitizenPrincipal) {
            CitizenPrincipal citizenPrincipal = (CitizenPrincipal) releaseOfDataEvent.getPrincipal();
            logEntityBuilder.loggedInCitizenSsn(citizenPrincipal.getSubjectSerialNumber())
                .loggedInCitizenName(citizenPrincipal.getName())
                .loggedInCitizenMiddleAndSurname(citizenPrincipal.getMiddleAndSurName());
            logEntityBuilder.ruleset(RuleSet.CITIZEN_DIRECT_ACCESS);
        }

        Log log = logEntityBuilder.build();

        logRepository.save(log);
    }
}
