package se.sll.ahrr.logging.service;

/*-
 * #%L
 * logging
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import se.sll.ahrr.event.DenyAccessEvent;
import se.sll.ahrr.event.ReleaseOfDataEvent;
import se.sll.ahrr.listener.EventListener;

@Service
@Profile("log_to_console")
public class ConsoleEventLogger implements EventListener {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Subscribe
    public void releaseOfData(ReleaseOfDataEvent releaseOfDataEvent) {
        logger.info("Releasing {} to {}", releaseOfDataEvent.getEntry(), releaseOfDataEvent.getPrincipal().getUsername());
    }

    @Subscribe
    public void denyAccessEvent(DenyAccessEvent denyAccessEvent) {
        logger.info("Denying access to {} for {} for reason {}", denyAccessEvent.getEntry(), denyAccessEvent.getPrincipal().getUsername(), denyAccessEvent.reason());
    }

}
