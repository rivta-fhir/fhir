package se.sll.ahrr.logging.entity;

/*-
 * #%L
 * logging
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.Builder;
import lombok.Getter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import se.sll.ahrr.codesystem.valueset.RuleSet;
import se.sll.ahrr.domain.InformationType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Builder
public class Log {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private final LocalDateTime timestamp;
    private final String careProviderId;
    private final String careUnitId;
    private final String patientId;
    private final String informationId;
    private final String sourceSystemId;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private final LocalDateTime informationTimestamp;

    @Enumerated(EnumType.STRING)
    private final InformationType infoType;
    private final boolean allowedForPatient;

    private final String loggedInCareActorHsaId;
    private final String loggedInCareActorGivenName;
    private final String loggedInCareActorMiddleAndSurName;
    private final String loggedInCareActorCareUnitId;
    private final String loggedInCareActorCareUnitName;
    private final String loggedInCareActorCareProviderId;
    private final String loggedInCareActorCareProviderName;
    private final String loggedInCareActorAssignmentId;
    private final String loggedInCareActorAssignmentName;
    private final String loggedInCareActorCommissionPurpose;

    private final String loggedInCitizenSsn;
    private final String loggedInCitizenName;
    private final String loggedInCitizenMiddleAndSurname;

    @Enumerated(EnumType.STRING)
    private final RuleSet ruleset;
}
