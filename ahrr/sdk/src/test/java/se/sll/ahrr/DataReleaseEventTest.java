package se.sll.ahrr;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.domain.Principal;
import se.sll.ahrr.event.ReleaseOfDataEvent;
import se.sll.ahrr.security.EventEmittingAccessControl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestContext.class, webEnvironment = WebEnvironment.MOCK)
@ActiveProfiles("dev")
public class DataReleaseEventTest {

    @Autowired
    EventEmittingAccessControl accessControl;

    @Autowired
    EventBus eventBus;

    private class CountdownEventListener {

        private CountDownLatch countDownLatch;

        CountdownEventListener(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }

        @Subscribe
        public void dataReleaseEvent(ReleaseOfDataEvent event) {
            countDownLatch.countDown();
        }
    }

    @Test
    public void assertEventsAreEmittedForGrantedData() throws Exception {
        PatientDataEntry<Object> pu1 = PatientDataEntry.builder()
            .patientId("191212121212")
            .infoType(InformationType.PU)
            .allowedForPatient(true)
            .build();

        PatientDataEntry<Object> pu2 = PatientDataEntry.builder()
            .patientId("193601286499")
            .infoType(InformationType.PU)
            .allowedForPatient(true)
            .build();


        Principal principal1 = new CitizenPrincipal("191212121212", "", "");
        Principal principal2 = new CitizenPrincipal("193601286499", "", "");

        CountDownLatch countDownLatch = new CountDownLatch(2);

        CountdownEventListener countdownEventListener = new CountdownEventListener(countDownLatch);

        eventBus.register(countdownEventListener);

        accessControl.performAccessControl(pu1, principal1);
        accessControl.performAccessControl(pu2, principal2);

        countDownLatch.await(1, TimeUnit.SECONDS);

        assertEquals(0, countDownLatch.getCount());
    }
}
