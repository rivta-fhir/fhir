package se.sll.ahrr.internal.service;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.dataloader.RivtaDataLoader;

import java.util.HashMap;
import java.util.Map;

public class DataLoaderProvider {

    private final Map<Class, RivtaDataLoader> dataloaders = new HashMap<>();
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public boolean addDataLoader(final RivtaDataLoader rivtaDataLoader) {
        try {
            dataloaders.put(rivtaDataLoader.getRivtaResponseType(), rivtaDataLoader);
            return true;
        } catch (Exception e) {
            log.error("Error while adding dataloader", e);
            return false;
        }
    }

    public RivtaDataLoader getDataLoader(Class<?> type) {
        if (dataloaders.containsKey(type)) {
            return dataloaders.get(type);
        }
        return null;
    }

    public <T> T loadRivtaResponse(final String clientId, final Class<T> type) {
        try {
            if (!dataloaders.containsKey(type)) return null;
            return (T) dataloaders.get(type).loadRivtaResponse(clientId);
        } catch (Exception e) {
            log.error("Error while loading rivta response", e);
            return null;
        }
    }
}
