package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
/*
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.ehr.patientconsent._1.PDLAssertionType;
import riv.ehr.patientconsent.accesscontrol.checkconsentresponder._1.CheckConsentResponseType;
import riv.ehr.patientconsent.querying.getconsentsforpatientresponder._1.GetConsentsForPatientResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.codesystem.CodeSystem;
import se.sll.ahrr.codesystem.valueset.ConsentAction;
import se.sll.ahrr.codesystem.valueset.ConsentType;
import se.sll.ahrr.common.FhirUtilities;
import se.sll.ahrr.consents.ConsentDataTransformer;
import se.sll.ahrr.dataloader.CheckConsentsDataLoader;
import se.sll.ahrr.dataloader.GetConsentsForPatientDataLoader;
import se.sll.ahrr.dataloader.RegisterExtendedConsentDataLoader;
import se.sll.ahrr.domain.Actor;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.consent", name = "version", havingValue = "1.0", matchIfMissing = true)
public class ConsentResourceProvider extends AbstractRivtaResourceProvider<Consent, GetConsentsForPatientResponseType, PDLAssertionType> {
    private RegisterExtendedConsentDataLoader registerExtendedConsentDataLoader;
    private CheckConsentsDataLoader checkConsentsDataLoader;
    private ConsentDataTransformer rivTransformer;
    private GetConsentsForPatientDataLoader getConsentsForPatientDataLoader;
    private Riv2FhirTransformer<List<PDLAssertionType>, Consent> fhirTransformer;


    public ConsentResourceProvider(HttpServletRequest request,
                                   UserService userService,
                                   AccessControl accessControl,
                                   GetConsentsForPatientDataLoader getConsentsForPatientDataLoader,
                                   Riv2FhirTransformer<List<PDLAssertionType>, Consent> fhirTransformer,
                                   RegisterExtendedConsentDataLoader registerExtendedConsentDataLoader,
                                   CheckConsentsDataLoader checkConsentsDataLoader,
                                   ConsentDataTransformer rivTransformer) {
        super(request, userService, accessControl, null, null);

        this.fhirTransformer = fhirTransformer;
        this.getConsentsForPatientDataLoader = getConsentsForPatientDataLoader;
        this.rivTransformer = rivTransformer;
        this.registerExtendedConsentDataLoader = registerExtendedConsentDataLoader;
        this.checkConsentsDataLoader = checkConsentsDataLoader;
    }

    @Override
    protected List<PDLAssertionType> getResultsFromResponse(GetConsentsForPatientResponseType getConsentsForPatientResponseType) {
        return getConsentsForPatientResponseType.getGetConsentsResultType().getPdlAssertions();
    }

    @Override
    protected PatientDataEntry getPatientDataEntryFromRivResponse(PDLAssertionType rivtaObject) {
        return PatientDataEntry.builder()
            .patientId(rivtaObject.getPatientId())
            .infoType(getInformationType(rivtaObject))
            .payload(rivtaObject)
            .build();
    }

    @Override
    protected Object getRivtaHeader(PDLAssertionType rivtaObject) {
        return null;
    }

    @Override
    protected InformationType getInformationType(PDLAssertionType rivtaObject) {
        return InformationType.CONSENT;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Consent.class;
    }

    @Override
    @Search
    public List<Consent> loadDataForPatient() throws Exception {
        try {
            CareActorPrincipal principal = (CareActorPrincipal) userService.getLoggedInUser();
            return fhirTransformer.transformToFhir(getConsentsForPatientDataLoader.loadRivtaResponse(this.getSSN(this.request), principal.getCareProviderId()).getGetConsentsResultType().getPdlAssertions());
        } catch (Exception e) {
            log.error("Unexpected error in loadDataForPatient()", e);
            throw e;
        }
    }

    @Override
    @Read
    public Consent loadDataForPatient(@IdParam IIdType id) throws Exception {
        try {
            CareActorPrincipal principal = (CareActorPrincipal) userService.getLoggedInUser();
            return fhirTransformer.transformToFhir(getConsentsForPatientDataLoader.loadRivtaResponse(this.getSSN(this.request), principal.getCareProviderId()).getGetConsentsResultType().getPdlAssertions()).stream()
                .filter(r -> r != null &&
                    r.getIdElement() != null &&
                    r.getIdElement().getValue() != null &&
                    r.getIdElement().getValue().equals(id.getIdPart())).findFirst().orElse(null);
        } catch (Exception e) {
            log.error("Unexpected error in loadDataForPatient()", e);
            throw e;
        }
    }


    @Create
    public MethodOutcome createConsent(@ResourceParam Consent consent) {
        log.info("Registering new consent");
        CareActorPrincipal principal = (CareActorPrincipal) userService.getLoggedInUser();

        try {
            String id = registerExtendedConsentDataLoader.registerConsent(
                rivTransformer.transformToRiv(Collections.singletonList(consent),
                    Actor.builder()
                        .assignmentId(principal.getAssignmentId())
                        .assignmentName(principal.getAssignmentName())
                        .hsaId(principal.getHsaId())
                        .build()));
            return new MethodOutcome(new IdType(id));
        } catch (Exception e) {
            log.error("Error while registering consent", e);
            OperationOutcome.OperationOutcomeIssueComponent operationOutcomeIssueComponent = new OperationOutcome.OperationOutcomeIssueComponent();
            operationOutcomeIssueComponent.setCode(OperationOutcome.IssueType.INVALID);
            operationOutcomeIssueComponent.setSeverity(OperationOutcome.IssueSeverity.FATAL);
            operationOutcomeIssueComponent.setDiagnostics(e.getMessage());
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.addIssue(operationOutcomeIssueComponent);

            throw new UnprocessableEntityException(e.getMessage(), operationOutcome);
        }
    }

    @Operation(name = "$status", idempotent = true)
    public ListResource consentStatus() throws Exception {
        CareActorPrincipal principal = (CareActorPrincipal) userService.getLoggedInUser();
        CheckConsentResponseType checkConsentResponseType = checkConsentsDataLoader.loadRivtaResponse(getSSN(this.request), principal.getCareProviderId(), principal.getCareUnitId(), principal.getHsaId());

        ListResource consentList = new ListResource();
        if (checkConsentResponseType.getCheckResultType().isHasConsent()) {
            Consent consent = new Consent();
            consent.setCategory(Collections.singletonList(FhirUtilities.codeableConceptWith(new Coding(CodeSystem.CONSENT_TYPE.getValue(), ConsentType.sjf.name(), ConsentType.sjf.getDisplayName()))));
            consent.setAction(Collections.singletonList(FhirUtilities.codeableConceptWith(new Coding(CodeSystem.CONSENT_ACTION.getValue(), ConsentAction.access.name(), ConsentAction.access.getDisplayName()))));
            consent.setStatus(Consent.ConsentState.ACTIVE);
            consentList.addEntry(new ListResource.ListEntryComponent(new Reference(consent)));

        }
        return consentList;
    }
}
*/