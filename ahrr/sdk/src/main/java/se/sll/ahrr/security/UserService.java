package se.sll.ahrr.security;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.CitizenPrincipal;
import se.sll.ahrr.domain.CommissionRight;
import se.sll.ahrr.domain.Principal;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class UserService<T extends AbstractAuthenticationToken> implements AuthenticationUserDetailsService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public UserDetails loadUserDetails(Authentication authentication) throws UsernameNotFoundException {
        final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();

        return CareActorPrincipal.builder()
                .hsaId("SE165565594230-MOCKUSER")
                .careActorGivenName("Mock")
                .careActorMiddleAndSurName("User")
                .careUnitId("SE165565594230-MOCK-UNIT")
                .careUnitName("Mock unit")
                .careProviderId("SE165565594230-MOCK-PROVIDER")
                .careProviderName("Mock provider")
                .title("Specialistläkare")
                .assignmentId("SE165565594230-MOCK-ASSIGNEMENT")
                .assignmentName("Mock assignment")
                .commissionPurpose("Vård och behandling")
                .commissionRights(Collections.singletonList(
                        CommissionRight.builder().infoType("alla").permission("Läsa").scope("SJF").build()
                ))
                .build();
    }

    public Principal getLoggedInUser() {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            throw new PreAuthenticatedCredentialsNotFoundException("No principal found");
        }

        return (Principal) auth.getPrincipal();
    }
}
