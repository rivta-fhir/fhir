package se.sll.ahrr.config;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.rest.server.IResourceProvider;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.sll.ahrr.fhir.FhirServlet;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.listener.EventListener;
import se.sll.ahrr.security.EventEmittingAccessControl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Executors;

@Configuration
public class AhrrAutoConfiguration {

    private Logger log = LoggerFactory.getLogger(getClass());

    private Collection<AbstractRivtaResourceProvider> resourceProviders;
    private Collection<EventListener> eventListeners;
    private EventEmittingAccessControl eventEmittingAccessControl;

    public AhrrAutoConfiguration(Collection<AbstractRivtaResourceProvider> resourceProviders, Collection<EventListener> eventListeners, EventEmittingAccessControl eventEmittingAccessControl) {
        this.resourceProviders = resourceProviders;
        this.eventListeners = eventListeners;
        this.eventEmittingAccessControl = eventEmittingAccessControl;
    }

    @Bean
    EventBus eventBus() {
        EventBus eventBus = new AsyncEventBus(Executors.newCachedThreadPool());
        eventListeners.forEach(eventBus::register);
        eventEmittingAccessControl.setEventBus(eventBus);
        return eventBus;
    }

    @Bean
    public ServletRegistrationBean fhirServlet() {
        ArrayList<IResourceProvider> iResourceProviders = new ArrayList<>();
        resourceProviders.forEach(
            r -> {
                log.info("Initializing resource provider {}", r.getClass().getSimpleName());
                iResourceProviders.add(r);
            }
        );

        final FhirServlet fhirServlet = new FhirServlet();
        fhirServlet.setResourceProviders(iResourceProviders);
        final ServletRegistrationBean srb = new ServletRegistrationBean();
        srb.setUrlMappings(Collections.singletonList("/fhir/*"));
        srb.setServlet(fhirServlet);
        return srb;
    }
}
