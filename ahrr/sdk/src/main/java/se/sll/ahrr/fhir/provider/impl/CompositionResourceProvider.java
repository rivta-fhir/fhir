package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.beanutils.PropertyUtils;
import org.hl7.fhir.dstu3.model.Composition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.CPatientSummaryHeaderType;
import riv.clinicalprocess.healthcond.description._2.CareDocumentationType;
import riv.clinicalprocess.healthcond.description._2.HealthcareProfessionalType;
import riv.clinicalprocess.healthcond.description.getcaredocumentationresponder._2.GetCareDocumentationResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@ConditionalOnProperty(prefix = "providers.composition", name = "version", havingValue = "2.1", matchIfMissing = true)
public class CompositionResourceProvider extends AbstractRivtaResourceProvider<Composition, GetCareDocumentationResponseType, CareDocumentationType> {

    public CompositionResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetCareDocumentationResponseType> dataLoader, Riv2FhirTransformer<List<CareDocumentationType>, Composition> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    public Class<Composition> getResourceType() {
        return Composition.class;
    }

    @Override
    protected List<CareDocumentationType> getResultsFromResponse(GetCareDocumentationResponseType getCareDocumentationResponseType) {
        return getCareDocumentationResponseType.getCareDocumentation();
    }

    @Override
    protected PatientDataEntry getPatientDataEntryFromRivResponse(final CareDocumentationType rivtaObject) {
        try {
            final CPatientSummaryHeaderType header = rivtaObject.getCareDocumentationHeader();

            Optional<HealthcareProfessionalType> healthcareProfessionalOptional = Optional.ofNullable(header.getAccountableHealthcareProfessional());

            PatientDataEntry.PatientDataEntryBuilder<Object> builder = PatientDataEntry.builder()
                .patientId(header.getPatientId().getId())
                .id(header.getDocumentId())
                .sourceSystemId(header.getSourceSystemHSAid())
                .infoType(getInformationType(rivtaObject))
                .allowedForPatient((Boolean) PropertyUtils.getProperty(header, "approvedForPatient"))
                .payload(rivtaObject);

            if (healthcareProfessionalOptional.isPresent()) {
                HealthcareProfessionalType healthcareProfessional = healthcareProfessionalOptional.get();
                builder.careProviderId(healthcareProfessional.getHealthcareProfessionalCareGiverHSAId())
                    .careUnitId(healthcareProfessional.getHealthcareProfessionalCareUnitHSAId())
                    .timestamp(getLocalDateTimeFromRivta(healthcareProfessional.getAuthorTime()));
            }
            return builder.build();

        } catch (Exception e) {
            log.error("Error in getPatientDataEntryFromRivResponse()", e);
            return null;
        }
    }

    @Override
    protected Object getRivtaHeader(CareDocumentationType rivtaObject) {
        return rivtaObject.getCareDocumentationHeader();
    }

    @Override
    protected InformationType getInformationType(CareDocumentationType rivtaObject) {
        return InformationType.VOO;
    }
}
