package se.sll.ahrr.fhir.provider.impl.encounter;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.Encounter;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._2.CareContactType;
import riv.clinicalprocess.logistics.logistics.getcarecontactsresponder._2.GetCareContactsResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.encounter", name = "version", havingValue = "2.0")
public class EncounterResourceProviderV2_0 extends AbstractRivtaResourceProvider<Encounter, GetCareContactsResponseType, CareContactType>
{

    @Autowired
    public EncounterResourceProviderV2_0(HttpServletRequest request, UserService userService, AccessControl accessControl, @Qualifier("careContactDataLoaderV2_0") RivtaDataLoader<GetCareContactsResponseType> dataLoader, @Qualifier("careContactDataTransformerV2_0") Riv2FhirTransformer<List<CareContactType>, Encounter> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Encounter.class;
    }

    @Override
    protected List<CareContactType> getResultsFromResponse(GetCareContactsResponseType getCareContactsResponseType) {
        return getCareContactsResponseType.getCareContact();
    }

    @Override
    protected Object getRivtaHeader(CareContactType rivtaObject) {
        return rivtaObject.getCareContactHeader();
    }

    @Override
    protected InformationType getInformationType(CareContactType rivtaObject) {
        return InformationType.VKO;
    }
}
