package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.ImagingStudy;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.actoutcome._3.ImagingOutcomeType;
import riv.clinicalprocess.healthcond.actoutcome.getimagingoutcomeresponder._1.GetImagingOutcomeResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.imagingStudy", name = "version", havingValue = "1.0", matchIfMissing = true)
public class ImagingStudyResourceProvider extends AbstractRivtaResourceProvider<ImagingStudy, GetImagingOutcomeResponseType, ImagingOutcomeType> {


    public ImagingStudyResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetImagingOutcomeResponseType> dataLoader, Riv2FhirTransformer<List<ImagingOutcomeType>, ImagingStudy> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    public Class<? extends IBaseResource> getResourceType() {
        return ImagingStudy.class;
    }

    @Override
    protected List<ImagingOutcomeType> getResultsFromResponse(GetImagingOutcomeResponseType getImagingOutcomeResponseType) {
        return getImagingOutcomeResponseType.getImagingOutcome();
    }

    @Override
    protected Object getRivtaHeader(ImagingOutcomeType rivtaObject) {
        return rivtaObject.getImagingOutcomeHeader();
    }

    @Override
    protected InformationType getInformationType(ImagingOutcomeType rivtaObject) {
        return InformationType.UND;
    }
}
