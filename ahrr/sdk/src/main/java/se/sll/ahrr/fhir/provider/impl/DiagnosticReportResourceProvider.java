package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.actoutcome._3.LaboratoryOrderOutcomeType;
import riv.clinicalprocess.healthcond.actoutcome.getlaboratoryorderoutcomeresponder._3.GetLaboratoryOrderOutcomeResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.diagnostic", name = "version", havingValue = "3.1", matchIfMissing = true)
public class DiagnosticReportResourceProvider extends AbstractRivtaResourceProvider<DiagnosticReport, GetLaboratoryOrderOutcomeResponseType, LaboratoryOrderOutcomeType> {

    public DiagnosticReportResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetLaboratoryOrderOutcomeResponseType> dataLoader, Riv2FhirTransformer<List<LaboratoryOrderOutcomeType>, DiagnosticReport> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return DiagnosticReport.class;
    }

    @Override
    protected List<LaboratoryOrderOutcomeType> getResultsFromResponse(GetLaboratoryOrderOutcomeResponseType getLaboratoryOrderOutcomeResponseType) {
        return getLaboratoryOrderOutcomeResponseType.getLaboratoryOrderOutcome();
    }

    @Override
    protected Object getRivtaHeader(LaboratoryOrderOutcomeType rivtaObject) {
        return rivtaObject.getLaboratoryOrderOutcomeHeader();
    }

    @Override
    protected InformationType getInformationType(LaboratoryOrderOutcomeType rivtaObject) {
        return InformationType.UND;
    }
}
