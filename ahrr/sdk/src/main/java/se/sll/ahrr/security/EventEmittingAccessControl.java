package se.sll.ahrr.security;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.eventbus.EventBus;
import org.springframework.stereotype.Service;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.domain.Principal;
import se.sll.ahrr.event.DenyAccessEvent;
import se.sll.ahrr.event.ReleaseOfDataEvent;
import se.sll.ahrr.security.domain.AccessControlledResult;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.*;

import java.time.LocalDateTime;

@Service
public class EventEmittingAccessControl extends AccessControl {

    private EventBus eventBus;

    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public AccessControlledResult performAccessControl(PatientDataEntry patientDataEntry, Principal principal) {
        AccessControlledResult accessControlledResult = super.performAccessControl(patientDataEntry, principal);
        if (eventBus == null) {
            throw new IllegalStateException("Event bus is null, cannot continue");
        }
        if (accessControlledResult.getResult() == AccessResult.ACCESS_GRANTED) {
            eventBus.post(new ReleaseOfDataEvent() {
                private LocalDateTime localDateTime = LocalDateTime.now();

                @Override
                public Principal getPrincipal() {
                    return principal;
                }

                @Override
                public PatientDataEntry getEntry() {
                    return patientDataEntry;
                }

                @Override
                public LocalDateTime timestamp() {
                    return localDateTime;
                }
            });
        } else {
            eventBus.post(new DenyAccessEvent() {
                private LocalDateTime localDateTime = LocalDateTime.now();

                @Override
                public Principal getPrincipal() {
                    return principal;
                }

                @Override
                public PatientDataEntry getEntry() {
                    return patientDataEntry;
                }

                @Override
                public String reason() {
                    return accessControlledResult.getAccessDeniedReason().name();
                }

                @Override
                public LocalDateTime timestamp() {
                    return localDateTime;
                }
            });
        }
        return accessControlledResult;
    }
}
