package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.Flag;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.healthcond.description._2.AlertInformationType;
import riv.clinicalprocess.healthcond.description.getalertinformationresponder._2.GetAlertInformationResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.flag", name = "version", havingValue = "2.0", matchIfMissing = true)
public class FlagResourceProvider extends AbstractRivtaResourceProvider<Flag, GetAlertInformationResponseType, AlertInformationType> {

    public FlagResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetAlertInformationResponseType> dataLoader, Riv2FhirTransformer<List<AlertInformationType>, Flag> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    protected List<AlertInformationType> getResultsFromResponse(GetAlertInformationResponseType getAlertInformationResponseType) {
        return getAlertInformationResponseType.getAlertInformation();
    }

    @Override
    protected Object getRivtaHeader(AlertInformationType rivtaObject) {
        return rivtaObject.getAlertInformationHeader();
    }

    @Override
    protected InformationType getInformationType(AlertInformationType rivtaObject) {
        return InformationType.UPP;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Flag.class;
    }
}
