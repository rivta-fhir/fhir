package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.CarePlan;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.logistics.logistics._3.CarePlanType;
import riv.clinicalprocess.logistics.logistics._3.PatientSummaryHeaderType;
import riv.clinicalprocess.logistics.logistics.getcareplansresponder._2.GetCarePlansResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.carePlan", name = "version", havingValue = "2.0", matchIfMissing = true)
public class CarePlanResourceProvider extends AbstractRivtaResourceProvider<CarePlan, GetCarePlansResponseType, CarePlanType> {

    public CarePlanResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetCarePlansResponseType> dataLoader, Riv2FhirTransformer<List<CarePlanType>, CarePlan> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    protected List<CarePlanType> getResultsFromResponse(GetCarePlansResponseType getCarePlansResponseType) {
        return getCarePlansResponseType.getCarePlan();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return CarePlan.class;
    }

    @Override
    protected PatientDataEntry getPatientDataEntryFromRivResponse(final CarePlanType rivtaObject) {
        try {
            final PatientSummaryHeaderType header = rivtaObject.getCarePlanHeader();

            return PatientDataEntry.builder()
                .careProviderId(header.getAccountableHealthcareProfessional().getHealthcareProfessionalCareGiverHSAId())
                .careUnitId(header.getAccountableHealthcareProfessional().getHealthcareProfessionalCareUnitHSAId())
                .timestamp(getLocalDateTimeFromRivta(header.getAccountableHealthcareProfessional().getAuthorTime()))
                .patientId(header.getPatientId().getId())
                .id(header.getDocumentId())
                .sourceSystemId(header.getSourceSystemHSAId())
                .infoType(InformationType.VPO)
                .allowedForPatient(header.isApprovedForPatient())
                .payload(rivtaObject)
                .build();

        } catch (Exception e) {
            log.error("Error in getPatientDataEntryFromRivResponse()", e);
            return null;
        }
    }

    @Override
    protected Object getRivtaHeader(CarePlanType rivtaObject) {
        return null;
    }

    @Override
    protected InformationType getInformationType(CarePlanType rivtaObject) {
        return InformationType.VPO;
    }
}
