package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.population.residentmaster._1.ResidentType;
import riv.population.residentmaster.lookupresidentforfullprofileresponder._1.LookupResidentForFullProfileResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.patient", name = "version", havingValue = "1.2", matchIfMissing = true)
public class PatientResourceProvider extends AbstractRivtaResourceProvider<Patient, LookupResidentForFullProfileResponseType, ResidentType> {

    public PatientResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, @Qualifier("lookupResidentForFullProfileDataLoader") RivtaDataLoader<LookupResidentForFullProfileResponseType> dataLoader, Riv2FhirTransformer<List<ResidentType>, Patient> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    protected List<ResidentType> getResultsFromResponse(LookupResidentForFullProfileResponseType lookupResidentForFullProfileResponseType) {
        return lookupResidentForFullProfileResponseType.getResident();
    }

    @Override
    protected PatientDataEntry getPatientDataEntryFromRivResponse(ResidentType rivtaObject) {
        return PatientDataEntry.builder()
            .infoType(getInformationType(rivtaObject))
            .patientId(rivtaObject.getPersonpost().getPersonId())
            .allowedForPatient(true)
            .build();
    }

    @Override
    protected Object getRivtaHeader(ResidentType rivtaObject) {
        return null;
    }

    @Override
    protected InformationType getInformationType(ResidentType rivtaObject) {
        return InformationType.PU;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Patient.class;
    }
}
