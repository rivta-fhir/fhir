package se.sll.ahrr.fhir.provider;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import org.apache.commons.beanutils.PropertyUtils;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.domain.PatientDataEntry;
import se.sll.ahrr.domain.Principal;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.domain.AccessResult;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractRivtaResourceProvider<ResourceType extends Resource, RivtaResponseType, RivtaObjectType> implements IResourceProvider {
    protected final HttpServletRequest request;
    protected Logger log;
    protected UserService userService;
    private final AccessControl accessControl;
    private RivtaDataLoader<RivtaResponseType> dataLoader;
    private Riv2FhirTransformer<List<RivtaObjectType>, ResourceType> fhirTransformer;

    public AbstractRivtaResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<RivtaResponseType> dataLoader, Riv2FhirTransformer<List<RivtaObjectType>, ResourceType> fhirTransformer) {
        this.request = request;
        this.log = LoggerFactory.getLogger(this.getClass());
        this.userService = userService;
        this.accessControl = accessControl;
        this.dataLoader = dataLoader;
        this.fhirTransformer = fhirTransformer;
    }

    /**
     * Returns a list of response objects from a RIVTA reponse,
     * for example a GetCareDocumentationResponse would return
     * a list of CareDocumentationType.
     *
     * @param responseType the rivta response
     * @return a list of rivta objects
     */
    protected abstract List<RivtaObjectType> getResultsFromResponse(RivtaResponseType responseType);

    protected abstract Object getRivtaHeader(RivtaObjectType rivtaObject);

    protected abstract InformationType getInformationType(RivtaObjectType rivtaObject);

    /**
     * Returns a patient data entry from a rivta object.
     *
     * @param rivtaObject the rivta object
     * @return a PatientDataEntry
     */
    protected PatientDataEntry getPatientDataEntryFromRivResponse(RivtaObjectType rivtaObject) {
        try {
            final Object header = getRivtaHeader(rivtaObject);

            Optional healthcareProfessionalOptional = Optional.ofNullable(PropertyUtils.getProperty(header, "accountableHealthcareProfessional"));

            PatientDataEntry.PatientDataEntryBuilder<Object> builder = PatientDataEntry.builder()
                    .patientId((String) PropertyUtils.getProperty(PropertyUtils.getProperty(header, "patientId"), "id"))
                    .id((String) PropertyUtils.getProperty(header, "documentId"))
                    .sourceSystemId((String) PropertyUtils.getProperty(header, "sourceSystemHSAId"))
                    .infoType(getInformationType(rivtaObject))
                    .allowedForPatient((Boolean) PropertyUtils.getProperty(header, "approvedForPatient"))
                    .payload(rivtaObject);

            if (healthcareProfessionalOptional.isPresent()) {
                Object healthcareProfessional = healthcareProfessionalOptional.get();
                builder.careProviderId((String) PropertyUtils.getProperty(healthcareProfessional, "healthcareProfessionalCareGiverHSAId"))
                        .careUnitId((String) PropertyUtils.getProperty(healthcareProfessional, "healthcareProfessionalCareUnitHSAId"))
                        .timestamp(getLocalDateTimeFromRivta((String) PropertyUtils.getProperty(healthcareProfessional, "authorTime")));
            }
            return builder.build();
        } catch (Exception e) {
            log.error("Error in getPatientDataEntryFromRivResponse()", e);
            return null;
        }
    }

    protected LocalDateTime getLocalDateTimeFromRivta(final String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    private List<RivtaObjectType> getRivResponseAndPerformAccessControl(final String patientIdentifier) throws Exception
    {
        RivtaResponseType rivtaResponse = rivtaResponse = dataLoader.loadRivtaResponse(patientIdentifier);

        if (rivtaResponse == null) {
            return null;
        }
        return getResultsFromResponse(rivtaResponse).parallelStream()
                .collect(Collectors.toList());
    }

    @Search
    public List<ResourceType> loadDataForPatient(@OptionalParam(name="patient-identifier") final String patientIdentifier, @OptionalParam(name="patient-identifier-system") final String patientIdentifierSystem) throws Exception {
        try
        {
            return fhirTransformer.transformToFhir(getRivResponseAndPerformAccessControl(patientIdentifier));
        }
        catch (Exception e)
        {
            log.error("Unexpected error in loadDataForPatient()", e);
            throw e;
        }
    }

    @Read
    public ResourceType loadDataForPatient(@IdParam IIdType id) throws Exception {
        try {
            return fhirTransformer.transformToFhir(getRivResponseAndPerformAccessControl(id.getValue())).stream()
                    .filter(r -> r != null &&
                            r.getIdElement() != null &&
                            r.getIdElement().getValue() != null &&
                            r.getIdElement().getValue().equals(id.getIdPart())).findFirst().orElse(null);
        } catch (Exception e) {
            log.error("Unexpected error in loadDataForPatient()", e);
            throw e;
        }
    }

/*    private void clearCache() {
        dataLoader.clearCache(getSSN(this.request));
    }

    @Operation(name = "$force", idempotent = true)
    public Bundle loadDataForPatientWithoutCache() throws Exception {
        log.debug("Clearing cache");
        clearCache();

        List<ResourceType> resources = loadDataForPatient();

        Bundle bundle = new Bundle();
        bundle.setType(Bundle.BundleType.SEARCHSET);

        List<Bundle.BundleEntryComponent> bundleEntryComponents = new LinkedList<>();

        resources.forEach(
                condition -> {
                    Bundle.BundleEntryComponent bundleEntryComponent = new Bundle.BundleEntryComponent();
                    bundleEntryComponent.setResource(condition);
                    bundleEntryComponents.add(bundleEntryComponent);
                }
        );

        bundle.setEntry(bundleEntryComponents);

        return bundle;
    }*/

}
