package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.hl7.fhir.dstu3.model.Immunization;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.clinicalprocess.activityprescription.actoutcome._2.VaccinationMedicalRecordType;
import riv.clinicalprocess.activityprescription.actoutcome.getvaccinationhistoryresponder._2.GetVaccinationHistoryResponseType;
import se.sll.ahrr.Riv2FhirTransformer;
import se.sll.ahrr.dataloader.RivtaDataLoader;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "providers.immunization", name = "version", havingValue = "2.0", matchIfMissing = false)
public class ImmunizationResourceProvider extends AbstractRivtaResourceProvider<Immunization, GetVaccinationHistoryResponseType, VaccinationMedicalRecordType> {

    public ImmunizationResourceProvider(HttpServletRequest request, UserService userService, AccessControl accessControl, RivtaDataLoader<GetVaccinationHistoryResponseType> dataLoader, Riv2FhirTransformer<List<VaccinationMedicalRecordType>, Immunization> fhirTransformer) {
        super(request, userService, accessControl, dataLoader, fhirTransformer);
    }

    @Override
    protected List<VaccinationMedicalRecordType> getResultsFromResponse(GetVaccinationHistoryResponseType getVaccinationHistoryResponseType)
    {
        return getVaccinationHistoryResponseType.getVaccinationMedicalRecord();
    }

    @Override
    protected Object getRivtaHeader(VaccinationMedicalRecordType rivtaObject)
    {
        return rivtaObject.getVaccinationMedicalRecordHeader();
    }

    @Override
    protected InformationType getInformationType(VaccinationMedicalRecordType rivtaObject)
    {
        return InformationType.UNKNOWN;
    }

    @Override
    public Class<? extends IBaseResource> getResourceType()
    {
        return Immunization.class;
    }

}
