package se.sll.ahrr.fhir.provider.impl;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import org.hl7.fhir.dstu3.model.AuditEvent;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import riv.ehr.log._1.LogType;
import riv.ehr.log.store.storelogresponder._1.StoreLogResponseType;
import se.sll.ahrr.dataloader.StoreLogDataLoader;
import se.sll.ahrr.domain.CareActorPrincipal;
import se.sll.ahrr.domain.InformationType;
import se.sll.ahrr.fhir.provider.AbstractRivtaResourceProvider;
import se.sll.ahrr.log.LogTransformer;
import se.sll.ahrr.security.UserService;
import se.sll.ahrr.security.service.accesscontrol.AccessControl;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
/*
@Service
@ConditionalOnProperty(prefix = "providers.log", name = "version", havingValue = "1.0", matchIfMissing = true)
public class LogResourceProvider extends AbstractRivtaResourceProvider<AuditEvent, StoreLogResponseType, LogType> {
    private LogTransformer rivTransformer;
    private StoreLogDataLoader storeLogDataLoader;

    public LogResourceProvider(HttpServletRequest request,
                               UserService userService,
                               AccessControl accessControl,
                               StoreLogDataLoader storeLogDataLoader,
                               LogTransformer rivTransformer) {
        super(request, userService, accessControl, null, null);

        this.storeLogDataLoader = storeLogDataLoader;
        this.rivTransformer = rivTransformer;
    }


    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return AuditEvent.class;
    }

    @Override
    @Search
    public List<AuditEvent> loadDataForPatient() throws Exception {
        return Collections.emptyList();
    }

    @Override
    @Read
    public AuditEvent loadDataForPatient(@IdParam IIdType id) throws Exception {
        return null;
    }

    @Override
    protected List<LogType> getResultsFromResponse(StoreLogResponseType storeLogResponseType) {
        return null;
    }

    @Override
    protected Object getRivtaHeader(LogType rivtaObject) {
        return null;
    }

    @Override
    protected InformationType getInformationType(LogType rivtaObject) {
        return null;
    }

    @Create
    public MethodOutcome registerAuditEvent(@ResourceParam AuditEvent auditEvent) {
        log.debug("Registering new audit event");
        try {
            storeLogDataLoader.storeLog(rivTransformer.transformToRiv(auditEvent, (CareActorPrincipal) userService.getLoggedInUser(), getSSN(request)));
            return new MethodOutcome();
        } catch (Exception e) {
            log.error("Error while registering audit event", e);
            OperationOutcome.OperationOutcomeIssueComponent operationOutcomeIssueComponent = new OperationOutcome.OperationOutcomeIssueComponent();
            operationOutcomeIssueComponent.setCode(OperationOutcome.IssueType.INVALID);
            operationOutcomeIssueComponent.setSeverity(OperationOutcome.IssueSeverity.FATAL);
            operationOutcomeIssueComponent.setDiagnostics(e.getMessage());
            OperationOutcome operationOutcome = new OperationOutcome();
            operationOutcome.addIssue(operationOutcomeIssueComponent);

            throw new UnprocessableEntityException(e.getMessage(), operationOutcome);
        }
    }
}*/
