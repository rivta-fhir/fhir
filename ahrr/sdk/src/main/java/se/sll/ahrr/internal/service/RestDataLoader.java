package se.sll.ahrr.internal.service;

/*-
 * #%L
 * sdk
 * %%
 * Copyright (C) 2017 Inera AB
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.xml.XmlMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sll.ahrr.dataloader.RivtaDataLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@SuppressWarnings("Duplicates")
public class RestDataLoader<T> implements RivtaDataLoader<T> {

    private Class<T> rivtaResponseType;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public RestDataLoader(final String url, final Class<T> rivtaResponseType) {
        this.url = url;
        this.rivtaResponseType = rivtaResponseType;
    }

    private final String url;

    public T loadRivtaResponse(final String clientId) {
        try {

            String xml = makeRequest(clientId);
            XmlMapper xmlMapper = new XmlMapper();

            return xmlMapper.readValue(xml, rivtaResponseType);
        } catch (IOException e) {
            log.error("Error while loading rivta response", e);
            return null;
        }
    }

    @Override
    public void clearCache(String clientId) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public Class<T> getRivtaResponseType() {
        return rivtaResponseType;
    }

    private String makeRequest(final String clientId) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
            .url(url + clientId)
            .build();
        try {
            Response response = client.newCall(request).execute();
            final InputStream inputStream = response.body().byteStream();
            return inputStreamToString(inputStream);
        } catch (IOException e) {
            log.error("Error while making rest request", e);
            return null;
        }
    }

    private String inputStreamToString(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line);
        }
        bufferedReader.close();

        return stringBuilder.toString();
    }
}
