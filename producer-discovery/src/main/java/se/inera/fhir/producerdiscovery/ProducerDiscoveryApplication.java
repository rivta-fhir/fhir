package se.inera.fhir.producerdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerDiscoveryApplication.class, args);
	}

}
