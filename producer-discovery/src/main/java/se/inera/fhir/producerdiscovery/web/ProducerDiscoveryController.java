package se.inera.fhir.producerdiscovery.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import se.inera.fhir.common.dto.SourceSystemDto;
import se.inera.fhir.producerdiscovery.service.ProducerDiscoveryService;

import java.util.Map;

@RestController
@RequestMapping("/")
public class ProducerDiscoveryController {

    private Logger log = LoggerFactory.getLogger(getClass());

    private ProducerDiscoveryService producerDiscoveryService;

    public ProducerDiscoveryController(ProducerDiscoveryService producerDiscoveryService) {
        this.producerDiscoveryService = producerDiscoveryService;
    }

    @GetMapping("/sourcesystem")
    public Flux<SourceSystemDto> findSourceSystems(
            @RequestParam(name = "fhirVersion") final String fhirVersion,
            @RequestParam(name = "resource") final String resource,
            @RequestParam Map<String,String> queryParams
    ) {
        queryParams.remove("resource");
        queryParams.remove("fhirVersion");
        log.info("Call to findSourceSystem with resource: {}, parameters: {}, fhirVersion: {}", resource, queryParams, fhirVersion);

        return this.producerDiscoveryService.getSourceSystems(
                fhirVersion,
                resource,
                queryParams
        ).log();
    }

}
