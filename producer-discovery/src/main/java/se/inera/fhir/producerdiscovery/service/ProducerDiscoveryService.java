package se.inera.fhir.producerdiscovery.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.common.dto.SourceSystemDto;
import se.inera.fhir.producerdiscovery.dto.InformationIndexEntryDto;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProducerDiscoveryService {

    @Value("${serviceDiscoveryUrl}")
    private String serviceDiscoveryUrl;

    @Value("${informationIndexUrl}")
    private String informationIndexUrl;

    public Flux<SourceSystemDto> getSourceSystems(
            final String fhirVersion,
            final String resource,
            final Map<String,String> queryParams
    ) {
        final String patientIdentifier = queryParams.get("patient-identifier");
        final String patientIdentifierSystem = queryParams.get("patient-identifier-system");
        final List<String> searchParams = queryParams.keySet().stream().collect(Collectors.toList());
        if (patientIdentifier == null || patientIdentifierSystem == null) {
            return getSourceSystemDtos(null, fhirVersion, resource, searchParams);
        }


        return getIdsFromInformationIndex(patientIdentifier, patientIdentifierSystem, resource)
                .flatMapMany(idList -> {
                    if (idList.isEmpty()) {
                        return Flux.empty();
                    }
                    return getSourceSystemDtos(idList, fhirVersion, resource, searchParams);
                });
    }

    private Flux<SourceSystemDto> getSourceSystemDtos(
            final List<String> idList,
            final String fhirVersion,
            final String resource,
            final List<String> searchParams) {
        return WebClient
                .builder()
                .baseUrl(serviceDiscoveryUrl)
                .build().get()
                .uri(builder -> {
                    if (fhirVersion != null) {
                        builder.queryParam("fhirVersion", fhirVersion);
                    }
                    if (idList != null && !idList.isEmpty()) {
                        builder.queryParam("ids", String.join(",", idList));
                    }
                    if (searchParams != null && !searchParams.isEmpty()) {
                        builder.queryParam("searchParams", String.join(",", searchParams));
                    }
                    builder.queryParam("resource", resource);
                    return builder.build();
                })
                .exchange().flatMapMany(
                        c -> c.bodyToFlux(SourceSystemDto.class));
    }

    private Mono<List<String>> getIdsFromInformationIndex(
            final String patientIdentifier,
            final String patientIdentifierSystem,
            final String resource
    ) {
        return WebClient
                .builder()
                .baseUrl(informationIndexUrl)
                .build().get()
                .uri(builder -> {
                    builder.queryParam("patient-identifier", patientIdentifier);
                    builder.queryParam("patient-identifier-system", patientIdentifierSystem);
                    builder.queryParam("resource", resource);
                    return builder.build();
                })
                .exchange().flatMapMany(
                        c -> c.bodyToFlux(InformationIndexEntryDto.class))
                .map(InformationIndexEntryDto::getSourceSystemId)
                .collectList();
    }
}
