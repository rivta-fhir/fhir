package se.inera.fhir.producerdiscovery.dto;

import java.util.List;

public class InformationIndexEntryDto {
    private String sourceSystemId;
    private String patientIdentifier;
    private String patientIdentifierSystem;
    private String resourceType;
    private List<String> profiles;

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        this.sourceSystemId = sourceSystemId;
    }

    public String getPatientIdentifier() {
        return patientIdentifier;
    }

    public void setPatientIdentifier(String patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }

    public String getPatientIdentifierSystem() {
        return patientIdentifierSystem;
    }

    public void setPatientIdentifierSystem(String patientIdentifierSystem) {
        this.patientIdentifierSystem = patientIdentifierSystem;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }
}
