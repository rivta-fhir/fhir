#!/bin/bash

BASE_URL=fhir-labs.app-test1.ind-ocp.sth.basefarm.net

CLUSTERINFO="2f8b7319776e5631ec4d58c233e7caa1488ab945  -"

if [ ! "$CLUSTERINFO" = "$(oc project | shasum)" ]
then
  echo "Wrong cluster"
  exit 1
fi

TAG=$(git tag --points-at | tail -n 1)


if [ -n "${1}" ]
then
    echo "Deploying version ${1} (ignoring tags)"
    TAG=${1}
fi

if [ -z "${TAG}" ]
then
  TAG=$(git rev-parse HEAD)
fi

TAG=${TAG%%}

echo "${TAG}"

HELM_STRING="fhir-labs fhir-labs --set version=${TAG} --set ingress=openshift --set hostname=$BASE_URL --set mocksBaseUrl=http://nmt-soap-mock-server:8080/soaprequests --set ImagePullPolicy=IfNotPresent"

helm diff upgrade --allow-unreleased ${HELM_STRING}

echo "Deploying ${TAG}"
helm upgrade --install ${HELM_STRING}

echo "Waiting for 60s to allow services to restart"
sleep 60

BASE_URL=https://$BASE_URL ./populate-indexes.sh

