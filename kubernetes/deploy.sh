#!/bin/bash

CLUSTERINFO="531bf60be797529bda57ac130ea77e2675fdefc1  -"

if [ ! "$CLUSTERINFO" = "$(kubectl cluster-info | shasum)" ]
then
  echo "Wrong cluster"
  exit 1
fi

TAG=$(git tag --points-at | tail -n 1)


if [ ! -z ${1} ]
then
    echo "Deploying version ${1} (ignoring tags)"
    TAG=${1}
fi

if [ -z ${TAG} ]
then
  TAG=$(git rev-parse HEAD)
fi

TAG=${TAG%%}

echo $TAG

HELM_STRING="fhir-labs --namespace fhir-labs fhir-labs --set version=${TAG}"

helm diff upgrade --allow-unreleased ${HELM_STRING}

echo "Deploying ${TAG}"
helm upgrade --install ${HELM_STRING}

BASE_URL=https://fhir-labs.utv.chorus.se ./populate-indexes.sh
