#!/usr/bin/env bash

patients=(199701062383 193601286499 194602226559)

echo "Using base url $BASE_URL"

function clearIe() {
  echo "Clearing ie for system $1"
  curl -X DELETE "$BASE_URL/information-index/sourcesystem/$1" -H 'Content-Type: application/json' -s 
}

function insertIeEntry() {
  curl -d "{\"sourceSystemId\":\"$3\", \"patientIdentifier\":\"$1\", \"patientIdentifierSystem\":\"1.2.752.129.2.1.3.1\",\"resourceType\":\"$2\"}" ${BASE_URL}/information-index/information-entry -H 'Content-Type: application/json' -s 
}

function upsertProducerCapabilityStatement() {
  curl -d "{\"id\":\"$1\", \"url\": \"$2\"}" ${BASE_URL}/capabilities/sourcesystem -H 'Content-Type: application/json' -s 
}

echo "Updating capability statement repo"
upsertProducerCapabilityStatement ahrr http://ahrr:8080/fhir
upsertProducerCapabilityStatement SE5565594230-fhirlabs-thor http://thor-producer:8080/fhir
upsertProducerCapabilityStatement SE5565594230-fhirlabs-odin http://odin-producer:8080/fhir

clearIe SE5565594230-fhirlabs-thor
clearIe SE5565594230-fhirlabs-odin
clearIe ahrr

for patient in "${patients[@]}"; do
  echo "Inserting ie entries for $patient"
  insertIeEntry "${patient}" Condition ahrr
  insertIeEntry "${patient}" CarePlan ahrr
  insertIeEntry "${patient}" Composition ahrr
  insertIeEntry "${patient}" DiagnosticReport ahrr
  insertIeEntry "${patient}" Encounter ahrr
  insertIeEntry "${patient}" Flag ahrr
  insertIeEntry "${patient}" ImagingStudy ahrr
  insertIeEntry "${patient}" Immunization ahrr
  insertIeEntry "${patient}" MedicationStatement ahrr
  insertIeEntry "${patient}" Observation ahrr
done

insertIeEntry "193601286499" Observation SE5565594230-fhirlabs-thor
insertIeEntry "193601286499" Observation SE5565594230-fhirlabs-odin
insertIeEntry "191212121212" Observation SE5565594230-fhirlabs-thor
insertIeEntry "191212121212" Observation SE5565594230-fhirlabs-odin
