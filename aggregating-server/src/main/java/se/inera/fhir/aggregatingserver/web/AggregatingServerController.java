package se.inera.fhir.aggregatingserver.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import se.inera.fhir.aggregatingserver.service.AggregatorServerService;
import se.inera.fhir.common.dto.Bundle;

import java.util.Map;

@RestController
@RequestMapping("/")
public class AggregatingServerController {

    private Logger log = LoggerFactory.getLogger(getClass());

    private AggregatorServerService aggregatorServerService;

    public AggregatingServerController(AggregatorServerService aggregatorServerService) {
        this.aggregatorServerService = aggregatorServerService;
    }

    @GetMapping("{fhirVersion}/metadata")
    public Mono<Object> getMetadata(
            @PathVariable final String fhirVersion
    ) {
        return aggregatorServerService.getAggregatedCapabilityStatement(fhirVersion);
    }

    @GetMapping("{fhirVersion}/{resource}")
    public Mono<Bundle> findSourceSystems(
            @PathVariable final String fhirVersion,
            @PathVariable final String resource,
            @RequestParam(value = "flatten", required = false, defaultValue = "false") final boolean flatten,
            @RequestParam final Map<String,String> queryParams
    ) {
        queryParams.remove("flatten");
        log.info("Call to findSourceSystem with fhirVersion: {}, resource: {}, parameters: {}", fhirVersion, resource, queryParams);

        if (flatten) {
            log.info("Returning single bundle.");
            return this.aggregatorServerService.getAggregatedEntryList(
                    fhirVersion,
                    resource,
                    queryParams
            ).collectList().map(Bundle::new);
        }

        return this.aggregatorServerService.getAggregatedBundles(
                fhirVersion,
                resource,
                queryParams
        ).collectList().map(Bundle::new);
    }

}
