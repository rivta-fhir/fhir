package se.inera.fhir.aggregatingserver.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.common.dto.SourceSystemDto;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AggregatorServerService {

    @Value("${producerDiscoveryUrl}")
    private String producerDiscoveryUrl;

    @Value("${serviceDiscoveryUrl}")
    private String serviceDiscoveryUrl;

    @Value("${virtualServiceUrl}")
    private String virtualServiceUrl;

    public Mono<Object> getAggregatedCapabilityStatement(String fhirVersion) {
        return WebClient
                .builder()
                .baseUrl(serviceDiscoveryUrl + "/" + fhirVersion + "/metadata")
                .build().get()
                .exchange().flatMap(response -> response.bodyToMono(Object.class));
    }

    public Flux<Object> getAggregatedBundles(
            final String fhirVersion,
            final String resource,
            final Map<String,String> parameters
    ) {
        return getSourceSystemDtos(fhirVersion, resource, parameters)
                .flatMap(sourceSystemDto ->
                        getResultForSourceSystem(sourceSystemDto.getId(), fhirVersion, resource, parameters));
    }

    public Flux<Object> getAggregatedEntryList(
            final String fhirVersion,
            final String resource,
            final Map<String,String> parameters
    ) {
        return getSourceSystemDtos(fhirVersion, resource, parameters)
                .flatMap(sourceSystemDto ->
                        getResultForSourceSystem(sourceSystemDto.getId(), fhirVersion, resource, parameters))
                .map(this::extractEntryList)
                .collectList()
                .map(this::flattenEntryList)
                .flatMapMany(o -> Flux.fromArray(o.toArray()));
    }

    private Flux<SourceSystemDto> getSourceSystemDtos(
            final String fhirVersion,
            final String resource,
            final Map<String,String> parameters
    ) {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.setAll(parameters);
        queryParams.add("resource", resource);
        queryParams.add("fhirVersion", fhirVersion);
        return WebClient
                .builder()
                .baseUrl(producerDiscoveryUrl)
                .build().get()
                .uri(builder -> builder.queryParams(queryParams).build())
                .exchange().flatMapMany(
                        c -> c.bodyToFlux(SourceSystemDto.class));
    }

    private Flux<Object> getResultForSourceSystem(
            final String sourceSystemId,
            final String fhirVersion,
            final String resource,
            final Map<String,String> parameters
    ) {
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        queryParams.setAll(parameters);
        return WebClient
                .builder()
                .baseUrl(virtualServiceUrl + '/' + fhirVersion + '/' + sourceSystemId + '/' + resource)
                .build().get()
                .uri(builder -> builder.queryParams(queryParams).build())
                .exchange()
                .flatMapMany(response -> response.bodyToFlux(Object.class));
    }

    private List<Object> extractEntryList(Object bundle) {
        try {
            Map<String, Object> map = (Map<String, Object>) bundle;
            List<Object> entry = (List<Object>) map.getOrDefault("entry", new LinkedList<>());
            return entry;
        } catch (Throwable t) {
            return new LinkedList<>();
        }
    }

    private List<Object> flattenEntryList(List<List<Object>> lists) {
        return lists.stream()
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
