package se.inera.fhir.aggregatingserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AggregatingServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AggregatingServerApplication.class, args);
	}

}
