package se.inera.fhir.common.dto;

import java.time.LocalDateTime;

public class SourceSystemDto {
    private String id;
    private String url;
    private String fhirVersion;
    private LocalDateTime updatedAt;

    public SourceSystemDto() {

    }

    public SourceSystemDto(String id, String url, String fhirVersion, LocalDateTime updatedAt) {
        this.id = id;
        this.url = url;
        this.fhirVersion = fhirVersion;
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFhirVersion() {
        return fhirVersion;
    }

    public void setFhirVersion(String fhirVersion) {
        this.fhirVersion = fhirVersion;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "SourceSystemDto{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", updatedAt=" + updatedAt +
                '}';
    }

}
