package se.inera.fhir.common.util;

import java.util.List;
import java.util.Map;

public class LinkRewriter {

    public static Object replaceUrls(
            final String producerUrl,
            final String replacementUrl,
            Object object) {
        if (object instanceof String) {
            return ((String) object).replace(producerUrl, replacementUrl);
        }
        if (object instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) object;
            map.forEach((s, o) -> map.replace(s,replaceUrls(producerUrl, replacementUrl, o)));
            return map;
        }
        if (object instanceof List) {
            List<Object> list = (List<Object>) object;
            list.replaceAll(o -> replaceUrls(producerUrl, replacementUrl, o));
            return list;
        }
        return object;
    }
}
