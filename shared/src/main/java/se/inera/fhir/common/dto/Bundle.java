package se.inera.fhir.common.dto;

import java.util.List;
import java.util.UUID;

public class Bundle {
    public Bundle(List<Object> entry) {
        this.entry = entry;
    }
    private List<Object> entry;

    public String getResourceType() {
        return "Bundle";
    }

    public String getId() {
        return UUID.randomUUID().toString();
    }

    public String getType() {
        return "searchset";
    }

    public int getCount() {
        return entry.size();
    }

    public List<Object> getEntry() {
        return entry;
    }

    public void setEntry(List<Object> entry) {
        this.entry = entry;
    }
}
