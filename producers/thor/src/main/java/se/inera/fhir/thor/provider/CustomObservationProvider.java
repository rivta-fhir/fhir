package se.inera.fhir.thor.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import org.hl7.fhir.r4.model.*;
import org.springframework.stereotype.Service;
import se.inera.fhir.thor.resource.CustomObservation;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CustomObservationProvider extends AbstractJaxRsResourceProvider<CustomObservation> {

    private final List<CustomObservation> observations = new ArrayList<>();

    @Override
    public Class<CustomObservation> getResourceType() {
        return CustomObservation.class;
    }

    @Read
    public CustomObservation read(@IdParam IdType id) {
        return observations
                .stream()
                .filter(o -> o.getId().equals(id.getIdPart()))
                .findFirst()
                .orElse(null);
    }

    @Search
    public List<CustomObservation> list(@OptionalParam(name="patient-identifier") final String patientIdentifier, @OptionalParam(name="patient-identifier-system") final String patientIdentifierSystem) {
        if (patientIdentifier != null) {
            return observations.stream()
                    .filter(
                            observation -> observation.getSubject().getIdentifier().getValue().equals(patientIdentifier)
                    ).collect(Collectors.toList());
        }

        return observations;
    }

    private CustomObservation createObservation(
            final String patientIdentifier,
            final String patientId,
            final String code,
            final String displayName,
            final Double value,
            final String unit,
            final Double referenceValueHigh,
            final Double referenceValueLow,
            final Date issuedAt) {
        final CustomObservation o1 = new CustomObservation();
        o1.setSubject(new Reference()
                .setIdentifier(new Identifier()
                        .setValue(patientIdentifier)
                        .setSystem("urn:oid:1.2.752.129.2.1.3.1"))
                .setType("Patient")
                .setReference("http://thor-producer:8080/fhir/Patient/" + patientId));
        o1.setId(UUID.randomUUID().toString());
        o1.setCode(new CodeableConcept(new Coding("fhir.inera.se/labs", code, displayName)));
        SimpleQuantity systolic = new SimpleQuantity();
        systolic.setCode(code);
        systolic.setSystem("fhir.inera.se/labs");
        systolic.setValue(value);
        systolic.setUnit(unit);
        o1.setValue(systolic);
        Observation.ObservationReferenceRangeComponent reference = new Observation.ObservationReferenceRangeComponent();
        SimpleQuantity refHigh = new SimpleQuantity();
        refHigh.setValue(referenceValueHigh);
        refHigh.setUnit(unit);
        reference.setHigh(refHigh);
        SimpleQuantity refLow = new SimpleQuantity();
        refLow.setValue(referenceValueLow);
        refLow.setUnit(unit);
        reference.setLow(refLow);
        o1.setReferenceRange(Collections.singletonList(reference));
        o1.setIssued(issuedAt);
        return o1;
    }

    public CustomObservationProvider(
            FhirContext ctx) {
        super(ctx);


        observations.add(
                createObservation(
                        "191212121212",
                        "ca9b4674-4d4c-473d-b19c-3943aa89b9d8",
                        "bloodpressure-systolic",
                        "Blood pressure - systolic",
                        145.0,
                        "mmHg",
                        140.0,
                        70.0,
                        new GregorianCalendar(
                                2019,
                                Calendar.SEPTEMBER,
                                21,
                                10,
                                44
                        ).getTime()
                )
        );
        observations.add(
                createObservation(
                        "193601286499",
                        "c518e9e4-9dfb-4874-9aa4-b87a7b5eb10b",
                        "HBA1c",
                        "HBA1c",
                        30.0,
                        "mmol/mol",
                        52.0,
                        0.0,
                        new GregorianCalendar(
                                2019,
                                Calendar.SEPTEMBER,
                                21,
                                10,
                                44
                        ).getTime()
                )
        );
        observations.add(
                createObservation(
                        "191212121212",
                        "ca9b4674-4d4c-473d-b19c-3943aa89b9d8",
                        "bloodpressure-diastolic",
                        "Blood pressure - diastolic",
                        95.0,
                        "mmHg",
                        90.0,
                        50.0,
                        new GregorianCalendar(
                                2019,
                                Calendar.SEPTEMBER,
                                21,
                                10,
                                44
                        ).getTime()
                )
        );
        observations.add(
                createObservation(
                        "191212121212",
                        "ca9b4674-4d4c-473d-b19c-3943aa89b9d8",
                        "crp",
                        "C-Reactive Protein (CRP)",
                        153.0,
                        "mg/L",
                        3.0,
                        0.0,
                        new GregorianCalendar(
                                2019,
                                Calendar.SEPTEMBER,
                                30,
                                15,
                                30
                        ).getTime()
                )
        );
    }
}
