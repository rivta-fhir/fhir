package se.inera.fhir.thor.config;

import ca.uhn.fhir.rest.server.IServerAddressStrategy;
import org.apache.commons.lang3.Validate;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Determines the server's base using the incoming request
 */
public class CustomRequestAddressStrategy implements IServerAddressStrategy {

    private String internalValue;
    private String externalValue;

    public CustomRequestAddressStrategy(String internalValue, String externalValue) {
        Validate.notBlank(internalValue, "internalValue must not be null or empty");
        Validate.notBlank(externalValue, "externalValue must not be null or empty");
        this.internalValue = internalValue;
        this.externalValue = externalValue;
    }

    @Override
    public String determineServerBase(ServletContext context, HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String uri = request.getRequestURI();
        String host = url.substring(0, url.indexOf(uri));
        if (host.split("\\.").length < 2) {
            return internalValue;
        }
        return externalValue;
    }

}
