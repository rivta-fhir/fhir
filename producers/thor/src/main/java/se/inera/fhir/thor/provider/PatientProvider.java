package se.inera.fhir.thor.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import org.hl7.fhir.r4.model.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PatientProvider extends AbstractJaxRsResourceProvider<Patient> {

    private final List<Patient> patients = new ArrayList<>();

    @Override
    public Class<Patient> getResourceType() {
        return Patient.class;
    }

    @Read
    public Patient read(@IdParam IdType id) {
        return patients
                .stream()
                .filter(o -> o.getId().equals(id.getIdPart()))
                .findFirst()
                .orElse(null);
    }

    @Search
    public List<Patient> list(@OptionalParam(name="patient-identifier") final String patientIdentifier, @OptionalParam(name="patient-identifier-system") final String patientIdentifierSystem) {
        if (patientIdentifier != null) {
            return patients.stream()
                    .filter(
                            patient -> patient.getIdentifier().stream().anyMatch(identifier -> identifier.getValue().equals(patientIdentifier))
                    ).collect(Collectors.toList());
        }

        return patients;
    }

    private Patient createPatient(
            final String id,
            final String pid,
            final Address address) {
        final Patient patient = new Patient();
        patient.addIdentifier(new Identifier().setValue(pid)
                .setSystem("urn:oid:1.2.752.129.2.1.3.1"));
        patient.setId(id);
        patient.setActive(true);
        patient.addAddress(address);
        return patient;
    }

    private Address createAddress(
            final String streetAddress,
            final String city,
            final String postalCode,
            final String country){
        Address address = new Address();
        address.setUse(Address.AddressUse.HOME);
        address.setType(Address.AddressType.BOTH);
        address.addLine(streetAddress);
        address.setCity(city);
        address.setCountry(country);
        address.setPostalCode(postalCode);
        return address;
    }

    public PatientProvider(
            FhirContext ctx) {
        super(ctx);
        patients.add(
                createPatient(
                        "ca9b4674-4d4c-473d-b19c-3943aa89b9d8",
                        "191212121212",
                        createAddress(
                                "Testgatan 12",
                                "Teststad",
                                "12121",
                                "Sverige")
                )
        );
        patients.add(
                createPatient(
                        "c518e9e4-9dfb-4874-9aa4-b87a7b5eb10b",
                        "193601286499",
                        createAddress(
                                "Testvägen 36",
                                "Testby",
                                "11111",
                                "Sverige")
                )
        );
    }
}

