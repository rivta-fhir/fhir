package se.inera.fhir.thor.resource;

import ca.uhn.fhir.model.api.annotation.Child;
import ca.uhn.fhir.model.api.annotation.Description;
import ca.uhn.fhir.model.api.annotation.Extension;
import ca.uhn.fhir.model.api.annotation.ResourceDef;
import ca.uhn.fhir.util.ElementUtil;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Observation;

@ResourceDef(name="Observation", profile="http://fhir.inera.se/StructureDefinition/CustomObservation")
public class CustomObservation extends Observation {

    @Child(name="careProvider")
    @Extension(url="http://fhir.inera.se/informationowner/careProvider", definedLocally=false, isModifier=false)
    @Description(shortDefinition="Defines the care provider id responsible for the information")
    private Coding careProvider;

    @Child(name="careUnit")
    @Extension(url="http://fhir.inera.se/informationowner/careUnit", definedLocally=false, isModifier=false)
    @Description(shortDefinition="Defines the care unit id responsible for the information")
    private Coding careUnit;

    @Override
    public boolean isEmpty() {
        return super.isEmpty() && ElementUtil.isEmpty(careProvider, careUnit);
    }

    public Coding getCareProvider() {
        return careProvider;
    }

    public void setCareProvider(Coding careProvider) {
        this.careProvider = careProvider;
    }

    public Coding getCareUnit() {
        return careUnit;
    }

    public void setCareUnit(Coding careUnit) {
        this.careUnit = careUnit;
    }
}
