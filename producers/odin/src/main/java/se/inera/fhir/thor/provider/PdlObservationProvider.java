package se.inera.fhir.thor.provider;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.jaxrs.server.AbstractJaxRsResourceProvider;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.OptionalParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.Search;
import org.hl7.fhir.r4.model.*;
import org.springframework.stereotype.Service;
import se.inera.fhir.thor.resource.PdlObservation;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PdlObservationProvider extends AbstractJaxRsResourceProvider<PdlObservation> {

    private final List<PdlObservation> observations = new ArrayList<>();

    @Override
    public Class<PdlObservation> getResourceType() {
        return PdlObservation.class;
    }

    @Read
    public PdlObservation read(@IdParam IdType id) {
        return observations
                .stream()
                .filter(o -> o.getId().equals(id.getIdPart()))
                .findFirst()
                .orElse(null);
    }

    @Search
    public List<PdlObservation> list(@OptionalParam(name="patient-identifier") final String patientIdentifier, @OptionalParam(name="patient-identifier-system") final String patientIdentifierSystem) {
        if (patientIdentifier != null) {
            return observations.stream()
                    .filter(
                            observation -> observation.getSubject().getIdentifier().getValue().equals(patientIdentifier)
                    ).collect(Collectors.toList());
        }

        return observations;
    }

    public PdlObservationProvider(
            FhirContext ctx) {
        super(ctx);

        PdlObservation observation = new PdlObservation();
        observation.setId(UUID.randomUUID().toString());
        observation.setMeta(new Meta().addProfile("http://rivta.se/fhir/StructureDefinition/core/se-core-vitalsigns-v1/HeadCircum").setVersionId("1.0"));
        observation.addCategory()
                .addCoding().setSystem("http://hl7.org/fhir/observation-category")
                .setCode("vital-signs");
        //Vårdgivare
        Coding vgCod = new Coding();
        vgCod.setCode("SE5565594230-1234");
        vgCod.setSystem("http://hl7.org/fhir/v2/0203");
        vgCod.setDisplay("Vårdgivare");
        observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(vgCod)).setSystem("urn:oid:1.2.752.129.2.1.4.1")
                .setValue("SE5565594230-1234"));
        observation.setCareProvider(vgCod);
        //Vårdenhet
        Coding veCod = new Coding();
        veCod.setCode("XX");
        veCod.setSystem("http://hl7.org/fhir/v2/0203");
        veCod.setDisplay("SE5565594230-12345");
        observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(veCod)).setSystem("urn:oid:1.2.752.129.2.1.4.1")
                .setValue("SE5565594230-12345"));
        observation.setCareUnit(veCod);

        //patient
        Coding pnrCod = new Coding();
        pnrCod.setCode("PN");
        pnrCod.setSystem("http://hl7.org/fhir/v2/0203");
        pnrCod.setDisplay("Personnummer");
        observation.addPerformer().setIdentifier(new Identifier().setType(new CodeableConcept().addCoding(pnrCod))
                .setSystem("urn:oid:1.2.752.129.2.1.4.1")
                .setValue("1912121212"));
        observation.setValue(new Quantity().setValue(34)
                .setUnit("cm")
                .setSystem("http://unitsofmeasure.org")
                .setCode("cm")
        );

        Extension ext = new Extension();
        ext.setUrl("http://rivta.se/fhir/StructureDefinition/core/se-core-extentions/Source-v1");
        ext.setValue(new StringType("SE162321000065-B08"));
// Add the extension to the resource
//        observation.setComment("med kläder");
        observation.setCode(new CodeableConcept().addCoding(new Coding().setCode("363812007")
                .setSystem("http://snomed.info/sct").setDisplay("Huvudomkrets")));
        observation.setSubject(new Reference()
                .setIdentifier(new Identifier()
                        .setValue("191212121212")
                        .setSystem("urn:oid:1.2.752.129.2.1.3.1"))
                .setType("Patient")
                .setReference("http://thor-producer:8080/fhir/Patient/ca9b4674-4d4c-473d-b19c-3943aa89b9d8"));
        observation.setEffective(new DateTimeType(String.valueOf("2013-04-02")));
        observation.setIssued(new GregorianCalendar(
                2013,
                Calendar.APRIL,
                2,
                13,
                36
        ).getTime());
        observation.setStatus(PdlObservation.ObservationStatus.FINAL);
        Reference contextRef = new Reference();
        //      observation.setContext(contextRef);
        // Create a bundle that will be used as a transaction

        observations.add(observation);
    }
}
