package se.inera.fhir.thor.config;

import ca.uhn.fhir.rest.server.RestfulServer;
import ca.uhn.fhir.spring.boot.autoconfigure.FhirRestfulServerCustomizer;
import org.springframework.stereotype.Service;
import se.inera.fhir.thor.provider.CapStatProvider;

@Service
public class HapiFhirCustomizer implements FhirRestfulServerCustomizer {
    private CapStatProvider capStatProvider;

    public HapiFhirCustomizer(CapStatProvider capStatProvider) {
        this.capStatProvider = capStatProvider;
    }

    @Override
    public void customize(RestfulServer server) {
        server.setServerConformanceProvider(capStatProvider);
        server.setServerAddressStrategy(new CustomRequestAddressStrategy("http://odin-producer:8080/fhir", "https://fhir-labs.utv.chorus.se/producers/odin/fhir"));
    }
}
