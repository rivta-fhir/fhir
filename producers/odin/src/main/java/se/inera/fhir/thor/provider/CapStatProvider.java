package se.inera.fhir.thor.provider;
import ca.uhn.fhir.rest.annotation.Metadata;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import org.hl7.fhir.r4.hapi.rest.server.ServerCapabilityStatementProvider;
import org.hl7.fhir.r4.model.*;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CapStatProvider extends ServerCapabilityStatementProvider {

    @Override
    public CapabilityStatement getServerConformance(HttpServletRequest theRequest, RequestDetails theRequestDetails) {
        CapabilityStatement capabilityStatement = super.getServerConformance(theRequest, theRequestDetails);

        capabilityStatement.getRest()
                .forEach(rest -> {
                    rest.getResource().stream()
                            .filter(resource -> resource.getType().equals("Observation"))
                            .forEach(observations -> {
                                observations.addSupportedProfile("http://fhir.inera.se/StructureDefinition/PdlObservation");
                            });
                });

        return capabilityStatement;


    }



}