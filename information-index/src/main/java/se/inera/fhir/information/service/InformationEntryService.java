package se.inera.fhir.information.service;

import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.information.document.InformationEntry;

@Service
public class InformationEntryService {

    private ReactiveMongoTemplate reactiveMongoTemplate;

    public InformationEntryService(ReactiveMongoTemplate reactiveMongoTemplate) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }

    public Flux<InformationEntry> deleteForSourceSystem(final String sourceSystem) {
        return reactiveMongoTemplate.findAllAndRemove(Query.query(
                Criteria.where("sourceSystemId").is(sourceSystem)
        ), InformationEntry.class);
    }

    public Mono<InformationEntry> insertInformationEntry(Mono<InformationEntry> informationEntry) {
        return reactiveMongoTemplate.save(informationEntry);
    }

    public Flux<InformationEntry> getInformationEntries() {
        return reactiveMongoTemplate.findAll(InformationEntry.class);
    }

    public Flux<InformationEntry> findInformationEntries(
            final String patientIdentifier,
            final String patientIdentifierSystem,
            final String resource
    ) {
        final Query query = Query.query(
                new Criteria().andOperator(
                        Criteria.where("patientIdentifier").is(patientIdentifier),
                        Criteria.where("patientIdentifierSystem").is(patientIdentifierSystem),
                        Criteria.where("resourceType").is(resource)
                )
        );

        return reactiveMongoTemplate.find(query, InformationEntry.class);
    }
}
