package se.inera.fhir.information.web;

import com.mongodb.client.result.DeleteResult;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.information.document.InformationEntry;
import se.inera.fhir.information.service.InformationEntryService;

@RestController
@RequestMapping("/")
public class InformationEntryController {

    private InformationEntryService informationEntryService;

    public InformationEntryController(InformationEntryService informationEntryService) {
        this.informationEntryService = informationEntryService;
    }

    @DeleteMapping("/sourcesystem/{sourceSystem}")
    public Flux<InformationEntry> deleteForSourceSystem(@PathVariable String sourceSystem) {
        return this.informationEntryService.deleteForSourceSystem(sourceSystem);
    }

    @PostMapping("/information-entry")
    public Mono<InformationEntry> createInformationEntry(@RequestBody Mono<InformationEntry> informationEntry) {
        return this.informationEntryService.insertInformationEntry(informationEntry);
    }

    @GetMapping("/sourcesystem")
    public Flux<InformationEntry> findSourceSystems(
            @RequestParam(name = "patient-identifier") final String patientIdentifier,
            @RequestParam(name = "patient-identifier-system") final String patientIdentifierSystem,
            @RequestParam(name = "resource") final String resource
    ) {
        return this.informationEntryService.findInformationEntries(
                patientIdentifier,
                patientIdentifierSystem,
                resource
        );
    }
}
