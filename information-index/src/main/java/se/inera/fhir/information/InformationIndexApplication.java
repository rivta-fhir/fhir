package se.inera.fhir.information;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformationIndexApplication {

	public static void main(String[] args) {
		SpringApplication.run(InformationIndexApplication.class, args);
	}

}
