package se.inera.fhir.information.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "ie")
public class InformationEntry {
    @Id
    private String id;
    private String sourceSystemId;
    private String patientIdentifier;
    private String patientIdentifierSystem;
    private String resourceType;
    private List<String> profiles;

    @Override
    public String toString() {
        return "InformationEntry{" +
                "id='" + id + '\'' +
                ", sourceSystemId='" + sourceSystemId + '\'' +
                ", patientIdentifier='" + patientIdentifier + '\'' +
                ", patientIdentifierSystem='" + patientIdentifierSystem + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", profiles=" + profiles +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceSystemId() {
        return sourceSystemId;
    }

    public void setSourceSystemId(String sourceSystemId) {
        this.sourceSystemId = sourceSystemId;
    }

    public String getPatientIdentifier() {
        return patientIdentifier;
    }

    public void setPatientIdentifier(String patientIdentifier) {
        this.patientIdentifier = patientIdentifier;
    }

    public String getPatientIdentifierSystem() {
        return patientIdentifierSystem;
    }

    public void setPatientIdentifierSystem(String patientIdentifierSystem) {
        this.patientIdentifierSystem = patientIdentifierSystem;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public List<String> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<String> profiles) {
        this.profiles = profiles;
    }
}
