package se.inera.fhir.capability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CapabilityServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapabilityServerApplication.class, args);
	}

}
