package se.inera.fhir.capability.web;

import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.capability.document.SourceSystem;
import se.inera.fhir.capability.service.SourceSystemService;
import se.inera.fhir.common.dto.SourceSystemDto;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/sourcesystem")
public class SourceSystemController {
    private SourceSystemService sourceSystemService;

    public SourceSystemController(SourceSystemService sourceSystemService) {
        this.sourceSystemService = sourceSystemService;
    }

    @GetMapping
    public Flux<SourceSystemDto> getSourceSystems(
            @RequestParam(value = "ids", required = false) final String ids,
            @RequestParam(value = "fhirVersion", required = false) final String fhirVersion,
            @RequestParam(value = "resource", required = false) final String resourceType,
            @RequestParam(value = "profile", required = false) final String profile,
            @RequestParam(value = "searchParams", required = false) final String searchParams) {
        List<String> idList = null;
        if (ids != null) {
            idList = Arrays.asList(ids.split(","));
        }
        List<String> searchParamList = null;
        if (searchParams != null) {
            searchParamList = Arrays.asList(searchParams.split(","));
        }
        return sourceSystemService.getSourceSystemDtos(idList, fhirVersion, resourceType, profile, searchParamList);
    }

    @GetMapping("/{fhirVersion}/metadata")
    public Mono<Object> getCapabilityStatement(
            @PathVariable final String fhirVersion) throws Exception {
        return sourceSystemService.getAggregatedCapabilityStatement(fhirVersion);
    }

    @GetMapping("/{id}")
    public Mono<SourceSystemDto> getSourceSystem(@PathVariable String id) {
        return sourceSystemService.getSourceSystemDto(id);
    }

    @PostMapping
    public Mono<SourceSystem> createSourceSystem(@RequestBody SourceSystemDto sourceSystem) {
        return sourceSystemService.upsertSourceSystemDto(sourceSystem);
    }

}
