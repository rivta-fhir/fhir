package se.inera.fhir.capability.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "aggregated_capability_statement")
public class AggregatedCapabilityStatement {
    @Id
    private String fhirVersion;
    private Object capabilityStatement;

    public AggregatedCapabilityStatement(String fhirVersion, Object capabilityStatement) {
        this.fhirVersion = fhirVersion;
        this.capabilityStatement = capabilityStatement;
    }

    public String getFhirVersion() {
        return fhirVersion;
    }

    public void setFhirVersion(String fhirVersion) {
        this.fhirVersion = fhirVersion;
    }

    public Object getCapabilityStatement() {
        return capabilityStatement;
    }

    public void setCapabilityStatement(Object capabilityStatement) {
        this.capabilityStatement = capabilityStatement;
    }
}
