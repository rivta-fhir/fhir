package se.inera.fhir.capability.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "source_systems")
public class SourceSystem {
    @Id
    private String id;
    private String url;
    private Object capabilityStatement;
    private String fhirVersion;
    private LocalDateTime updatedAt;

    public SourceSystem(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Object getCapabilityStatement() {
        return capabilityStatement;
    }

    public void setCapabilityStatement(Object capabilityStatement) {
        this.capabilityStatement = capabilityStatement;
    }

    public String getFhirVersion() {
        return fhirVersion;
    }

    public void setFhirVersion(String fhirVersion) {
        this.fhirVersion = fhirVersion;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
