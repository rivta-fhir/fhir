package se.inera.fhir.capability.aggregation;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CapabilityStatementAggregator {
    public static Object generateCapabilityStatement(final String fhirVersion, List<Object> sources) {
        if (sources == null || sources.size() == 0) {
            return null;
        }
        Map<String, Object> result = (Map<String, Object>) sources.get(0);
        result.put("resourceType", "CapabilityStatement");
        result.put("status", "active");
        result.put("date", ZonedDateTime.now().withNano(0).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        result.put("publisher", "Not provided");
        result.put("kind", "instance");

        Map<String, Object> software = new LinkedHashMap<>();
        software.put("name", "fhir_labs");
        software.put("version", "1.0.0");
        result.put("software", software);

        Map<String, Object> implementation = new LinkedHashMap<>();
        implementation.put("description", "FHIR LABS");
        implementation.put("url", "https://fhir-labs.utv.chorus.se/aggregating-server/" + fhirVersion);
        result.put("implementation", implementation);
        result.put("fhirVersion", fhirVersion);

        List<String> format = new LinkedList<>();
        format.add("application/fhir+xml");
        format.add("application/fhir+json");
        result.put("format", format);


        List<Map<String,Object>> rest = new LinkedList<>();
        for (Object source: sources) {
            Map<String,Object> sourceMap = (Map<String,Object>) source;
            List<Map<String,Object>> restSource =  (List<Map<String,Object>>) sourceMap.get("rest");
            mergeRest(rest, restSource);
        }
        result.put("rest", rest);


        return result;
    }

    private static void mergeRest(List<Map<String,Object>> result, List<Map<String,Object>> sources) {
        for (Map<String,Object> source: sources) {
            mergeRestElement(result, source);
        }
    }

    private static void mergeRestElement(List<Map<String,Object>> result, Map<String,Object> source) {
        for (Map<String,Object> element: result) {
            if (element.get("mode").equals(source.get("mode"))) {
                mergeResources((List<Map<String,Object>>) element.get("resource"), (List<Map<String,Object>>) source.get("resource"));
                return;
            }
        }
        List<Map<String,Object>> resource = new LinkedList<>();
        mergeResources(resource, (List<Map<String,Object>>) source.get("resource"));
        Map<String, Object> element = new LinkedHashMap<>();
        element.put("mode", source.get("mode"));
        element.put("resource", resource);
        result.add(element);
    }

    private static void mergeResources(List<Map<String,Object>> result, List<Map<String,Object>> sources) {
        for (Map<String,Object> source: sources) {
            Optional<Map<String, Object>> r = result.stream().filter(resultObject -> resultObject.get("type").equals(source.get("type")) &&
                    resultObject.get("profile").equals(source.get("profile"))).findFirst();
            if(r.isPresent()) {
                mergeResource(r.get(),source);
            } else {
                result.add(source);
            }
        }
    }

    private static void mergeResource(Map<String,Object> result, Map<String,Object> source) {
        for(String key: source.keySet()) {
            if (key.equals("type") || key.equals("profile")) {
                continue;
            }
            if (source.get(key) instanceof List){
                if (!result.containsKey(key)) {
                    result.put(key, new LinkedList<>());
                }
                List<Object> resultList = (List<Object>) result.get(key);
                List<Object> sourceList = (List<Object>) source.get(key);
                for (Object item: sourceList) {
                    if (!resultList.contains(item)) {
                        resultList.add(item);
                    }
                }
            }
        }

    }

}
