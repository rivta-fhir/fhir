package se.inera.fhir.capability.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.inera.fhir.capability.aggregation.CapabilityStatementAggregator;
import se.inera.fhir.capability.document.AggregatedCapabilityStatement;
import se.inera.fhir.capability.document.SourceSystem;
import se.inera.fhir.common.dto.SourceSystemDto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SourceSystemService {
    private Logger log = LoggerFactory.getLogger(getClass());

    private ReactiveMongoTemplate reactiveMongoTemplate;

    public SourceSystemService(ReactiveMongoTemplate reactiveMongoTemplate) {
        this.reactiveMongoTemplate = reactiveMongoTemplate;
    }

    private Mono<SourceSystem> upsertSourceSystem(SourceSystem sourceSystem) {
        return this.reactiveMongoTemplate.save(sourceSystem);
    }

    public Mono<SourceSystem> upsertSourceSystemDto(SourceSystemDto dto) {
        return upsertSourceSystem(new SourceSystem(dto.getId(), dto.getUrl()));
    }

    public Flux<SourceSystemDto> getSourceSystemDtos(final List<String> ids, final String fhirVersion, final String resourceType, final String profile, final List<String> searchParams) {

        Query query = new Query();

        if (ids != null && !ids.isEmpty()) {
            query.addCriteria(Criteria.where("id").in(ids));
        }
        if (fhirVersion != null) {
            query.addCriteria(Criteria.where("fhirVersion").is(fhirVersion));
        }
        if (profile != null || resourceType != null || (searchParams != null && !searchParams.isEmpty())) {
            Criteria criteria = null;
            if (profile != null) {
                criteria = new Criteria().orOperator(
                        Criteria.where("profile").is(profile),
                        Criteria.where("supportedProfile").all(profile));
            }
            if (resourceType != null) {
                if (criteria == null) {
                    criteria = Criteria.where("type").is(resourceType);
                } else {
                    criteria = criteria.and("type").is(resourceType);
                }
            }
            if (searchParams != null && !searchParams.isEmpty()) {
                if (criteria == null) {
                    criteria = Criteria.where("searchParam.name").all(searchParams);
                } else {
                    criteria = criteria.and("searchParam.name").all(searchParams);
                }
            }
            query.addCriteria(Criteria.where("capabilityStatement.rest.resource").elemMatch(criteria));
        }

        query.fields().exclude("capabilityStatement");
        return this.reactiveMongoTemplate.find(query, SourceSystem.class)
                .map(it -> new SourceSystemDto(it.getId(), it.getUrl(), it.getFhirVersion(), it.getUpdatedAt()));
    }
    private Flux<SourceSystem> getSourceSystems() {
        return this.reactiveMongoTemplate.findAll(SourceSystem.class);
    }

    private Flux<Object> getCapabilityStatements(@NotNull String fhirVersion) {
        Query query = new Query();
        query.addCriteria(Criteria.where("fhirVersion").is(fhirVersion));
        return this.reactiveMongoTemplate.find(query, SourceSystem.class)
                .map(it -> it.getCapabilityStatement());
    }

    private Mono<AggregatedCapabilityStatement> insertAggregatedCapabilityStatement(AggregatedCapabilityStatement aggregatedCapabilityStatement) {
        return this.reactiveMongoTemplate.save(aggregatedCapabilityStatement);
    }

    public Mono<Object> getAggregatedCapabilityStatement(String fhirVersion) {
        return this.reactiveMongoTemplate.findById(fhirVersion, AggregatedCapabilityStatement.class)
                .map(it -> it.getCapabilityStatement());
    }

    public Mono<SourceSystemDto> getSourceSystemDto(final String id) {
        return this.reactiveMongoTemplate.findById(id, SourceSystem.class)
                .map(it -> new SourceSystemDto(it.getId(), it.getUrl(), it.getFhirVersion(), it.getUpdatedAt()));
    }

    private Mono<Object> fetchCapabilityStatementForSourceSystem(SourceSystem sourceSystem) {
        log.info("Fetching capability statement for source system with id: {}", sourceSystem.getId());
        return WebClient
                .builder()
                .baseUrl(sourceSystem.getUrl() + "/metadata")
                .defaultHeader("Content-type", "application/fhir+json")
                .defaultHeader("Accept", "application/fhir+json")
                .build().get()
                .exchange()
                .flatMap(c -> c.bodyToMono(Object.class));
    }

    private String validateAndGetFhirVersion(Object object) {
        if (object instanceof Map &&
                ((Map) object).containsKey("resourceType") &&
                ((Map) object).get("resourceType") instanceof String &&
                ((Map) object).get("resourceType").equals("CapabilityStatement") &&
                ((Map) object).containsKey("fhirVersion") &&
                ((Map) object).get("fhirVersion") instanceof String) {
            String fhirVersion = (String) ((Map) object).get("fhirVersion");
            return fhirVersion.substring(0,3);
        }
        return null;
    }

    @Scheduled(fixedRateString ="${scheduled.collectCapabilityStatements}")
    private void collectCapabilityStatements() {
        getSourceSystems()
                .flatMap(sourceSystem ->
                        fetchCapabilityStatementForSourceSystem(sourceSystem)
                        .flatMap(object -> {
                            String fhirVersion = validateAndGetFhirVersion(object);
                            if (fhirVersion == null) {
                                log.info("Could not validate CapabilityStatement: {}", object.toString());
                                return Mono.just(false);
                            }
                            log.info("Updated source system with id: {}", sourceSystem.getId());
                            sourceSystem.setCapabilityStatement(object);
                            sourceSystem.setUpdatedAt(LocalDateTime.now());
                            sourceSystem.setFhirVersion(fhirVersion);
                            return upsertSourceSystem(sourceSystem).map(s -> true);
                        }).onErrorReturn(false))
                .filter(success -> success)
                .collectList()
                .subscribe(updated -> {
                    log.info("Completed update of {} source systems", updated.size());
                    generateAggregatedCapabilityStatements();
                });

    }

    private void generateAggregatedCapabilityStatements() {
        reactiveMongoTemplate.findDistinct("fhirVersion", SourceSystem.class,String.class)
                .flatMap(fhirVersion -> {
                    return getCapabilityStatements(fhirVersion)
                            .collectList()
                            .map(objects -> CapabilityStatementAggregator.generateCapabilityStatement(fhirVersion, objects))
                            .flatMap(o -> insertAggregatedCapabilityStatement(new AggregatedCapabilityStatement(fhirVersion, o)));
                }).collectList().subscribe(objects -> {
            log.info("Aggregated capability statements [{}]", objects.stream().map(it -> it.getFhirVersion()).collect(Collectors.joining(", ")));
        });
    }
}
